﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public class ContentLibraryService : IContentLibraryService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/ContentLibrary";
        #endregion


        public ContentLibraryService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public IEnumerable<ContentView> GetContentLibrary(long SchoolId)
        {
            var uri = API.ContentLibrary.GetContentLibrary(_path, SchoolId);
            List<ContentView> content = new List<ContentView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<ContentView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (ContentView source in sourceModel)
                    {
                        var destination = new ContentView();
                        EntityMapper<ContentView, ContentView>.Map(source, destination);
                        content.Add(destination);
                    }
                }
            }
            return content;
        }

        public IEnumerable<SchoolLevels> GetSchoolLevelsBySchoolId()
        {
            var result = new List<SchoolLevels>();
            int schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var uri = API.ContentLibrary.GetSchoolLevelsBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolLevels>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<ContentView> GetPaginateContentLibrary(int pageNumber, int pageSize, string SearchString = "", string sortBy="", string categoryIds = "")
        {
            var result = new List<ContentView>();
            int schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var uri = API.ContentLibrary.GetPaginateContentLibrary(_path, schoolId, pageNumber, pageSize, SearchString, sortBy, categoryIds);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<ContentView>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<ContentView> GetStudentContentLibrary(int pageNumber, int pageSize, string SearchString = "", string sortBy="", string categoryIds="")
        {
            var result = new List<ContentView>();
            int schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var uri = API.ContentLibrary.GetStudentContentLibrary(_path, schoolId, pageNumber, pageSize, SearchString, sortBy, categoryIds);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<ContentView>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<ContentView> GetContentProvider(long SchoolId, string divisionIds, string sortBy="")
        {
            var uri = API.ContentLibrary.GetContentProvider(_path, SchoolId, divisionIds, sortBy);
            List<ContentView> content = new List<ContentView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<ContentView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (ContentView source in sourceModel)
                    {
                        var destination = new ContentView();
                        EntityMapper<ContentView, ContentView>.Map(source, destination);
                        content.Add(destination);
                    }
                }
            }
            return content;
        }
        public int InsertUpdateContentResource(ContentEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new Content();
            EntityMapper<ContentEdit, Content>.Map(model, sourceModel);
            if (model.IsAddMode)
                uri = API.ContentLibrary.InsertContentResource(_path);
            else
                uri = API.ContentLibrary.UpdateContentResource(_path);
            sourceModel.UserId = SessionHelper.CurrentSession.Id;
            sourceModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public Content getContentResourceById(int resourceId)
        {
            var uri = API.ContentLibrary.getContentResourceById(_path, resourceId);
            Content resource = new Content();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                resource = EntityMapper<string, Content>.MapFromJson(jsonDataProviders);
            }
            return resource;
        }
        public IEnumerable<Content> getSubjectsByContentId(int contentId)
        {
            var uri = API.ContentLibrary.getSubjectsByContentId(_path, contentId);
            IEnumerable<Content> resource = new List<Content>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                resource = EntityMapper<string, IEnumerable<Content>>.MapFromJson(jsonDataProviders);
            }
            return resource;
        }
        public int DeleteContentResourceData(int id)
        {
            var uri = API.ContentLibrary.DeleteContentResourceData(_path, id, SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public bool ChangeContentLibraryStatus(int contentId, bool status)
        {
            bool bFlag = false;
            long schoolId = SessionHelper.CurrentSession.SchoolId;
            var uri = API.ContentLibrary.ChangeContentLibraryStatus(_path, contentId, status, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }
        public bool AssignContentResourceToSchool(int contentId, bool status)
        {
            bool bFlag = false;
            long schoolId = SessionHelper.CurrentSession.SchoolId;
            var uri = API.ContentLibrary.AssignContentResourceToSchool(_path, contentId, status, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }

    }
}