﻿using Phoenix.Common.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IContentLibraryService
    {
        IEnumerable<ContentView> GetContentLibrary(long SchoolId);
        IEnumerable<SchoolLevels> GetSchoolLevelsBySchoolId();
        IEnumerable<ContentView> GetStudentContentLibrary(int pageNumber, int pageSize, string SearchString = "", string sortBy = "", string categoryIds = "");
        IEnumerable<ContentView> GetContentProvider(long SchoolId, string divisionIds, string sortBy = "");
        int InsertUpdateContentResource(ContentEdit model);
        Phoenix.Models.Content getContentResourceById(int resourceId);
        IEnumerable<Phoenix.Models.Content> getSubjectsByContentId(int contentId);
        int DeleteContentResourceData(int id);
        bool ChangeContentLibraryStatus(int contentId, bool status);
        bool AssignContentResourceToSchool(int contentId, bool status);
        IEnumerable<ContentView> GetPaginateContentLibrary(int pageNumber, int pageSize, string SearchString = "", string sortBy = "", string categoryIds = "");
    }
}
