﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Models;
using SMS.Web.Areas.SMS.Model;

namespace Phoenix.VLE.Web.Services
{
    public interface IClassListService
    {
        IEnumerable<ClassListModel> GetClassList(long SchoolId, long GroupId, long GradeId, long SectionId);
        BasicDetails GetStudentDetails(string stu_id);
        StudentProfileModel GetStudentProfileDetail(long stu_id, long SchoolId, DateTime nowDate);
        IEnumerable<ParentDetailModel> GetQuickContactDetail(long StudentId, long SchoolId);
        TimeTableModel GetStudentFindMeDetail(long StudentId, long SchoolId, DateTime nowDate);
        IEnumerable<WeekDayModel> GetWeeklyTimeTableDetail(long StudentId, long SchoolId, int WeekCount, DateTime nowDate);
        StudentDashboardModel GetStudentDashboardDetails(string stu_id);
        IEnumerable<AttendanceChart> GetAttendanceChart(string stu_id);
        IEnumerable<AttendenceList> GetAttendenceList(string stu_id, DateTime EndDate);
        long StudentOnReportMasterCU(StudentOnReportMasterModel studentOnReport);
        IEnumerable<StudentOnReportMasterModel> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId);
        IEnumerable<StudentOnReportModel> GetStudentOnReportDetails(StudentOnReportParameterModel detailsParameter);
        long StudentOnReportDetailsCU(StudentOnReportModel studentOnReport);
        AttendanceProfileModel GetAttendanceProfileDetail(long StudentId, long SchoolId, string AttendanceDt, string AttendanceType);
        ChangeGroupStudentModel GetCourseWiseSchoolGroups(long StudentId);
        bool ChangeCourseWiseStudentSchoolGroup(ChangeGroupStudentModel changeGroupStudentModel);
        IEnumerable<ClassListModel> GetStudentListBySearch(long SchoolId, long UserId, string SearchString);
    }
}
