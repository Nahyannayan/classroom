﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using SMS.Web.Areas.SMS.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;


namespace Phoenix.VLE.Web.Services
{
    public class ClassListService : IClassListService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/ClassList";
        #endregion

        public ClassListService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }


        #region Methods
        public IEnumerable<ClassListModel> GetClassList(long SchoolId, long GroupId, long GradeId, long SectionId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.ClassList.GetClassList(_path, SchoolId, GroupId, GradeId, SectionId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<ClassListModel>>() : new List<ClassListModel>();
        }
        public ChangeGroupStudentModel GetCourseWiseSchoolGroups(long StudentId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.ClassList.GetCourseWiseSchoolGroups(_path, StudentId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<ChangeGroupStudentModel>() : new ChangeGroupStudentModel();
        }
        public BasicDetails GetStudentDetails(string stu_id)
        {
            var uri = API.ClassList.GetStudentDetails(_path, stu_id);
            BasicDetails basicdetails = new BasicDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                basicdetails = EntityMapper<string, BasicDetails>.MapFromJson(jsonDataProviders);
            }
            return basicdetails;
        }
        public StudentProfileModel GetStudentProfileDetail(long stu_id, long SchoolId, DateTime nowDate)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.ClassList.GetStudentProfileDetail(_path, stu_id, SchoolId, nowDate)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<StudentProfileModel>() : new StudentProfileModel();
            //return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<StudentProfile>() : default(StudentProfile);
        }
        public IEnumerable<ParentDetailModel> GetQuickContactDetail(long StudentId, long SchoolId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.ClassList.GetQuickContactDetail(_path, StudentId, SchoolId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<ParentDetailModel>>() : new List<ParentDetailModel>();
        }
        public TimeTableModel GetStudentFindMeDetail(long StudentId, long SchoolId, DateTime nowDate)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.ClassList.GetStudentFindMeDetail(_path, StudentId, SchoolId, nowDate)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<TimeTableModel>() : new TimeTableModel();
        }
        public IEnumerable<WeekDayModel> GetWeeklyTimeTableDetail(long StudentId, long SchoolId, int WeekCount, DateTime nowDate)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.ClassList.GetWeeklyTimeTableDetail(_path, StudentId, SchoolId, WeekCount, nowDate)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<WeekDayModel>>() : new List<WeekDayModel>();
        }
        public AttendanceProfileModel GetAttendanceProfileDetail(long StudentId, long SchoolId, string AttendanceDt, string AttendanceType)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.ClassList.GetAttendanceProfileDetail(_path, StudentId, SchoolId, AttendanceDt, AttendanceType)).Result;

            return responseMessage.IsSuccessStatusCode ?
                responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<AttendanceProfileModel>() : new AttendanceProfileModel();
            //return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<StudentProfile>() : default(StudentProfile);
        }
        public bool ChangeCourseWiseStudentSchoolGroup(ChangeGroupStudentModel changeGroupStudentModel)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.ClassList.ChangeCourseWiseStudentSchoolGroup(_path), changeGroupStudentModel).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        public StudentDashboardModel GetStudentDashboardDetails(string stu_id)
        {
            var uri = API.ClassList.GetStudentDashboardDetails(_path, stu_id);
            StudentDashboardModel _obj = new StudentDashboardModel();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, StudentDashboardModel>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }

        public IEnumerable<AttendanceChart> GetAttendanceChart(string stu_id)
        {
            var uri = API.ClassList.GetAttendanceChart(_path, stu_id);
            IEnumerable<AttendanceChart> _obj = new List<AttendanceChart>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<AttendanceChart>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }
        public IEnumerable<AttendenceList> GetAttendenceList(string stu_id, DateTime EndDate)
        {
            var uri = API.ClassList.GetAttendenceList(_path, stu_id, EndDate);
            IEnumerable<AttendenceList> _obj = new List<AttendenceList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<AttendenceList>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }

        public IEnumerable<StudentOnReportMasterModel> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId)
        {
            var uri = API.ClassList.GetStudentOnReportMasters(_path, studentId, academicYearId, schoolId);
            IEnumerable<StudentOnReportMasterModel> studentOnReport = new List<StudentOnReportMasterModel>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentOnReport = EntityMapper<string, IEnumerable<StudentOnReportMasterModel>>.MapFromJson(jsonDataProviders);
            }
            return studentOnReport;
        }

        public long StudentOnReportMasterCU(StudentOnReportMasterModel studentOnReport)
        {
            var uri = API.ClassList.StudentOnReportMasterCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentOnReport).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt64(jsonDataProviders);
            }
            return 0;
        }

        public IEnumerable<StudentOnReportModel> GetStudentOnReportDetails(StudentOnReportParameterModel detailsParameter)
        {
            var uri = API.ClassList.GetStudentOnReportDetails(_path);
            IEnumerable<StudentOnReportModel> studentOnReport = new List<StudentOnReportModel>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, detailsParameter).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentOnReport = EntityMapper<string, IEnumerable<StudentOnReportModel>>.MapFromJson(jsonDataProviders);
            }
            return studentOnReport;
        }

        public long StudentOnReportDetailsCU(StudentOnReportModel studentOnReport)
        {
            var uri = API.ClassList.StudentOnReportDetailsCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentOnReport).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt64(jsonDataProviders);
            }
            return 0;
        }
        #endregion
        public IEnumerable<ClassListModel> GetStudentListBySearch(long SchoolId, long UserId, string SearchString)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.ClassList.GetStudentListBySearch(_path, SchoolId, UserId, SearchString)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<ClassListModel>>() : new List<ClassListModel>();
        }
    }
}