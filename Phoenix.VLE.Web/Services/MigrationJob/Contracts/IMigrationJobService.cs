﻿using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IMigrationJobService
    {
        IEnumerable<MigrationDetail> GetMigrationDetailList(long SchoolId);
       // IEnumerable<SyncDetailById> GetSyncDetailbyId(SyncDetailById obj);
         bool GetSyncDetailbyId(SyncDetailById obj);
        IEnumerable<MigrationDetailCount> GetMigrationDetailCountList();
    }
}
