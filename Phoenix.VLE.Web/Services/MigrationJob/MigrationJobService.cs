﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
namespace Phoenix.VLE.Web.Services
{
    public class MigrationJobService : IMigrationJobService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/MigrationJob";
        #endregion
        public MigrationJobService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public IEnumerable<MigrationDetail> GetMigrationDetailList(long SchoolId)
        {
            var uri = API.MigrationJob.GetMigrationDetailList(_path, SchoolId);
            IEnumerable<MigrationDetail> Migration = new List<MigrationDetail>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Migration = EntityMapper<string, IEnumerable<MigrationDetail>>.MapFromJson(jsonDataProviders);
            }
            return Migration;
        }
        public IEnumerable<MigrationDetailCount> GetMigrationDetailCountList()
        {
            var uri = API.MigrationJob.GetMigrationDetailCountList(_path);
            IEnumerable<MigrationDetailCount> MigrationCount = new List<MigrationDetailCount>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                MigrationCount = EntityMapper<string, IEnumerable<MigrationDetailCount>>.MapFromJson(jsonDataProviders);
            }
            return MigrationCount;
        }

        public bool GetSyncDetailbyId(SyncDetailById obj)
        {
            var response = _client.PostAsJsonAsync(API.MigrationJob.GetSyncDetailbyId(_path), obj).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }
    }
}