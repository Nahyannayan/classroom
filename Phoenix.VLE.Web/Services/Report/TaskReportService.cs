﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class TaskReportService : ITaskReportService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.HSEAPIUrl;
        readonly string _path = "api/TaskReport";
        #endregion




        /// <summary>
        /// Author : obaidullah Waseem
        /// Created Date : 19-JUNE-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public TaskReportService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }


        /// <summary>
        /// Author : Obaidullah Waseem
        /// Created At: 19 June 2019
        /// Description : This method is use fetch Task Report
        /// </summary>
        /// <param name="taskParam"></param>
        /// <returns></returns>
       
        public IEnumerable<TaskReport.TaskReportField> GetTaskReport(TaskReport.TaskReportParam taskParam)
        {
            var uri = API.TaskReport.GetTaskReport(_path);
            IEnumerable<TaskReport.TaskReportField> categories = new List<TaskReport.TaskReportField>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri,taskParam).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                categories = EntityMapper<string, IEnumerable<TaskReport.TaskReportField>>.MapFromJson(jsonDataProviders);
            }
            return categories;
        }

        /// <summary>
        /// Author : Obaidullah Waseem
        /// Created At: 24 June 2019
        /// Description : This method is use fetch Task Status By User Report
        /// </summary>
        /// <param name="taskParam"></param>
        /// <returns></returns>

        public IEnumerable<TaskReport.TaskStatusByUserReport> GetTaskStatusByUserReport(TaskReport.TaskReportParam taskParam)
        {
            var uri = API.TaskReport.GetTaskStatusReport(_path);
            IEnumerable<TaskReport.TaskStatusByUserReport> categories = new List<TaskReport.TaskStatusByUserReport>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, taskParam).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                categories = EntityMapper<string, IEnumerable<TaskReport.TaskStatusByUserReport>>.MapFromJson(jsonDataProviders);
            }
            return categories;
        }



    }
    
}