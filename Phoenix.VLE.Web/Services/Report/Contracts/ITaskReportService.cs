﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;


namespace Phoenix.VLE.Web.Services
{
    public interface ITaskReportService
    {
        IEnumerable<TaskReport.TaskReportField> GetTaskReport(TaskReport.TaskReportParam taskparam);

        IEnumerable<TaskReport.TaskStatusByUserReport> GetTaskStatusByUserReport(TaskReport.TaskReportParam taskparam);
    }
}
