﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class IncidentReportService : IIncidentReportService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.HSEAPIUrl;
        readonly string _path = "api/v1/IncidentReport";
        #endregion

        public IncidentReportService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public IEnumerable<InjuredBodyPartCount> GetInjuredBodyPart(string incidentIds)
        {
            var uri = API.IncidentReport.GetInjuredBodyPartReport(_path, incidentIds);
            IEnumerable<InjuredBodyPartCount> injuredBodyPartCounts = new List<InjuredBodyPartCount>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                injuredBodyPartCounts = EntityMapper<string, IEnumerable<InjuredBodyPartCount>>.MapFromJson(jsonDataProviders);
            }
            return injuredBodyPartCounts;
        }

        public IEnumerable<ReportComparisonData> GetReportComparisionDataAsync(ReportParameters reportParameter)
        {
            var uri = API.IncidentReport.GetReportComparisonData(_path);
            IEnumerable<ReportComparisonData> reportComparisonData = new List<ReportComparisonData>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, reportParameter).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reportComparisonData = EntityMapper<string, IEnumerable<ReportComparisonData>>.MapFromJson(jsonDataProviders);
            }
            return reportComparisonData;
        }
    }
}