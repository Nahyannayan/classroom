﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class AssessmentService : IAssessmentService
    {
        #region private variables
        static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixUrl;
        readonly string _path = "api/v1/Assessment";
        #endregion

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created Date : 11-Jun-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public AssessmentService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        #region Methods

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 Aug 2019
        /// Description : This method is use fetch StudentList
        public IEnumerable<StudentList> GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID)
        {
            var uri = API.Assessment.GetStudentList(_path, GRD_ID, ACD_ID, SGR_ID, SCT_ID);
            IEnumerable<StudentList> studentList = new List<StudentList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentList = EntityMapper<string, IEnumerable<StudentList>>.MapFromJson(jsonDataProviders);
            }
            return studentList;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 Aug 2019
        /// Description : This method is get the report header configurations.
        public IEnumerable<ReportHeader> GetReportHeaders(string GRD_ID, Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv)
        {
            var uri = API.Assessment.GetReportHeaders(_path, GRD_ID, ACD_ID, SBG_ID, RPF_ID, RSM_ID, prv,0);
            IEnumerable<ReportHeader> reportHeaders = new List<ReportHeader>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reportHeaders = EntityMapper<string, IEnumerable<ReportHeader>>.MapFromJson(jsonDataProviders);
            }
            return reportHeaders;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 Aug 2019
        /// Description : This method is get the report header configurations.
        public IEnumerable<ReportHeaderDropDown> GetReportHeadersDropdowns(Int32 RSM_ID, Int32 SBG_ID, Int32 RSD_ID)
        {
            var uri = API.Assessment.GetReportHeadersDropdowns(_path, RSM_ID, SBG_ID, RSD_ID);
            IEnumerable<ReportHeaderDropDown> reportHeadersdropdowns = new List<ReportHeaderDropDown>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reportHeadersdropdowns = EntityMapper<string, IEnumerable<ReportHeaderDropDown>>.MapFromJson(jsonDataProviders);
            }
            return reportHeadersdropdowns;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 Aug 2019
        /// Description : This method is get the report header configurations.
        public IEnumerable<AssessmentData> GetAssessmentData(Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv)
        {
            var uri = API.Assessment.GetAssessmentData(_path, ACD_ID, SBG_ID, RPF_ID, RSM_ID, prv);
            IEnumerable<AssessmentData> assessdata = new List<AssessmentData>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessdata = EntityMapper<string, IEnumerable<AssessmentData>>.MapFromJson(jsonDataProviders);
            }
            return assessdata;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 May 2019
        /// Description : This method is insert or update the assessment data
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool InsertAssessmentData(string student_xml,string username, int bEdit)
        {

            var uri = API.Assessment.InsertAssessmentData(_path, username, bEdit);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, student_xml).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<MarkEntry> GetAssessmentActivityList(long ACD_ID = 0, long CAM_ID = 0, string GRD_ID = "", long STM_ID = 0, long TRM_ID = 0, long SGR_ID = 0, long SBG_ID = 0, int GRADE_ACCESS = 0, string Username = "", string SuperUser = "")
        {
            var uri = API.Assessment.GetAssessmentActivityList(_path,ACD_ID, CAM_ID, GRD_ID, STM_ID, TRM_ID, SGR_ID, SBG_ID, GRADE_ACCESS, Username, SuperUser);
            IEnumerable<MarkEntry> assessMarkEntrydata = new List<MarkEntry>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessMarkEntrydata = EntityMapper<string, IEnumerable<MarkEntry>>.MapFromJson(jsonDataProviders);
            }
            return assessMarkEntrydata;
        }

        public IEnumerable<MarkEntryAOLData> GetMarkEntryAOLData(long CAS_ID)
        {
            var uri = API.Assessment.GetMarkEntryAOLData(_path, CAS_ID);
            IEnumerable<MarkEntryAOLData> assessMarkEntrydata = new List<MarkEntryAOLData>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessMarkEntrydata = EntityMapper<string, IEnumerable<MarkEntryAOLData>>.MapFromJson(jsonDataProviders);
            }
            return assessMarkEntrydata;
        }

        public bool InsertMarkEntryAOLData(List<MarkEntryAOLData> lstmarkEntryAOLData, string Username ="", bool bWithoutSkill=false, long CAS_ID = 0)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Assessment.InsertMarkEntryAOLData(_path, Username, bWithoutSkill, CAS_ID), lstmarkEntryAOLData).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<MarkEntryData> GetMarkEntryData(long CAS_ID, double MIN_MARK, double MAX_MARK)
        {
            var uri = API.Assessment.GetMarkEntryData(_path, CAS_ID,MIN_MARK,MAX_MARK);
            IEnumerable<MarkEntryData> assessMarkEntrydata = new List<MarkEntryData>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessMarkEntrydata = EntityMapper<string, IEnumerable<MarkEntryData>>.MapFromJson(jsonDataProviders);
            }
            return assessMarkEntrydata;
        }

        public bool InsertMarkEntryData(List<MarkEntryData> lstmarkEntryData, long SlabId, string entryType, long CAS_ID)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Assessment.InsertMarkEntryData(_path, SlabId, entryType, CAS_ID), lstmarkEntryData).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<AssessmentComments> GetAssessmentComments(int CAT_ID, long STU_ID)
        {
            var uri = API.Assessment.GetAssessmentComments(_path, CAT_ID, STU_ID);
            IEnumerable<AssessmentComments> assessmentCommentdata = new List<AssessmentComments>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessmentCommentdata = EntityMapper<string, IEnumerable<AssessmentComments>>.MapFromJson(jsonDataProviders);
            }
            return assessmentCommentdata;
        }

        public IEnumerable<AssessmentCategory> GetAssessmentCategories(long CAT_BSU_ID, string CAT_GRD_ID)
        {
            var uri = API.Assessment.GetAssessmentCategories(_path, CAT_BSU_ID, CAT_GRD_ID);
            IEnumerable<AssessmentCategory> assessmentCategoriesdata = new List<AssessmentCategory>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessmentCategoriesdata = EntityMapper<string, IEnumerable<AssessmentCategory>>.MapFromJson(jsonDataProviders);
            }
            return assessmentCategoriesdata;
        }
        public IEnumerable<SectionAccess> GetSectionAccess(string USERNAME, string IsSuperUser, long ACD_ID, long BSU_ID, int GRD_ACCESS, string GRD_ID)
        {
            var uri = API.Assessment.GetSectionAccess(_path, USERNAME, IsSuperUser, ACD_ID, BSU_ID, GRD_ACCESS, GRD_ID);
            IEnumerable<SectionAccess> sectionAccesses = new List<SectionAccess>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                sectionAccesses = EntityMapper<string, IEnumerable<SectionAccess>>.MapFromJson(jsonDataProviders);
            }
            return sectionAccesses;
        }
        public IEnumerable<HeaderOptional> GetReportHeaderOptional(string AOD_IDs)
        {
            var uri = API.Assessment.GetReportHeaderOptional(_path, AOD_IDs);
            IEnumerable<HeaderOptional> headerOptionals = new List<HeaderOptional>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                headerOptionals = EntityMapper<string, IEnumerable<HeaderOptional>>.MapFromJson(jsonDataProviders);
            }
            return headerOptionals;
        }
        public IEnumerable<AssessmentDataOptional> GetAssessmentDataOptional(long ACD_ID, long RPF_ID, long RSM_ID, long SBG_ID, long SGR_ID, string GRD_ID, long SCT_ID, string AOD_IDs)
        {
            var uri = API.Assessment.GetAssessmentDataOptional(_path, ACD_ID, RPF_ID, RSM_ID, SBG_ID, SGR_ID, GRD_ID, SCT_ID, AOD_IDs);
            IEnumerable<AssessmentDataOptional> assessmentDatas = new List<AssessmentDataOptional>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessmentDatas = EntityMapper<string, IEnumerable<AssessmentDataOptional>>.MapFromJson(jsonDataProviders);
            }
            return assessmentDatas;
        }

        public IEnumerable<AssessmentPreviousSchedule> GetAssessmentPreviousSchedule(long ACD_ID, string GRD_ID)
        {
            var uri = API.Assessment.GetAssessmentPreviousSchedule(_path, ACD_ID, GRD_ID);
            IEnumerable<AssessmentPreviousSchedule> assessmentDatas = new List<AssessmentPreviousSchedule>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessmentDatas = EntityMapper<string, IEnumerable<AssessmentPreviousSchedule>>.MapFromJson(jsonDataProviders);
            }
            return assessmentDatas;
        }

        public IEnumerable<AssessmentOptionalList> GetAssessmentOptionList(long BSU_ID, long ACD_ID)
        {
            var uri = API.Assessment.GetAssessmentOptionList(_path, BSU_ID,ACD_ID);
            IEnumerable<AssessmentOptionalList> assessmentDatas = new List<AssessmentOptionalList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessmentDatas = EntityMapper<string, IEnumerable<AssessmentOptionalList>>.MapFromJson(jsonDataProviders);
            }
            return assessmentDatas;
        }
        #endregion

    }
}