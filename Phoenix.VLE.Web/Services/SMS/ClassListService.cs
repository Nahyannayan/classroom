﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using SMS.Web.Areas.SMS.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;


namespace Phoenix.VLE.Web.Services
{
    public class ClassListService : IClassListService
    {
        #region private variables
        static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.SIMSApiUrl;
        readonly string _path = "api/v1/ClassList";
        #endregion

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created Date : 11-Jun-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public ClassListService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }


        #region Methods
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 May 2019
        /// Description : This method is use fetch Room Attendance  List for  particular grade on a particular date through Api
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public IEnumerable<ClassList> GetClassList(string username, int tt_id = 0, string grade = null, string section = null)
        {
            var uri = API.ClassList.GetClassList(_path, username, tt_id, grade, section);
            IEnumerable<ClassList> classLists = new List<ClassList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classLists = EntityMapper<string, IEnumerable<ClassList>>.MapFromJson(jsonDataProviders);
            }
            return classLists;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 24 May 2019
        /// Description : This method is used to load Student Profile based on student id  through Api
        /// </summary>
        /// <param stu_id="student id "></param>
        /// <returns></returns>
        public BasicDetails GetStudentDetails(string stu_id)
        {
            var uri = API.ClassList.GetStudentDetails(_path, stu_id);
            BasicDetails basicdetails = new BasicDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                basicdetails = EntityMapper<string, BasicDetails>.MapFromJson(jsonDataProviders);
            }
            return basicdetails;
        }
        public ParentDetails GetProfileParentDetails(string stu_id)
        {
            var uri = API.ClassList.GetProfileParentDetails(_path, stu_id);
            ParentDetails parentdetails = new ParentDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                parentdetails = EntityMapper<string, ParentDetails>.MapFromJson(jsonDataProviders);
            }
            return parentdetails;
        }


        public IEnumerable<SiblingDetails> GetSiblingDetails(string stu_id)
        {
            var uri = API.ClassList.GetSiblingDetails(_path, stu_id);
            IEnumerable<SiblingDetails> _details = new List<SiblingDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _details = EntityMapper<string, IEnumerable<SiblingDetails>>.MapFromJson(jsonDataProviders);
            }
            return _details;
        }


        public TransportDetails GetTransportDetails(string stu_id)
        {
            var uri = API.ClassList.GetTransportDetails(_path, stu_id);
            TransportDetails _obj = new TransportDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, TransportDetails>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }

        public MedicalDetails GetMedicalDetails(string stu_id)
        {
            var uri = API.ClassList.GetMedicalDetails(_path, stu_id);
            MedicalDetails _obj = new MedicalDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, MedicalDetails>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }
        public StudentDashboardDetails GetStudentDashboardDetails(string stu_id)
        {
            var uri = API.ClassList.GetStudentDashboardDetails(_path, stu_id);
            StudentDashboardDetails _obj = new StudentDashboardDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, StudentDashboardDetails>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }
        public IEnumerable<ActivitiesDetails> GetActivitiesDetails(string stu_id)
        {
            var uri = API.ClassList.GetActivitiesDetails(_path, stu_id);
            IEnumerable<ActivitiesDetails> _obj = new List<ActivitiesDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<ActivitiesDetails>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }
        public IEnumerable<AttendanceChart> GetAttendanceChart(string stu_id)
        {
            var uri = API.ClassList.GetAttendanceChart(_path, stu_id);
            IEnumerable<AttendanceChart> _obj = new List<AttendanceChart>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<AttendanceChart>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }

        public IEnumerable<AssessmentDetails> GetAssessmentDetails(string stu_id)
        {
            var uri = API.ClassList.GetAssessmentDetails(_path, stu_id);
            IEnumerable<AssessmentDetails> _obj = new List<AssessmentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<AssessmentDetails>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }

        public IEnumerable<AttendenceList> GetAttendenceList(string stu_id,DateTime EndDate)
        {
            var uri = API.ClassList.GetAttendenceList(_path, stu_id, EndDate);
            IEnumerable<AttendenceList> _obj = new List<AttendenceList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<AttendenceList>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }

        public IEnumerable<StudentOnReportMaster> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId)
        {
            var uri = API.ClassList.GetStudentOnReportMasters(_path, studentId, academicYearId, schoolId);
            IEnumerable<StudentOnReportMaster> studentOnReport = new List<StudentOnReportMaster>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentOnReport = EntityMapper<string, IEnumerable<StudentOnReportMaster>>.MapFromJson(jsonDataProviders);
            }
            return studentOnReport;
        }

        public long StudentOnReportMasterCU(StudentOnReportMaster studentOnReport)
        {
            var uri = API.ClassList.StudentOnReportMasterCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentOnReport).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt64(jsonDataProviders);
            }
            return 0;
        }

        public IEnumerable<StudentOnReportDetail> GetStudentOnReportDetails(StudentOnReportDetailsParameter detailsParameter)
        {
            var uri = API.ClassList.GetStudentOnReportDetails(_path);
            IEnumerable<StudentOnReportDetail> studentOnReport = new List<StudentOnReportDetail>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, detailsParameter).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentOnReport = EntityMapper<string, IEnumerable<StudentOnReportDetail>>.MapFromJson(jsonDataProviders);
            }
            return studentOnReport;
        }

        public long StudentOnReportDetailsCU(StudentOnReportDetail studentOnReport)
        {
            var uri = API.ClassList.StudentOnReportDetailsCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentOnReport).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt64(jsonDataProviders);
            }
            return 0;
        }

        public IEnumerable<BehaviorDetails> GetBehaviorDetail(long STU_ID)
        {
            var uri = API.ClassList.GetBehaviorDetail(_path, STU_ID);
            IEnumerable<BehaviorDetails> _details = new List<BehaviorDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _details = EntityMapper<string, IEnumerable<BehaviorDetails>>.MapFromJson(jsonDataProviders);
            }
            return _details;
        }

        #endregion

    }
}