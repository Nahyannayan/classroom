﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class ReportService : IReportsService
    {

        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.SIMSApiUrl;
        readonly string _path = "api/v1/Reports";
        #endregion

        public ReportService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }

        }

        public IEnumerable<ReportBinder> BindReportFilters(long RDSR_ID, string RDF_FILTER_CODE, string FILTER_PARAMS)
        {
           
            IEnumerable<ReportBinder> rptBindFilter = new List<ReportBinder>();
           
            HttpResponseMessage response = _client.PostAsJsonAsync(API.SIMSReport.BindReportFilters(_path, RDSR_ID, RDF_FILTER_CODE), FILTER_PARAMS).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                rptBindFilter = EntityMapper<string, IEnumerable<ReportBinder>>.MapFromJson(jsonDataProviders);
            }
            return rptBindFilter;
        }

        public IEnumerable<ReportFilterControls> GetReportFiltersType()
        {
            var uri = API.SIMSReport.GetReportFiltersType(_path);
            IEnumerable<ReportFilterControls> rptBindFilter = new List<ReportFilterControls>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                rptBindFilter = EntityMapper<string, IEnumerable<ReportFilterControls>>.MapFromJson(jsonDataProviders);
            }
            return rptBindFilter;
        }

        public Reports GetReportLayoutById(string Dev_Id)
        {
            var uri = API.SIMSReport.GetReportLayoutById(_path, Dev_Id);
            Reports rptBindFilter = new Reports();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                rptBindFilter = EntityMapper<string, Reports>.MapFromJson(jsonDataProviders);
            }
            return rptBindFilter;
        }

       

        public IEnumerable<ReportDesigner> LoadDesignedReports(string BSU_ID, string ModuleId)
        {
            var uri = API.SIMSReport.LoadDesignedReports(_path, BSU_ID, ModuleId);
            IEnumerable<ReportDesigner> rptBindFilter = new List<ReportDesigner>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                rptBindFilter = EntityMapper<string, IEnumerable<ReportDesigner>>.MapFromJson(jsonDataProviders);
            }
            return rptBindFilter;
        }



        public IEnumerable<ReportTypes> GetReportTypes()
        {
            var uri = API.SIMSReport.GetReportTypes(_path);
            IEnumerable<ReportTypes> rptBindFilter = new List<ReportTypes>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                rptBindFilter = EntityMapper<string, IEnumerable<ReportTypes>>.MapFromJson(jsonDataProviders);
            }
            return rptBindFilter;
        }

        public string GetDatasetForSp(string sp_Name, string filterparam)
        {
           
            IEnumerable<dynamic> rptBindFilter = new List<dynamic>();
           
            HttpResponseMessage response = _client.PostAsJsonAsync(API.SIMSReport.GetDatasetForSp(_path, sp_Name), filterparam).Result;
            var jsonDataProviders = string.Empty;
            if (response.IsSuccessStatusCode)
            {
                jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                rptBindFilter = EntityMapper<string, IEnumerable<dynamic>>.MapFromJson(jsonDataProviders);

              
            }
            return jsonDataProviders;
        }

        public ReportSetup GetDevIdFromRSM_ID(int RSM_ID)
        {
            var uri = API.SIMSReport.GetDevIdFromRSM_ID(_path, RSM_ID);
            ReportSetup rptBindFilter = new ReportSetup();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                rptBindFilter = EntityMapper<string, ReportSetup>.MapFromJson(jsonDataProviders);
            }
            return rptBindFilter;
        }

        public string GetReportTopHeaders(string IMG_BSU_ID, string IMG_TYPE)
        {
            var uri = API.SIMSReport.GetReportTopHeaders(_path, IMG_BSU_ID, IMG_TYPE);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
           
            var jsonDataProviders = string.Empty;
            if (response.IsSuccessStatusCode)
            {
                jsonDataProviders = response.Content.ReadAsStringAsync().Result;
               


            }
            return jsonDataProviders;
        }
    }
}