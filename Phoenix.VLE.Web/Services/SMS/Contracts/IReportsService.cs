﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;

namespace Phoenix.VLE.Web.Services
{
    public interface IReportsService
    {
        IEnumerable<ReportDesigner> LoadDesignedReports(string BSU_ID, string ModuleId);
        Reports GetReportLayoutById(string Dev_Id);

        IEnumerable<ReportBinder> BindReportFilters(long RDSR_ID, string RDF_FILTER_CODE, string FILTER_PARAMS);

        IEnumerable<ReportFilterControls> GetReportFiltersType();
        IEnumerable<ReportTypes> GetReportTypes();

        string GetDatasetForSp(string sp_Name, string filterparam);

        ReportSetup GetDevIdFromRSM_ID(int RSM_ID);
        string GetReportTopHeaders(string IMG_BSU_ID, string IMG_TYPE);

    }
}
