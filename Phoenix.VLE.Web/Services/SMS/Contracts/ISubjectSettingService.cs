﻿using Phoenix.VLE.Web.Areas.SMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISubjectSettingService
    {
        #region Made By Dhanaji
        IEnumerable<SubjectMaster> GetSubjectMasterList(int CLM_ID);
        bool SubjectMasterCUD(SubjectMaster subjectMaster, string mode);
        IEnumerable<SubjectMaster> GetSubjectMastersByCurriculum(long curriculumId);
        IEnumerable<SubjectGroup> GetSubjectGroupList(long ACD_ID, string IsSuperUser, string Username);
        IEnumerable<ShiftModel> GetShiftListById(long ACD_ID, string GRD_ID);
        IEnumerable<SubjectGroupTeacherDdl> GetSubjectGroupTeachers(long BSU_ID, string IsSuperUser, string Username);
        IEnumerable<SubjectGroupTeacher> GetSubjectGroupTeachersGrid(long SGR_ID, string IsSuperUser, string Username);
        bool SaveUpdateGroupTeacher(SubjectGroup subjectGroupTeacher, string UserName, string mode);
        bool SaveUpdateSubjectGroup(SubjectGroup subjectGroup, string mode);
        IEnumerable<SubjectGroupStudent> GetSubjectGroupStudentList(long ACD_ID, string GRD_ID, int SHF_ID, int STM_ID, long SBG_ID, long SGR_ID);
        #endregion

        IEnumerable<SubjectByGradeParent> BindGradesForSubject(long ACD_ID);
        IEnumerable<SubjectByGradeChild> BindSubjectsByGrade(long ACD_ID, string GRD_ID, int STM_ID, int SBG_ID = 0);


        IEnumerable<ParentSubject> GetParentSubjects(long ACD_ID, int STM_ID, string GRD_ID);
        IEnumerable<SubjectByGradeParent> GetGradeForSubjectCopy(long ACD_ID);
        bool AddOptionName(long schoolId, string optionName);
        IEnumerable<FromStream> GetStreamForSubjectCopy(long ACD_ID, string GRD_ID);

        string SubjectGrade(List<SubjectByGradeEntry> gradeEntries);
        string SubjectGradeDelete(int subjectGradeId);
    }
}
