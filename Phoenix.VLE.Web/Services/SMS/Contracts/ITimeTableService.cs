﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using SMS.Web.Areas.SMS.Model;

namespace Phoenix.VLE.Web.Services
{
   public  interface ITimeTableService
    {
        IEnumerable<Grades> GetGrades();
        IEnumerable<Grades> GetGradesByUser(string username);

        IEnumerable<Sections> GetSectionByGrade(int gradeId);

        IEnumerable<CalenderMonth> GetCurrentMonth();
        IEnumerable<TimeTable> GetTimeTablesByUserandDate(string username, DateTime dateTime, string type, string grade = null, string section = null);

    }
}
