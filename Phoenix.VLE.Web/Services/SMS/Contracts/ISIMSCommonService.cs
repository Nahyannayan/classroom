﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public interface ISIMSCommonService
    {
        IEnumerable<Areas.SMS.Model.Common.GradesAccess> GetGradesAccess(string username, string isSuperUser, Int32 acd_id, Int32 bsu_id, int grd_access, Int32 rsm_id=-1);
       
        IEnumerable<CourseGroupName> GetCourseGroupByCourse(long courseId, long userId);
        CurriculumRole GetUserCurriculumRole(string BSU_ID = "", string UserName = "");
        IEnumerable<CourseDetails> GetTeacherCourse(long id , long Schoolid, long curriculumId=0);
      
        
        IEnumerable<GradeTemplateMaster> GradeTemplateMaster(long acd_id);
        

        IEnumerable<Curriculum> GetCurriculum(long schoolId,int? CLM_ID = null);
        IEnumerable<ListItem> GetSelectListItems(string listCode, string whereCondition, object whereConditionParamValues);

        IEnumerable<ListItem> GetAcademicYearList(string schoolId, int curriculumId, bool? IsCurrentCurriculum);

        IEnumerable<Phoenix.VLE.Web.Models.Subjects> GetSubjectsByGrade(Int32 acd_id, string grd_id, string username = "", string IsSuperUser = "");

    }
}
