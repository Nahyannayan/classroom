﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using SMS.Web.Areas.SMS.Model;

namespace Phoenix.VLE.Web.Services
{
   public  interface ISENService
    {
        IEnumerable<BasicDetails> Get_studentInclusionList(string BSU_ID, string ACD_ID, string GRD_ID, string SCT_ID);
        IEnumerable<BasicDetails> Get_studentInclusionAll(string BSU_ID, string ACD_ID, string GRD_ID, string SCT_ID);

        bool InsertBulkSEN(String stuIds);

        bool updateSenStudent(string stuId);


        IEnumerable<SEN_KHDA_MASTER> Get_SEN_KHDA_MASTER();

        KHDA_STUDENT Get_KHDA_STUDENT(string stuId);
        IEnumerable<SEN_KHDA_TRANS> Get_SEN_KHDA_TRANS_LIST(string stuId);


        bool SaveSENKHDA(KHDA_STUDENT model, List<SEN_KHDA_TRANS> modalList,string uGUID);
        
    }
}
