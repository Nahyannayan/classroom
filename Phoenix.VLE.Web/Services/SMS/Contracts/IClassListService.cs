﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Models;
using SMS.Web.Areas.SMS.Model;

namespace Phoenix.VLE.Web.Services
{
   public  interface IClassListService
    {
        IEnumerable<ClassList> GetClassList(string username, int tt_id = 0, string grade = null, string section = null);
        BasicDetails GetStudentDetails(string stu_id);
        ParentDetails GetProfileParentDetails(string stu_id);

        IEnumerable<SiblingDetails> GetSiblingDetails(string stu_id);

        TransportDetails GetTransportDetails(string stu_id);
        MedicalDetails GetMedicalDetails(string stu_id);
        StudentDashboardDetails GetStudentDashboardDetails(string stu_id);
        IEnumerable<ActivitiesDetails> GetActivitiesDetails(string stu_id);
        IEnumerable<AttendanceChart> GetAttendanceChart(string stu_id); 
        IEnumerable<AssessmentDetails> GetAssessmentDetails(string stu_id);
        IEnumerable<AttendenceList> GetAttendenceList(string stu_id,DateTime EndDate);
        IEnumerable<BehaviorDetails> GetBehaviorDetail(long STU_ID);
        long StudentOnReportMasterCU(StudentOnReportMaster studentOnReport);
        IEnumerable<StudentOnReportMaster> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId);
        IEnumerable<StudentOnReportDetail> GetStudentOnReportDetails(StudentOnReportDetailsParameter detailsParameter);
        long StudentOnReportDetailsCU(StudentOnReportDetail studentOnReport);
    }
}
