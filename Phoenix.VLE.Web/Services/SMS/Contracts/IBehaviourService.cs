﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using SMS.Web.Areas.SMS.Model;

namespace Phoenix.VLE.Web.Services
{
   public  interface IBehaviourService
    {
        IEnumerable<ClassList> GetStudentList(string username, int tt_id = 0, string grade = null, string section = null);
        IEnumerable<Behaviour> LoadBehaviourByStudentId(string stu_id);
        IEnumerable<BehaviourDetails> GetBehaviourById(int id);
        bool InsertBehaviourDetails(BehaviourDetails entity, string bsu_id, string mode = "ADD");

        IEnumerable<StudentBehaviour> GetListOfStudentBehaviour();
        bool InsertUpdateStudentBehavior(List<StudentBehaviourFiles> studentBehaviourFiles,long studentId = 0, int behaviorId = 0, string behaviourComment = "");

        IEnumerable<StudentBehaviour> GetStudentBehaviorByStudentId(long studentId = 0);

        bool InsertBulkStudentBehaviour(List<StudentBehaviourFiles> studentBehaviourFiles,string bulkStudentds = "", int behaviourId = 0, string behaviourComment = "");

        bool UpdateBehaviourTypes(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0);

        IEnumerable<StudentBehaviourFiles> GetFileDetailsByStudentId(long studentId = 0);

        IEnumerable<ClassList> GetBehaviourClassList(string username, int tt_id = 0, string grade = null, string section = null);
        bool DeleteStudentBehaviourMapping(long studentId = 0, int behaviourId = 0);
    }
}
