﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Helpers;
using SMS.Web.Areas.SMS.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class BehaviourService : IBehaviourService
    {
        #region private variables
        static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.SIMSApiUrl;
        readonly string _path = "api/v1/Behaviour";
        #endregion

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created Date : 11-Jun-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public BehaviourService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }

        }
        #region Methods

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 11 July 2019
        /// Description : This method is use fetch Student List  for  particular grade or TimeTable ID  through Api
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public IEnumerable<ClassList> GetStudentList(string username, int tt_id = 0, string grade = null, string section = null)
        {
            var uri = API.Behaviour.GetStudentList(_path, username, tt_id, grade, section);
            IEnumerable<ClassList> studentlist = new List<ClassList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentlist = EntityMapper<string, IEnumerable<ClassList>>.MapFromJson(jsonDataProviders);
            }
            return studentlist;
        }


        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 14 July 2019
        /// Description : This method is use fetch the behaviour details of student(s) through Api
        /// </summary>
        /// <param name="stu_id"></param>
        /// <returns></returns>
        public IEnumerable<Behaviour> LoadBehaviourByStudentId(string stu_id)
        {
            var uri = API.Behaviour.LoadBehaviourByStudentId(_path, stu_id);
            IEnumerable<Behaviour> behaviours = new List<Behaviour>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviours = EntityMapper<string, IEnumerable<Behaviour>>.MapFromJson(jsonDataProviders);
            }
            return behaviours;
        }

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 15 July 2019
        /// Description : This method is use fetch the behaviour details by behaviour ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<BehaviourDetails> GetBehaviourById(int id)
        {
            var uri = API.Behaviour.GetBehaviourById(_path, id);
            IEnumerable<BehaviourDetails> behaviourDetails = new List<BehaviourDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviourDetails = EntityMapper<string, IEnumerable<BehaviourDetails>>.MapFromJson(jsonDataProviders);
            }
            return behaviourDetails;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 17 July 2019
        /// Description : This method is used to insert/update behaviour details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool InsertBehaviourDetails(BehaviourDetails model, string bsu_id, string mode = "ADD")
        {
            var uri = API.Behaviour.InsertBehaviourDetails(_path, bsu_id,mode);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<StudentBehaviour> GetListOfStudentBehaviour()
        {
            var uri = API.Behaviour.GetListOfStudentBehaviour(_path);
            IEnumerable<StudentBehaviour> behaviourDetails = new List<StudentBehaviour>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviourDetails = EntityMapper<string, IEnumerable<StudentBehaviour>>.MapFromJson(jsonDataProviders);
            }
            return behaviourDetails;
        }

        public bool InsertUpdateStudentBehavior(List<StudentBehaviourFiles> studentBehaviourFiles,long studentId = 0, int behaviorId = 0, string behaviourComment = "")
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Behaviour.InsertUpdateStudentBehavior(_path, studentId, behaviorId), studentBehaviourFiles).Result;
            return response.IsSuccessStatusCode;
           
        }

        public IEnumerable<StudentBehaviour> GetStudentBehaviorByStudentId(long studentId = 0)
        {
            var uri = API.Behaviour.GetStudentBehaviorByStudentId(_path,studentId);
            IEnumerable<StudentBehaviour> behaviourDetails = new List<StudentBehaviour>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviourDetails = EntityMapper<string, IEnumerable<StudentBehaviour>>.MapFromJson(jsonDataProviders);
            }
            return behaviourDetails;
        }

        public bool InsertBulkStudentBehaviour(List<StudentBehaviourFiles> studentBehaviourFiles,string bulkStudentds = "",int behaviourId =0, string behaviourComment = "")
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Behaviour.InsertBulkStudentBehaviour(_path, bulkStudentds, behaviourId, behaviourComment), studentBehaviourFiles).Result;
            return response.IsSuccessStatusCode;
           
        }

        public bool UpdateBehaviourTypes(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0)
        {
            var uri = API.Behaviour.UpdateBehaviourTypes(_path, behaviourId, behaviourType, behaviourPoint, categoryId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<StudentBehaviourFiles> GetFileDetailsByStudentId(long studentId = 0)
        {
            var uri = API.Behaviour.GetFileDetailsByStudentId(_path, studentId);
            IEnumerable<StudentBehaviourFiles> behaviourFileDetails = new List<StudentBehaviourFiles>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviourFileDetails = EntityMapper<string, IEnumerable<StudentBehaviourFiles>>.MapFromJson(jsonDataProviders);
            }
            return behaviourFileDetails;
        }

        public IEnumerable<ClassList> GetBehaviourClassList(string username, int tt_id = 0, string grade = null, string section = null)
        {
            var uri = API.Behaviour.GetBehaviourClassList(_path, username, tt_id, grade, section);
            IEnumerable<ClassList> classLists = new List<ClassList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classLists = EntityMapper<string, IEnumerable<ClassList>>.MapFromJson(jsonDataProviders);
            }
            return classLists;
        }

        public bool DeleteStudentBehaviourMapping(long studentId = 0, int behaviourId = 0)
        {
            var uri = API.Behaviour.DeleteStudentBehaviourMapping(_path, studentId, behaviourId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;

           
        }

        #endregion

    }
}