﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Helpers;
using SMS.Web.Areas.SMS.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class TimeTableService : ITimeTableService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.SIMSApiUrl;
        readonly string _path = "api/v1/TimeTable";
        #endregion

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created Date : 11-Jun-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public TimeTableService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }


        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 May 2019
        /// Description : This method is use fetch All active Task List through Api
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        #region Methods
        public IEnumerable<Grades> GetGrades()
        {
            var uri = API.TimeTable.GetGrades(_path);
            IEnumerable<Grades> grades = new List<Grades>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                grades = EntityMapper<string, IEnumerable<Grades>>.MapFromJson(jsonDataProviders);
            }
            return grades;
        }
        public IEnumerable<Grades> GetGradesByUser(string username)
        {
            var uri = API.TimeTable.GetGradesByUser(_path, username);
            IEnumerable<Grades> grades = new List<Grades>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                grades = EntityMapper<string, IEnumerable<Grades>>.MapFromJson(jsonDataProviders);
            }
            return grades;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 23 May 2019
        /// Description : This method is use fetch All active Task List through Api
        /// </summary>
        /// <param name="Userid"></param>
        /// <returns></returns>
        #region Methods for get Grade wise Section
        public IEnumerable<Sections> GetSectionByGrade(int gradeId)
        {
            var uri = API.TimeTable.GetSectionsByGrade(_path, gradeId);
            IEnumerable<Sections> sections = new List<Sections>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                sections = EntityMapper<string, IEnumerable<Sections>>.MapFromJson(jsonDataProviders);
            }
            return sections;
        }
        #endregion
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 7 May 2019
        /// Description : This method is use fetch  active Task List through Api by TaskID
        /// </summary>
        public IEnumerable<CalenderMonth> GetCurrentMonth()
        {
            var uri = API.TimeTable.GetCurrentMonth(_path);
            IEnumerable<CalenderMonth> days = new List<CalenderMonth>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                days = EntityMapper<string, IEnumerable<CalenderMonth>>.MapFromJson(jsonDataProviders);
            }
            return days;
        }

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 7 May 2019
        /// Description : This method is use fetch  Table for teacher for particular date
        /// </summary>
        public IEnumerable<TimeTable> GetTimeTablesByUserandDate(string username, DateTime dateTime, string type, string grade = null, string section = null)
        {
            var uri = API.TimeTable.GetTimeTablesByUserandDate(_path, username, dateTime, type, grade, section);
            IEnumerable<TimeTable> timetable = new List<TimeTable>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                timetable = EntityMapper<string, IEnumerable<TimeTable>>.MapFromJson(jsonDataProviders);
            }
            return timetable;
        }
        #endregion

    }
}