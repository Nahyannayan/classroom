﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Helpers;
using SMS.Web.Areas.SMS.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class SENService : ISENService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.SIMSApiUrl;
        readonly string _path = "api/v1/SEN";
        #endregion

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created Date : 11-Jun-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public SENService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        #region Methods

        public IEnumerable<BasicDetails> Get_studentInclusionList(string BSU_ID, string ACD_ID, string GRD_ID, string SCT_ID)
        {
            var uri = API.SEN.Get_studentInclusionList(_path, BSU_ID, ACD_ID, GRD_ID, SCT_ID);
            IEnumerable<BasicDetails> _obj = new List<BasicDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<BasicDetails>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }
        public IEnumerable<BasicDetails> Get_studentInclusionAll(string BSU_ID, string ACD_ID, string GRD_ID, string SCT_ID)
        {
            var uri = API.SEN.Get_studentInclusionAll(_path, BSU_ID, ACD_ID, GRD_ID, SCT_ID);
            IEnumerable<BasicDetails> _obj = new List<BasicDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<BasicDetails>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }
        public bool InsertBulkSEN(String stuIds)
        {
            var uri = API.SEN.InsertBulkSEN(_path);            
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, stuIds).Result;
            return response.IsSuccessStatusCode;
        }
       public bool updateSenStudent(string stuId)
        {
            var uri = API.SEN.updateSenStudent(_path, stuId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }


        public IEnumerable<SEN_KHDA_MASTER> Get_SEN_KHDA_MASTER()
        {
            var uri = API.SEN.Get_SEN_KHDA_MASTER(_path);
            IEnumerable<SEN_KHDA_MASTER> _obj = new List<SEN_KHDA_MASTER>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<SEN_KHDA_MASTER>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }

        public KHDA_STUDENT Get_KHDA_STUDENT(string stuId)
        {
            var uri = API.SEN.Get_KHDA_STUDENT(_path, stuId);
            KHDA_STUDENT _obj = new KHDA_STUDENT();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, KHDA_STUDENT>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }


        public IEnumerable<SEN_KHDA_TRANS> Get_SEN_KHDA_TRANS_LIST(string stuId)
        {
            var uri = API.SEN.Get_SEN_KHDA_TRANS_LIST(_path, stuId);
            IEnumerable<SEN_KHDA_TRANS> _obj = new List<SEN_KHDA_TRANS>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _obj = EntityMapper<string, IEnumerable<SEN_KHDA_TRANS>>.MapFromJson(jsonDataProviders);
            }
            return _obj;
        }
        public bool SaveSENKHDA(KHDA_STUDENT model, List<SEN_KHDA_TRANS> modalList, string uGUID)
        {
            var filePath = PhoenixConfiguration.Instance.WriteFilePath;
            var uri = API.SEN.SaveSENKHDA(_path, uGUID, filePath);
            var mainModel = new KHDA();
            mainModel.KHDA_STUDENT = model;
            mainModel.SEN_KHDA_TRANS_LIST = modalList;          
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, mainModel).Result;
            return response.IsSuccessStatusCode;
        }
        
        #endregion




    }
}