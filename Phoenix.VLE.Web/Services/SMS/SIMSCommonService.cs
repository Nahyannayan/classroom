﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class SIMSCommonService : ISIMSCommonService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Grades";
        #endregion

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created Date : 11-Jun-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public SIMSCommonService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        #region Methods

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 May 2019
        /// Description : This method is use fetch Room Attendance  List for  particular grade on a particular date through Api
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public IEnumerable<Areas.SMS.Model.Common.GradesAccess> GetGradesAccess(string username, string isSuperUser, Int32 acd_id, Int32 bsu_id, int grd_access, Int32 rsm_id = -1)
        {
            var uri = API.SIMSCommon.GetGradesAccess(_path, username, isSuperUser, acd_id, bsu_id, grd_access, rsm_id);
            IEnumerable<Areas.SMS.Model.Common.GradesAccess> grades = new List<Areas.SMS.Model.Common.GradesAccess>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                grades = EntityMapper<string, IEnumerable<Areas.SMS.Model.Common.GradesAccess>>.MapFromJson(jsonDataProviders);
            }
            return grades;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 May 2019
        /// Description : This method is use fetch Room Attendance  List for  particular grade on a particular date through Api
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public IEnumerable<Phoenix.VLE.Web.Models.Subjects> GetSubjectsByGrade(Int32 acd_id, string grd_id)
        {
            var uri = API.SIMSCommon.GetSubjectsByGrade(_path, acd_id, grd_id);
            IEnumerable<Phoenix.VLE.Web.Models.Subjects> subjects = new List<Phoenix.VLE.Web.Models.Subjects>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<Phoenix.VLE.Web.Models.Subjects>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 May 2019
        /// Description : This method is use fetch Room Attendance  List for  particular grade on a particular date through Api
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        //public IEnumerable<Areas.SMS.Model.Common.SubjectGroups> GetSubjectGroupBySubject(string username, string isSuperUser, Int32 bsu_id, string grd_id, Int32 sbg_id)
        //{
        //    var uri = API.SIMSCommon.GetSubjectGroupBySubject(_path, username, isSuperUser, bsu_id, grd_id, sbg_id);
        //    IEnumerable<Areas.SMS.Model.Common.SubjectGroups> subjectgroups = new List<Areas.SMS.Model.Common.SubjectGroups>();
        //    HttpResponseMessage response = _client.GetAsync(uri).Result;
        //    if (response.IsSuccessStatusCode)
        //    {
        //        var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
        //        subjectgroups = EntityMapper<string, IEnumerable<Phoenix.VLE.Web.Models.SubjectGroups>>.MapFromJson(jsonDataProviders);
        //    }
        //    return subjectgroups;
        //}


        public IEnumerable<GradeTemplateMaster> GradeTemplateMaster(long acd_id)
        {
            var uri = API.SIMSCommon.GradeTemplateMaster(_path, acd_id);
            IEnumerable<GradeTemplateMaster> GradeTemplateMaster = new List<GradeTemplateMaster>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                GradeTemplateMaster = EntityMapper<string, IEnumerable<GradeTemplateMaster>>.MapFromJson(jsonDataProviders);
            }
            return GradeTemplateMaster;
        }

        public IEnumerable<CourseDetails> GetTeacherCourse(long id,long Schoolid, long curriculumId=0)
        {
            var uri = API.SIMSCommon.GetTeacherCourse(_path, id,  Schoolid, curriculumId);
            IEnumerable<CourseDetails> CourseDetails = new List<CourseDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                CourseDetails = EntityMapper<string, IEnumerable<CourseDetails>>.MapFromJson(jsonDataProviders);
            }
            return CourseDetails;
        }
        public CurriculumRole GetUserCurriculumRole(string BSU_ID = "", string UserName = "")
        {
            var uri = API.SIMSCommon.GetUserCurriculumRole(_path, BSU_ID, UserName);
            CurriculumRole behaviours = new CurriculumRole();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviours = EntityMapper<string, CurriculumRole>.MapFromJson(jsonDataProviders);
            }
            return behaviours;
        }

        public IEnumerable<ListItem> GetSelectListItems(string listCode, string whereCondition, object whereConditionParamValues)
        {
            var uri = API.SIMSCommon.GetSelectListItems(_path, listCode, whereCondition, whereConditionParamValues);
            IEnumerable<ListItem> list = new List<ListItem>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<ListItem>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public IEnumerable<Curriculum> GetCurriculum(long schoolId, int? CLM_ID = null)
        {
            var uri = API.SIMSCommon.GetCurriculum(_path, schoolId, CLM_ID);
            IEnumerable<Curriculum> behaviours = new List<Curriculum>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviours = EntityMapper<string, IEnumerable<Curriculum>>.MapFromJson(jsonDataProviders);
            }
            return behaviours;
        }

        public IEnumerable<ListItem> GetAcademicYearList(string schoolId, int curriculumId, bool? IsCurrentCurriculum)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Phoenix.VLE.Web.Models.Subjects> GetSubjectsByGrade(int acd_id, string grd_id, string username = "", string IsSuperUser = "")
        {
            var uri = API.SIMSCommon.GetSubjectsByGrade(_path, acd_id, grd_id);
            IEnumerable<Phoenix.VLE.Web.Models.Subjects> subjects = new List<Phoenix.VLE.Web.Models.Subjects>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<Phoenix.VLE.Web.Models.Subjects>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }

        public IEnumerable<CourseGroupName> GetCourseGroupByCourse(long courseId, long userId)
        {
            var uri = API.SIMSCommon.GetCourseGroupByCourse(_path, courseId, userId);
            IEnumerable<CourseGroupName> subjects = new List<CourseGroupName>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<CourseGroupName>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }

        #endregion

    }
}