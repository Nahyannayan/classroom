﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Areas.SMS.Models;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class SubjectSettingService : ISubjectSettingService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.SIMSApiUrl;
        readonly string _path = "api/v1/SubjectSetting";
        #endregion

        /// <summary>
        /// Author : Dhanaji Patil
        /// Created Date : 19-Nov-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public SubjectSettingService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        #region Made By Dhanaji

        #region Subject Master
        public IEnumerable<SubjectMaster> GetSubjectMasterList(int CLM_ID)
        {
            var uri = API.SubjectSetting.GetStudentList(_path, CLM_ID);
            IEnumerable<SubjectMaster> subjectMasterList = new List<SubjectMaster>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjectMasterList = EntityMapper<string, IEnumerable<SubjectMaster>>.MapFromJson(jsonDataProviders);
            }
            return subjectMasterList;
        }
        public bool SubjectMasterCUD(SubjectMaster subjectMaster, string mode)
        {
            IEnumerable<SubjectMaster> subjectMasterList = new List<SubjectMaster>();
            HttpResponseMessage response = _client.PostAsJsonAsync(API.SubjectSetting.SubjectMasterCUD(_path, mode), subjectMaster).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<SubjectMaster> GetSubjectMastersByCurriculum(long curriculumId)
        {
            var uri = API.SubjectSetting.GetSubjectMastersByCurriculum(_path, curriculumId);
            IEnumerable<SubjectMaster> SubjectMasterList = new List<SubjectMaster>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                SubjectMasterList = EntityMapper<string, IEnumerable<SubjectMaster>>.MapFromJson(jsonDataProviders);
            }
            return SubjectMasterList;
        }
        #endregion

        #region Subject Group
        public IEnumerable<SubjectGroup> GetSubjectGroupList(long ACD_ID, string IsSuperUser, string Username)
        {
            var uri = API.SubjectSetting.GetSubjectGroupList(_path, ACD_ID, IsSuperUser, Username);
            IEnumerable<SubjectGroup> subjectGroupList = new List<SubjectGroup>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjectGroupList = EntityMapper<string, IEnumerable<SubjectGroup>>.MapFromJson(jsonDataProviders);
            }
            return subjectGroupList;
        }
        public IEnumerable<ShiftModel> GetShiftListById(long ACD_ID, string GRD_ID)
        {
            var uri = API.SubjectSetting.GetShiftListById(_path, ACD_ID, GRD_ID);
            IEnumerable<ShiftModel> ShiftModelList = new List<ShiftModel>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                ShiftModelList = EntityMapper<string, IEnumerable<ShiftModel>>.MapFromJson(jsonDataProviders);
            }
            return ShiftModelList;
        }
        public IEnumerable<SubjectGroupTeacherDdl> GetSubjectGroupTeachers(long BSU_ID, string IsSuperUser, string Username)
        {
            var uri = API.SubjectSetting.GetSubjectGroupTeachers(_path, BSU_ID, IsSuperUser, Username);
            IEnumerable<SubjectGroupTeacherDdl> SubjectGroupTeacheList = new List<SubjectGroupTeacherDdl>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                SubjectGroupTeacheList = EntityMapper<string, IEnumerable<SubjectGroupTeacherDdl>>.MapFromJson(jsonDataProviders);
            }
            return SubjectGroupTeacheList;
        }
        public IEnumerable<SubjectGroupTeacher> GetSubjectGroupTeachersGrid(long SGR_ID, string IsSuperUser, string Username)
        {
            var uri = API.SubjectSetting.GetSubjectGroupTeachersGrid(_path, SGR_ID, IsSuperUser, Username);
            IEnumerable<SubjectGroupTeacher> SubjectGroupTeacheList = new List<SubjectGroupTeacher>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                SubjectGroupTeacheList = EntityMapper<string, IEnumerable<SubjectGroupTeacher>>.MapFromJson(jsonDataProviders);
            }
            return SubjectGroupTeacheList;
        }
        public bool SaveUpdateGroupTeacher(SubjectGroup subjectGroupTeacher, string UserName, string mode)
        {           
            HttpResponseMessage response = _client.PostAsJsonAsync(API.SubjectSetting.SaveUpdateGroupTeacher(_path, UserName, mode), subjectGroupTeacher).Result;
            return response.IsSuccessStatusCode;
        }
        public bool SaveUpdateSubjectGroup(SubjectGroup subjectGroup, string mode)
        {          
            HttpResponseMessage response = _client.PostAsJsonAsync(API.SubjectSetting.SaveUpdateSubjectGroup(_path, mode), subjectGroup).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<SubjectGroupStudent> GetSubjectGroupStudentList(long ACD_ID, string GRD_ID, int SHF_ID, int STM_ID, long SBG_ID, long SGR_ID)
        {
            var uri = API.SubjectSetting.GetSubjectGroupStudentList(_path, ACD_ID, GRD_ID, SHF_ID, STM_ID, SBG_ID, SGR_ID);
            IEnumerable<SubjectGroupStudent> _SubjectGroupStudentList = new List<SubjectGroupStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _SubjectGroupStudentList = EntityMapper<string, IEnumerable<SubjectGroupStudent>>.MapFromJson(jsonDataProviders);
            }
            return _SubjectGroupStudentList;
        }
        #endregion

        #endregion

        public IEnumerable<SubjectByGradeParent> BindGradesForSubject(long ACD_ID)
        {
            var uri = API.SubjectSetting.BindGradesForSubject(_path, ACD_ID);
            IEnumerable<SubjectByGradeParent> subjectMasterList = new List<SubjectByGradeParent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjectMasterList = EntityMapper<string, IEnumerable<SubjectByGradeParent>>.MapFromJson(jsonDataProviders);
            }
            return subjectMasterList;
        }

        public IEnumerable<SubjectByGradeChild> BindSubjectsByGrade(long ACD_ID, string GRD_ID, int STM_ID, int SBG_ID = 0)
        {
            var uri = API.SubjectSetting.BindSubjectsByGrade(_path, ACD_ID, GRD_ID, STM_ID, SBG_ID);
            IEnumerable<SubjectByGradeChild> subjectMasterList = new List<SubjectByGradeChild>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjectMasterList = EntityMapper<string, IEnumerable<SubjectByGradeChild>>.MapFromJson(jsonDataProviders);
            }
            return subjectMasterList;
        }
        #region Subject Grade Entry

        public IEnumerable<ParentSubject> GetParentSubjects(long ACD_ID, int STM_ID, string GRD_ID)
        {
            var uri = API.SubjectSetting.GetParentSubjects(_path, ACD_ID, STM_ID, GRD_ID);
            IEnumerable<ParentSubject> ParentSubjectList = new List<ParentSubject>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                ParentSubjectList = EntityMapper<string, IEnumerable<ParentSubject>>.MapFromJson(jsonDataProviders);
            }
            return ParentSubjectList;
        }

        public IEnumerable<SubjectByGradeParent> GetGradeForSubjectCopy(long ACD_ID)
        {
            var uri = API.SubjectSetting.GetGradeForSubjectCopy(_path, ACD_ID);
            IEnumerable<SubjectByGradeParent> SubjectByGradeParentList = new List<SubjectByGradeParent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                SubjectByGradeParentList = EntityMapper<string, IEnumerable<SubjectByGradeParent>>.MapFromJson(jsonDataProviders);
            }
            return SubjectByGradeParentList;
        }

        public IEnumerable<FromStream> GetStreamForSubjectCopy(long ACD_ID, string GRD_ID)
        {
            var uri = API.SubjectSetting.GetStreamForSubjectCopy(_path, ACD_ID, GRD_ID);
            IEnumerable<FromStream> FromStreamList = new List<FromStream>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                FromStreamList = EntityMapper<string, IEnumerable<FromStream>>.MapFromJson(jsonDataProviders);
            }
            return FromStreamList;
        }

        public bool AddOptionName(long schoolId, string optionName)
        {
            var uri = API.SubjectSetting.AddOptionName(_path, schoolId, optionName);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return true;
            }
            return false;
        }

        public string SubjectGrade(List<SubjectByGradeEntry> gradeEntries)
        {
            //var uri = API.SubjectSetting.SubjectGrade(_path);
            var json = JsonConvert.SerializeObject(gradeEntries);
            //var content = new StringContent(json, Encoding.UTF8, "application/json");
            //HttpResponseMessage response = _client.PostAsync(uri, content).Result;

            HttpResponseMessage response = _client.PostAsJsonAsync(API.SubjectSetting.SubjectGrade(_path), gradeEntries).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return jsonDataProviders;
            }

            return "Error";
        }
        public string SubjectGradeDelete(int subjectGradeId)
        {
            HttpResponseMessage response = _client.GetAsync(API.SubjectSetting.SubjectGradeDelete(_path, subjectGradeId)).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return jsonDataProviders;
            }

            return "Error";
        }

        #endregion


    }
}