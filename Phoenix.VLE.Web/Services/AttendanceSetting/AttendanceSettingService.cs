﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System.Text;
using Phoenix.VLE.Web.Models;
namespace Phoenix.VLE.Web.Services
{
    public class AttendanceSettingService : IAttendanceSettingService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/AttendanceSetting";
        #endregion

        public AttendanceSettingService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        public IEnumerable<GradeDetails> GetGradeDetails()
        {
            var uri = API.GetGradeDetails(_path);
            IEnumerable<GradeDetails> Details = new List<GradeDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Details = EntityMapper<string, IEnumerable<GradeDetails>>.MapFromJson(jsonDataProviders);
            }
            return Details;
        }

        #region Attendance Calendar
        public IEnumerable<GradeAndSection> GetGradeAndSectionList(long SchoolId)
        {
            var uri = API.AttendanceSetting.GetGradeAndSectionList(_path, SchoolId);
            IEnumerable<GradeAndSection> list = new List<GradeAndSection>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<GradeAndSection>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }
        public SchoolWeekEnd GetSchoolWeekEnd(long BSU_ID)
        {
            var uri = API.AttendanceSetting.GetSchoolWeekEnd(_path, BSU_ID);
            SchoolWeekEnd objSchoolWeekEnd = new SchoolWeekEnd();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objSchoolWeekEnd = EntityMapper<string, SchoolWeekEnd>.MapFromJson(jsonDataProviders);
            }
            return objSchoolWeekEnd;
        }
        public bool SaveCalendarEvent(AttendanceCalendar attendanceCalendar, string DATAMODE)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.AttendanceSetting.SaveCalendarEvent(_path, DATAMODE), attendanceCalendar).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<AttendanceCalendar> GetCalendarDetail(long SchoolId,long SCH_ID = 0, bool IsListView = false)
        {
            var uri = API.AttendanceSetting.GetCalendarDetail(_path, SchoolId, SCH_ID, IsListView);
            IEnumerable<AttendanceCalendar> list = new List<AttendanceCalendar>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<AttendanceCalendar>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }
        public AcademicYearDetail GetAcademicYearDetail(long SchoolId)
        {
            var uri = API.AttendanceSetting.GetAcademicYearDetail(_path, SchoolId);
            AcademicYearDetail modelObj = new AcademicYearDetail();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                modelObj = EntityMapper<string, AcademicYearDetail>.MapFromJson(jsonDataProviders);
            }
            return modelObj;
        }
        #endregion Attendance Calendar


        #region Parameter Setting
        public bool SaveParameterSetting(ParameterSetting parameterSetting, string DATAMODE)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.AttendanceSetting.SaveParameterSetting(_path, DATAMODE), parameterSetting).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<ParameterSetting> GetParameterSettingList(long BSU_ID)
        {
            var uri = API.AttendanceSetting.GetParameterSettingList(_path, BSU_ID);
            IEnumerable<ParameterSetting> list = new List<ParameterSetting>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<ParameterSetting>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }
        #endregion
        #region Leave approval permission
        public IEnumerable<LeaveApprovalPermissionModel> GetLeaveApprovalPermission(long ACD_ID, long schoolId, int divisionId)
        {
            var uri = API.AttendanceSetting.GetLeaveApprovalPermission(_path, ACD_ID, schoolId, divisionId);
            IEnumerable<LeaveApprovalPermissionModel> leaveApprovalObject = new List<LeaveApprovalPermissionModel>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                leaveApprovalObject = EntityMapper<string, IEnumerable<LeaveApprovalPermissionModel>>.MapFromJson(jsonDataProviders);
            }
            return leaveApprovalObject;
        }

        public int LeaveApprovalPermissionCU(LeaveApprovalPermissionModel leaveApproval)
        {
            var uri = API.AttendanceSetting.LeaveApprovalPermissionCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, leaveApproval).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt32(jsonDataProviders);
            }
            return 0;
        }
        #endregion
        #region Att type
        public IEnumerable<AttendanceType> GetAttendanceType(long BSU_ID)
        {
            var uri = API.AttendanceSetting.GetAttendanceType(_path, BSU_ID);
            IEnumerable<AttendanceType> attendances = new List<AttendanceType>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<AttendanceType>>.MapFromJson(jsonDataProviders);
            }
            return attendances;
        }
        public bool SaveAttendanceType(AttendanceType AttendanceType, string DATAMODE)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.AttendanceSetting.SaveAttendanceType(_path, DATAMODE), AttendanceType).Result;
            return response.IsSuccessStatusCode;
        }
        #endregion
        #region AttendancePeriod 

        public IEnumerable<AttendacePeriodModel> GetAttendancePeriodList(long gradeId)
        {
            var uri = API.AttendanceSetting.GetAttendancePeriodList(_path, gradeId);
            IEnumerable<AttendacePeriodModel> list = new List<AttendacePeriodModel>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<AttendacePeriodModel>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public IEnumerable<AttendanceType> GetAttendanceTypeListBYId(long AttendanceConfigurationTypeID)
        {
            var uri = API.AttendanceSetting.GetAttendancePeriodList(_path, AttendanceConfigurationTypeID);
            IEnumerable<AttendanceType> list = new List<AttendanceType>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<AttendanceType>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }


        public bool AddUpdateAttendancePeriod(long schoolId, long academicId, string gradeId, long CreatedBy, List<AttendacePeriodModel> objAttendancePeriod)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.AttendanceSetting.AddUpdateAttendancePeriod(_path, schoolId, academicId, gradeId, CreatedBy), objAttendancePeriod).Result;
            return response.IsSuccessStatusCode;
        }
        #endregion
    }
}