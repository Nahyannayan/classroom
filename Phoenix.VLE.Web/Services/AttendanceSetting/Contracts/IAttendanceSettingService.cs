﻿using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IAttendanceSettingService
    {
        IEnumerable<GradeDetails> GetGradeDetails();

        #region Attendance Calendar
        IEnumerable<GradeAndSection> GetGradeAndSectionList(long SchoolId);
        SchoolWeekEnd GetSchoolWeekEnd(long BSU_ID);
        bool SaveCalendarEvent(AttendanceCalendar attendanceCalendar, string DATAMODE);
        IEnumerable<AttendanceCalendar> GetCalendarDetail(long SchoolId, long SCH_ID = 0, bool IsListView = false);
        AcademicYearDetail GetAcademicYearDetail(long SchoolId);
        #endregion Attendance Calendar

        #region Parameter Setting
        bool SaveParameterSetting(ParameterSetting parameterSetting, string DATAMODE);
        IEnumerable<ParameterSetting> GetParameterSettingList(long BSU_ID);
        #endregion
        #region Leave approval permission
        IEnumerable<LeaveApprovalPermissionModel> GetLeaveApprovalPermission(long ACD_ID, long schoolId, int divisionId);
        int LeaveApprovalPermissionCU(LeaveApprovalPermissionModel leaveApproval);
        #endregion

        #region Att Type
        IEnumerable<AttendanceType> GetAttendanceType(long BSU_ID);
        bool SaveAttendanceType(AttendanceType AttendanceType, string DATAMODE);
        #endregion

        #region Attendance Period
        IEnumerable<AttendacePeriodModel> GetAttendancePeriodList(long gradeId);
        IEnumerable<AttendanceType> GetAttendanceTypeListBYId(long AttendanceConfigurationTypeID);
        
        bool AddUpdateAttendancePeriod(long schoolId, long academicId, string gradeId, long CreatedBy, List<AttendacePeriodModel> objAttendancePeriod);


        #endregion

    }
}
