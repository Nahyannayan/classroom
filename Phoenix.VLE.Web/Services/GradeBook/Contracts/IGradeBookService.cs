﻿using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IGradeBookService
    {
        TeacherGradeBook GetTeacherGradeBook(long SchoolGroupId, long teacherId);
        long TeacherGradebookCreateNewRow(TeacherInternalAssessment createNewRow);
        bool InternalAssessmentCU(List<InternalAssessmentScore> assessmentScore);
        IEnumerable<GradeBookAssignmentQuiz> GetAssignmentQuizScore(long schoolGroupId, bool isAssignment);
        TeacherGradeBook GetInternalAssessments(long schoolGroupId, long teacherId);
        IEnumerable<StandardExamination> GetStandardExaminations(long schoolGroupId, int assessmentTypeId);
        GradebookProgressTracker GetProgressTracker(long schoolGroupId, DateTime? startDate = null, DateTime? endDate = null);
        TeacherInternalAssessment GetSubInternalAssessment(long internalAssessmentId);
        IEnumerable<InternalAssessmentScore> GetInternalAssessmentScoreByAssessmentId(long internalAssessmentId);
    }
}
