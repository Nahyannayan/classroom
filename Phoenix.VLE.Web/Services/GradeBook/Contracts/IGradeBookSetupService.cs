﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IGradeBookSetupService
    {
        long SaveGradeBookForm(GradeBook gradeBook); 
        bool DeleteGradeBookDetail(GradeBook gradeBook);
        GradeBook GetGradeBookDetail(long gradeBookId);
        IEnumerable<GradeBook> GetGradeBookDetailPagination(int curriculumId, long schoolId, int pageNum, int pageSize, string searchString, string GradeIds, string CourseIds, string sortBy);
        IEnumerable<GradeAndCourse> GetGradeAndCourseList(int curriculumId, long SchoolId);
        bool GradebookFormulaCU(GradebookFormula gradebookFormula);
        IEnumerable<GradebookFormula> GetGradebookFormulas(long SchoolId);
        GradebookFormula GetGradebookFormulaDetailById(long FormulaId);
        IEnumerable<GradebookGrade> GetGradeListExceptGradebookGrade(long SchoolId, long GradebookId=0);
        long ProcessingRuleSetupCU(GradebookRuleSetup gradebookRuleSetup);
        bool DeleteRuleProcessingSetup(long ruleSetupId);
        IEnumerable<GradebookRuleSetup> GetGradebookRuleSetups(string assessmentIds);
        IEnumerable<GradingTemplateItem> GetGradingTemplatesByIds(string ids);
        IEnumerable<GradeBookGradeDetail> ValidateGradeAndCourse(string courseIds, string gradeIds);
    }
}
