﻿using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class GradeBookSetupService : IGradeBookSetupService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/GradeBookSetup";
        #endregion

        public GradeBookSetupService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        public GradeBook GetGradeBookDetail(long gradeBookId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.GetGradeBookDetail(_path, gradeBookId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<GradeBook>() : default(GradeBook);
        }

        public long SaveGradeBookForm(GradeBook gradeBook)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.GradeBookSetup.SaveGradeBookForm(_path), gradeBook).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToLong() : 0;
        }
        public bool DeleteGradeBookDetail(GradeBook gradeBook)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.GradeBookSetup.DeleteGradeBookDetail(_path), gradeBook).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        public IEnumerable<GradeBook> GetGradeBookDetailPagination(int curriculumId, long schoolId, int pageNum, int pageSize, string searchString, string GradeIds, string CourseIds, string sortBy)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.GetGradeBookDetailPagination(_path,curriculumId, schoolId, pageNum, pageSize, searchString, GradeIds, CourseIds, sortBy)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GradeBook>>() : default(IEnumerable<GradeBook>);
        }
        public IEnumerable<GradeAndCourse> GetGradeAndCourseList(int curriculumId, long SchoolId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.GetGradeAndCourseList(_path,curriculumId, SchoolId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GradeAndCourse>>() : default(IEnumerable<GradeAndCourse>);
        }

        public bool GradebookFormulaCU(GradebookFormula gradebookFormula)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.GradeBookSetup.GradebookFormulaCU(_path), gradebookFormula).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }

        public IEnumerable<GradebookFormula> GetGradebookFormulas(long SchoolId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.GetGradebookFormulas(_path, SchoolId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GradebookFormula>>() : new List<GradebookFormula>();
        }
        public GradebookFormula GetGradebookFormulaDetailById(long FormulaId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.GetGradebookFormulaDetailById(_path, FormulaId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<GradebookFormula>() : new GradebookFormula();
        }
        public IEnumerable<GradebookGrade> GetGradeListExceptGradebookGrade(long SchoolId, long GradebookId = 0)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.GetGradeListExceptGradebookGrade(_path, SchoolId, GradebookId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GradebookGrade>>() : default(IEnumerable<GradebookGrade>);
        }

        public long ProcessingRuleSetupCU(GradebookRuleSetup gradebookRuleSetup)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.GradeBookSetup.ProcessingRuleSetupCU(_path), gradebookRuleSetup).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToString().Replace("\"","").ToLong() : 0;
        }

        public bool DeleteRuleProcessingSetup(long ruleSetupId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.DeleteRuleProcessingSetup(_path, ruleSetupId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }

        public IEnumerable<GradebookRuleSetup> GetGradebookRuleSetups(string assessmentIds)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.GetGradebookRuleSetups(_path, assessmentIds)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GradebookRuleSetup>>() : new List<GradebookRuleSetup>();
        }
        public IEnumerable<GradingTemplateItem> GetGradingTemplatesByIds(string ids)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.GetGradingTemplatesByIds(_path, ids)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GradingTemplateItem>>() : new List<GradingTemplateItem>();
        }
        public IEnumerable<GradeBookGradeDetail> ValidateGradeAndCourse(string courseIds, string gradeIds)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBookSetup.ValidateGradeAndCourse(_path, courseIds, gradeIds)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GradeBookGradeDetail>>() : new List<GradeBookGradeDetail>();
        }
    }
}