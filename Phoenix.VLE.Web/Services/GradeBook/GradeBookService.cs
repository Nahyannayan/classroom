﻿using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class GradeBookService : IGradeBookService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/GradeBook";
        #endregion

        public GradeBookService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        public TeacherGradeBook GetTeacherGradeBook(long SchoolGroupId, long teacherId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBook.GetTeacherGradeBook(_path, SchoolGroupId,teacherId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<TeacherGradeBook>() : new TeacherGradeBook();
        }
        public long TeacherGradebookCreateNewRow(TeacherInternalAssessment createNewRow)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.GradeBook.CreateNewRow(_path), createNewRow).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToLong() : 0;
        }
        public bool InternalAssessmentCU(List<InternalAssessmentScore> assessmentScore)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.GradeBook.InternalAssessmentCU(_path), assessmentScore).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }

        public IEnumerable<GradeBookAssignmentQuiz> GetAssignmentQuizScore(long schoolGroupId, bool isAssignment)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBook.GetAssignmentQuizScore(_path, schoolGroupId, isAssignment)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GradeBookAssignmentQuiz>>() : new List<GradeBookAssignmentQuiz>();
        }

        public TeacherGradeBook GetInternalAssessments(long schoolGroupId, long teacherId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBook.GetInternalAssessments(_path, schoolGroupId, teacherId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<TeacherGradeBook>() : new TeacherGradeBook();
        }

        public IEnumerable<StandardExamination> GetStandardExaminations(long schoolGroupId, int assessmentTypeId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBook.GetStandardExaminations(_path, schoolGroupId, assessmentTypeId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<StandardExamination>>() : new List<StandardExamination>();
        }

        public GradebookProgressTracker GetProgressTracker(long schoolGroupId, DateTime? startDate = null, DateTime? endDate = null)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBook.GetProgressTracker(_path, schoolGroupId, startDate, endDate)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<GradebookProgressTracker>() : new GradebookProgressTracker();
        }

        public TeacherInternalAssessment GetSubInternalAssessment(long internalAssessmentId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBook.GetSubInternalAssessment(_path, internalAssessmentId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<TeacherInternalAssessment>() : new TeacherInternalAssessment();
        }
        public IEnumerable<InternalAssessmentScore> GetInternalAssessmentScoreByAssessmentId(long internalAssessmentId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.GradeBook.GetInternalAssessmentScoreByAssessmentId(_path, internalAssessmentId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<InternalAssessmentScore>>() : new List<InternalAssessmentScore>();
        }
    }
}