﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class SuggestionService : ISuggestionService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/studentsuggestion";
        #endregion

        public SuggestionService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public int UpdateSuggestionCategoryData(SuggestionEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new Suggestion();
            EntityMapper<SuggestionEdit, Suggestion>.Map(model, sourceModel);
            sourceModel.UserId = (int)SessionHelper.CurrentSession.Id;
            if (model.IsAddMode)
                uri = API.Suggestion.InsertSuggestion(_path);
            else
                uri = API.Suggestion.UpdateSuggestion(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public IEnumerable<Suggestion> getStudentSuggestionByUserId()
        {
            int userId = (int)SessionHelper.CurrentSession.Id;
            var uri = API.Suggestion.getStudentSuggestionByUserId(_path, userId);
            IEnumerable<Suggestion> suggestions = new List<Suggestion>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                suggestions = EntityMapper<string, IEnumerable<Suggestion>>.MapFromJson(jsonDataProviders);
            }
            return suggestions;
        }
        public IEnumerable<Suggestion> getStudentSuggestionBySchoolId()
        {
            int schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var uri = API.Suggestion.getStudentSuggestionBySchoolId(_path, schoolId);
            IEnumerable<Suggestion> suggestions = new List<Suggestion>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                suggestions = EntityMapper<string, IEnumerable<Suggestion>>.MapFromJson(jsonDataProviders);
            }
            return suggestions;
        }
        public IEnumerable<Suggestion> PaginateStudentSuggestionByUserId(int pageNumber, int pageSize, string SearchString = "", string sortBy="")
        {
            var result = new List<Suggestion>();
            int userId = SessionHelper.CurrentSession.IsStudent()?(int)SessionHelper.CurrentSession.Id:(int)SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            var uri = API.Suggestion.PaginateStudentSuggestionByUserId(_path, userId, pageNumber, pageSize, SearchString, sortBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Suggestion>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<Suggestion> PaginateStudentSuggestionBySchoolId(int pageNumber, int pageSize, string SearchString = "", string sortBy = "")
        {
            var result = new List<Suggestion>();
            int schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var uri = API.Suggestion.PaginateStudentSuggestionBySchoolId(_path, schoolId, pageNumber, pageSize, SearchString, sortBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Suggestion>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public Suggestion getStudentSuggestionById(int suggestionId)
        {
            var uri = API.Suggestion.getStudentSuggestionById(_path, suggestionId);
            Suggestion suggestions = new Suggestion();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                suggestions = EntityMapper<string, Suggestion>.MapFromJson(jsonDataProviders);
            }
            return suggestions;
        }

        public Suggestion GetStudentSuggestionByIdAndUserId(int suggestionId,long userId)
        {
            var uri = API.Suggestion.GetStudentSuggestionByIdAndUserId(_path, suggestionId, userId);
            Suggestion suggestions = new Suggestion();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                suggestions = EntityMapper<string, Suggestion>.MapFromJson(jsonDataProviders);
            }
            return suggestions;
        }

        public int DeleteSuggestionData(int id)
        {
            var uri = API.Suggestion.DeleteSuggestionData(_path, id, SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public bool AddUpdateThinkBoxUser(ThinkBoxEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new ThinkBox();
            EntityMapper<ThinkBoxEdit, ThinkBox>.Map(model, sourceModel);
            uri = API.Suggestion.AddUpdateThinkBoxUser(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
           // result = Convert.ToInt32(jsonDataProviders);
            return true;
        }
     
        public IEnumerable<ThinkBox> getThinkBoxUsersBySchoolId(long schoolId)
        {
            var result = new List<ThinkBox>();
            var uri = API.Suggestion.getThinkBoxUsersBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<ThinkBox>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool DeleteUserFromThinkBox(long userId)
        {
            long schoolId = SessionHelper.CurrentSession.SchoolId;
            var uri = API.Suggestion.DeleteUserFromThinkBox(_path, userId, schoolId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public bool HasThinkBoxUserPermission()
        {
            long userId = SessionHelper.CurrentSession.Id;
            var uri = API.Suggestion.HasThinkBoxUserPermission(_path, userId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public byte[] ExportStudentSuggestions_CSVFile(string path)
        {
            try
            {
                var writer = new StreamWriter(path);
                string isCorrectAnswer = string.Empty;
                IEnumerable<Suggestion> suggestionsList = (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent() ? getStudentSuggestionByUserId() : getStudentSuggestionBySchoolId());
                
                writer.WriteLine("Suggestion Id, Title, Description, Category, Student Name, Created On");

                foreach (var item in suggestionsList)
                {
                    if (item.Title.Contains("\""))
                    {
                        item.Title = item.Title.Replace("\"", "\"\"");
                    }
                    if (item.Title.Contains(","))
                    {
                        item.Title = String.Format("\"{0}\"", item.Title);
                    }
                    if (item.Title.Contains(System.Environment.NewLine))
                    {
                        item.Title = String.Format("\"{0}\"", item.Title);
                    }

                    if (item.Description.Contains("\""))
                    {
                        item.Description = item.Description.Replace("\"", "\"\"");
                    }
                    if (item.Description.Contains(","))
                    {
                        item.Description = String.Format("\"{0}\"", item.Description);
                    }
                    if (item.Description.Contains(System.Environment.NewLine))
                    {
                        item.Description = String.Format("\"{0}\"", item.Description);
                    }

                    writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", item.SuggestionId, item.Title, item.Description, item.Type, item.UserName, item.CreatedOn));
                }

                writer.Flush();
                writer.Close();
                writer.Dispose();

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}