﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public interface ISuggestionService
    {
        int UpdateSuggestionCategoryData(SuggestionEdit model);
        IEnumerable<Suggestion> getStudentSuggestionByUserId();
        IEnumerable<Suggestion> getStudentSuggestionBySchoolId();
        Suggestion getStudentSuggestionById(int suggestionId);
        int DeleteSuggestionData(int id);
        bool AddUpdateThinkBoxUser(ThinkBoxEdit model);
        bool HasThinkBoxUserPermission();
        IEnumerable<ThinkBox> getThinkBoxUsersBySchoolId(long schoolId);
        bool DeleteUserFromThinkBox(long userId);
        IEnumerable<Suggestion> PaginateStudentSuggestionByUserId(int pageNumber, int pageSize, string SearchString = "",string sortBy="");
        IEnumerable<Suggestion> PaginateStudentSuggestionBySchoolId(int pageNumber, int pageSize, string SearchString = "", string sortBy = "");
        byte[] ExportStudentSuggestions_CSVFile(string path);
        Suggestion GetStudentSuggestionByIdAndUserId(int suggestionId, long userId);
    }
}