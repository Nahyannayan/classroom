﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.VLE.Web.Models.EditModels;
namespace Phoenix.VLE.Web.Services
{
    public interface IRSSFeedService
    {
        IEnumerable<RSSFeed> GetAllRSSFeed();
        bool AddUpdateRSSFeed(RSSFeedEdit objRSSFeed);

        RSSFeed GetRSSFeedById(int Id);
    }
}
