﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services;

namespace Phoenix.VLE.Web.Services
{
    public class RSSFeedService : IRSSFeedService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/RSSFeed";
        #endregion
        public RSSFeedService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool AddUpdateRSSFeed(RSSFeedEdit objRSSFeed)
        {
            var uri = string.Empty;
            uri = API.RssFeed.AddUpdateRssFeed(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, objRSSFeed).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<RSSFeed> GetAllRSSFeed()
        {
            var uri = API.RssFeed.GetRSSFeedList(_path);
            IEnumerable<RSSFeed> rssFeed = new List<RSSFeed>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                rssFeed = EntityMapper<string, IEnumerable<RSSFeed>>.MapFromJson(jsonDataProviders);
            }
            return rssFeed;
        }

        public RSSFeed GetRSSFeedById(int Id)
        {
            var user = new RSSFeed();
            var uri = API.RssFeed.GetRSSFeedId(_path, Id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                user = EntityMapper<string, RSSFeed>.MapFromJson(jsonDataProviders);
            }
            return user;
        }
    }
}