﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class AssessmentServiceReport : IAssessmentReport
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        private IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        readonly string GET_ACADEMICREPORT = ConfigurationManager.AppSettings["GET_ACADEMICREPORT"];
        readonly string GET_REPORTCARD = ConfigurationManager.AppSettings["GET_REPORTCARD"];
        private ILoggerClient _loggerClient;
        public AssessmentServiceReport(IPhoenixAPIParentCornerService PhoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));               
            }
            _loggerClient = LoggerClient.Instance;
            _phoenixAPIParentCornerService = PhoenixAPIParentCornerService;
        }
        #endregion
        public IEnumerable<PdfInfoModel> GetPdfListReport(string StudenntNumber, string SelectedYearId)
        {
            string json = "";
            List<PdfInfoModel> pdfInfoList = new List<PdfInfoModel>();
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;

            try
            {
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GET_ACADEMICREPORT);
                httpRequestMessage.Headers.Add("ACDID", SelectedYearId);
                httpRequestMessage.Headers.Add("STUID", StudenntNumber);
                httpRequestMessage.Headers.Add("FILTER", "FILTER");
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        var data = new JavaScriptSerializer().Deserialize<List<PdfInfoModel>>(json);

                        foreach (var item in data)
                        {
                            PdfInfoModel pm = new PdfInfoModel();
                            pm.rpF_ID = item.rpF_ID;
                            pm.rpF_DESCR = item.rpF_DESCR;
                            string dte = item.rpP_RELEASEDATE;
                            DateTime dt = Convert.ToDateTime(dte);
                            pm.rpP_RELEASEDATE = dt.ToString("dd MMMM yyyy");
                            pm.ACD_ID = SelectedYearId;
                            pm.StudentNumber = StudenntNumber;
                            pdfInfoList.Add(pm);
                        }
                    }
                }
                return pdfInfoList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in AssessmentServiceReport.GetPdfListReport()" + ex.Message);
                return pdfInfoList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, pdfInfoList, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            return JsonDeserilize;
        }

        public IEnumerable<PdfUrlModel> RequestToPdfView(string ACD_ID, string rpF_ID, string UserName, string StudentNumber)
        {
            string json = "";
            List<PdfUrlModel> pdfUrlModel = new List<PdfUrlModel>();
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;

            try
            {
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GET_REPORTCARD);
                httpRequestMessage.Headers.Add("ACDID", ACD_ID);
                httpRequestMessage.Headers.Add("STUID", StudentNumber);
                httpRequestMessage.Headers.Add("RPFID", rpF_ID);
                httpRequestMessage.Headers.Add("OLUNAME", UserName);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        pdfUrlModel = new JavaScriptSerializer().Deserialize<List<PdfUrlModel>>(json);
                    }
                }
                return pdfUrlModel;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in AssessmentServiceReport.RequestToPdfView()" + ex.Message);
                return pdfUrlModel;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, pdfUrlModel, response, SessionHelper.CurrentSession.UserName);
            }
        }
    }
}