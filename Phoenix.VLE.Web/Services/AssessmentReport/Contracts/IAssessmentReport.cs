﻿using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IAssessmentReport
    {
        IEnumerable<PdfInfoModel>GetPdfListReport(string studentId, string username);
        IEnumerable<PdfUrlModel> RequestToPdfView(string ACD_ID, string rpF_ID,string UserName,string StudentNumber);
    }
}
