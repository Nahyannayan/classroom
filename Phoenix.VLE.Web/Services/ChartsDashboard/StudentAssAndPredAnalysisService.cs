﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class StudentAssAndPredAnalysisService:IStudentAssAndPredAnalysisService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });

        // TODO: put this is a base class if needed
        readonly string _baseUrl = Constants.PhoenixAPIUrl;

        readonly string _path = "api/v1/chartsdashboard";

        //readonly string _configurationPath = "api/v1/configuration";
        #endregion

        public StudentAssAndPredAnalysisService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods

        public List<FilStudent> GetStudentFilters(long bsuid,string classId,string assessment,string academicYear)
        {
            var uri = API.ChartsDashboard.GetAllStudents(_path, bsuid, classId, assessment, academicYear);
            var studentAnalysis = new List<FilStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonData = response.Content.ReadAsStringAsync().Result;
                studentAnalysis = JsonConvert.DeserializeObject<List<FilStudent>>(jsonData);
            }
            return studentAnalysis;
        }

        public StudentChartRes GetStudentAssessmentPredictionCharts(long bsuid, string classId, string assessment, string academicYear, string studentId)
        {
            var uri = API.ChartsDashboard.GetStudentAssessmentPrediction(_path, bsuid, classId,assessment,academicYear,studentId);
            var studentChartRes = new StudentChartRes();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonData = response.Content.ReadAsStringAsync().Result;
                studentChartRes = JsonConvert.DeserializeObject<StudentChartRes>(jsonData);
            }
            return studentChartRes;
        }

        public List<AssessmentDataByStudent> GetAllAssessmentData(long bsuid, string classId, string assessment, string academicYear, string studentId)
        {
            var uri = API.ChartsDashboard.GetAllAssessment(_path, bsuid, classId, assessment, academicYear, studentId);
            var assessmentDataByStudent = new List<AssessmentDataByStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonData = response.Content.ReadAsStringAsync().Result;
                assessmentDataByStudent = JsonConvert.DeserializeObject<List<AssessmentDataByStudent>>(jsonData);
            }
            return assessmentDataByStudent;
        }

        public StudentFilterRes GetStudentProgressFilter(long bsuid)
        {
            var uri = API.ChartsDashboard.GetStudentProgress(_path, bsuid);
            var studentFilter = new StudentFilterRes();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonData = response.Content.ReadAsStringAsync().Result;
                studentFilter = JsonConvert.DeserializeObject<StudentFilterRes>(jsonData);
            }
            return studentFilter;
        }
        #endregion
    }
}