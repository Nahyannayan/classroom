﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public interface IStudentAssAndPredAnalysisService
    {
        List<FilStudent> GetStudentFilters(long bsuid, string classId, string assessment, string academicYear);
        StudentChartRes GetStudentAssessmentPredictionCharts(long bsuid, string classId, string assessment, string academicYear, string studentId);
        List<AssessmentDataByStudent> GetAllAssessmentData(long bsuid, string classId, string assessment, string academicYear, string studentId);
        StudentFilterRes GetStudentProgressFilter(long bsuid);
    }
}