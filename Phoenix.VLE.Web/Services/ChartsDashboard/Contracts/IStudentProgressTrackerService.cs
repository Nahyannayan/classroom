﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public interface IStudentProgressTrackerService
    {
        FilterRes GetStudentProgressFilter(long bsuid);
        ChartRes GetChart(long bsuid, string academicYear, string subject, string classId, string assessment, string teacher);
    }
}