﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class StudentProgressTrackerService: IStudentProgressTrackerService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });

        // TODO: put this is a base class if needed
        readonly string _baseUrl = Constants.PhoenixAPIUrl;

        readonly string _path = "api/v1/chartsdashboard";

        //readonly string _configurationPath = "api/v1/configuration";
        #endregion

        public StudentProgressTrackerService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }


        #region Methods

        public FilterRes GetStudentProgressFilter(long bsuid=131001)
        {
            var uri = API.ChartsDashboard.GetStudentProgress(_path, bsuid);
            var studentProgress = new FilterRes();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonData = response.Content.ReadAsStringAsync().Result;
                studentProgress = JsonConvert.DeserializeObject<FilterRes>(jsonData);
            }
            return studentProgress;
        }

        public ChartRes GetChart(long bsuid,string academicYear,string subject,string classId,string assessment, string teacher)
        {
            var uri = API.ChartsDashboard.GetChartAndPercentage(_path, bsuid,academicYear,subject,classId,assessment,teacher);
            var chartRes = new ChartRes();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonData = response.Content.ReadAsStringAsync().Result;
                chartRes = JsonConvert.DeserializeObject<ChartRes>(jsonData);
            }
            return chartRes;
        }
        #endregion
    }
}