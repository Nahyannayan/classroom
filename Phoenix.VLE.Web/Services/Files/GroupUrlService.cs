﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class GroupUrlService : IGroupUrlService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/groupurl";
        #endregion
        public GroupUrlService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public IEnumerable<GroupUrl> GetAll()
        {
            throw new NotImplementedException();
        }


        public int Insert(GroupUrl entity)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.GroupUrl.AddGroupUrl(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int Update(GroupUrl entityToUpdate)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.GroupUrl.UpdateGroupUrl(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        IEnumerable<GroupUrl> IGroupUrlService.GetAll()
        {
            throw new NotImplementedException();
        }

      public  GroupUrl GetById(int id)
        {
            var result = new GroupUrl();
            var uri = API.GroupUrl.GetGroupUrlById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, GroupUrl>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<GroupUrl> GetGroupUrlListByGroupId(int groupId)
        {
            var result = new List<GroupUrl>();
            var uri = API.GroupUrl.GetGroupUrlListByGroupId(_path,groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupUrl>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }


    }
}