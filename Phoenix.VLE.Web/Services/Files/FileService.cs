﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class FileService : IFileService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/files";
        #endregion
        public FileService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);


            }
        }

        public bool Delete(int id, long userId)
        {
            var uri = API.Files.DeleteFile(_path, id, userId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<File> GetAll()
        {
            var result = new List<File>();
            var uri = API.Files.GetFiles(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<File>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public File GetById(int id)
        {
            var result = new File();
            var uri = API.Files.GetFileById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, File>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public DateTime GetServerDateTime()
        {
            DateTime result= DateTime.Now;
            var uri = API.Common.GetServerDateTime(_path);
            //HttpResponseMessage response = _client.PostAsJsonAsync(uri).Result;
            HttpResponseMessage responseMessage = _client.GetAsync(API.Common.GetServerDateTime(_path)).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = responseMessage.Content.ReadAsStringAsync().Result;
                result = Convert.ToDateTime(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<FileManagementModule> GetFileManagementModules()
        {
            var result = new List<FileManagementModule>();
            var uri = API.Files.GetFilemanagementModules(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<FileManagementModule>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<File> GetFilesByFolderId(int folderId, bool? isActive)
        {
            var result = new List<File>();
            var uri = API.Files.GetFilesByFolderId(_path, folderId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<File>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<File> GetFilesByModuleId(int schoolId, int moduleId, bool? isActive)
        {
            var result = new List<File>();
            var uri = API.Files.GetFilesByModuleId(_path, schoolId, moduleId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<File>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<File> GetFilesBySectionId(int sectionId, int moduleId, bool? isActive)
        {
            var result = new List<File>();
            var uri = API.Files.GetFilesBySectionId(_path, sectionId, moduleId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<File>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<File> GetFilesBySchoolId(int schoolId, bool? isActive)
        {
            var result = new List<File>();
            var uri = API.Files.GetFilesBySchoolId(_path, schoolId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<File>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<File> GetFilesByUserId(int userId, int moduleId, bool? isActive)
        {
            var result = new List<File>();
            var uri = API.Files.GetFilesByUserId(_path, userId, moduleId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<File>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<VLEFileType> GetFileTypes(string DocumentType = "")
        {
            var result = new List<VLEFileType>();
            var uri = API.Files.GetFileTypes(_path, DocumentType);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<VLEFileType>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool Insert(File entity)
        {
            var uri = string.Empty;
            uri = API.Files.AddFile(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            return response.IsSuccessStatusCode;
        }

        public bool Update(File entityToUpdate)
        {
            var uri = string.Empty;
            uri = API.Files.UpdateFile(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<StudentGroupsFiles> GetStudentGroupsFiles(long userId)
        {
            var result = new List<StudentGroupsFiles>();
            var uri = API.Files.GetStudentGroupsFiles(_path, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<StudentGroupsFiles>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public bool RenameFile(long fileId, string FileName, long updatedBy)
        {
            var uri = API.Files.RenameFile(_path, fileId, FileName, updatedBy);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;

        }

        public IEnumerable<File> GetAllGroupFiles(int id)
        {
            var result = new List<File>();
            var uri = API.Files.GetAllGroupFiles(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<File>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int ClearGroupFiles(long userId, int schoolGroupId)
        {
            int result = 0;
            var model = new SchoolGroup() { SchoolGroupId = schoolGroupId, CreatedById = userId };
            var uri = API.Files.ClearGroupFiles(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToInt32(jsonDataProviders);
            }
            return result;
        }

        #region Cloud Storage Files Save
        public bool SaveCloudFiles(IList<File> files)
        {
            var uri = API.Files.SaveCloudFiles(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, files).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public async Task<FileExplorer> GetGroupFileExplorer(int moduleId, long sectionId, long folderId, long studentUserId, bool isActive)
        {
            var result = new FileExplorer();
            var uri = API.Files.GetGroupFileExplorer(_path, moduleId, sectionId, folderId, studentUserId, isActive);
            HttpResponseMessage responseMessage = await _client.GetAsync(uri);
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = responseMessage.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, FileExplorer>.MapFromJson(jsonDataProviders);
            }

            return result;
        }
        #endregion

        #region Bulk Files CD
        public IEnumerable<File> GetFilesByIds(string Ids)
        {
            var result = new List<File>();
            var uri = API.Files.GetFilesByIds(_path, Ids);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<File>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public bool FilesFolderBulkDelete(MyFilesModel model)
        {
            var uri = API.Files.FilesFolderBulkDelete(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, model).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        public bool FilesFolderBulkCreate(MyFilesModel model)
        {
            var uri = API.Files.FilesFolderBulkCreate(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, model).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        #endregion
    }
}