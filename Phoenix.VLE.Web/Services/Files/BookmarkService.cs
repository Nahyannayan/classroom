﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class BookmarkService : IBookmarkService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/bookmark";
        #endregion
        public BookmarkService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public Bookmark GetBookmark(long id)
        {
            var bookmark = new Bookmark();
            var uri = API.Bookmarks.GetBookmark(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bookmark = EntityMapper<string, Bookmark>.MapFromJson(jsonDataProviders);
            }
            return bookmark;
        }
        public Bookmark GetSelectedSchoolGroupsPerBoookmark(int id)
        {
            var bookmark = new Bookmark();
            var uri = API.Bookmarks.GetBookmark(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bookmark = EntityMapper<string, Bookmark>.MapFromJson(jsonDataProviders);
            }
            return bookmark;
        }

        public bool AddUpdateBookmarkData(BookmarkEdit model)
        {
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new Bookmark();
            EntityMapper<BookmarkEdit, Bookmark>.Map(model, sourceModel);
            if (model.SchoolGroupIds != null)
            {
                sourceModel.SelectedSchoolGroupIds = string.Join(",", model.SchoolGroupIds);
            }

            if (model.IsAddMode)
                uri = API.Bookmarks.AddBookmark(_path);
            else
                uri = API.Bookmarks.UpdateBookmark(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }
        public bool DeleteBookmarkData(long bookmarkId)
        {
            bool result = false;
            var uri = string.Empty;
            uri = API.Bookmarks.DeleteBookmark(_path, bookmarkId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<Bookmark> GetBookmarks(long userId)
        {
            var uri = API.Bookmarks.GetBookmarks(_path, userId);
            IEnumerable<Bookmark> bookmarks = new List<Bookmark>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bookmarks = EntityMapper<string, IEnumerable<Bookmark>>.MapFromJson(jsonDataProviders);
            }
            return bookmarks;
        }

        public IEnumerable<SchoolGroup> GetBookmarkSchoolGroups(long bookmarkId)
        {
            var uri = API.Bookmarks.GetBookmarkSchoolGroups(_path, bookmarkId);
            IEnumerable<SchoolGroup> schoolGroups = new List<SchoolGroup>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schoolGroups = EntityMapper<string, IEnumerable<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return schoolGroups;
        }
    }
}