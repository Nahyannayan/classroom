﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class FolderService: IFolderService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/folders";
        #endregion
        public FolderService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool Delete(long id, long userId)
        {
            var uri = API.Folders.DeleteFolder(_path, id, userId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<Folder> GetAll()
        {
            var result = new List<Folder>();
            var uri = API.Folders.GetFolders(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Folder>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public Folder GetById(int id)
        {
            var result = new Folder();
            var uri = API.Folders.GetFolderById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, Folder>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Folder> GetFoldersByModuleId(int schoolId, int moduleId, bool? isActive)
        {
            var result = new List<Folder>();
            var uri = API.Folders.GetFoldersByModuleId(_path, schoolId,moduleId,isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Folder>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Folder> GetFoldersBySectionId(int sectionId, int moduleId, bool? isActive)
        {
            var result = new List<Folder>();
            var uri = API.Folders.GetFoldersBySectionId(_path, sectionId, moduleId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Folder>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Folder> GetFoldersByParentFolderId(int parentFolderId, int moduleId, bool? isActive)
        {
            var result = new List<Folder>();
            var uri = API.Folders.GetFoldersByParentFolderId(_path, parentFolderId, moduleId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Folder>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Folder> GetFoldersBySchoolId(int schoolId, bool? isActive)
        {
            var result = new List<Folder>();
            var uri = API.Folders.GetFoldersBySchoolId(_path, schoolId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Folder>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Folder> GetFoldersByUserId(int userId, int moduleId, bool? isActive)
        {
            var result = new List<Folder>();
            var uri = API.Folders.GetFoldersByUserId(_path, userId,moduleId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Folder>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<FolderTree> GetFolderTree(int folderId)
        {
            var result = new List<FolderTree>();
            var uri = API.Folders.GetFolderTree(_path, folderId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<FolderTree>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int Insert(Folder entity)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.Folders.AddFolder(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int Update(Folder entityToUpdate)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.Folders.UpdateFolder(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int GetGroupPermission(int userId, int groupId)
        {
            int result = 0;
            var uri = API.Folders.GetGroupPermission(_path, userId, groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToInt32(jsonDataProviders);
            }
            return result;
        }
    }
}