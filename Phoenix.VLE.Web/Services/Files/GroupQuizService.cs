﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class GroupQuizService : IGroupQuizService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/groupquiz";
        #endregion
        public GroupQuizService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public int Insert(GroupQuiz entity)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.GroupQuiz.AddGroupQuiz(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public IEnumerable<GroupQuiz> GetGroupQuizListByGroupId(int groupId)
        {
            var result = new List<GroupQuiz>();
            var uri = API.GroupQuiz.GetGroupQuizListByGroupId(_path, groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupQuiz>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<QuizEdit> GetQuizByCourse(string courseIds, int schoolId)
        {
            var uri = API.GroupQuiz.GetQuizByCourse(_path, courseIds, schoolId);
            IEnumerable<QuizEdit> lstQuiz = new List<QuizEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstQuiz = EntityMapper<string, IEnumerable<QuizEdit>>.MapFromJson(jsonDataProviders);
            }
            return lstQuiz;
        }
        public IEnumerable<GroupQuiz> GetGroupQuizListByGroupIdAndDateRange(int groupId, DateTime startDate, DateTime endDate)
        {
            var result = new List<GroupQuiz>();
            var uri = API.GroupQuiz.GetGroupQuizListByGroupIdAndDateRange(_path, groupId, startDate, endDate);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupQuiz>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public GroupQuiz GetById(int id)
        {
            var result = new GroupQuiz();
            var uri = API.GroupQuiz.GetGroupQuizById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, GroupQuiz>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int Update(GroupQuiz entityToUpdate)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.GroupQuiz.UpdateGroupQuiz(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public bool InsertUpdateQuizAnswers(QuizResult model)
        {
            var uri = string.Empty;
            uri = API.GroupQuiz.InsertUpdateQuizAnswers(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;

        }
        public bool LogQuizQuestionAnswer(QuizResult model)
        {
            var uri = string.Empty;
            uri = API.GroupQuiz.LogQuizQuestionAnswer(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }
        public bool UpdateQuizGrade(int gradeId, int gradingTemplateId, int QuizResultId)
        {
            bool bFlag = false;
            var uri = API.GroupQuiz.UpdateQuizGrade(_path, gradeId, gradingTemplateId, QuizResultId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }
        public bool InsertQuizFeedback(QuizFeedbackEdit quizFeedback)
        {
            var uri = string.Empty;
            uri = API.GroupQuiz.InsertQuizFeedback(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, quizFeedback).Result;
            return response.IsSuccessStatusCode;

        }
        public QuizResult GetQuizResultByUserId(int quizId, int userId, int resourceId)
        {
            var uri = API.GroupQuiz.GetQuizResultByUserId(_path, quizId, userId, resourceId);
            QuizResult quizResponse = new QuizResult();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quizResponse = EntityMapper<string, QuizResult>.MapFromJson(jsonDataProviders);
            }
            return quizResponse;
        }
        public QuizResult GetLogResultQuestionAnswer(int quizId, int userId, int resourceId, string ResourceType)
        {
            var uri = API.GroupQuiz.GetLogResultQuestionAnswer(_path, quizId, userId, resourceId, ResourceType);
            QuizResult quizResponse = new QuizResult();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quizResponse = EntityMapper<string, QuizResult>.MapFromJson(jsonDataProviders);
            }
            return quizResponse;
        }
        public GroupQuiz GetQuizStudentDetailsByQuizId(int groupQuizId, int groupId, int selectedGroupId)
        {
            var uri = API.GroupQuiz.GetQuizStudentDetailsByQuizId(_path, groupQuizId, groupId, selectedGroupId);
            GroupQuiz quizDetails = new GroupQuiz();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quizDetails = EntityMapper<string, GroupQuiz>.MapFromJson(jsonDataProviders);
            }
            return quizDetails;
        }
        public GroupQuiz GetFormStudentDetailsByQuizId(int quizId, int groupId)
        {
            var uri = API.GroupQuiz.GetFormStudentDetailsByQuizId(_path, quizId, groupId);
            GroupQuiz quizDetails = new GroupQuiz();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quizDetails = EntityMapper<string, GroupQuiz>.MapFromJson(jsonDataProviders);
            }
            return quizDetails;
        }
    }
}