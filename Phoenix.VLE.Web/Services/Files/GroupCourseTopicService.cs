﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class GroupCourseTopicService : IGroupCourseTopicService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/GroupCourseTopics";
        #endregion

        public GroupCourseTopicService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);


            }
        }

        public bool Delete(int id, long userId)
        {
            var uri = API.GroupCourseTopic.DeleteGroupCourseTopic(_path, id, userId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<GroupCourseTopic> GetAll()
        {
            var result = new List<GroupCourseTopic>();
            var uri = API.GroupCourseTopic.GetAllGroupCourseTopics(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupCourseTopic>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public GroupCourseTopic GetById(int id)
        {
            var result = new GroupCourseTopic();
            var uri = API.GroupCourseTopic.GetGroupCourseTopicById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, GroupCourseTopic>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<GroupCourseTopic> GetGroupCourseTopics(long? groupCourseTopicId, long? groupId, long? topicId, long? userId, bool? isSubTopic, bool? isActive)
        {
            var result = new List<GroupCourseTopic>();
            var uri = API.GroupCourseTopic.GetGroupCourseTopics(_path, groupCourseTopicId,groupId,topicId,userId,isSubTopic, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupCourseTopic>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<GroupCourseTopic> GetGroupCourseTopicsByGroupId(long groupId, bool? isActive)
        {
            var result = new List<GroupCourseTopic>();
            var uri = API.GroupCourseTopic.GetGroupCourseTopicByGroupId(_path,  groupId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupCourseTopic>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int Insert(GroupCourseTopic entity)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.GroupCourseTopic.AddGroupCourseTopic(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int Update(GroupCourseTopic entityToUpdate)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.GroupCourseTopic.UpdateGroupCourseTopic(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int UpdateUnitSortOrder(IEnumerable<GroupCourseTopic> model)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.GroupCourseTopic.UpdateUnitSortOrder(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
    }
}