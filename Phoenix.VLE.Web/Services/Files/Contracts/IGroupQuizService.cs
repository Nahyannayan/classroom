﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IGroupQuizService
    {
        int Insert(GroupQuiz entity);
        IEnumerable<GroupQuiz> GetGroupQuizListByGroupId(int sectionId);
        IEnumerable<GroupQuiz> GetGroupQuizListByGroupIdAndDateRange(int groupId, DateTime startDate, DateTime endDate);
        GroupQuiz GetById(int id);
        IEnumerable<QuizEdit> GetQuizByCourse(string courseIds, int schoolId);
        int Update(GroupQuiz entityToUpdate);
        bool InsertUpdateQuizAnswers(QuizResult model);
        bool UpdateQuizGrade(int gradeId, int gradingTemplateId, int QuizResultId);
        bool InsertQuizFeedback(QuizFeedbackEdit quizFeedback);
        bool LogQuizQuestionAnswer(QuizResult model);
        QuizResult GetLogResultQuestionAnswer(int quizId, int userId, int resourceId, string ResourceType);
        QuizResult GetQuizResultByUserId(int quizId, int userId, int resourceId);
        GroupQuiz GetQuizStudentDetailsByQuizId(int groupQuizId, int groupId, int selectedGroupId);
        GroupQuiz GetFormStudentDetailsByQuizId(int quizId, int groupId);
    }
}
