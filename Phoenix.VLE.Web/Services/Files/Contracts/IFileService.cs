﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Models;
using System.Threading.Tasks;
using Phoenix.VLE.Web.ViewModels;

namespace Phoenix.VLE.Web.Services
{
    public interface IFileService
    {
        IEnumerable<File> GetFilesByUserId(int userId,int moduleId, bool? isActive);
        IEnumerable<StudentGroupsFiles> GetStudentGroupsFiles(long userId);
        IEnumerable<File> GetFilesBySchoolId(int schoolId, bool? isActive);
        IEnumerable<File> GetFilesByModuleId(int schoolId, int moduleId, bool? isActive);
        IEnumerable<File> GetFilesByFolderId(int folderId, bool? isActive);
        IEnumerable<FileManagementModule> GetFileManagementModules();
        IEnumerable<VLEFileType> GetFileTypes(string DocumentType = "");

        IEnumerable<File> GetFilesBySectionId(int sectionId, int moduleId, bool? isActive);

        Task<FileExplorer> GetGroupFileExplorer(int moduleId, long sectionId, long folderId, long studentUserId, bool isActive);

        IEnumerable<File> GetAll();
        File GetById(int id);
        DateTime GetServerDateTime();
        bool Insert(File entity);
        bool Delete(int id, long userId);
        bool Update(File entityToUpdate);
        bool RenameFile(long fileId, string FileName, long updatedBy);
        IEnumerable<File> GetAllGroupFiles(int id);
        int ClearGroupFiles(long userId, int schoolGroupId);
        bool SaveCloudFiles(IList<File> files);
        IEnumerable<File> GetFilesByIds(string Ids);
        bool FilesFolderBulkDelete(MyFilesModel model);
        bool FilesFolderBulkCreate(MyFilesModel model);
    }
}