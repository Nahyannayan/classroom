﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Models;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IGroupCourseTopicService
    {
        IEnumerable<GroupCourseTopic> GetGroupCourseTopicsByGroupId(long groupId, bool? isActive);
        IEnumerable<GroupCourseTopic> GetGroupCourseTopics(long? groupCourseTopicId, long? groupId, long? topicId, long? userId, bool? isSubTopic, bool? isActive);
        IEnumerable<GroupCourseTopic> GetAll();
        GroupCourseTopic GetById(int id);
        int Insert(GroupCourseTopic entity);
        bool Delete(int id,long userId);
        int Update(GroupCourseTopic entityToUpdate);
        int UpdateUnitSortOrder(IEnumerable<GroupCourseTopic> model);
    }
}