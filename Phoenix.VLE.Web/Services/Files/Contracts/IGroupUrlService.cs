﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
   public interface IGroupUrlService
    {
        IEnumerable<GroupUrl> GetAll();
        GroupUrl GetById(int id);
        int Insert(GroupUrl entity);
        int Update(GroupUrl entityToUpdate);
        IEnumerable<GroupUrl> GetGroupUrlListByGroupId(int groupId);
    }
}
