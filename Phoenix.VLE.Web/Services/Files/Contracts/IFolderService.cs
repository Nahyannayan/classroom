﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.VLE.Web.Services
{
    public interface IFolderService
    {
        IEnumerable<Folder> GetFoldersByUserId(int userId,int moduleId, bool? isActive);
        IEnumerable<Folder> GetFoldersBySchoolId(int schoolId, bool? isActive);
        IEnumerable<Folder> GetFoldersByModuleId(int schoolId, int moduleId, bool? isActive);
        IEnumerable<Folder> GetFoldersByParentFolderId(int parentFolderId, int moduleId, bool? isActive);
        IEnumerable<FolderTree> GetFolderTree(int folderId);

        IEnumerable<Folder> GetFoldersBySectionId(int sectionId, int moduleId, bool? isActive);

        IEnumerable<Folder> GetAll();
        Folder GetById(int id);
        int Insert(Folder entity);
        bool Delete(long id, long userId);
        int Update(Folder entityToUpdate);
        int GetGroupPermission(int userId, int groupId);
    }
}
