﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public interface IBookmarkService
    {
        Bookmark GetBookmark(long id);
        // List<SchoolGroup> GetSelectedSchoolGroupsPerBoookmark();
        bool AddUpdateBookmarkData(BookmarkEdit model);
        IEnumerable<Bookmark> GetBookmarks(long userId);
        IEnumerable<SchoolGroup> GetBookmarkSchoolGroups(long bookmarkId);
        bool DeleteBookmarkData(long bookmarkId);
    }
}