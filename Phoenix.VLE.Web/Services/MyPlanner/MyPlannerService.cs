﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace Phoenix.VLE.Web.Services
{
    public class MyPlannerService : IMyPlannerService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/events";
        #endregion

        public MyPlannerService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Public  Methods
        public IEnumerable<EventCategoryView> GetEventCategory()
        {
            var uri = API.MyPlanner.GetEventCategories(_path, SessionHelper.CurrentSession.SchoolId);
            IEnumerable<EventCategoryView> lstEventCategoryView = new List<EventCategoryView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstEventCategoryView = EntityMapper<string, IEnumerable<EventCategoryView>>.MapFromJson(jsonDataProviders);
            }
            return lstEventCategoryView;
        }
        public IEnumerable<EventTypeView> GetEventType()
        {
            var uri = API.MyPlanner.GetEventTypes(_path);
            IEnumerable<EventTypeView> lstEventTypeView = new List<EventTypeView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstEventTypeView = EntityMapper<string, IEnumerable<EventTypeView>>.MapFromJson(jsonDataProviders);
            }
            return lstEventTypeView;
        }
        public IEnumerable<EventDurationView> GetEventDuration()
        {
            var uri = API.MyPlanner.GetEventDuration(_path);
            IEnumerable<EventDurationView> lstEventDurationView = new List<EventDurationView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstEventDurationView = EntityMapper<string, IEnumerable<EventDurationView>>.MapFromJson(jsonDataProviders);
            }
            return lstEventDurationView;
        }
        public int InsertUpdateMyPlanner(EventEdit model)
        {
            //bool result = false;
            var uri = string.Empty;
            var sourceModel = new Event();
            EntityMapper<EventEdit, Event>.Map(model, sourceModel);
            if (model.IsAddMode)
                uri = API.MyPlanner.InsertPlanner(_path);
            else
                uri = API.MyPlanner.UpdatePlanner(_path);
            sourceModel.UserId = SessionHelper.CurrentSession.Id;
            sourceModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            sourceModel.EventUserEmail = SessionHelper.CurrentSession.Email;
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public bool AcceptEventRequest(int eventId, long userId)
        {
            var sourceModel = new Event();
            var uri = API.MyPlanner.acceptEventRequest(_path, eventId, userId);
            sourceModel.UserId = SessionHelper.CurrentSession.Id;
            sourceModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            return response.IsSuccessStatusCode;
        }

        public bool AcceptEventRequestExternalUser(int eventId, string emailId)
        {
            var sourceModel = new Event();
            var uri = API.MyPlanner.acceptEventRequestExternalUser(_path, eventId, emailId);
            sourceModel.UserId = SessionHelper.CurrentSession.Id;
            sourceModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            return response.IsSuccessStatusCode;
        }

        public string GetEvents(long userId, string fromdate, string toDate, int? categoryId)
        {
            DateTime _fromDate = Convert.ToDateTime(fromdate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            var jsonDataProviders = String.Empty;
            var uri = API.MyPlanner.getEventByUserId(_path, userId, _fromDate, _toDate, categoryId);
            Suggestion suggestions = new Suggestion();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            }
            return jsonDataProviders;
        }

        public string GetAllMonthlyEvents(long userId, string fromdate, string toDate, string eventCategoryIds)
        {
            DateTime _fromDate = Convert.ToDateTime(fromdate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            var jsonDataProviders = String.Empty;
            var uri = API.MyPlanner.getAllEventByUserId(_path, userId, _fromDate, _toDate, eventCategoryIds);
            Suggestion suggestions = new Suggestion();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            }
            return jsonDataProviders;
        }

        public string GetWeeklyTimeTableEvents(long userId, long schoolId, string fromdate, string toDate, bool isTeacher, bool isDashboardEvents)
        {
            DateTime _fromDate = Convert.ToDateTime(fromdate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            var jsonDataProviders = String.Empty;
            var uri = API.MyPlanner.getWeeklyTimeTableEvents(_path, userId, schoolId, _fromDate, _toDate, isTeacher, isDashboardEvents);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            }
            return jsonDataProviders;
        }

        public string GetTodayOrWeeklyTimeTableEvents(long userId, long schoolId, int fromWeekDay, int toWeekDay, string fromdate, string toDate, bool isTeacher, bool isWeeklyEvent)
        {
            DateTime _fromDate = Convert.ToDateTime(fromdate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            var jsonDataProviders = String.Empty;
            var uri = API.MyPlanner.getTodayOrWeeklyTimeTableEvents(_path, userId, schoolId, fromWeekDay, toWeekDay, _fromDate, _toDate, isTeacher, isWeeklyEvent);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            }
            return jsonDataProviders;
        }

        public async Task<PlannerTimetableView> GetPlannerTimetableData(long userId, long schoolId, string fromDate, string toDate, bool isTeacher)
        {
            DateTime _fromDate = Convert.ToDateTime(fromDate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            var plannerTimetable = new PlannerTimetableView();
            var uri = API.MyPlanner.getPlannerTimetableData(_path, userId, schoolId, _fromDate, _toDate, isTeacher);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                plannerTimetable = EntityMapper<string, PlannerTimetableView>.MapFromJson(jsonDataProviders);
            }
            return plannerTimetable;
        }

        public async Task<PlannerTimetableView> GetPlannerTimetableDataWithPaging(int pageNumber,int pageSize,long userId, long schoolId, string fromDate, string toDate, bool isTeacher)
        {
            DateTime _fromDate = Convert.ToDateTime(fromDate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            var plannerTimetable = new PlannerTimetableView();
            var uri = API.MyPlanner.getPlannerTimetableDataWithPaging(_path, pageNumber, pageSize, userId, schoolId, _fromDate, _toDate, isTeacher);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                plannerTimetable = EntityMapper<string, PlannerTimetableView>.MapFromJson(jsonDataProviders);
            }
            return plannerTimetable;
        }

        public async Task<IEnumerable<WeeklyEventView>> GetTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, string fromDate, string toDate, bool isTeacher)
        {
            DateTime _fromDate = Convert.ToDateTime(fromDate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            IEnumerable<WeeklyEventView> timetableEvents = new List<WeeklyEventView>();
            var uri = API.MyPlanner.getTimetableDataWithPaging(_path, pageNumber, pageSize, userId, schoolId, _fromDate, _toDate, isTeacher);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                timetableEvents = EntityMapper<string, IEnumerable<WeeklyEventView>>.MapFromJson(jsonDataProviders);
            }
            return timetableEvents;
        }

        public async Task<IEnumerable<EventView>> GetOnlineMeetingEvents(int pageNumber, int pageSize, long userId, long schoolId, string fromDate, string toDate, bool isOnlineMeetingEvents)
        {
            DateTime _fromDate = Convert.ToDateTime(fromDate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            IEnumerable<EventView> eventsList = new List<EventView>();
            var uri = API.MyPlanner.getOnlineMeetingEvents(_path, pageNumber,pageSize, userId, schoolId, _fromDate, _toDate, isOnlineMeetingEvents);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                eventsList = EntityMapper<string, IEnumerable<EventView>>.MapFromJson(jsonDataProviders);
            }
            return eventsList;
        }

        public async Task<IEnumerable<WeeklyEventView>> GetSchoolTimetableEvents(int pageNumber, int pageSize, long userId, long schoolId, string fromDate, string toDate, bool isTeacher)
        {
            DateTime _fromDate = Convert.ToDateTime(fromDate);
            DateTime _toDate = Convert.ToDateTime(toDate);
            IEnumerable<WeeklyEventView> eventsList = new List<WeeklyEventView>();
            var uri = API.MyPlanner.getSchoolTimetableEvents(_path, pageNumber, pageSize, userId, schoolId, _fromDate, _toDate, isTeacher);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                eventsList = EntityMapper<string, IEnumerable<WeeklyEventView>>.MapFromJson(jsonDataProviders);
            }
            return eventsList;
        }

        public async Task<IEnumerable<WeeklyEventView>> GetTimetableEventsReport(TimetableReportsRequest model)
        {
            IEnumerable<WeeklyEventView> eventsList = new List<WeeklyEventView>();
            var uri = API.MyPlanner.getTimetableEventsReport(_path);
            HttpResponseMessage response = await _client.PostAsJsonAsync(uri, model);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                eventsList = EntityMapper<string, IEnumerable<WeeklyEventView>>.MapFromJson(jsonDataProviders);
            }
            return eventsList;
        }

        public async Task<IEnumerable<EventView>> GetLiveSessionsReport(TimetableReportsRequest model)
        {
            IEnumerable<EventView> eventsList = new List<EventView>();
            var uri = API.MyPlanner.getLiveSessionsReport(_path);
            HttpResponseMessage response = await _client.PostAsJsonAsync(uri, model);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                eventsList = EntityMapper<string, IEnumerable<EventView>>.MapFromJson(jsonDataProviders);
            }
            return eventsList;
        }

        public IEnumerable<EventEdit> GetEventById(int id, long? userId)
        {
            var uri = API.MyPlanner.getEventById(_path, id, userId);
            IEnumerable<EventEdit> lstEventView = new List<EventEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstEventView = EntityMapper<string, IEnumerable<EventEdit>>.MapFromJson(jsonDataProviders);
            }
            return lstEventView;
        }

        public IEnumerable<EditModels.EventUser> GetInternalEventUserByEventId(int eventId)
        {
            var uri = API.MyPlanner.getInternalEventUserByEventId(_path, eventId);
            IEnumerable<EditModels.EventUser> lstEventUser = new List<EditModels.EventUser>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstEventUser = EntityMapper<string, IEnumerable<EditModels.EventUser>>.MapFromJson(jsonDataProviders);
            }
            return lstEventUser;
        }
        public IEnumerable<EditModels.EventUser> GetExternalEventUserByEventId(int eventId)
        {
            var uri = API.MyPlanner.getExternalEventUserByEventId(_path, eventId);
            IEnumerable<EditModels.EventUser> lstEventUser = new List<EditModels.EventUser>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstEventUser = EntityMapper<string, IEnumerable<EditModels.EventUser>>.MapFromJson(jsonDataProviders);
            }
            return lstEventUser;
        }
        #endregion


    }
}