﻿using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IMyPlannerService
    {
        IEnumerable<EventCategoryView> GetEventCategory();
        IEnumerable<EventTypeView> GetEventType();
        IEnumerable<EventDurationView> GetEventDuration();
        int InsertUpdateMyPlanner(EventEdit model);
        string GetEvents(long userId, string fromDate, string toDate, int? categoryId);
        bool AcceptEventRequest(int eventId, long userId);
        bool AcceptEventRequestExternalUser(int eventId, string emailId);
        IEnumerable<EventEdit> GetEventById(int id, long? userId = 0);
        IEnumerable<EditModels.EventUser> GetInternalEventUserByEventId(int eventId);
        IEnumerable<EditModels.EventUser> GetExternalEventUserByEventId(int eventId);
        string GetAllMonthlyEvents(long userId, string fromdate, string toDate, string eventCategoryIds);
        string GetWeeklyTimeTableEvents(long userId, long schoolId, string fromdate, string toDate, bool isTeacher, bool isDashboardEvents);
        string GetTodayOrWeeklyTimeTableEvents(long userId, long schoolId, int fromWeekDay, int toWeekDay, string fromdate, string toDate, bool isTeacher, bool isWeeklyEvent);
        Task<PlannerTimetableView> GetPlannerTimetableData(long userId, long schoolId, string fromDate, string toDate, bool isTeacher);
        Task<IEnumerable<EventView>> GetOnlineMeetingEvents(int pageNumber, int pageSize, long userId, long schoolId, string fromDate, string toDate, bool isOnlineMeetingEvents);
        Task<IEnumerable<WeeklyEventView>> GetSchoolTimetableEvents(int pageNumber, int pageSize, long userId, long schoolId, string fromDate, string toDate, bool isTeacher);
        Task<PlannerTimetableView> GetPlannerTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, string fromDate, string toDate, bool isTeacher);
        Task<IEnumerable<WeeklyEventView>> GetTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, string fromDate, string toDate, bool isTeacher);
        Task<IEnumerable<WeeklyEventView>> GetTimetableEventsReport(TimetableReportsRequest model);
        Task<IEnumerable<EventView>> GetLiveSessionsReport(TimetableReportsRequest model);
    }
}
