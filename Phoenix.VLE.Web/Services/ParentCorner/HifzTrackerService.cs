﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.ParentCorner.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services.ParentCorner
{
    public class HifzTrackerService : IHifzTrackerService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        private ILoggerClient _loggerClient;
        private IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        public HifzTrackerService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion


        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get hifz tracker list.
        /// </summary>
        /// <returns>Get hifz tracker list.</returns>
        public IEnumerable<HifzTrackerEdit> GetHifzTrackerList(List<string> StudentIdList)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            List<HifzTrackerEdit> hifzTrackerList = new List<HifzTrackerEdit>();
            HifzTrackerRoot hifzTrackerRoot = new HifzTrackerRoot();
            try
            {
                foreach (string studentId in StudentIdList)
                {
                    var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();

                    if (token != null)
                    {
                        if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                        {
                            string AuthorizedToken = token.token_type + " " + token.access_token;
                            httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GetHifzTrackerListAPI);
                            httpRequestMessage.Headers.Add("Status", "All");
                            httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
                            httpRequestMessage.Headers.Add("StudentID", studentId);
                            using (response = _client.SendAsync(httpRequestMessage).Result)
                            {
                                if (response.IsSuccessStatusCode)
                                {
                                    JavaScriptSerializer se = new JavaScriptSerializer();
                                    json = response.Content.ReadAsStringAsync().Result;
                                    hifzTrackerRoot = new JavaScriptSerializer().Deserialize<HifzTrackerRoot>(json);

                                    if (hifzTrackerRoot.data != null && hifzTrackerRoot.data.Count > 0)
                                    {
                                        foreach (HifzTrackerEdit hifzTracker in hifzTrackerRoot.data)
                                        {
                                            hifzTracker.StudentId = studentId;
                                        }
                                        hifzTrackerList.AddRange(hifzTrackerRoot.data);
                                    }
                                }
                            }
                        }
                    }
                }
                return hifzTrackerList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in HifzTrackerService.GetHifzTrackerList()" + ex.Message);
                return hifzTrackerList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, hifzTrackerList, response, SessionHelper.CurrentSession.UserName);
            }
        }

        /// <summary>
        /// Author:Tejalben
        /// Summery:Save hifz tracker file.
        /// Date:02 Aug 2020
        /// </summary>
        /// <param name="formFile"></param>
        /// <param name="hifzTrackerFileEdit"></param>
        /// <returns></returns>
        public string SaveHifzFile(HttpFileCollectionBase formFile, HifzTrackerFileEdit hifzTrackerFileEdit)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            ResultSaveHifz result = new ResultSaveHifz();
            var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
            try
            {
                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;
                        httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.SaveHifzTrackerAPI);

                        byte[] content = Encoding.ASCII.GetBytes(new JavaScriptSerializer().Serialize(hifzTrackerFileEdit));
                        var bytes = new ByteArrayContent(content);
                        MultipartFormDataContent multiContent = new MultipartFormDataContent();
                        multiContent.Add(bytes, "request");
                        byte[] imageData = null;
                        if (formFile != null && formFile.Count > 0)
                        {
                            using (var br = new BinaryReader(formFile[0].InputStream))
                            {
                                imageData = br.ReadBytes((int)formFile[0].ContentLength);
                                ByteArrayContent filebytes = new ByteArrayContent(imageData);
                                multiContent.Add(filebytes, "file", formFile[0].FileName);
                            }
                        }
                        httpRequestMessage.Content = multiContent;
                        httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                        using (response = _client.SendAsync(httpRequestMessage).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                JavaScriptSerializer se = new JavaScriptSerializer();
                                json = response.Content.ReadAsStringAsync().Result;
                            }
                        }

                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in HifzTrackerService.SaveHifzFile()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }

        }
    }
}

