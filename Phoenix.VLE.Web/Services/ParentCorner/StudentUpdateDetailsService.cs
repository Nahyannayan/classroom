﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.ParentCorner.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services.ParentCorner
{
    public class StudentUpdateDetailsService : IStudentUpdateDetailsService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        private ILoggerClient _loggerClient;
        private IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        public StudentUpdateDetailsService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion
        public List<StudentupdateMaster> GetMasterList(string master, string bsuid, string acdid, string clmid = "", string param1 = "")
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            StudentupdateMasterRoot studentupdateMasterRoot = new StudentupdateMasterRoot();
            List<StudentupdateMaster> StudentupdateMasterList = new List<StudentupdateMaster>();
            try
            {
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.MastersAPI);
                httpRequestMessage.Headers.Add("master", master);
                httpRequestMessage.Headers.Add("bsuid", bsuid);
                httpRequestMessage.Headers.Add("acdid", acdid);
                httpRequestMessage.Headers.Add("clmid", clmid);
                httpRequestMessage.Headers.Add("param1", param1);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        studentupdateMasterRoot = new JavaScriptSerializer().Deserialize<StudentupdateMasterRoot>(json);

                        if (studentupdateMasterRoot.data != null && studentupdateMasterRoot.data.Count > 0)
                            StudentupdateMasterList = studentupdateMasterRoot.data;
                    }
                }
                return StudentupdateMasterList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return StudentupdateMasterList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, StudentupdateMasterList, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public GetParentUpdateDetails GetParentUpdateDetails(string Stuid)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            GetParentUpdateDetailsRoot getParentUpdateDetailsRoot = new GetParentUpdateDetailsRoot();
            GetParentUpdateDetails getParentUpdateDetails = new GetParentUpdateDetails();
            try
            {
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GET_PARENT_CONTACT);
                httpRequestMessage.Headers.Add("Stuid", Stuid);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        getParentUpdateDetailsRoot.data = new JavaScriptSerializer().Deserialize<List<GetParentUpdateDetails>>(json);
                        getParentUpdateDetails = getParentUpdateDetailsRoot.data[0];
                    }
                }
                return getParentUpdateDetails;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetParentUpdateDetails()" + ex.Message);
                return getParentUpdateDetails;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, getParentUpdateDetails, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public GetHealthrestricted getHealthrestricted(string Stuid)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            GetHealthrestrictedRoot getHealthrestrictedRoot = new GetHealthrestrictedRoot();
            GetHealthrestricted getHealthrestricted = new GetHealthrestricted();
            try
            {
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GET_STUDENT_HEALTH_RESTRICTIONS);
                httpRequestMessage.Headers.Add("Stuid", Stuid);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        getHealthrestrictedRoot.data = new JavaScriptSerializer().Deserialize<List<GetHealthrestricted>>(json);
                        getHealthrestricted = getHealthrestrictedRoot.data[0];
                    }
                }
                return getHealthrestricted;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.getHealthrestricted()" + ex.Message);
                return getHealthrestricted;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, getHealthrestricted, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public List<GetStudentDiseases> getStudentDiseases(string Stuid, bool Isinfection)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            //GetParentUpdateDetailsRoot getParentUpdateDetailsRoot = new GetParentUpdateDetailsRoot();
            List<GetStudentDiseases> getStudentDiseases = new List<GetStudentDiseases>();
            var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
            try
            {
                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;
                        httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GET_STUDENT_DISEASES);
                        httpRequestMessage.Headers.Add("Stuid", Stuid);
                        httpRequestMessage.Headers.Add("isInfectious", Convert.ToString(Isinfection));
                        httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
                        using (response = _client.SendAsync(httpRequestMessage).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                JavaScriptSerializer se = new JavaScriptSerializer();
                                json = response.Content.ReadAsStringAsync().Result;
                                getStudentDiseases = new JavaScriptSerializer().Deserialize<List<GetStudentDiseases>>(json);
                            }
                        }
                    }
                }
                return getStudentDiseases;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.getStudentDiseases()" + ex.Message);
                return getStudentDiseases;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, getStudentDiseases, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public GetStudentDiseasesDetail getStudentDiseasesDetail(string Stuid)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            //GetStudentDiseasesDetailRoot getStudentDiseasesDetailRoot = new GetStudentDiseasesDetailRoot();
            GetStudentDiseasesDetail getStudentDiseasesDetail = new GetStudentDiseasesDetail();           
            try
            {
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GET_STUDENT_DISEASES_DETAILS);
                httpRequestMessage.Headers.Add("Stuid", Stuid);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        var result = new JavaScriptSerializer().Deserialize<List<GetStudentDiseasesDetail>>(json);
                        getStudentDiseasesDetail = result[0];
                    }
                }
                return getStudentDiseasesDetail;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.getStudentDiseasesDetail()" + ex.Message);
                return getStudentDiseasesDetail;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, getStudentDiseasesDetail, response, SessionHelper.CurrentSession.UserName);
            }
        }

        public bool UpdateStudentDetails(string postData, string type)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            bool success = false;
            string json = "";
            //StudentupdateMasterRoot studentupdateMasterRoot = new StudentupdateMasterRoot();
            //List<StudentupdateMaster> StudentupdateMasterList = new List<StudentupdateMaster>();
            try
            { 
                httpRequestMessage = null;

                if (type == "Student")
                {
                    httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.UPDATE_STUDENT);
                }
                else if (type == "Parent")
                {
                    httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.POST_PARENT_INFO);
                }
                else if (type == "Health")
                {
                    httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.UPDATE_STUDENT_HEALTH);
                }
                else if (type == "Consent")
                {
                    httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.POST_STUDENT_CONSENT_INFO);
                }

                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                httpRequestMessage.Content = new StringContent(postData, Encoding.UTF8, "application/json");

                //var content = new StringContent(json, Encoding.UTF8, "application/json");
                //HttpResponseMessage response = _client.PostAsync(uri, content).Result;
                //var httpMessage = _client.PostAsJsonAsync(UPDATE_STUDENT, postData).Result;
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        if (json.Contains("Success\":\"true"))
                        {
                            success = true;
                        }
                    }
                }
                return success;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.UpdateStudentDetails()" + ex.Message);
                return success;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, success, response, SessionHelper.CurrentSession.UserName);
            }
        }
        #region UpdateStudentData
        //public bool UpdateStudentData(StudentInfoPost studentinfo)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    HttpRequestMessage httpRequestMessage = null;
        //    bool success = false;
        //    var uri = _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.UPDATE_STUDENT;
        //    var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
        //    string json = "";
        //    try
        //    {
        //        if (token != null)
        //        {
        //            if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
        //            {
        //                string AuthorizedToken = token.token_type + " " + token.access_token;
        //                //HttpRequestMessage httpRequestMessage = null;

        //                httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.UPDATE_STUDENT);

        //                httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
        //                httpRequestMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");
        //                using (response = _client.PostAsJsonAsync(uri, studentinfo).Result)
        //                {
        //                    if (response.IsSuccessStatusCode)
        //                    {
        //                        JavaScriptSerializer se = new JavaScriptSerializer();
        //                        json = response.Content.ReadAsStringAsync().Result;
        //                    }
        //                    success = response.IsSuccessStatusCode;
        //                }

        //            }
        //        }
        //        return success;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerClient.LogWarning("Error in StudentUpdateDetailsService.UpdateStudentDetails()" + ex.Message);
        //        return success;
        //    }
        //    finally
        //    {
        //        _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, success, response, SessionHelper.CurrentSession.UserName);
        //    }
        //}
        #endregion
        #region UpdateParentInfo
        //public bool UpdateParentInfo(ParentInfoPost parent)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    HttpRequestMessage httpRequestMessage = null;
        //    bool success = false;
        //    var uri = _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.POST_PARENT_INFO;
        //    var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
        //    string json = "";
        //    try
        //    {
        //        if (token != null)
        //        {
        //            if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
        //            {
        //                string AuthorizedToken = token.token_type + " " + token.access_token;
        //                //HttpRequestMessage httpRequestMessage = null;

        //                httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, uri);
        //                httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
        //                httpRequestMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");
        //                using (response = _client.PostAsJsonAsync(uri, parent).Result)
        //                {
        //                    if (response.IsSuccessStatusCode)
        //                    {
        //                        JavaScriptSerializer se = new JavaScriptSerializer();
        //                        json = response.Content.ReadAsStringAsync().Result;
        //                        success = response.IsSuccessStatusCode;
        //                    }
        //                }
        //            }
        //        }
        //        return success;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerClient.LogWarning("Error in StudentUpdateDetailsService.UpdateParentInfo()" + ex.Message);
        //        return success;
        //    }
        //    finally
        //    {
        //        _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, success, response, SessionHelper.CurrentSession.UserName);
        //    }
        //}
        #endregion
        #region
        //public bool UpdateStudentConsentInfo(StudentConsentInfoPost studentConsentInfo)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    HttpRequestMessage httpRequestMessage = null;
        //    bool success = false;
        //    var uri = _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.POST_STUDENT_CONSENT_INFO;
        //    var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
        //    string json = "";
        //    try
        //    {
        //        if (token != null)
        //        {
        //            if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
        //            {
        //                string AuthorizedToken = token.token_type + " " + token.access_token;
        //                //HttpRequestMessage httpRequestMessage = null;

        //                httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, uri);

        //                httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
        //                httpRequestMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");
        //                using (response = _client.PostAsJsonAsync(uri, studentConsentInfo).Result)
        //                {
        //                    if (response.IsSuccessStatusCode)
        //                    {
        //                        JavaScriptSerializer se = new JavaScriptSerializer();
        //                        json = response.Content.ReadAsStringAsync().Result;
        //                        success = response.IsSuccessStatusCode;
        //                    }
        //                }

        //            }
        //        }
        //        return success;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerClient.LogWarning("Error in StudentUpdateDetailsService.UpdateStudentConsentInfo()" + ex.Message);
        //        return success;
        //    }
        //    finally
        //    {
        //        _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, success, response, SessionHelper.CurrentSession.UserName);
        //    }
        //}
        #endregion
    }
}