﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services.ParentCorner.Contracts
{
   public interface IStudentUpdateDetailsService
    {
        List<StudentupdateMaster> GetMasterList(string master,string bsuid,string acdid,string clmid,string param1);
        GetParentUpdateDetails GetParentUpdateDetails(string Stuid);
        GetHealthrestricted getHealthrestricted(string Stuid);
        List<GetStudentDiseases> getStudentDiseases(string Stuid,bool Isinfection);
        GetStudentDiseasesDetail getStudentDiseasesDetail(string Stuid);
        bool UpdateStudentDetails(string postData, string type);
        //bool UpdateStudentData(StudentInfoPost studentinfo);
        //bool UpdateParentInfo(ParentInfoPost parent);
        //bool UpdateStudentConsentInfo(StudentConsentInfoPost studentConsentInfo);
    }
}
