﻿using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.Services.ParentCorner.Contracts
{
    public interface ITCRequestService
    {
        IEnumerable<TransferBusinessunitList> GetBusinessUnitList();
        IEnumerable<TransferReasonList> GetReasonList();
        IEnumerable<TransferQuestionListList> GetQuestionList();
        IEnumerable<TransferTypeListList> GetTransferTypeList();
        string SaveTCRequest(TCRequestEdit data);
        string SaveTCExitQuestion(TCExitQuestion data);
        StudentTcDetailsRoot GetTcRequestStatusByStudentNo(string studentNo);
    }
}