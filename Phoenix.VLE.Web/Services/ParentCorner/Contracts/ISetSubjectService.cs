﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISetSubjectService
    {
        IEnumerable<SubjectDetails> GetSubjectList(string StudentNumber);
        IEnumerable<StudentDetails> GetExamPaperList(string StudentNumber);
        string SaveSubjectSelection(string data);
        string RemoveSubjectSelection(string data);
        string ExamSetupRequestForApproval(string data);
    }
}
