﻿using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.Services.ParentCorner.Contracts
{
    public interface IHifzTrackerService
    {
        IEnumerable<HifzTrackerEdit> GetHifzTrackerList(List<string> StudentIdList);

        string SaveHifzFile(HttpFileCollectionBase formFile, HifzTrackerFileEdit hifzTrackerFileEdit);
    }
}
