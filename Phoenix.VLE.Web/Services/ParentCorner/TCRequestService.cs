﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.ParentCorner.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services.ParentCorner
{
    public class TCRequestService : ITCRequestService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        readonly string GetBusinesUnitListAPI = ConfigurationManager.AppSettings["GetBusinessUnitList"];
        readonly string GetReasonsListAPI = ConfigurationManager.AppSettings["GetReasonsList"];
        readonly string GetQuestionsListAPI = ConfigurationManager.AppSettings["GetQuestionList"];
        readonly string GetTransferTypesListAPI = ConfigurationManager.AppSettings["GetTransferTypeList"];
        readonly string SaveTCRequestDataAPI = ConfigurationManager.AppSettings["SaveTCRequestData"];
        readonly string SaveTCExitQuestionDataAPI = ConfigurationManager.AppSettings["SaveTCExitQuestionData"];
        readonly string GetTCRequestStatusByStudentNoAPI = ConfigurationManager.AppSettings["GetTCRequestStatusByStudentNo"];
        private ILoggerClient _loggerClient;
        private readonly PhoenixAPIParentCornerService _phoenixAPIParentCornerService;

        public TCRequestService(PhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion

        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            return JsonDeserilize;
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get business list.
        /// </summary>
        /// <returns>Get business list.</returns>
        public IEnumerable<TransferBusinessunitList> GetBusinessUnitList()
        {
            string json = "";
            IEnumerable<TransferBusinessunitList> obj = new List<TransferBusinessunitList>();
            TransferBusinessunitListRoot transferBusinessunitRoot = new TransferBusinessunitListRoot();
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetBusinesUnitListAPI);
            try
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        transferBusinessunitRoot = new JavaScriptSerializer().Deserialize<TransferBusinessunitListRoot>(json);

                        if (transferBusinessunitRoot.data != null && transferBusinessunitRoot.data.Count > 0)
                            obj = transferBusinessunitRoot.data;
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return obj;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, obj, response, SessionHelper.CurrentSession.UserName);
            }

        }


        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get business list.
        /// </summary>
        /// <returns>Get reason list.</returns>
        public IEnumerable<TransferReasonList> GetReasonList()
        {
            string json = "";
            IEnumerable<TransferReasonList> obj = new List<TransferReasonList>();
            TransferReasonListRoot transferReasonListRoot = new TransferReasonListRoot();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetReasonsListAPI);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        transferReasonListRoot = new JavaScriptSerializer().Deserialize<TransferReasonListRoot>(json);

                        if (transferReasonListRoot.data != null && transferReasonListRoot.data.Count > 0)
                            obj = transferReasonListRoot.data;
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return obj;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, obj, response, SessionHelper.CurrentSession.UserName);
            }

        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get business list.
        /// </summary>
        /// <returns>Get question list.</returns>
        public IEnumerable<TransferQuestionListList> GetQuestionList()
        {
            string json = "";
            IEnumerable<TransferQuestionListList> obj = new List<TransferQuestionListList>();
            TransferQuestionListRoot transferQuestionListRoot = new TransferQuestionListRoot();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetQuestionsListAPI);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        transferQuestionListRoot = new JavaScriptSerializer().Deserialize<TransferQuestionListRoot>(json);

                        if (transferQuestionListRoot.data != null && transferQuestionListRoot.data.Count > 0)
                            obj = transferQuestionListRoot.data;
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return obj;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, obj, response, SessionHelper.CurrentSession.UserName);
            }
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get transfer type list.
        /// </summary>
        /// <returns>Get transfer type list.</returns>
        public IEnumerable<TransferTypeListList> GetTransferTypeList()
        {
            string json = "";
            IEnumerable<TransferTypeListList> obj = new List<TransferTypeListList>();
            TransferTypeListListRoot transferTypeListListRoot = new TransferTypeListListRoot();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetTransferTypesListAPI);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        transferTypeListListRoot = new JavaScriptSerializer().Deserialize<TransferTypeListListRoot>(json);

                        if (transferTypeListListRoot.data != null && transferTypeListListRoot.data.Count > 0)
                            obj = transferTypeListListRoot.data;
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return obj;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, obj, response, SessionHelper.CurrentSession.UserName);
            }
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Save TC request.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string SaveTCRequest(TCRequestEdit data)
        {
            string json = "";
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, SaveTCRequestDataAPI);
            try
            {

                TCRequest tCRequest = new TCRequest();
                tCRequest.ISGemsTC = data.ISGemsTC;
                tCRequest.LastAttDate = data.LastAttDate;
                tCRequest.LeaveDate = DateTime.Now.Date.ToString();
                tCRequest.StudentID = data.StudentID;
                tCRequest.TotalDays = data.TotalDays;
                tCRequest.TotalPresentDays = data.TotalPresentDays;
                tCRequest.TransferBSUID = data.TransferBSUID;
                tCRequest.TransferFeedback = data.TransferFeedback;
                tCRequest.TransferReasonID = data.TransferReasonID;
                tCRequest.TransferTypeID = data.TransferTypeID;
                tCRequest.TransferZone = data.TransferZone;
                var obj = new JavaScriptSerializer().Serialize(data);
                HttpContent content = new StringContent(obj, Encoding.UTF8, "application/json");
                httpRequestMessage.Content = content;
                httpRequestMessage.Headers.Add("data", obj);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        TCExitQuestion tCExitQuestion = new TCExitQuestion();
                        tCExitQuestion.StudentID = data.StudentID;
                        tCExitQuestion.IsOnlineTransfer = data.IsOnlineTransfer;
                        tCExitQuestion.Feedback = data.TransferFeedback;
                        tCExitQuestion.AnswerList = data.AnswerList;
                        string result = SaveTCExitQuestion(tCExitQuestion);
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Save TC request.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string SaveTCExitQuestion(TCExitQuestion data)
        {
            string json = "";
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, SaveTCExitQuestionDataAPI);
            try
            {
                var obj = new JavaScriptSerializer().Serialize(data);
                HttpContent content = new StringContent(obj, Encoding.UTF8, "application/json");
                httpRequestMessage.Content = content;
                httpRequestMessage.Headers.Add("data", obj);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }
        }

        /// <summary>
        /// Author:Tejalben
        /// Summery:Get tc request data by student id.
        /// Date:30 July 2020
        /// </summary>
        /// <param name="studentNo"></param>
        /// <returns></returns>
        public StudentTcDetailsRoot GetTcRequestStatusByStudentNo(string studentNo)
        {
            string json = "";
            StudentTcDetailsRoot studentTcDetailsRoot = new StudentTcDetailsRoot();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetTCRequestStatusByStudentNoAPI);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {

                httpRequestMessage.Headers.Add("stuno", studentNo);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        studentTcDetailsRoot = new JavaScriptSerializer().Deserialize<StudentTcDetailsRoot>(json);
                    }
                }
                return studentTcDetailsRoot;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return studentTcDetailsRoot;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, studentTcDetailsRoot, response, SessionHelper.CurrentSession.UserName);
            }
        }
    }
}