﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class SetSubjectService : ISetSubjectService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixBaseUriParentCorner"];
        //readonly string _path = "api/v1/TimeTable";

        //readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        //readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        //readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        //readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        //readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];

        //readonly string GetExamPaperListAPI = ConfigurationManager.AppSettings["GetExamPaperListAPI"];
        //readonly string GetSubjectWiseAPI = ConfigurationManager.AppSettings["GetSubjectWiseAPI"];
        //readonly string SaveExamPaperAPI = ConfigurationManager.AppSettings["SaveExamPaperAPI"];
        //readonly string RemoveExamPaperAPI = ConfigurationManager.AppSettings["RemoveExamPaperAPI"];
        //readonly string ExamApprovalRequestAPI = ConfigurationManager.AppSettings["ExamApprovalRequestAPI"];
        //readonly string GetSelectedPaperDetailsAPI = ConfigurationManager.AppSettings["GetSelectedPaperDetailsAPI"];
        private IPhoenixAPIParentCornerService _phoenixAPIService;
        #endregion
        private ILoggerClient _loggerClient;
        public SetSubjectService(IPhoenixAPIParentCornerService phoenixAPIService)
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
            _loggerClient = LoggerClient.Instance;
            _phoenixAPIService = phoenixAPIService;
        }

        public IEnumerable<SubjectDetails> GetSubjectList(string StudentNumber)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            SubjectDetailsRoot subjectRoot = new SubjectDetailsRoot();
            List<SubjectDetails> subjectList = new List<SubjectDetails>();           
            string strAPIUrl = _phoenixAPIService.GetExamPaperListAPI;
            try
            {

                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, strAPIUrl); // GetExamPaperListAPI
                httpRequestMessage.Headers.Add("StudentID", StudentNumber);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);

                _loggerClient.LogWarning("GetSubjectList");
                _loggerClient.LogWarning("StudentNumber - " + StudentNumber.ToString());
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        _loggerClient.LogWarning(json);
                        subjectRoot = new JavaScriptSerializer().Deserialize<SubjectDetailsRoot>(json);
                        if (subjectRoot.data != null && subjectRoot.data.Count > 0)
                            subjectList = subjectRoot.data;
                    }
                }
                return subjectList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in SetSubjectService.GetSubjectList()" + ex.Message);
                return subjectList;
            }
            finally
            {
                _phoenixAPIService.InsertLogDetails(httpRequestMessage, subjectList, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public IEnumerable<StudentDetails> GetExamPaperList(string StudentNumber)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            StudentDetailsRoot examRoot = new StudentDetailsRoot();
            List<StudentDetails> examPaperList = new List<StudentDetails>();
            string strAPIUrl = _phoenixAPIService.GetSelectedPaperDetailsAPI;
            try
            {
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, strAPIUrl);//GetSelectedPaperDetailsAPI
                _loggerClient.LogWarning("GetExamPaperList");
                _loggerClient.LogWarning("StudentNumber - " + StudentNumber.ToString());
                httpRequestMessage.Headers.Add("StudentID", StudentNumber);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        _loggerClient.LogWarning(json);
                        examRoot = new JavaScriptSerializer().Deserialize<StudentDetailsRoot>(json);

                        if (examRoot.data != null && examRoot.data.Count > 0)
                            examPaperList = examRoot.data;
                    }
                }
                return examPaperList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in SetSubjectService.GetExamPaperList()" + ex.Message);
                return examPaperList;
            }
            finally
            {
                _phoenixAPIService.InsertLogDetails(httpRequestMessage, examPaperList, response, SessionHelper.CurrentSession.UserName);
            }
        }

        public string SaveSubjectSelection(string data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            try
            {
                string strAPIUrl = _phoenixAPIService.SaveExamPaperAPI;

                //SaveExamPaperAPI
                using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, strAPIUrl))
                {
                    HttpContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    httpRequestMessage.Content = content;
                    httpRequestMessage.Headers.Add("data", data);
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                    using (response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            json = response.Content.ReadAsStringAsync().Result;
                        }
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in SetSubjectService.GetExamPaperList()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public string RemoveSubjectSelection(string data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            string strAPIUrl = _phoenixAPIService.RemoveExamPaperAPI;
            try
            {
                using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, strAPIUrl))
                {
                    HttpContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    httpRequestMessage.Content = content;
                    httpRequestMessage.Headers.Add("data", data);
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                    using (response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            json = response.Content.ReadAsStringAsync().Result;
                        }
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in SetSubjectService.RemoveSubjectSelection()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }

        }

        public string ExamSetupRequestForApproval(string data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            string strAPIUrl = _phoenixAPIService.ExamApprovalRequestAPI;
            try
            {
                using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, strAPIUrl))
                {
                    HttpContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    httpRequestMessage.Content = content;
                    httpRequestMessage.Headers.Add("data", data);
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                    using (response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            json = response.Content.ReadAsStringAsync().Result;
                        }
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in SetSubjectService.ExamSetupRequestForApproval()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }

        }

        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            return JsonDeserilize;
        }

    }
}