﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.ViewModels;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Utils.OAuth;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolGroupService : ISchoolGroupService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/schoolgroup";
        #endregion

        public SchoolGroupService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool DeleteSchoolGroup(int id, long UserId = 0)
        {
            var uri = API.SchoolGroup.DeleteSchoolGroup(_path, id, UserId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;

            return response.IsSuccessStatusCode;
        }

        public SchoolGroup GetSchoolGroupByID(int id)
        {
            var result = new SchoolGroup();
            var uri = API.SchoolGroup.GetSchoolGroupById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, SchoolGroup>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroups(int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string SearchString = "")
        {
            var result = new List<SchoolGroup>();
            int userId = (int)SessionHelper.CurrentSession.Id;
            var uri = API.SchoolGroup.GetSchoolGroups(_path, userId, pageNumber, pageSize, IsBeSpokeGroup, SearchString);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public async Task<IEnumerable<SchoolGroup>> GetOtherschoolgroups(int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string SearchString = "")
        {
            var result = new List<SchoolGroup>();
            int userId = (int)SessionHelper.CurrentSession.Id;
            var uri = API.SchoolGroup.GetSchoolGroups(_path, userId, pageNumber, pageSize, IsBeSpokeGroup, SearchString);
            HttpResponseMessage response = await _client.GetAsyncExt(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        
        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupsForAdminSpace(SchoolGroupsReportRequest model)
        {
            IEnumerable<SchoolGroup> schoolGroupsList = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetSchoolGroupsForAdminSpace(_path);
            HttpResponseMessage response = await _client.PostAsJsonAsync(uri, model);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schoolGroupsList = EntityMapper<string, IEnumerable<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return schoolGroupsList;
        }

        public IEnumerable<Course> GetCoursesBySchoolId(long schoolId)
        {
            var result = new List<Course>();
            var uri = API.SchoolGroup.GetCoursesBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Course>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SchoolGroup> GetSchoolGroupsByStudentId(long studentId)
        {
            var result = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetSchoolGroupsByStudentId(_path, studentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public async Task<IEnumerable<SchoolGroup>> GetStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "")
        {
            var result = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetStudentGroups(_path, studentId, PageNumber, PageSize, IsBespokeGroup, SearchString);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public async Task<IEnumerable<SchoolGroup>> GetOtherschoolStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "")
        {
            var result = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetStudentGroups(_path, studentId, PageNumber, PageSize, IsBespokeGroup, SearchString);
            HttpResponseMessage response = await _client.GetAsyncExt(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        
        public bool UpdateSchoolGroup(SchoolGroupEdit model, string studentIdsToAdd, string studentIdsToRemove, string memberstoAdd, string memberstoRemove)
        {
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new SchoolGroup();
            EntityMapper<SchoolGroupEdit, SchoolGroup>.Map(model, sourceModel);
            sourceModel.StudentIdsToAdd = studentIdsToAdd;
            sourceModel.StudentIdsToRemove = studentIdsToRemove;
            sourceModel.MembersToAdd = memberstoAdd;
            sourceModel.MembersToRemove = memberstoRemove;
            sourceModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            if (model.IsAddMode)
                uri = API.SchoolGroup.InsertGroup(_path);
            else
                uri = API.SchoolGroup.UpdateSchoolGroup(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }
        public int UpdateSchoolGroupDetails(SchoolGroupEdit model)
        {
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new SchoolGroup();
            EntityMapper<SchoolGroupEdit, SchoolGroup>.Map(model, sourceModel);
            sourceModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            sourceModel.CreatedById = SessionHelper.CurrentSession.Id;
            sourceModel.IsActive = true;
            if (model.IsAddMode)
                uri = API.SchoolGroup.InsertGroup(_path);
            else
                uri = API.SchoolGroup.UpdateSchoolGroup(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            //   return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var groupId = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
                return groupId;
            }
            return 0;
        }

        public IEnumerable<SchoolGroup> GetSubjectSchoolGroups(int subjectId, long schoolId)
        {
            var result = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetSubjectSchoolGroup(_path, subjectId, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool UpdateTeacherGivenName(SchoolGroupEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new SchoolGroup();
            EntityMapper<SchoolGroupEdit, SchoolGroup>.Map(model, sourceModel);
            uri = API.SchoolGroup.UpdateTeacherGivenName(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<SchoolGroup> GetSchoolGroupsBySchoolId(long schoolId)
        {
            var result = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetGroupsBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SchoolGroup> GetSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            var result = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetGroupsByUserId(_path, userId, isTeacher);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SchoolLevel> GetAllSchoolLevel(long schoolId)
        {
            var result = new List<SchoolLevel>();
            var uri = API.SchoolGroup.GetAllSchoolLevel(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolLevel>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }


        public IEnumerable<SchoolCourse> GetAllCourses(long schoolId, long schoolLevelId, long departmentlId)
        {
            var result = new List<SchoolCourse>();
            var uri = API.SchoolGroup.GetAllCourses(_path, schoolId, schoolLevelId, departmentlId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolCourse>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }


        public IEnumerable<ListItem> GetUnassignedMembers(int schoolId, long userId, int? userTypeId, int schoolGroupId, string selectedGroupIds)
        {
            var result = new List<ListItem>();
            var uri = "";
            if (userTypeId is null)
            {
                uri = API.SchoolGroup.GetUnassignedMembersWithSelectedGroups(_path, LocalizationHelper.CurrentSystemLanguageId, schoolId, userId, userTypeId, schoolGroupId, selectedGroupIds);
            }
            else
            {
                uri = API.SchoolGroup.GetUnassignedMembers(_path, LocalizationHelper.CurrentSystemLanguageId, schoolId, userId, userTypeId, schoolGroupId);
            }
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<ListItem>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool AddUpdateMembersToGroup(List<GroupMemberMapping> newMembers)
        {
            bool result = false;
            var uri = string.Empty;
            var postData = JsonConvert.SerializeObject(newMembers);
            var sourceModel = new List<GroupMemberMapping>();
            uri = API.SchoolGroup.AddUpdateMembersToGroup(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, newMembers).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<GroupMemberMapping> GetAssignedMembers(int schoolGroupId)
        {

            var result = new List<GroupMemberMapping>();
            var uri = API.SchoolGroup.GetAssignedMembers(_path, LocalizationHelper.CurrentSystemLanguageId, schoolGroupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupMemberMapping>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<GroupMessage> GetGroupMessageListByGroup(int groupId, long id)
        {

            var result = new List<GroupMessage>();
            var uri = API.SchoolGroup.GetGroupMessageListByGroup(_path, groupId, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupMessage>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public bool InsertGroupMessage(GroupMessageEdit model)
        {
            var uri = string.Empty;
            model.CreatedBy = Convert.ToInt64(SessionHelper.CurrentSession.Id);
            var sourceModel = new GroupMessage();
            EntityMapper<GroupMessageEdit, GroupMessage>.Map(model, sourceModel);
            sourceModel.MemberIds = model.SelectedStudentId;
            uri = API.SchoolGroup.InsertGroupMessage(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<SchoolGroup> GetSchoolGroupsHavingBlogs(long id)
        {
            var result = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetSchoolGroupsHavingBlogs(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public bool UpdateLastSeen(long userId, int schoolGroupId, bool isParent)
        {
            var uri = string.Empty;
            uri = API.SchoolGroup.UpdateLastSeenDate(_path, userId, schoolGroupId, isParent);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }
        public bool UpdateArchiveGroups(long schoolGroupId, long teacherId)
        {
            var uri = string.Empty;
            uri = API.SchoolGroup.UpdateArchiveSchoolGroup(_path, schoolGroupId, teacherId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }
        public SchoolGroup GetArchivedSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            var result = new SchoolGroup();
            var uri = API.SchoolGroup.GetArchivedGroupsByUserId(_path, userId, isTeacher);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, SchoolGroup>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public SchoolGroup GetActiveSchoolGroupsByUserId(long teacherId, bool isTeacher)
        {
            var result = new SchoolGroup();
            var uri = API.SchoolGroup.GetActiveGroupsByUserId(_path, teacherId, isTeacher);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, SchoolGroup>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SchoolGroup> GetArchivedSchoolGroups(int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string SearchString = "")
        {
            var result = new List<SchoolGroup>();
            int userId = (int)SessionHelper.CurrentSession.Id;
            var uri = API.SchoolGroup.GetArchivedSchoolGroups(_path, userId, pageNumber, pageSize, IsBeSpokeGroup, SearchString);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool UpdateGroupMeeting(MeetingResponse model)
        {
            var uri = API.SchoolGroup.UpdateGroupMeeting(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public bool RemoveGroupMeeting(string meetingId)
        {
            SchoolGroup schoolGroup = new SchoolGroup();
            schoolGroup.MeetingRoomId = meetingId;
            var uri = API.SchoolGroup.RemoveGroupMeeting(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, schoolGroup).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateTeamsMeetingInfo(MSTeamsReponse meetingInfo)
        {
            var uri = API.SchoolGroup.UpdateTeamsMeetingInfo(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, meetingInfo).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<MSTeamsReponse> GetAllTeamsMeetings(int schoolGroupId)
        {
            IEnumerable<MSTeamsReponse> meetingsList = new List<MSTeamsReponse>();
            var uri = API.SchoolGroup.GetAllTeamsMeetings(_path, schoolGroupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                meetingsList = EntityMapper<string, List<MSTeamsReponse>>.MapFromJson(jsonDataProviders);
            }
            return meetingsList;
        }

        public bool UpdateZoomMeetingInfo(ZoomMeetingResponse model)
        {
            var uri = API.SchoolGroup.UpdateZoomMeetingInfo(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<ZoomMeetingResponse> GetAllZoomMeetings(int schoolGroupId, long userId, string meetingId)
        {
            IEnumerable<ZoomMeetingResponse> meetingsList = new List<ZoomMeetingResponse>();
            var uri = API.SchoolGroup.GetAllZoomMeetings(_path, schoolGroupId, meetingId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                meetingsList = EntityMapper<string, List<ZoomMeetingResponse>>.MapFromJson(jsonDataProviders);
            }
            return meetingsList;
        }

        public bool UpdateZoomMeetingParticipants(List<ZoomMeetingResponse> meetingParticipants)
        {
            var uri = API.SchoolGroup.UpdateZoomMeetingParticipants(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, meetingParticipants).Result;
            return response.IsSuccessStatusCode;
        }

        public bool SaveGroupAdobeMeeting(AdobeConnectMeeting meetingInfo)
        {
            var uri = API.SchoolGroup.SaveGroupAdobeMeeting(_path);
            HttpResponseMessage httpResponse = _client.PostAsJsonAsync(uri, meetingInfo).Result;
            return httpResponse.IsSuccessStatusCode;
        }

        public IEnumerable<AdobeConnectMeeting> GetAllAdobeMeetings(int schoolGroupId)
        {
            IEnumerable<AdobeConnectMeeting> meetingsList = new List<AdobeConnectMeeting>();
            var uri = API.SchoolGroup.GetAllAdobeMeetings(_path, schoolGroupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                meetingsList = EntityMapper<string, List<AdobeConnectMeeting>>.MapFromJson(jsonDataProviders);
            }
            return meetingsList;
        }

        public IEnumerable<MeetingResponse> GetAllGroupBBBMeetings(int schoolGroupId)
        {
            IEnumerable<MeetingResponse> meetingList = new List<MeetingResponse>();
            var uri = API.SchoolGroup.GetAllGroupBBBMeetings(_path, schoolGroupId);
            HttpResponseMessage responseMessage = _client.GetAsync(uri).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = responseMessage.Content.ReadAsStringAsync().Result;
                meetingList = EntityMapper<string, List<MeetingResponse>>.MapFromJson(jsonDataProviders);
            }
            return meetingList;
        }

        public SendEmailNotificationView GenerateEmailTemplateForSendGroupMessage(string title, string message, string studentInternalIds, string notifyTo, string attachmentFiles)
        {
            string EmailBody = "";
            System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(Constants.GroupMessageTemplate), Encoding.UTF8);
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
            EmailBody = EmailBody.Replace("@@Message", message);
            EmailBody = EmailBody.Replace("@@Teacher", SessionHelper.CurrentSession.FullName);
            EmailBody = EmailBody.Replace("@@Attachments", attachmentFiles);

            SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
            sendEmailNotificationView.FromMail = Constants.NoReplyMail;
            sendEmailNotificationView.LogType = "SendGroupMessage";
            sendEmailNotificationView.StudentId = studentInternalIds;
            sendEmailNotificationView.Subject = title;
            sendEmailNotificationView.Message = EmailBody;
            sendEmailNotificationView.NotifyTo = notifyTo;
            sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;

            return sendEmailNotificationView;
        }

        public MeetingResponse GetGroupBBBMeetingInfo(string meetingId)
        {
            MeetingResponse meetingInfo = new MeetingResponse();
            var uri = API.SchoolGroup.GetGroupBBBMeetingInfo(_path, meetingId);
            HttpResponseMessage httpResponseMessage = _client.GetAsync(uri).Result;
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var jsonResult = httpResponseMessage.Content.ReadAsStringAsync().Result;
                meetingInfo = EntityMapper<string, IEnumerable<MeetingResponse>>.MapFromJson(jsonResult).FirstOrDefault();
            }
            return meetingInfo;
        }

        public IEnumerable<EventView> GetCurrentGroupMeeting(long id)
        {
            IEnumerable<EventView> meetingList = new List<EventView>();
            var uri = API.SchoolGroup.GetCurrentGroupMeeting(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonResult = response.Content.ReadAsStringAsync().Result;
                meetingList = EntityMapper<string, IEnumerable<EventView>>.MapFromJson(jsonResult);
            }
            return meetingList;
        }

        #region Group WebEx Meeting
        public bool UpdateGroupWebExMeeting(WebExMeeting meetingInfo)
        {
            var uri = API.SchoolGroup.UpdateGroupWebExMeeting(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, meetingInfo.Body.BodyContent).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<WebExMeetingData> GetAllGroupWebExMeetings(int schoolGroupId)
        {
            IEnumerable<WebExMeetingData> meetingList = new List<WebExMeetingData>();
            var uri = API.SchoolGroup.GetAllGroupWebExMeetings(_path, schoolGroupId);
            HttpResponseMessage responseMessage = _client.GetAsync(uri).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonResult = responseMessage.Content.ReadAsStringAsync().Result;
                meetingList = EntityMapper<string, IEnumerable<WebExMeetingData>>.MapFromJson(jsonResult);
            }
            return meetingList;
        }
        #endregion


        public IEnumerable<SchoolGroup> GetSchoolGroupWithPagination(long id, short pageSize, int pageIndex)
        {
            IEnumerable<SchoolGroup> schoolGroups = new List<SchoolGroup>();
            var uri = API.SchoolGroup.GetSchoolGroupWithPagination(_path, id, pageSize, pageIndex);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonResult = response.Content.ReadAsStringAsync().Result;
                schoolGroups = EntityMapper<string, IEnumerable<SchoolGroup>>.MapFromJson(jsonResult);
            }
            return schoolGroups;
        }

        public IEnumerable<UserEmailAccountView> GetSchoolGroupUserEmailAddress(string schoolGroupIds, string studentIds)
        {
            IEnumerable<UserEmailAccountView> userEmailAccountList = new List<UserEmailAccountView>();
            var uri = API.SchoolGroup.GetSchoolGroupUserEmailAddress(_path, schoolGroupIds, studentIds);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonResult = response.Content.ReadAsStringAsync().Result;
                userEmailAccountList = EntityMapper<string, IEnumerable<UserEmailAccountView>>.MapFromJson(jsonResult);
            }
            return userEmailAccountList;
        }

        public ZoomMeetingResponse CreateSynchronousLesson(ZoomMeetingEdit model)
        {
            var meetingInfo = new ZoomMeetingView();
            var meetingResponse = new ZoomMeetingResponse();
            EntityMapper<ZoomMeetingEdit, ZoomMeetingView>.Map(model, meetingInfo);
            var uri = API.SchoolGroup.CreateSynchounousLesson(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, meetingInfo).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                meetingResponse = EntityMapper<string, ZoomMeetingResponse>.MapFromJson(jsonDataProviders);
            }
            return meetingResponse;
        }

        public IEnumerable<RecordedSessionView> GetGroupRecordedSessions(int groupId, int pageIndex, int pageSize, string searchText, string fromDate, string toDate)
        {
            IEnumerable<RecordedSessionView> recordedSessions = new List<RecordedSessionView>();
            var uri = API.SchoolGroup.GetGroupRecordedSessions(_path, groupId, pageIndex, pageSize, searchText, fromDate, toDate);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                recordedSessions = EntityMapper<string, IEnumerable<RecordedSessionView>>.MapFromJson(jsonDataProviders);
            }
            return recordedSessions;
        }

        public IEnumerable<ZoomMeetingView> GetAllLiveSessions(long userId, int pageIndex, int pageSize, string searchText, string type, string groupId)
        {
            IEnumerable<ZoomMeetingView> liveSessions = new List<ZoomMeetingView>();
            var uri = API.SchoolGroup.GetAllLiveSessions(_path, userId, pageIndex, pageSize, searchText, type, groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                liveSessions = EntityMapper<string, IEnumerable<ZoomMeetingView>>.MapFromJson(jsonDataProviders);
            }
            return liveSessions;
        }

        public ZoomMeetingView GenerateZoomMeetingJoinURL(ZoomMeetingView model)
        {
            var responseModel = new ZoomMeetingView();
            var uri = API.SchoolGroup.GenerateZoomMeetingJoinURL(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                responseModel = EntityMapper<string, ZoomMeetingView>.MapFromJson(jsonDataProviders);
            }
            return responseModel;
        }

        public MeetingJoinResponse GetUserLiveSessionInfo(ZoomMeetingJoinView model)
        {
            var meetingInfo = new MeetingJoinResponse();
            var uri = API.School.GetUserLiveSessionInfo(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                meetingInfo = EntityMapper<string, MeetingJoinResponse>.MapFromJson(jsonDataProviders);
            }
            return meetingInfo;
        }

        public async Task<bool> DeleteLiveSession(ZoomMeetingView liveSession)
        {
            var uri = API.SchoolGroup.DeleteLiveSession(_path);
            HttpResponseMessage response = await _client.PostAsJsonAsync(uri, liveSession);
            return response.IsSuccessStatusCode;

        }

        public async Task<bool> DeleteTeamsSession(MSTeamsReponse model)
        {
            var uri = API.SchoolGroup.DeleteTeamsSession(_path);
            HttpResponseMessage response = await _client.PostAsJsonAsync(uri, model);
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<GroupMemberMapping> GetUsersMailingDetails(string userIds)
        {
            var uri = API.SchoolGroup.GetUsersMailingDetails(_path, userIds);
            IEnumerable<GroupMemberMapping> list = new List<GroupMemberMapping>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<GroupMemberMapping>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public bool InsertGroupPrimaryTeacher(GroupPrimaryTeacher model)
        {
            var uri = API.SchoolGroup.InsertGroupPrimaryTeacher(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            
            return response.IsSuccessStatusCode;
        }

        public bool DeleteGroupPrimaryTeacher(long id)
        {
            var uri = API.SchoolGroup.DeleteGroupPrimaryTeacher(_path, id);
            HttpResponseMessage response = _client.DeleteAsyncExt(uri).Result;

            return response.IsSuccessStatusCode;
        }

        public bool UpdateSchoolGroupsToHideClassGroups(SchoolGroupEdit model)
        {
            var sourceModel = new SchoolGroup();
            EntityMapper<SchoolGroupEdit, SchoolGroup>.Map(model, sourceModel);
            sourceModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            sourceModel.CreatedById = SessionHelper.CurrentSession.Id;

            var uri = API.SchoolGroup.UpdateSchoolGroupsToHide(_path);
            HttpResponseMessage response = _client.PostAsJsonAsyncExt(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public int CheckStudentGroupsAvailable(long studentId, int IsBespokeGroup = 0)
        {
            var uri = API.SchoolGroup.CheckStudentGroupsAvailable(_path, studentId, IsBespokeGroup);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public IEnumerable<long> GetDisabledGroupList(long BlogId, long SchoolId) {
            var uri = API.SchoolGroup.GetDisabledGroupList(_path, BlogId, SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            IEnumerable<long> list = new List<long>();
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<long>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public int UpdateBlogWithNewUpdate(long UpdateBlogId, long UpdatFromBlogId, bool IsApproved, bool IsRejected) {
            var uri = API.SchoolGroup.UpdateBlogWithNewUpdate(_path, UpdateBlogId, UpdatFromBlogId, IsApproved, IsRejected);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
    }
}