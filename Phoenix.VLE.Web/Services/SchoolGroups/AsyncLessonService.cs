﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.ViewModels;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Utils.OAuth;

namespace Phoenix.VLE.Web.Services
{
    public class AsyncLessonService : IAsyncLessonService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/asynclesson";
        #endregion

        public AsyncLessonService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public int InsertAsyncLesson(AsyncLesson entity)
        {
            var result = 0;
            var uri = API.AsyncLesson.InsertAsyncLesson(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
            }
            return result;
        }

        public int UpdateAsyncLesson(AsyncLesson entity)
        {
            var result = 0;
            var uri = API.AsyncLesson.UpdateAsyncLesson(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
            }
            return result;
        }

        public int DeleteAsyncLesson(long id, long userId)
        {
            var uri = API.AsyncLesson.DeleteAsyncLesson(_path, id, userId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
                return result;
            }
            return 0;
        }

        public ViewAsyncLessonDetail GetAsyncLessons(long? asyncLessonId, long? sectionId, long? moduleId, long? folderId, bool? isActive, int? IsIncludeResources, int? IsIncludeStudents)
        {
            var result = new ViewAsyncLessonDetail();
            var uri = API.AsyncLesson.GetAsyncLessons(_path, asyncLessonId, sectionId, moduleId, folderId, isActive, IsIncludeResources, IsIncludeStudents);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, ViewAsyncLessonDetail>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public AsyncLesson GetAsyncLessonById(long asynLessonId, bool? isActive)
        {
            var result = new AsyncLesson();
            var uri = API.AsyncLesson.GetAsyncLessonById(_path, asynLessonId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, AsyncLesson>.MapFromJson(jsonDataProviders);
            }
            return result;
        }


        public int InsertResourceActivity(IList<AsyncLessonResourcesActivities> model)
        {
            var result = 0;
            var uri = API.AsyncLesson.InsertResourceActivity(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            if (response.IsSuccessStatusCode)
            {
                result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
            }
            return result;
        }

        public int UpdateResourceActivity(AsyncLessonResourcesActivities entity)
        {
            var result = 0;
            var uri = API.AsyncLesson.UpdateResourceActivity(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
            }
            return result;
        }

        public int DeleteResourceActivity(long id, long userId)
        {
            var uri = API.AsyncLesson.DeleteResouceActivity(_path, id, userId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
                return result;
            }
            return 0;
        }

        public IEnumerable<AsyncLessonResourcesActivities> GetResourceActivity(long asyncLessonId, long userId, bool? isActive)
        {
            var result = new List<AsyncLessonResourcesActivities>();
            var uri = API.AsyncLesson.GetResourcesActivities(_path, asyncLessonId, userId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<AsyncLessonResourcesActivities>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public long UpdateStudentMapping(AsyncLessonStudentMapping entity)
        {
            var result = 0;
            var uri = API.AsyncLesson.UpdateStudentMapping(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
            }
            return result;
        }

        public IEnumerable<AsyncLessonStudentMapping> GetAsyncLessonStudentMappingByAsyncLessonId(long asyncLessonId,long? userId, bool? isActive)
        {
            var result = new List<AsyncLessonStudentMapping>();
            var uri = API.AsyncLesson.GetAsyncLessonStudentMappingByAsyncLessonId(_path, asyncLessonId, userId,isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<AsyncLessonStudentMapping>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int InsertComment(AsyncLessonComments entity)
        {
            var result = 0;
            var uri = API.AsyncLesson.InsertComment(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
            }
            return result;
        }

        public int UpdateComment(AsyncLessonComments entity)
        {
            var result = 0;
            var uri = API.AsyncLesson.UpdateComment(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
            }
            return result;
        }

        public int DeleteComment(long id, long userId)
        {
            var uri = API.AsyncLesson.DeleteComment(_path, id, userId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
                return result;
            }
            return 0;
        }

        public IEnumerable<AsyncLessonComments> GetAsyncLessonComments(long? asyncLessonCommentId, long? resourceActivityId, long studentId, long asyncLessonId)
        {
            var result = new List<AsyncLessonComments>();
            var uri = API.AsyncLesson.GetAsyncLessonComments(_path, asyncLessonCommentId, resourceActivityId, studentId, asyncLessonId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<AsyncLessonComments>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int UpdateResourceActivityStatus(UpdateAsyncLessonResourcesActivity model)
        {
            var result = 0;
            var uri = API.AsyncLesson.UpdateResouceActivityStatus(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            if (response.IsSuccessStatusCode)
            {
                result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
            }
            return result;
        }
    }
}