﻿using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Phoenix.VLE.Web.Services
{
    public interface IAsyncLessonService
    {
        int InsertAsyncLesson(AsyncLesson entity);
        int UpdateAsyncLesson(AsyncLesson entity);
        int DeleteAsyncLesson(long id, long userId);
        ViewAsyncLessonDetail GetAsyncLessons(long? asyncLessonId, long? sectionId, long? moduleId, long? folderId, bool? isActive, int? IsIncludeResources, int? IsIncludeStudents);
        AsyncLesson GetAsyncLessonById(long asynLessonId, bool? isActive);

        int InsertResourceActivity(IList<AsyncLessonResourcesActivities> model);
        int UpdateResourceActivity(AsyncLessonResourcesActivities entity);
        int DeleteResourceActivity(long id, long userId);
        IEnumerable<AsyncLessonResourcesActivities> GetResourceActivity(long asyncLessonId, long userId, bool? isActive);
        int UpdateResourceActivityStatus(UpdateAsyncLessonResourcesActivity model);

        long UpdateStudentMapping(AsyncLessonStudentMapping entity);
        IEnumerable<AsyncLessonStudentMapping> GetAsyncLessonStudentMappingByAsyncLessonId(long asyncLessonId,long? userId, bool? isActive);

        int InsertComment(AsyncLessonComments entity);
        int UpdateComment(AsyncLessonComments entity);
        int DeleteComment(long id, long userId);
        IEnumerable<AsyncLessonComments> GetAsyncLessonComments(long? asyncLessonCommentId, long? resourceActivityId, long studentId, long asyncLessonId);
    }
}
