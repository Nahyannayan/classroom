﻿using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISchoolGroupService
    {
        Task<IEnumerable<SchoolGroup>> GetSchoolGroups(int PageNumber, int PageSize, int IsBeSpokeGroup = 0, string SearchString = "");
        Task<IEnumerable<SchoolGroup>> GetOtherschoolgroups(int PageNumber, int PageSize, int IsBeSpokeGroup = 0, string SearchString = "");
        
        IEnumerable<SchoolGroup> GetSchoolGroupsByStudentId(long studentId);
        Task<IEnumerable<SchoolGroup>> GetStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "");
        Task<IEnumerable<SchoolGroup>> GetOtherschoolStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "");
        SchoolGroup GetSchoolGroupByID(int id);
        bool UpdateSchoolGroup(SchoolGroupEdit model, string studentIdsToAdd, string studentIdsToRemove, string memberstoAdd, string memberstoRemove);
        bool DeleteSchoolGroup(int id, long UserId = 0);
        IEnumerable<SchoolGroup> GetSubjectSchoolGroups(int subjectId, long schoolId);
        bool UpdateTeacherGivenName(SchoolGroupEdit model);

        IEnumerable<SchoolGroup> GetSchoolGroupsBySchoolId(long schoolId);
        int UpdateSchoolGroupDetails(SchoolGroupEdit model);
        IEnumerable<SchoolGroup> GetSchoolGroupsByUserId(long userId, bool isTeacher);
        IEnumerable<ListItem> GetUnassignedMembers(int schoolId, long userId, int? userTypeId, int schoolGroupId, string selectedgroupIds);
        bool AddUpdateMembersToGroup(List<GroupMemberMapping> newMembers);
        IEnumerable<GroupMemberMapping> GetAssignedMembers(int groupId);
        bool InsertGroupMessage(GroupMessageEdit groupMessage);
        IEnumerable<SchoolGroup> GetSchoolGroupsHavingBlogs(long id);
        IEnumerable<GroupMessage> GetGroupMessageListByGroup(int groupId, long id);
        bool UpdateLastSeen(long userId, int schoolGroupId, bool isParent);
        bool UpdateArchiveGroups(long schoolGroupId, long teacherId);
        SchoolGroup GetArchivedSchoolGroupsByUserId(long userId, bool isTeacher);
        SchoolGroup GetActiveSchoolGroupsByUserId(long userId, bool isTeacher);
        IEnumerable<SchoolGroup> GetArchivedSchoolGroups(int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string SearchString = "");
        bool UpdateGroupMeeting(MeetingResponse model);
        bool RemoveGroupMeeting(string meetingId);
        bool UpdateTeamsMeetingInfo(MSTeamsReponse meetingInfo);
        IEnumerable<MSTeamsReponse> GetAllTeamsMeetings(int schoolGroupId);
        bool UpdateZoomMeetingInfo(ZoomMeetingResponse model);
        IEnumerable<ZoomMeetingResponse> GetAllZoomMeetings(int schoolGroupId, long userId, string meetingId = "");
        bool SaveGroupAdobeMeeting(AdobeConnectMeeting meetingInfo);
        IEnumerable<AdobeConnectMeeting> GetAllAdobeMeetings(int schoolGroupId);
        IEnumerable<MeetingResponse> GetAllGroupBBBMeetings(int schoolGroupId);
        SendEmailNotificationView GenerateEmailTemplateForSendGroupMessage(string title, string message, string studentInternalIds,string notifyTo, string attachmentFiles);
        MeetingResponse GetGroupBBBMeetingInfo(string meetingId);
        IEnumerable<EventView> GetCurrentGroupMeeting(long id);
        bool UpdateGroupWebExMeeting(WebExMeeting meetingInfo);
        IEnumerable<WebExMeetingData> GetAllGroupWebExMeetings(int schoolGroupId);
        IEnumerable<SchoolGroup> GetSchoolGroupWithPagination(long id, short pageSize, int pageIndex);
        bool UpdateZoomMeetingParticipants(List<ZoomMeetingResponse> meetingParticipants);
        IEnumerable<UserEmailAccountView> GetSchoolGroupUserEmailAddress(string schoolGroupIds, string studentIds);

        IEnumerable<SchoolLevel> GetAllSchoolLevel(long schoolId);
        IEnumerable<SchoolCourse> GetAllCourses(long schoolId, long schoolLevelId, long departmentlId);

        ZoomMeetingResponse CreateSynchronousLesson(ZoomMeetingEdit model);
        IEnumerable<RecordedSessionView> GetGroupRecordedSessions(int groupId, int pageIndex, int pageSize, string searchText, string fromDate, string toDate);
        IEnumerable<ZoomMeetingView> GetAllLiveSessions(long userId, int pageIndex, int pageSize, string searchText, string type, string groupId);
        ZoomMeetingView GenerateZoomMeetingJoinURL(ZoomMeetingView model);
        MeetingJoinResponse GetUserLiveSessionInfo(ZoomMeetingJoinView model);
        Task<bool> DeleteLiveSession(ZoomMeetingView liveSession);
        Task<IEnumerable<SchoolGroup>> GetSchoolGroupsForAdminSpace(SchoolGroupsReportRequest model);
        IEnumerable<Course> GetCoursesBySchoolId(long schoolId);
        Task<bool> DeleteTeamsSession(MSTeamsReponse model);
        IEnumerable<GroupMemberMapping> GetUsersMailingDetails(string userIds);

        bool InsertGroupPrimaryTeacher(GroupPrimaryTeacher model);
        bool DeleteGroupPrimaryTeacher(long id);
        bool UpdateSchoolGroupsToHideClassGroups(SchoolGroupEdit model);
        int CheckStudentGroupsAvailable(long studentId, int IsBespokeGroup = 0);

        IEnumerable<long> GetDisabledGroupList(long BlogId, long SchoolId);

        int UpdateBlogWithNewUpdate(long UpdateBlogId, long UpdatFromBlogId, bool IsApproved, bool IsRejected);
    }
}
