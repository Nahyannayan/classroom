﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.Models.HSEObjects;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.PTM.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
namespace Phoenix.VLE.Web.Services.PTM
{
    public class PTMCategoryService : IPTMCategoryService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/PTMCategory";
        private ILoggerClient _loggerClient;
        private IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        #endregion
        public PTMCategoryService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _loggerClient = LoggerClient.Instance;
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        /// <summary>
        /// Author:Pranav
        /// Date:22 Dec 2020
        /// Get Category list.
        /// </summary>
        /// <returns>Get Category Set list.</returns>
        public IEnumerable<CategoryList> GetCategorySet(int SchoolGradeId)
        {
            IEnumerable<CategoryList> categories = new List<CategoryList>();
            HttpResponseMessage response = new HttpResponseMessage();
            //HttpRequestMessage httpRequestMessage = null;
            try
            {
                int languageId = LocalizationHelper.CurrentSystemLanguageId;
                var uri = API.PTMCateorySet.GetCategorySet(_path, languageId, SchoolGradeId);
                
                response = _client.GetAsync(uri).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                    categories = EntityMapper<string, IEnumerable<CategoryList>>.MapFromJson(jsonDataProviders);
                }
                return categories;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PTMcategoryService.GetCategorySet()" + ex.Message);
                return categories;
            }
            //finally
            //{
            //    _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, categories, response, SessionHelper.CurrentSession.UserName);
            //}

        }
        /// <summary>
        /// Author:Pranav
        /// Date:22 Dec 2020
        /// Get CategoryBy Id.
        /// </summary>
        /// <returns>Get CategoryBy Id.</returns>
        public CategoryList GetCategorySetById(int id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var category = new CategoryList();
           // HttpRequestMessage httpRequestMessage = null;
            try
            {
                var uri = API.PTMCateorySet.GetCategorySetById(_path, id);
                 response = _client.GetAsync(uri).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                    category = EntityMapper<string, CategoryList>.MapFromJson(jsonDataProviders);
                }
                return category;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PTMcategoryService.GetCategorySetById()" + ex.Message);
                return category;
            }
            //finally
            //{
            //    _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, category, response, SessionHelper.CurrentSession.UserName);
            //}

        }
        /// <summary>
        /// Author:Pranav
        /// Date:22 Dec 2020
        /// UpdateCategory
        /// </summary>
        /// <returns>Update Category</returns>
        public int UpdateCategory(CategoryListEdit model)
        {
            int result = 0;
            HttpResponseMessage response = new HttpResponseMessage();
            //HttpRequestMessage httpRequestMessage = null;
            try {
               
                var uri = string.Empty;
                var sourceModel = new CategoryList();
                EntityMapper<CategoryListEdit, CategoryList>.Map(model, sourceModel);
                sourceModel.CreatedBy = Convert.ToString((int)SessionHelper.CurrentSession.Id);
                //sourceModel.SchoolGradeId = (int)SessionHelper.CurrentSession.SchoolId;
                if (model.IsAddMode)
                    uri = API.PTMCateorySet.InsertCategorySet(_path);
                else
                    uri = API.PTMCateorySet.UpdateCategorySet(_path);
                response = _client.PostAsJsonAsync(uri, sourceModel).Result;
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToInt32(jsonDataProviders);
                return result;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PTMcategoryService.UpdateCategory()" + ex.Message);
                return result;
            }
            //finally
            //{
            //    _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, result, response, SessionHelper.CurrentSession.UserName);
            //}

        }
        /// <summary>
        /// Author:Pranav
        /// Date:22 Dec 2020
        /// DeleteCategory
        /// </summary>
        /// <returns>Delete Category</returns>
        public int DeleteCategory(int id)
        {
            int result = 0;
            HttpResponseMessage response = new HttpResponseMessage();
           // HttpRequestMessage httpRequestMessage = null;
            try
            {
                var uri = API.PTMCateorySet.DeleteCategorySet(_path, id);
                response = _client.DeleteAsync(uri).Result;
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToInt32(jsonDataProviders);
                return result;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PTMcategoryService.DeleteCategory()" + ex.Message);
                return result;
            }
            //finally
            //{
            //    _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, result, response, SessionHelper.CurrentSession.UserName);
            //}
        }
        public void InsertLogDetails(HttpRequestMessage httpRequestMessage, dynamic outputModel, HttpResponseMessage httpResponseMessage, string userName)
        {
            HttpResponseMessage responses = new HttpResponseMessage();
            try
            {
                //if (_client.BaseAddress == null)
                //{
                //    _client.BaseAddress = new Uri(Constants.PhoenixAPIUrl);
                //}

                //httpRequestMessage.Headers.Remove("Authorization");
                //httpRequestMessage.Headers.Remove("Accept");
                LogDetails logDetails = new LogDetails();
                logDetails.InputJson = JsonConvert.SerializeObject(httpRequestMessage.Headers);
                logDetails.OutputJson = JsonConvert.SerializeObject(outputModel);
                logDetails.IsSuccessStatusCode = httpResponseMessage.IsSuccessStatusCode;
                logDetails.StatusCode = Convert.ToString(httpResponseMessage.StatusCode);
                logDetails.Header = JsonConvert.SerializeObject(httpRequestMessage.Headers);
                logDetails.BaseUrl = Convert.ToString(httpResponseMessage.RequestMessage.RequestUri);
                logDetails.userName = userName;
                var uri = API.Users.InsertLogDetails("api/v1/Users");
                var content = new StringContent(logDetails.ToString(), Encoding.UTF8, "application/json");
                responses = _client.PostAsJsonAsync(uri, logDetails).Result;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning(ex.Message);
            }
            finally
            {
                _loggerClient.LogWarning("Add log details:" + responses.RequestMessage);
            }
        }

    }
}