﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.PTM.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services.PTM
{
    public class PTMDeclineReasonService : IPTMDeclineReasonService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/PTMDeclineReason";
        private ILoggerClient _loggerClient;
        #endregion
        public PTMDeclineReasonService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _loggerClient = LoggerClient.Instance;
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        /// <summary>
        /// Author:Pranav
        /// Date:22 Dec 2020
        /// Get DeclineReasonSet.
        /// </summary>
        /// <returns>Get DeclineReasonSet list.</returns>
        public IEnumerable<DeclineReasonList> GetDeclineReasonSet(int SchoolGradeId)
        {
            IEnumerable<DeclineReasonList> declinereason = new List<DeclineReasonList>();
            try
            {
                int languageId = LocalizationHelper.CurrentSystemLanguageId;
                var uri = API.PTMDeclineReasonSet.GetDeclineReasonSet(_path, languageId, SchoolGradeId);
                HttpResponseMessage response = _client.GetAsync(uri).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                    declinereason = EntityMapper<string, IEnumerable<DeclineReasonList>>.MapFromJson(jsonDataProviders);
                }
                return declinereason;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PTMDeclineResoneService.GetDeclineReasonSet()" + ex.Message);
                return declinereason;
            }
        }
        /// <summary>
        /// Author:Pranav
        /// Date:22 Dec 2020
        /// GetDeclineReasonSetById.
        /// </summary>
        /// <returns>GetDeclineReasonSetById</returns>
        public DeclineReasonList GetDeclineReasonSetById(int id)
        {
            var declinereason = new DeclineReasonList();
            try
            {
                var uri = API.PTMDeclineReasonSet.GetDeclineReasonSetById(_path, id);
                HttpResponseMessage response = _client.GetAsync(uri).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                    declinereason = EntityMapper<string, DeclineReasonList>.MapFromJson(jsonDataProviders);
                }
                return declinereason;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PTMDeclineResoneService.GetDeclineReasonSetById()" + ex.Message);
                return declinereason;
            }

        }
        /// <summary>
        /// Author:Pranav
        /// Date:22 Dec 2020
        /// UpdateDeclineReason.
        /// </summary>
        /// <returns>UpdateDeclineReason</returns>
        public int UpdateDeclineReason(DeclineReasonListEdit model)
        {
            int result = 0;
            try
            {
                var uri = string.Empty;
                var sourceModel = new DeclineReasonList();
                EntityMapper<DeclineReasonListEdit, DeclineReasonList>.Map(model, sourceModel);
                sourceModel.CreatedBy = Convert.ToString((int)SessionHelper.CurrentSession.Id);
                //sourceModel.SchoolGradeId = (int)SessionHelper.CurrentSession.SchoolId;
                if (model.IsAddMode)
                    uri = API.PTMDeclineReasonSet.InsertDeclineReasonSet(_path);
                else
                    uri = API.PTMDeclineReasonSet.UpdateDeclineReasonSet(_path);
                HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToInt32(jsonDataProviders);
                return result;

            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PTMDeclineResoneService.UpdateDeclineReason()" + ex.Message);
                return result;
            }
        }
        /// <summary>
        /// Author:Pranav
        /// Date:22 Dec 2020
        /// DeleteDeclineReason.
        /// </summary>
        /// <returns>DeleteDeclineReason</returns>
        public int DeleteDeclineReason(int id)
        {
            int result = 0;
            try
            {
                var uri = API.PTMDeclineReasonSet.DeleteDeclineReasonSet(_path, id);
                HttpResponseMessage response = _client.DeleteAsync(uri).Result;
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToInt32(jsonDataProviders);
                return result;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PTMDeclineResoneService.DeleteDeclineReason()" + ex.Message);
                return result;
            }
        }
    }
}