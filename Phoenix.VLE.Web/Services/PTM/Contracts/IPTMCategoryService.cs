﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services.PTM.Contracts
{
   public interface IPTMCategoryService
    {
        IEnumerable<CategoryList> GetCategorySet(int SchoolGradeId);
        CategoryList GetCategorySetById(int id);
        int UpdateCategory(CategoryListEdit model);
        int DeleteCategory(int id);
        void InsertLogDetails(HttpRequestMessage httpRequestMessage, dynamic outputModel, HttpResponseMessage httpResponseMessage, string userName);
    }
}
