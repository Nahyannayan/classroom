﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services.PTM.Contracts
{
  public  interface IPTMDeclineReasonService
    {
        IEnumerable<DeclineReasonList> GetDeclineReasonSet(int SchoolGradeId);
        DeclineReasonList GetDeclineReasonSetById(int id);
        int UpdateDeclineReason(DeclineReasonListEdit model);
        int DeleteDeclineReason(int id);
    }
}
