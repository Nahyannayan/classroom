﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class QuizQuestionsService : IQuizQuestionsService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/QuizQuestions";

        #endregion
        public QuizQuestionsService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public QuizQuestionsEdit GetQuizQuestionById(int questionId)
        {
            var uri = API.QuizQuestions.GetQuizQuestionById(_path, questionId);
            QuizQuestionsEdit destination = new QuizQuestionsEdit();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, QuizQuestionsView>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    EntityMapper<QuizQuestionsView, QuizQuestionsEdit>.Map(sourceModel, destination);
                }
            }
            return destination;
        }

        public IEnumerable<QuizQuestionsEdit> GetQuizQuestionsByQuizId(int quizId)
        {
            var uri = API.QuizQuestions.GetQuizQuestionsByQuizId(_path, quizId,SessionHelper.CurrentSession.IsTeacher());
            List<QuizQuestionsEdit> quiz = new List<QuizQuestionsEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizQuestionsView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizQuestionsView source in sourceModel)
                    {
                        var destination = new QuizQuestionsEdit();
                        EntityMapper<QuizQuestionsView, QuizQuestionsEdit>.Map(source, destination);
                        quiz.Add(destination);
                    }
                }
            }
            return quiz;
        }
        public IEnumerable<QuizQuestionsEdit> GetQuizQuestionsPaginationByQuizId(int quizId, int PageNumber, int PageSize)
        {
            var uri = API.QuizQuestions.GetQuizQuestionsPaginationByQuizId(_path, quizId, PageNumber, PageSize);
            List<QuizQuestionsEdit> quiz = new List<QuizQuestionsEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizQuestionsView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizQuestionsView source in sourceModel)
                    {
                        var destination = new QuizQuestionsEdit();
                        EntityMapper<QuizQuestionsView, QuizQuestionsEdit>.Map(source, destination);
                        quiz.Add(destination);
                    }
                }
            }
            return quiz;
        }

        public IEnumerable<QuizQuestionsEdit> GetAllQuizQuestions()
        {
            var uri = API.QuizQuestions.GetAllQuizQuestions(_path);
            List<QuizQuestionsEdit> quiz = new List<QuizQuestionsEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizQuestionsView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizQuestionsView source in sourceModel)
                    {
                        var destination = new QuizQuestionsEdit();
                        EntityMapper<QuizQuestionsView, QuizQuestionsEdit>.Map(source, destination);
                        quiz.Add(destination);
                    }
                }
            }
            return quiz;
        }
        public IEnumerable<QuizQuestionsEdit> GetFilteredQuizQuestions(string courseIds, string lstObjectives)
        {
            var uri = API.QuizQuestions.GetFilteredQuizQuestions(_path, courseIds, lstObjectives);
            List<QuizQuestionsEdit> quiz = new List<QuizQuestionsEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizQuestionsView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizQuestionsView source in sourceModel)
                    {
                        var destination = new QuizQuestionsEdit();
                        EntityMapper<QuizQuestionsView, QuizQuestionsEdit>.Map(source, destination);
                        quiz.Add(destination);
                    }
                }
            }
            return quiz;
        }
        public bool InsertUpdatePoolQuestions(QuizQuestionsEdit quizQuestionsEdit)
        {
            var uri = API.QuizQuestions.InsertUpdatePoolQuestions(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, quizQuestionsEdit).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<Subject> GetQuestionSubjects(int questionId)
        {
            var uri = API.QuizQuestions.GetQuestionSubjects(_path, questionId);
            IEnumerable<Subject> subjects = new List<Subject>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<Subject>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }
        public IEnumerable<Course> GetQuestionCourses(int questionId)
        {
            var uri = API.QuizQuestions.GetQuestionCourses(_path, questionId);
            IEnumerable<Course> subjects = new List<Course>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<Course>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }

        public IEnumerable<Objective> GetQuestionObjectives(int questionId)
        {
            var uri = API.QuizQuestions.GetQuestionObjectives(_path, questionId);
            IEnumerable<Objective> lstObjectives = new List<Objective>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObjectives = EntityMapper<string, IEnumerable<Objective>>.MapFromJson(jsonDataProviders);
            }
            return lstObjectives;
        }
        public List<QuizQuestionFiles> GetFilesByQuizQuestionId(int quizQuestionId)
        {
            var uri = API.QuizQuestions.GetFilesByQuizQuestionsId(_path, quizQuestionId);
            List<QuizQuestionFiles> quizFiles = new List<QuizQuestionFiles>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizQuestionFiles>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizQuestionFiles source in sourceModel)
                    {
                        var destination = new QuizQuestionFiles();
                        EntityMapper<QuizQuestionFiles, QuizQuestionFiles>.Map(source, destination);
                        quizFiles.Add(destination);
                    }
                }
            }
            return quizFiles;
        }
        public QuizQuestionFiles GetFilesByQuizQuestionFileId(int quizQuestionFileId)
        {
            var destination = new QuizQuestionFiles();
            var uri = API.QuizQuestions.GetFileByQuizQuestionFileId(_path, quizQuestionFileId);
            QuizQuestionFiles quizFile = new QuizQuestionFiles();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, QuizQuestionFiles>.MapFromJson(jsonDataProviders);
                EntityMapper<QuizQuestionFiles, QuizQuestionFiles>.Map(sourceModel, destination);
                //if (sourceModel != null)
                //{
                //    foreach (QuizQuestionFiles source in sourceModel)
                //    {
                //        var destination = new QuizQuestionFiles();
                //        EntityMapper<QuizQuestionFiles, QuizQuestionFiles>.Map(source, destination);
                //    }
                //}
            }
            return destination;
        }
        public bool QuizQuestionsDelete(QuizQuestionsEdit quizQuestionsEdit)
        {
            var uri = API.QuizQuestions.QuizQuestionsDelete(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, quizQuestionsEdit).Result;
            return response.IsSuccessStatusCode;
        }
        public int DeleteQuizQuestion(int QuizQuestionId, int QuizId)
        {
            var uri = API.QuizQuestions.DeleteQuizQuestion(_path, QuizQuestionId, QuizId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public bool QuizQuestionsCUD(QuizQuestionsEdit quizQuestionsEdit, List<Objective> lstObjectives)
        {
            var uri = string.Empty;
            quizQuestionsEdit.lstObjectives = lstObjectives;
            if (quizQuestionsEdit.CourseIds != null)
            {
                quizQuestionsEdit.Courses = string.Join(",", quizQuestionsEdit.CourseIds);
            }
            if (quizQuestionsEdit.IsAddMode)
            {
                uri = API.QuizQuestions.QuizQuestionsInsert(_path);
            }
            else
            {
                uri = API.QuizQuestions.QuizQuestionsUpdate(_path);
            }
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, quizQuestionsEdit).Result;
            return response.IsSuccessStatusCode;
        }
        public int GetIdByQuestionType(string questionType)
        {
            var uri = API.QuizQuestions.GetIdByQuestionType(_path, questionType);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public bool ImportQuizQuestions(ImportQuiz importQuiz)
        {
            var uri = string.Empty;
            uri = API.QuizQuestions.ImportQuizQuestion(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, importQuiz).Result;
            return response.IsSuccessStatusCode;
        }
  
        public int DeleteQuizQuestionFile(int id)
        {
            var uri = API.QuizQuestions.DeleteQuizQuestionFile(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public int DeleteMTPResource(int id)
        {
            var uri = API.QuizQuestions.DeleteMTPResource(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public bool QuizQuestionsUpdate(QuizQuestionsEdit quizQuestionsEdit)
        {
            var uri = API.QuizQuestions.QuizQuestionsUpdate(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, quizQuestionsEdit).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateQuizCorrectAnswerByQuizAnswerId(QuizAnswersEdit quizAnswersEdit)
        {
            var uri = API.QuizQuestions.UpdateQuizCorrectAnswerByQuizAnswerId(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, quizAnswersEdit).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<QuizAnswersEdit> GetQuizAnswerByQuizQuestionId(int quizQuestionId)
        {
            var uri = API.QuizQuestions.GetQuizAnswerByQuizQuestionId(_path, quizQuestionId);
            List<QuizAnswersEdit> quizAnswersViews = new List<QuizAnswersEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizAnswersView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizAnswersView source in sourceModel)
                    {
                        var destination = new QuizAnswersEdit();
                        EntityMapper<QuizAnswersView, QuizAnswersEdit>.Map(source, destination);
                        quizAnswersViews.Add(destination);
                    }
                }
            }
            return quizAnswersViews;
        }
        public bool UpdateQuizAnswerData(QuizResponse model)
        {
            var uri = string.Empty;
            uri = API.QuizQuestions.UpdateQuizAnswerData(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;

        }

        public QuizResponse GetQuizResponseByUserId(int quizId,int userId, int studentId, int taskId)
        {
            var uri = API.QuizQuestions.GetQuizResponseByUserId(_path, quizId,userId, studentId,taskId);
            QuizResponse quizResponse = new QuizResponse();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quizResponse = EntityMapper<string,QuizResponse>.MapFromJson(jsonDataProviders);
            }
            return quizResponse;
        }

        public bool SortQuestionByOrder(QuizQuestionsEdit quizQuestionsEdit)
        {
            var uri = API.QuizQuestions.SortQuestionByOrder(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, quizQuestionsEdit).Result;
            return response.IsSuccessStatusCode;
        }

    }
}