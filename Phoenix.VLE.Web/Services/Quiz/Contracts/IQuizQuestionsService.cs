﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IQuizQuestionsService
    {
        QuizQuestionsEdit GetQuizQuestionById(int questionId);
        IEnumerable<QuizQuestionsEdit> GetQuizQuestionsByQuizId(int quizId);
        IEnumerable<QuizQuestionsEdit> GetQuizQuestionsPaginationByQuizId(int quizId, int PageNumber, int PageSize);
        IEnumerable<QuizQuestionsEdit> GetAllQuizQuestions();
        IEnumerable<QuizQuestionsEdit> GetFilteredQuizQuestions(string courseIds, string lstObjectives);
        List<QuizQuestionFiles> GetFilesByQuizQuestionId(int quizQuestionId);
        IEnumerable<Objective> GetQuestionObjectives(int questionId);
        IEnumerable<Subject> GetQuestionSubjects(int questionId);
        bool InsertUpdatePoolQuestions(QuizQuestionsEdit quizQuestionsEdit);
        int GetIdByQuestionType(string questionType);
        bool ImportQuizQuestions(ImportQuiz importQuiz);
        int DeleteQuizQuestionFile(int id);
        int DeleteMTPResource(int id);
        bool QuizQuestionsCUD(QuizQuestionsEdit quizQuestionsEdit, List<Objective> lstObjectives);
        bool QuizQuestionsDelete(QuizQuestionsEdit quizQuestionsEdit);
        QuizQuestionFiles GetFilesByQuizQuestionFileId(int quizQuestionFileId);
        int DeleteQuizQuestion(int QuizQuestionId, int QuizId);
        bool UpdateQuizCorrectAnswerByQuizAnswerId(QuizAnswersEdit quizAnswersEdit);
        IEnumerable<QuizAnswersEdit> GetQuizAnswerByQuizQuestionId(int quizQuestionId);
        bool UpdateQuizAnswerData(QuizResponse model);
        bool SortQuestionByOrder(QuizQuestionsEdit quizQuestionsEdit);
        QuizResponse GetQuizResponseByUserId(int quizId, int userId, int studentId, int taskId);
        IEnumerable<Course> GetQuestionCourses(int questionId);

    }
}
