﻿using Phoenix.Common.Queries;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IQuizService
    {



        IEnumerable<QuizEdit> GetAllQuiz();
        QuizEdit GetQuiz(int quizId);
        QuizEdit GetTaskQuizDetail(int quizId, long taskId);
        QuizEdit GetQuizResourseDetail(int resourseId, string resourseType);
        List<QuizFile> GetFilesByQuizId(int quizId);
        bool QuizCUD(QuizEdit model);
        bool UploadQuestionAnswerFiles(List<QuizQuestionAnswerFiles> model);
        QuizFile GetFileByQuizFileId(int quizFileId);
        bool QuizDelete(QuizEdit model);
        int LogQuizTime(int id, int resourceId, string resourceType, long userId, bool IsStartQuiz);
        int DeleteQuizQuestionFile(int id);
        bool ActiveDeactiveQuiz(QuizEdit model);
        IEnumerable<GroupQuiz> GetAllQuizByStudentId(long studentId);
        IQueryResult<QuizView> GetPaginateQuizByUser(long Id, bool isForm, QueryConstraints<QuizView> constraints);
        IEnumerable<QuizView> GetAllQuizByUser(long Id, bool isForm);
        int DeleteQuestionAnswerFile(int id, long userId);
        IEnumerable<QuizView> GetAllQuizByUserAndSource(long Id,long sourceId,string sourceType, bool isForm);
        IEnumerable<QuizDetails> GetQuizDetailsByQuizIdAndUserId(long userId, int quizId);
        byte[] GenerateQuizQuestionsCSVFile(string path, long userId, int quizId);
        IEnumerable<QuizAnswer> GetAnswersByQuestionId(long userId, int questionId);
        QuizResult GetQuizReportCardByUserId(int quizId, int userId, int resourceId, string resourceType);
        int GetQuizQuestionsCount(long userId, int quizId);
        IEnumerable<Subject> GetQuizSubjectsGrade(int quizId);
        QuizEdit GetQuizDetails(int quizId, int quizResourceId);
        IEnumerable<Objective> GetQuizObjectives(int quizId);
        IEnumerable<QuizQuestionAnswerFiles> GetQuizQuestionAnswerFiles(long resourceId, string resourceType, long questionId, long studentId);
        byte[] GenerateQuestionWiseReportXLSFile(string path, List<QuestionWiseReport> questionWiseReports);
        Task<string> IsSafeQuiz(int QuizId);
        QuizReport GetQuizReport(int quizId, long UserId);
        QuizReport GetGroupQuizReport(int quizId, int groupQuizId);
        byte[] GenerateQuizReportXLSFile(string path, QuizReport quizReport);
        int CloneQuizData(int quizId);
        int ShareQuizData(int quizId, string teacherIds, long SharedBy);
    }
}
