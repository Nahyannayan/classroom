﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Phoenix.Common.Localization;
using Phoenix.Common.Queries;
using System.Net;
using HtmlAgilityPack;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Phoenix.VLE.Web.Services
{
    public class QuizService : IQuizService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Quiz";
        #endregion

        public QuizService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public IEnumerable<QuizEdit> GetAllQuiz()
        {
            var uri = API.Quiz.GetAllQuiz(_path, SessionHelper.CurrentSession.SchoolId);
            List<QuizEdit> quiz = new List<QuizEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizView source in sourceModel)
                    {
                        var destination = new QuizEdit();
                        EntityMapper<QuizView, QuizEdit>.Map(source, destination);
                        quiz.Add(destination);
                    }
                }
            }
            return quiz;
        }
        public QuizEdit GetQuiz(int quizId)
        {
            var uri = API.Quiz.GetQuiz(_path, quizId, SessionHelper.CurrentSession.SchoolId);
            QuizEdit destination = new QuizEdit();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, QuizView>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    EntityMapper<QuizView, QuizEdit>.Map(sourceModel, destination);
                }
            }
            return destination;
        }
        public QuizEdit GetQuizResourseDetail(int resourseId, string resourseType)
        {
            var uri = API.Quiz.GetQuizResourseDetail(_path, resourseId, resourseType);
            QuizEdit destination = new QuizEdit();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, QuizView>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    EntityMapper<QuizView, QuizEdit>.Map(sourceModel, destination);
                }
            }
            return destination;
        }
        public QuizEdit GetTaskQuizDetail(int quizId, long taskId)
        {
            var uri = API.Quiz.GetTaskQuizDetail(_path, quizId, taskId, SessionHelper.CurrentSession.SchoolId);
            QuizEdit destination = new QuizEdit();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, QuizView>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    EntityMapper<QuizView, QuizEdit>.Map(sourceModel, destination);
                }
            }
            return destination;
        }
        public QuizEdit GetQuizDetails(int quizId, int quizResourceId)
        {
            var uri = API.Quiz.GetQuizDetails(_path, quizId, quizResourceId, SessionHelper.CurrentSession.SchoolId);
            QuizEdit destination = new QuizEdit();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, QuizView>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    EntityMapper<QuizView, QuizEdit>.Map(sourceModel, destination);
                }
            }
            return destination;
        }
        public IEnumerable<Subject> GetQuizSubjectsGrade(int quizId)
        {
            var uri = API.Quiz.GetQuizSubjectsGrade(_path, quizId);
            IEnumerable<Subject> subjects = new List<Subject>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<Subject>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }
        public QuizReport GetQuizReport(int quizId, long UserId)
        {
            var uri = API.Quiz.GetQuizReport(_path, quizId, UserId);
            QuizReport reports = new QuizReport();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reports = EntityMapper<string, QuizReport>.MapFromJson(jsonDataProviders);
            }
            return reports;
        }
        public QuizReport GetGroupQuizReport(int quizId, int groupQuizId)
        {
            var uri = API.Quiz.GetGroupQuizReport(_path, quizId, groupQuizId);
            QuizReport reports = new QuizReport();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reports = EntityMapper<string, QuizReport>.MapFromJson(jsonDataProviders);
            }
            return reports;
        }
        public List<QuizFile> GetFilesByQuizId(int quizId)
        {
            var uri = API.Quiz.GetFilesByQuizId(_path, quizId);
            List<QuizFile> quizFiles = new List<QuizFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizFile>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizFile source in sourceModel)
                    {
                        var destination = new QuizFile();
                        EntityMapper<QuizFile, QuizFile>.Map(source, destination);
                        quizFiles.Add(destination);
                    }
                }
            }
            return quizFiles;
        }
        public QuizFile GetFileByQuizFileId(int quizFileId)
        {
            var destination = new QuizFile();
            var uri = API.Quiz.GetFileByQuizFileId(_path, quizFileId);
            QuizFile quizFiles = new QuizFile();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, QuizFile>.MapFromJson(jsonDataProviders);
                EntityMapper<QuizFile, QuizFile>.Map(sourceModel, destination);
            }
            return destination;
        }
        public IEnumerable<QuizQuestionAnswerFiles> GetQuizQuestionAnswerFiles(long resourceId, string resourceType, long questionId, long studentId)
        {
            IEnumerable<QuizQuestionAnswerFiles> lstFiles = new List<QuizQuestionAnswerFiles>();
            var uri = API.Quiz.GetQuizQuestionAnswerFiles(_path, resourceId, resourceType, questionId, studentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstFiles = EntityMapper<string, IEnumerable<QuizQuestionAnswerFiles>>.MapFromJson(jsonDataProviders);
            }
            return lstFiles;
        }
        public IEnumerable<Objective> GetQuizObjectives(int quizId)
        {
            var uri = API.Quiz.GetQuizObjectives(_path, quizId);
            IEnumerable<Objective> lstObjectives = new List<Objective>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObjectives = EntityMapper<string, IEnumerable<Objective>>.MapFromJson(jsonDataProviders);
            }
            return lstObjectives;
        }
        public bool QuizCUD(QuizEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new QuizView();
            EntityMapper<QuizEdit, QuizView>.Map(model, sourceModel);
            sourceModel.lstObjectives = model.lstObjectives;
            if (model.CourseIds != null)
            {
                sourceModel.Courses = string.Join(",", model.CourseIds);
            }
            if (model.IsAddMode)
                uri = API.Quiz.QuizInsert(_path);
            else
                uri = API.Quiz.QuizUpdate(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }
        public bool UploadQuestionAnswerFiles(List<QuizQuestionAnswerFiles> model)
        {
            var uri = API.Quiz.UploadQuestionAnswerFiles(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }
        public int LogQuizTime(int id, int resourceId, string resourceType, long userId, bool IsStartQuiz)
        {
            var uri = API.Quiz.LogQuizTime(_path, id, resourceId, resourceType, userId, IsStartQuiz);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public bool QuizDelete(QuizEdit model)
        {
            var uri = API.Quiz.QuizDelete(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }
        public bool ActiveDeactiveQuiz(QuizEdit model)
        {
            var uri = API.Quiz.ActiveDeactiveQuiz(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }
        public int DeleteQuizQuestionFile(int id)
        {
            var uri = API.Quiz.DeleteQuizFile(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public int DeleteQuestionAnswerFile(int id, long userId)
        {
            var uri = API.Quiz.DeleteQuestionAnswerFile(_path, id, userId);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public IEnumerable<QuizView> GetAllQuizByUser(long Id, bool isForm)
        {
            var uri = API.Quiz.GetAllQuizByUser(_path, Id, isForm);
            List<QuizView> quiz = new List<QuizView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quiz = EntityMapper<string, List<QuizView>>.MapFromJson(jsonDataProviders);
            }
            return quiz;
        }
        public IQueryResult<QuizView> GetPaginateQuizByUser(long Id, bool isForm, QueryConstraints<QuizView> constraints)
        {
            var quizPaginationRequest = new QuizPaginationRequest();
            quizPaginationRequest.UserId = Id;
            quizPaginationRequest.IsForm = isForm;
            quizPaginationRequest.SearchString = constraints.SearchString;
            quizPaginationRequest.StartIndex = constraints.StartIndex;
            quizPaginationRequest.PageSize = constraints.PageSize;
            quizPaginationRequest.SortedColumnsString = constraints.SortedColumnsString;

            var uri = API.Quiz.GetPaginateQuizByUser(_path);
            QuizPagination quiz = new QuizPagination();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, quizPaginationRequest).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quiz = EntityMapper<string, QuizPagination>.MapFromJson(jsonDataProviders);
            }
            if (constraints == null)
            {
                return new QueryResult<QuizView>(quiz.items, quiz.totalCount);
            }
            return new QueryResult<QuizView>(quiz.items, quiz.totalCount, quiz.totalCount, constraints.PageSize);
        }
        public IEnumerable<GroupQuiz> GetAllQuizByStudentId(long studentId)
        {
            var result = new List<GroupQuiz>();
            var uri = API.Quiz.GetAllQuizByStudentId(_path, studentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<GroupQuiz>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<QuizView> GetAllQuizByUserAndSource(long Id, long sourceId, string sourceType, bool isForm)
        {
            var uri = API.Quiz.GetAllQuizByUserAndSource(_path, Id, sourceId, sourceType, isForm);
            List<QuizView> quiz = new List<QuizView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quiz = EntityMapper<string, List<QuizView>>.MapFromJson(jsonDataProviders);
            }
            return quiz;
        }

        public IEnumerable<QuizDetails> GetQuizDetailsByQuizIdAndUserId(long userId, int quizId)
        {
            var uri = API.Quiz.GetQuizDetailsByQuizIdAndUserId(_path, userId, quizId);
            List<QuizDetails> quiz = new List<QuizDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quiz = EntityMapper<string, List<QuizDetails>>.MapFromJson(jsonDataProviders);
            }
            return quiz;
        }
        public QuizResult GetQuizReportCardByUserId(int quizId, int userId, int resourceId, string resourceType)
        {
            var uri = API.GroupQuiz.GetQuizReportCardByUserId(_path, quizId, userId, resourceId, resourceType);
            QuizResult quizResponse = new QuizResult();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quizResponse = EntityMapper<string, QuizResult>.MapFromJson(jsonDataProviders);
            }
            return quizResponse;
        }
        public IEnumerable<QuizAnswer> GetAnswersByQuestionId(long userId, int questionId)
        {
            var uri = API.Quiz.GetAnswersByQuestionId(_path, userId, questionId);
            List<QuizAnswer> quizAnswers = new List<QuizAnswer>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quizAnswers = EntityMapper<string, List<QuizAnswer>>.MapFromJson(jsonDataProviders);
            }
            return quizAnswers;
        }

        public byte[] GenerateQuizQuestionsCSVFile(string path, long userId, int quizId)
        {
            try
            {
                var writer = new StreamWriter(path);
                string isCorrectAnswer = string.Empty;
                List<QuizDetails> quizDetails = new List<QuizDetails>();
                List<QuizAnswer> quizAnswers = new List<QuizAnswer>();
                quizDetails = GetQuizDetailsByQuizIdAndUserId(userId, quizId).ToList();

                // regex which match tags
                //System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("<[^>]*>");

                writer.WriteLine("Question Id, Question Text, Question Type, Marks, Answers, IsCorrectAnswer");

                foreach (var item in quizDetails)
                {
                    quizAnswers = new List<QuizAnswer>();
                    quizAnswers = GetAnswersByQuestionId(userId, item.QuizQuestionId).ToList();
                    item.QuizAnswers.AddRange(quizAnswers);
                }

                foreach (var item in quizDetails)
                {
                    if (item.QuestionText.Contains("\""))
                    {
                        item.QuestionText = item.QuestionText.Replace("\"", "\"\"");
                    }
                    if (item.QuestionText.Contains(","))
                    {
                        item.QuestionText = String.Format("\"{0}\"", item.QuestionText);
                    }
                    if (item.QuestionText.Contains(System.Environment.NewLine))
                    {
                        item.QuestionText = String.Format("\"{0}\"", item.QuestionText);
                    }

                    //Remove HTML tags from string
                    HtmlDocument htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(item.QuestionText);
                    item.QuestionText = htmlDoc.DocumentNode.InnerText;
                    item.QuestionText = HttpUtility.HtmlDecode(item.QuestionText);

                    //var jsonencode = System.Web.Helpers.Json.Encode(item.QuestionText);
                    //item.QuestionText = WebUtility.HtmlEncode(jsonencode);
                    writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", item.QuizQuestionId, item.QuestionText, item.QuestionTypeName, item.Marks, "", ""));
                    foreach (var answer in item.QuizAnswers)
                    {
                        isCorrectAnswer = (answer.IsCorrectAnswer == true) ? "Yes" : "";

                        if (answer.AnswerText.Contains("\""))
                        {
                            answer.AnswerText = answer.AnswerText.Replace("\"", "\"\"");
                        }
                        if (answer.AnswerText.Contains(","))
                        {
                            answer.AnswerText = String.Format("\"{0}\"", answer.AnswerText);
                        }
                        if (item.QuestionText.Contains(System.Environment.NewLine))
                        {
                            answer.AnswerText = String.Format("\"{0}\"", answer.AnswerText);
                        }

                        writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", " ", " ", " ", " ", answer.AnswerText, isCorrectAnswer));
                    }
                }

                writer.Flush();
                writer.Close();
                writer.Dispose();

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetQuizQuestionsCount(long userId, int quizId)
        {
            var result = 0;
            var uri = API.Quiz.GetQuizQuestionsCount(_path, userId, quizId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToInt32(jsonDataProviders);
            }
            return result;
        }

        public byte[] GenerateQuestionWiseReportXLSFile(string path, List<QuestionWiseReport> questionWiseReports)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell(ResourceManager.GetString("Quiz.Report.QuestionText"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.QuestionTypeName"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.CorrectAnswered"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.InCorrectAnswered"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.Percentage"), CellValues.String));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var item in questionWiseReports)
                    {
                        if (item.QuestionText.Contains("[blank_space_") && item.QuestionText.Contains("]"))
                        {
                            string[] r = item.QuestionText.Contains(' ') ? item.QuestionText.Split(' ') : item.QuestionText.Split('.');
                            foreach (var field in r)
                            {
                                if (field.Contains("[blank_space_") && field.Contains("]"))
                                {
                                    int pFrom = field.IndexOf("[");
                                    int pTo = field.LastIndexOf("]");
                                    if (pTo + 1 == field.Length)
                                    {
                                        //int optionNumber = Convert.ToInt32(field.Substring(pFrom, pTo - pFrom));
                                        item.QuestionText = item.QuestionText.Replace(field, "___________");
                                    }
                                    else
                                    {
                                        string subStr = field.Substring(pFrom, pTo + 1);
                                        item.QuestionText = item.QuestionText.Replace(subStr, "___________");
                                    }
                                }

                            }
                        }

                        row = new Row();

                        row.Append(
                            ConstructCell(item.QuestionText, CellValues.String),
                            ConstructCell(item.QuestionTypeName, CellValues.String),
                            ConstructCell(Convert.ToString(item.CorrectAnswered), CellValues.String),
                            ConstructCell(Convert.ToString(item.IncorrectAnswered), CellValues.String),
                            ConstructCell(Convert.ToString(item.Percentage) + "%", CellValues.String));

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        public byte[] GenerateQuizReportXLSFile(string path, QuizReport quizReport)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell(ResourceManager.GetString("Quiz.Report.QuizName"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.ResourseName"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.TeacherName"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.StudentName"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.StudentNumber"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.Year"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.Section"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.IsCompleted"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.Score"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.TimeTaken"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.StartTime"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Quiz.Report.FinishTime"), CellValues.String)
                        //ConstructCell(ResourceManager.GetString("Quiz.Report.QuestionNumber"), CellValues.String)
                        );
                    foreach (var i in quizReport.QuizReportQuestions)
                    {
                        var itemName = i.QuestionText == null ? String.Empty : Regex.Replace(i.QuestionText, @"<[^>]+>|&nbsp;", String.Empty).Trim();
                        itemName = i.QuestionText == null ? String.Empty : Regex.Replace(itemName, @"\s{2,}", String.Empty);
                        row.Append(
                            ConstructCell(itemName, CellValues.String)
                            );
                    }

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var item in quizReport.QuizReportData)
                    {
                        var ActualTime = String.Empty;
                        if (item.TimeTaken > 0)
                        {
                            var strHr = ResourceManager.GetString("Shared.Messages.Hours");
                            var strMin = ResourceManager.GetString("Shared.Messages.Minutes");
                            var strMinHr = ResourceManager.GetString("Shared.Messages.MinutesHr");
                            var strSec = ResourceManager.GetString("Shared.Messages.Seconds");
                            var strQuizTimeOut = ResourceManager.GetString("Quiz.Quiz.QuizTimeOut");
                            TimeSpan QuizTime = TimeSpan.FromMinutes(item.QuizTime);
                            string QuizTotalTime = String.Empty;
                            if (QuizTime.Hours == 0)
                            {
                                QuizTotalTime = string.Format(strMinHr, (int)QuizTime.Minutes, QuizTime.Seconds);
                            }
                            else
                            {
                                QuizTotalTime = string.Format(strHr, (int)QuizTime.TotalHours, QuizTime.Minutes);
                            }

                            TimeSpan timeTaken = TimeSpan.FromSeconds(item.TimeTaken);
                            string Time = String.Empty;
                            if (timeTaken.Hours == 0)
                            {
                                Time = (int)timeTaken.Minutes >= item.QuizTime ? strQuizTimeOut : string.Format(strMinHr, (int)timeTaken.Minutes, timeTaken.Seconds);
                            }
                            else
                            {
                                Time = (int)timeTaken.Minutes >= item.QuizTime ? strQuizTimeOut : string.Format(strHr, (int)timeTaken.TotalHours, timeTaken.Minutes);
                            }
                            var timeTakenToSolve = item.TimeTaken < 60 ? string.Format(strSec, item.TimeTaken.ToString()) : Time;
                            if (timeTakenToSolve == strQuizTimeOut)
                            {
                                ActualTime = timeTakenToSolve;
                            }
                            else
                            {
                                ActualTime = timeTakenToSolve + " / " + QuizTotalTime;
                            }
                        }
                        row = new Row();

                        row.Append(
                            ConstructCell(item.QuizName, CellValues.String),
                            ConstructCell(item.Resource, CellValues.String),
                             ConstructCell(item.TeacherName, CellValues.String),
                              ConstructCell(item.StudentName, CellValues.String),
                               ConstructCell(item.StudentNumber, CellValues.String),
                               ConstructCell(item.StudentYear, CellValues.String),
                                ConstructCell(item.Section, CellValues.String),
                                 ConstructCell("Yes", CellValues.String),
                                  ConstructCell(item.QuizScore, CellValues.String),
                                    ConstructCell(ActualTime, CellValues.String),
                                  ConstructCell(item.StartTime, CellValues.String),
                                  ConstructCell(item.FinishTime, CellValues.String)
                             //ConstructCell(item.Question, CellValues.String)
                             );
                        foreach (var q in quizReport.QuizReportQuestions)
                        {
                            var isExist = quizReport.QuizReportAnswers.Where(x => x.QuizQuestionId == q.QuizQuestionId && x.QuizResultId == item.QuizResultId).Any();
                            if (!isExist)
                            {
                                row.Append(
                              ConstructCell("0.00", CellValues.String)
                              );
                            }
                            else
                            {
                                foreach (var i in quizReport.QuizReportAnswers)
                                {

                                    if (item.QuizResultId == i.QuizResultId && q.QuizQuestionId == i.QuizQuestionId)
                                    {
                                        row.Append(
                                      ConstructCell(Convert.ToString(i.ObtainedMarks), CellValues.String)
                                      );
                                    }
                                }
                            }
                        }


                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }

        public async Task<string> IsSafeQuiz(int QuizId)
        {
            var uri = API.Quiz.GetSafeQuiz(_path, QuizId);
            string result = "";
            using (HttpResponseMessage response = await _client.GetAsync(uri))
            {
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result;

                }
            }
            return result;

        }

        public int CloneQuizData(int quizId)
        {
            var uri = API.Quiz.CloneQuizData(_path, quizId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int ShareQuizData(int quizId, string teacherIds, long SharedBy)
        {
            var uri = API.Quiz.ShareQuizData(_path, quizId, teacherIds, SharedBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
    }
}