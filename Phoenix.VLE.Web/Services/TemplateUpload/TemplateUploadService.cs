﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using static Phoenix.Models.TemplateUploadModel;
namespace Phoenix.VLE.Web.Services
{
    public class TemplateUploadService : ITemplateUploadService
    {


        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/TemplateLoginUser";
        #endregion


        public TemplateUploadService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public IEnumerable<StudentUploadTemplate> StudentTemplateBulkImport(List<StudentUploadTemplate> StudentList, long UserId)
        {
            var uri = API.TemplateUpload.StudentTemplateBulkImport(_path,UserId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, StudentList).Result;
            IEnumerable<StudentUploadTemplate> _StudentList = new List<StudentUploadTemplate>();
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _StudentList = EntityMapper<string, IEnumerable<StudentUploadTemplate>>.MapFromJson(jsonDataProviders);
            }
            return _StudentList;
        }

     
        public IEnumerable<StaffUploadTemplate> StaffTemplateBulkImport(List<StaffUploadTemplate> StaffList,long UserId)
        {
            var uri = API.TemplateUpload.StaffTemplateBulkImport(_path,UserId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, StaffList).Result;
            IEnumerable<StaffUploadTemplate> _StaffList = new List<StaffUploadTemplate>();
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _StaffList = EntityMapper<string, IEnumerable<StaffUploadTemplate>>.MapFromJson(jsonDataProviders);
            }
            return _StaffList;
        }

        public IEnumerable<GRADE_WISE_SUBJECT_M> GradeWiseSubjectTemplateBulkImport(List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList, long UserId)
        {
            var uri = API.TemplateUpload.GradeWiseSubjectTemplateBulkImport(_path,UserId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, GradeWiseSubjectList).Result;

            IEnumerable<GRADE_WISE_SUBJECT_M> _GradeWiseSubjectList = new List<GRADE_WISE_SUBJECT_M>();
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _GradeWiseSubjectList = EntityMapper<string, IEnumerable<GRADE_WISE_SUBJECT_M>>.MapFromJson(jsonDataProviders);
            }
            return _GradeWiseSubjectList;
        }

        public IEnumerable<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectTemplateBulkImport(List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectList, long UserId)
        {
            var uri = API.TemplateUpload.StudentWiseSubjectTemplateBulkImport(_path,UserId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, StudentWiseSubjectList).Result;
            IEnumerable<STUDENT_WISE_SUBJECT_MAPPING> _StudentWiseSubjectList = new List<STUDENT_WISE_SUBJECT_MAPPING>();
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                _StudentWiseSubjectList = EntityMapper<string, IEnumerable<STUDENT_WISE_SUBJECT_MAPPING>>.MapFromJson(jsonDataProviders);
            }
            return _StudentWiseSubjectList;
        }
    }
}
