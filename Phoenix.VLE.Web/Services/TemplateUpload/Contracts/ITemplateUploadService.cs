﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Phoenix.Models.TemplateUploadModel;

namespace Phoenix.VLE.Web.Services
{
    public interface ITemplateUploadService
    {
        IEnumerable<StudentUploadTemplate> StudentTemplateBulkImport(List<StudentUploadTemplate> StudentList,long UserId);
        IEnumerable<StaffUploadTemplate> StaffTemplateBulkImport(List<StaffUploadTemplate> StaffList,long UserId);
        IEnumerable<GRADE_WISE_SUBJECT_M> GradeWiseSubjectTemplateBulkImport(List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList,long UserId);
        IEnumerable<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectTemplateBulkImport(List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectList,long UserId);
    }
}
