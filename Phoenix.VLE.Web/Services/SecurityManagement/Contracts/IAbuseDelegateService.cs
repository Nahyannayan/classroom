﻿using Phoenix.Models;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IAbuseDelegateService
    {

        #region Abuse Contact Info
        AbuseContactInfo GetAbuseContactInfoBySchoolId(long schoolId);
        IEnumerable<AbuseContactInfo> GetAllAbuseContactInfo();
        AbuseContactInfo GetAbuseContactInfoById(long id);
        bool AddAbuseContactInfo(AbuseDelegateEdit model);
        bool UpdateAbuseContactInfo(AbuseDelegateEdit model);
        bool DeleteAbuseContactInfo(long id);
        #endregion

        #region Abuse Delegate
        IEnumerable<AbuseDelegate> GetAbuseDelegateBySchoolId(long schoolId);
        IEnumerable<AbuseDelegate> GetAllAbuseDelegate();
        AbuseDelegate GetAbuseDelegateById(long id);
        bool AddAbuseDelegate(AbuseDelegateEdit model);
        bool UpdateAbuseDelegate(AbuseDelegateEdit model);
        bool DeleteAbuseDelegate(long id);
        #endregion

        #region Abuse Report Ledger
        AbuseReportLedger GetAbuseReportLedgerBySafetyCategoryId(long safetyCategoryId);
        AbuseReportLedger GetAbuseReportLedgerByAbuseReportLedgerId(long abuseReportLedgerId);
        IEnumerable<AbuseReportLedger> GetAllAbuseReportLedgerAsync();
        bool AddAbuseReportLedger(AbuseReportLedger model);
        bool UpdateAbuseReportLedger(AbuseReportLedger model);
        bool DeleteAbuseReportLedger(long id);
        #endregion

        #region 
        IEnumerable<AbuseReportLedger>GetAbuseReportById();
        Task<bool> SaveMobileSettings(AbuseDelegateEdit abuseDelegateEditModel);
        #endregion
    }
}
