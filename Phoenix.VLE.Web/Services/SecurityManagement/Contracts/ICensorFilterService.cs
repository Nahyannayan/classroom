﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ICensorFilterService
    {
        IEnumerable<CensorUser> GetCensorUsers(int schoolId);
        IEnumerable<BannedWord> GetBannedWords(int schoolId);
        bool UpdateBannedWordData(BannedWord model);
        bool DeleteBannedWordData(BannedWord bannedWord);
    }
}
