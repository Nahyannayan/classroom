﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISafetyCategoriesService
    {
        IEnumerable<SafetyCategory> GetSafetyCategories(int schoolId);
        SafetyCategory GetSafetyCategoryById(int id);
        int UpdateSafetyCategoryData(SafetyCategoryEdit model);
        int DeleteSafetyCategoryData(int id);
    }
}
