﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public class AbuseDelegateService : IAbuseDelegateService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/abusedelegate";
        #endregion

        public AbuseDelegateService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool DeleteAbuseContactInfo(long id)
        {
            var uri = API.AbuseDelegate.DeleteAbuseDelegate(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteAbuseDelegate(long id)
        {
            var uri = API.AbuseDelegate.DeleteAbuseDelegate(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public AbuseContactInfo GetAbuseContactInfoById(long id)
        {
            var result = new AbuseContactInfo();
            var uri = API.AbuseDelegate.GetAbuseContactInfoById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, AbuseContactInfo>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public AbuseContactInfo GetAbuseContactInfoBySchoolId(long schoolId)
        {
            var result = new AbuseContactInfo();
            var uri = API.AbuseDelegate.GetAllAbuseContactInfoBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, AbuseContactInfo>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public AbuseDelegate GetAbuseDelegateById(long id)
        {
            var result = new AbuseDelegate();
            var uri = API.AbuseDelegate.GetAbuseDelegateById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, AbuseDelegate>.MapFromJson(jsonDataProviders);
            }
            return result;
        }


        public IEnumerable<AbuseDelegate> GetAbuseDelegateBySchoolId(long schoolId)
        {
            var result = new List<AbuseDelegate>();
            var uri = API.AbuseDelegate.GetAllAbuseDelegateBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<AbuseDelegate>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<AbuseContactInfo> GetAllAbuseContactInfo()
        {
            var result = new List<AbuseContactInfo>();
            var uri = API.AbuseDelegate.GetAllAbuseContactInfo(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<AbuseContactInfo>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<AbuseDelegate> GetAllAbuseDelegate()
        {
            var result = new List<AbuseDelegate>();
            var uri = API.AbuseDelegate.GetAllAbuseDelegates(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<AbuseDelegate>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool AddAbuseContactInfo(AbuseDelegateEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new AbuseContactInfo();
            EntityMapper<AbuseDelegateEdit, AbuseContactInfo>.Map(model, sourceModel);
            if (model.AbuseContactInfoId == 0)
                uri = API.AbuseDelegate.AddAbuseContactInfo(_path);
            else
                uri = API.AbuseDelegate.UpdateAbuseContactInfo(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool AddAbuseDelegate(AbuseDelegateEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new AbuseDelegate();
            EntityMapper<AbuseDelegateEdit, AbuseDelegate>.Map(model, sourceModel);
            if (model.AbuseContactInfoId == 0)
                uri = API.AbuseDelegate.AddAbuseDelegate(_path);
            else
                uri = API.AbuseDelegate.UpdateAbuseDelegate(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateAbuseContactInfo(AbuseDelegateEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new AbuseContactInfo();
            EntityMapper<AbuseDelegateEdit, AbuseContactInfo>.Map(model, sourceModel);
            uri = API.AbuseDelegate.UpdateAbuseContactInfo(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateAbuseDelegate(AbuseDelegateEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new AbuseDelegate();
            EntityMapper<AbuseDelegateEdit, AbuseDelegate>.Map(model, sourceModel);
            uri = API.AbuseDelegate.UpdateAbuseDelegate(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        #region Abuse Report Ledger
        public AbuseReportLedger GetAbuseReportLedgerBySafetyCategoryId(long safetyCategoryId)
        {
            var result = new AbuseReportLedger();
            var uri = API.AbuseDelegate.GetAllAbuseReportLedgerBySafetyCategoryId(_path, safetyCategoryId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, AbuseReportLedger>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public AbuseReportLedger GetAbuseReportLedgerByAbuseReportLedgerId(long abuseReportLedgerId)
        {
            var result = new AbuseReportLedger();
            var uri = API.AbuseDelegate.GetAllAbuseReportLedgerByAbuseReportLedgerId(_path, abuseReportLedgerId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, AbuseReportLedger>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<AbuseReportLedger> GetAllAbuseReportLedgerAsync()
        {
            var result = new List<AbuseReportLedger>();
            var uri = API.AbuseDelegate.GetAllAbuseReportLedger(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<AbuseReportLedger>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool AddAbuseReportLedger(AbuseReportLedger model)
        {
            var uri = string.Empty;
            var sourceModel = new AbuseReportLedger();
            EntityMapper<AbuseReportLedger, AbuseReportLedger>.Map(model, sourceModel);
            var json = JsonConvert.SerializeObject(model);
            if (model.AbuseReportLedgerId == 0)
                uri = API.AbuseDelegate.AddAbuseReportLedger(_path);
            else
                uri = API.AbuseDelegate.UpdateAbuseReportLedger(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateAbuseReportLedger(AbuseReportLedger model)
        {
            var uri = string.Empty;
            var sourceModel = new AbuseReportLedger();
            EntityMapper<AbuseReportLedger, AbuseReportLedger>.Map(model, sourceModel);
            uri = API.AbuseDelegate.UpdateAbuseReportLedger(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteAbuseReportLedger(long id)
        {
            var uri = API.AbuseDelegate.DeleteAbuseReportLedger(_path, (int)id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }
        #endregion

        #region AbuseReport
        public IEnumerable<AbuseReportLedger> GetAbuseReportById()
        {
            int userId = (int)SessionHelper.CurrentSession.Id;
            var uri = API.AbuseDelegate.GetAbuseReportByUserId(_path, userId);
            IEnumerable<AbuseReportLedger> subjects = new List<AbuseReportLedger>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<AbuseReportLedger>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }

        #endregion


        #region Mobile Settings

        public async Task<bool> SaveMobileSettings(AbuseDelegateEdit abuseDelegateEditModel)
        {
            var sourceModel = new AbuseContactInfo();
            EntityMapper<AbuseDelegateEdit, AbuseContactInfo>.Map(abuseDelegateEditModel, sourceModel);
            var uri = API.AbuseDelegate.SaveMobileSettings(_path);
            HttpResponseMessage response = await _client.PostAsJsonAsync(uri, sourceModel);
            return response.IsSuccessStatusCode;
        }
        #endregion
    }
}