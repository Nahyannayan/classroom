﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class SafetyCategoriesService : ISafetyCategoriesService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/safetycategories";
        #endregion

        public SafetyCategoriesService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        public IEnumerable<SafetyCategory> GetSafetyCategories(int schoolId)
        {
            int languageId = LocalizationHelper.CurrentSystemLanguageId;
            var uri = API.SafetyCategories.GetSafetyCategories(_path, languageId, schoolId);
            IEnumerable<SafetyCategory> categories = new List<SafetyCategory>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                categories = EntityMapper<string, IEnumerable<SafetyCategory>>.MapFromJson(jsonDataProviders);
            }
            return categories;
        }

        public SafetyCategory GetSafetyCategoryById(int id)
        {
            var category = new SafetyCategory();
            var uri = API.SafetyCategories.GetSafetyCategoryById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                category = EntityMapper<string, SafetyCategory>.MapFromJson(jsonDataProviders);
            }
            return category;
        }

        public int UpdateSafetyCategoryData(SafetyCategoryEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new SafetyCategory();
            EntityMapper<SafetyCategoryEdit, SafetyCategory>.Map(model, sourceModel);
            sourceModel.CreatedById = (int)SessionHelper.CurrentSession.Id;
            if(model.IsAddMode)
                uri = API.SafetyCategories.InsertSafetyCategory(_path);
            else
                uri = API.SafetyCategories.UpdateSafetyCategory(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int DeleteSafetyCategoryData(int id)
        {
            var uri = API.SafetyCategories.DeleteSafetyCategory(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        #endregion
    }
}
