﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class CensorFilterService : ICensorFilterService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/censorfilter";
        #endregion

        public CensorFilterService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        public IEnumerable<CensorUser> GetCensorUsers(int schoolId)
        {
            var uri = API.CensorFilter.GetCensorUsers(_path, schoolId);
            IEnumerable<CensorUser> users = new List<CensorUser>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                users = EntityMapper<string, IEnumerable<CensorUser>>.MapFromJson(jsonDataProviders);
            }
            return users;
        }

        public IEnumerable<BannedWord> GetBannedWords(int schoolId)
        {
            var uri = API.CensorFilter.GetBannedWords(_path, schoolId);
            IEnumerable<BannedWord> words = new List<BannedWord>();

            //To set authorization header, to service called before setting login cookie
            var token = Helpers.CommonHelper.GetCookieValue("st", "tk");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                words = EntityMapper<string, IEnumerable<BannedWord>>.MapFromJson(jsonDataProviders);
            }
            return words;
        }


        public bool UpdateBannedWordData(BannedWord model)
        {
            var uri = string.Empty;
            uri = API.CensorFilter.UpdateBannedWord(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteBannedWordData(BannedWord bannedWord)
        {
            var uri = API.CensorFilter.DeleteBannedWord(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, bannedWord).Result;
            return response.IsSuccessStatusCode;
        }
        #endregion
    }
}
