﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.ViewModels;
using System.Text;
using Phoenix.Models.Entities;

namespace Phoenix.VLE.Web.Services
{
    public class ExemplarWallService : IExemplarWallServices
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/exemplarwall";
        readonly string _pathTOSchool= "api/v1/school";
        
        #endregion

        public ExemplarWallService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public IEnumerable<SchoolGroup> GetSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            var result = new List<SchoolGroup>();
            var uri = API.ExemplarWall.GetGroupsByUserId(_path, userId, isTeacher);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public List<Student> GetStudentForTeacherBySchoolGroup(int id, int pageNumber, int pageSize, string schoolGroupIds, string searchString = "", string sortBy = "")
        {
            var students = new List<Student>();
            var uri = API.School.GetStudentForTeacherBySchoolGroup(_pathTOSchool, id, pageNumber, pageSize, schoolGroupIds, searchString, sortBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                students = EntityMapper<string, List<Student>>.MapFromJson(jsonDataProviders);
            }
            return students;
        }

        public IEnumerable<ExemplarUserDetails> GetBlogsByGroupNBlotTypeId(int SchoolLevel, int Department, int CourseId, int SchoolId, int groupId, 
            int blogTypeId, int? isPublish, Int64 UserId, Int64 CreatedBy, string SearchText, bool MyExemplarWork)
        {
            var result = new List<ExemplarUserDetails>();
            var uri = API.ExemplarWall.GetBlogByGroupNBlogTypeId(_path,  SchoolLevel, Department, CourseId, SchoolId, groupId, blogTypeId, isPublish,UserId, CreatedBy, SearchText, MyExemplarWork);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<ExemplarUserDetails>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<SchoolLevel> GetSchoolLevelBySchoolId(long schoolId)
        {
            var result = new List<SchoolLevel>();
            var uri = API.ExemplarWall.GetSchoolLevelBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolLevel>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SchoolDepartment> GetSchoolDepartmentList(long schoolId)
        {
            var result = new List<SchoolDepartment>();
            var uri = API.ExemplarWall.GetSchoolDepartment(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolDepartment>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SchoolGroup> GetSchoolGroupBasedOnSchoolLevel(long LevelId, long UserId, long CourseId, long SchoolId)
        {
            var result = new List<SchoolGroup>();
            var uri = API.ExemplarWall.GetSchoolGroupBasedOnSchoolLevel(_path, LevelId, UserId, CourseId,SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public int InsertExemplar(ExemplarWallModel entity)
        {
            int result = 0;
            var uri = string.Empty;

            uri = API.ExemplarWall.AddExemplarWallModel(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? 0 : 1;
            }
            return result;
        }
        public bool UpdateExemplar(ExemplarWallModel entity)
        {
            bool result = false;
            var uri = string.Empty;

            uri = API.ExemplarWall.UpdateExemplarWallModel(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? false :true;
            }
            return result;
        }
        public bool ShareExemplarPost(sharepost entity)
        {
            bool result = false;
            var uri = string.Empty;
            uri = API.ExemplarWall.ShareExemplarPost(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? false : true;
            }
            return result;
        }
        public ExemplarWallModel GetExemplar(long id)
        {
            var result = new ExemplarWallModel();
            var uri = API.ExemplarWall.GetExemplarWallById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, ExemplarWallModel>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public int DeleteExemplar(ExemplarWallModel entity)
        {
            int result = 0;
            var uri = string.Empty;

            uri = API.ExemplarWall.DeleteExemplar(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? 0 : 1;
            }
            return result;
        }
        public bool UpdateWallSortOrder(ExemplarWallOrder model)
        {
            var uri = string.Empty;
            uri = API.Blog.UpdateWallSortOrder(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;

        }
        public IEnumerable<ExemplarWallModel> GetPendingForApprovalExemplarPost(long SchoolId)
        {
            var result = new List<ExemplarWallModel>();
            var uri = API.ExemplarWall.GetPendingForApprovalExemplarPost(_path, SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<ExemplarWallModel>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public int UpdateExemplarPostStatusFromPendingToApprove(long ExemplarPostId) {

            var uri = API.ExemplarWall.UpdateExemplarPostStatusFromPendingToApprove(_path, ExemplarPostId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt32(jsonDataProviders);
            }
            return 0;
        }
        public bool GetApprovalStatusDetails(long SchoolId, string Opearion, string FlagType, bool Value) {
            var result = false;
            var uri = API.ExemplarWall.GetApprovalStatusDetails(_path, SchoolId, Opearion, FlagType, Value);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result= Convert.ToString(jsonDataProviders)=="true"?true:false;
            }
            return result;
        }

        public IEnumerable<Phoenix.Models.Course> GetCourseByDepartment(long DepartmentId) {
            var result =new List<Phoenix.Models.Course>(); ;
            var uri = API.ExemplarWall.GetCourseByDepartment(_path, DepartmentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Phoenix.Models.Course>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
    }
}