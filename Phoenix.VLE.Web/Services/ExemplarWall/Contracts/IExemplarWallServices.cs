﻿using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models.Entities;

namespace Phoenix.VLE.Web.Services
{
    public interface IExemplarWallServices
    {
        IEnumerable<SchoolGroup> GetSchoolGroupsByUserId(long userId, bool isTeacher);
        List<Student> GetStudentForTeacherBySchoolGroup(int id, int pageNumber, int pageSize, string schoolGroupIds, string searchString = "", string sortBy = "");

        IEnumerable<ExemplarUserDetails> GetBlogsByGroupNBlotTypeId(int SchoolLevel, int Department, int CourseId, int SchoolId, int groupId, int blogTypeId, 
            int? isPublish, Int64 UserId, Int64 CreatedBy, string SearchText, bool MyExemplarWork);
        IEnumerable<SchoolLevel> GetSchoolLevelBySchoolId(long schoolId);
        IEnumerable<SchoolDepartment> GetSchoolDepartmentList(long schoolId);


        IEnumerable<SchoolGroup> GetSchoolGroupBasedOnSchoolLevel(long LevelId, long UserId, long CourseId,long SchoolId);
        int InsertExemplar(ExemplarWallModel entity);
        bool UpdateExemplar(ExemplarWallModel entity);

        bool ShareExemplarPost(sharepost entity);
        ExemplarWallModel GetExemplar(long id);

        int DeleteExemplar(ExemplarWallModel entity);
        bool UpdateWallSortOrder(ExemplarWallOrder model);

        IEnumerable<ExemplarWallModel> GetPendingForApprovalExemplarPost(long SchoolId);
        int UpdateExemplarPostStatusFromPendingToApprove(long ExemplarPostId);
        bool GetApprovalStatusDetails(long SchoolId, string Opearion, string FlagType, bool Value);

        IEnumerable<Phoenix.Models.Course> GetCourseByDepartment(long DepartmentId);

    }
}
