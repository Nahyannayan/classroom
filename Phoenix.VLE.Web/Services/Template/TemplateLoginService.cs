﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.HSEObjects;
using Newtonsoft.Json;
using System.Text;

namespace Phoenix.VLE.Web.Services
{
    public class TemplateLoginService : ITemplateLoginService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/TemplateLoginUser";
        #endregion

        public TemplateLoginService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
               
            }
        }

        public LogInUser GetLoginUserByUserName(string userName)
        {
            var loginUser = new LogInUser();
            var encryptUserName = EncryptDecryptHelper.EncryptUrl(userName);
            var uri = API.LogInUser.GetLoginUserByUserName(_path, encryptUserName,"");
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                loginUser = EntityMapper<string, LogInUser>.MapFromJson(jsonDataProviders);
            }
            return loginUser;
        }

        public Boolean IsValidUser(string userName,string Password)
        {          
            var encryptUserName = EncryptDecryptHelper.EncryptUrl(userName);
            var encryptPassword = EncryptDecryptHelper.EncryptUrl(Password);
            var uri = API.TemplateLoginUser.IsValidUser(_path, encryptUserName, encryptPassword);
            HttpResponseMessage response = _client.GetAsync(uri).Result;    
            return response.IsSuccessStatusCode;
           
        }
        public Boolean ResetPassword(string userName, string Password)
        {

            var encryptUserName = EncryptDecryptHelper.EncryptUrl(userName);
            var encryptPassword = EncryptDecryptHelper.EncryptUrl(Password);
            var uri = API.TemplateLoginUser.ResetPassword(_path, encryptUserName, encryptPassword);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;

        }

        public IEnumerable<LogInUser> GetUserList(int schoolId)
        {
            var uri = API.LogInUser.GetUserList(_path, schoolId);
            IEnumerable<LogInUser> userList = new List<LogInUser>();

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                userList = EntityMapper<string, IEnumerable<LogInUser>>.MapFromJson(jsonDataProviders);
            }
            return userList;
        }

    }
}
