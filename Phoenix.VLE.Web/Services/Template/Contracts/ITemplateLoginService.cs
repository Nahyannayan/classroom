﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.VLE.Web.Services
{
    public interface ITemplateLoginService
    {
        LogInUser GetLoginUserByUserName(string userName);
        IEnumerable<LogInUser> GetUserList(int schoolId);
        Boolean IsValidUser(string userName,string Password);
        Boolean ResetPassword(string userName, string Password);
    }
}
