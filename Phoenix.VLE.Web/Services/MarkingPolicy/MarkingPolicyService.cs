﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public class MarkingPolicyService : IMarkingPolicyService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/MarkingPolicy";
        #endregion

        public MarkingPolicyService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
               
            }
        }

        public OperationDetails UpdateMarkingPolicyData(MarkingPolicyEdit model)
        {
          
            var uri = string.Empty;
            var sourceModel = new MarkingPolicy();
            OperationDetails op = new OperationDetails();
            EntityMapper<MarkingPolicyEdit, MarkingPolicy>.Map(model, sourceModel);
                       
            if (model.IsAddMode)
                uri = API.MarkingPolicy.InsertMarkingPolicy(_path);
            else
                uri = API.MarkingPolicy.UpdateMarkingPolicy(_path);
          
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string,OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }

        public OperationDetails DeleteMarkingPolicyData(MarkingPolicy MarkingPolicy)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.MarkingPolicy.DeleteMarkingPolicy(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, MarkingPolicy).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }

        public IEnumerable<MarkingPolicy>  GetMarkingPolicys(long userId, int schoolId)
        {
            var uri = API.MarkingPolicy.GetMarkingPolicys(_path,userId, schoolId);
            IEnumerable<MarkingPolicy> lstMarkingPolicy = new List<MarkingPolicy>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMarkingPolicy = EntityMapper<string, IEnumerable<MarkingPolicy>>.MapFromJson(jsonDataProviders);
            }
            return lstMarkingPolicy;
        }
        public IEnumerable<string> GetMarkingPolicySchoolIds(long MarkingPolicyId)
        {
            var uri = API.MarkingPolicy.GetMarkingPolicySchoolIds(_path, MarkingPolicyId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSchoolIds = EntityMapper<string, IEnumerable<string>>.MapFromJson(jsonDataProviders);
            }
            return lstSchoolIds;
        }

        public MarkingPolicy GetMarkingPolicyDetails(long MarkingPolicyId,long userId)
        {
            var MarkingPolicyDetails = new MarkingPolicy();
            var uri = API.MarkingPolicy.GetMarkingPolicyDetails(_path,MarkingPolicyId, userId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                MarkingPolicyDetails = EntityMapper<string, MarkingPolicy>.MapFromJson(jsonDataProviders);
            }
            return MarkingPolicyDetails;
        }
        

        public IEnumerable<MarkingPolicy> GetUserMarkingPolicys( long userId)
        {
            IEnumerable<MarkingPolicy> lstUserMarkingPolicys = new List<MarkingPolicy>();
            var uri = API.MarkingPolicy.GetUserMarkingPolicys(_path, userId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstUserMarkingPolicys = EntityMapper<string, IEnumerable<MarkingPolicy>>.MapFromJson(jsonDataProviders);
            }
            return lstUserMarkingPolicys;
        }

    }
}
