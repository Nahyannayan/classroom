﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.VLE.Web.Services
{
    public interface IMarkingPolicyService
    {
        OperationDetails UpdateMarkingPolicyData(MarkingPolicyEdit model);
        OperationDetails DeleteMarkingPolicyData(MarkingPolicy assignment);
        IEnumerable<MarkingPolicy> GetMarkingPolicys(long userId, int schoolId);
        IEnumerable<string> GetMarkingPolicySchoolIds(long MarkingPolicyId);
        MarkingPolicy GetMarkingPolicyDetails(long MarkingPolicyId,long UserId);
        IEnumerable<MarkingPolicy> GetUserMarkingPolicys(long userId);
    }
}
