﻿using Phoenix.Common.Models;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ITerminologyEditorService
    {
        IEnumerable<TerminologyEditorEdit> GetAllTerminologyEditor(long SchoolId);
        TerminologyEditorEdit GetTerminologyEditor(int? id, long SchoolId);
        bool TerminologyEditorCUD(TerminologyEditorEdit model);
        bool TerminologyEditorDelete(TerminologyEditorEdit model);
        bool CheckForTerminology(string term,int id);
    }
}
