﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public class TerminologyEditorService : ITerminologyEditorService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/TerminologyEditor";
        #endregion


        public TerminologyEditorService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public IEnumerable<TerminologyEditorEdit> GetAllTerminologyEditor(long SchoolId)
        {
            var uri = API.TerminologyEditor.GetAllTerminologyEditor(_path, SchoolId);
            List<TerminologyEditorEdit> terminologyEditor = new List<TerminologyEditorEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<TerminologyEditorView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (TerminologyEditorView source in sourceModel)
                    {
                        var destination = new TerminologyEditorEdit();
                        EntityMapper<TerminologyEditorView, TerminologyEditorEdit>.Map(source, destination);
                        terminologyEditor.Add(destination);
                    }
                }
            }
            return terminologyEditor;
        }

        public TerminologyEditorEdit GetTerminologyEditor(int? id, long SchoolId)
        {
            var uri = API.TerminologyEditor.GetTerminologyEditor(_path, id, SchoolId);
            List<TerminologyEditorEdit> terminologyEditor = new List<TerminologyEditorEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<TerminologyEditorView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (TerminologyEditorView source in sourceModel)
                    {
                        var destination = new TerminologyEditorEdit();
                        EntityMapper<TerminologyEditorView, TerminologyEditorEdit>.Map(source, destination);
                        terminologyEditor.Add(destination);
                    }
                }
            }
            return terminologyEditor.FirstOrDefault();
        }

        public bool TerminologyEditorCUD(TerminologyEditorEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new TerminologyEditorView();
            EntityMapper<TerminologyEditorEdit, TerminologyEditorView>.Map(model, sourceModel);
            if (model.IsAddMode)
                uri = API.TerminologyEditor.TerminologyEditorInsert(_path);
            else
                uri = API.TerminologyEditor.TerminologyEditorUpdate(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }
        public bool TerminologyEditorDelete(TerminologyEditorEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new TerminologyEditorView();
            EntityMapper<TerminologyEditorEdit, TerminologyEditorView>.Map(model, sourceModel);
            uri = API.TerminologyEditor.TerminologyEditorDelete(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }
        public bool CheckForTerminology(string term,int id)
        {
            var uri = string.Empty;
            uri = API.TerminologyEditor.CheckForTerminology(_path,term,id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;

            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            return Convert.ToBoolean(jsonDataProviders);
        }
    }
}