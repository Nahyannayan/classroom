﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Services
{
    public class GradingTemplateService : IGradingTemplateService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/gradingtemplate";
        #endregion

        public GradingTemplateService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        public int DeleteGradingTemplate(int id)
        {
            var uri = API.GradingTemplate.DeleteGradingTemplate(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            return Convert.ToInt32(jsonDataProviders);
        }

        public GradingTemplate GetGradingTemplateById(int id)
        {

            var gradingTemplate = new GradingTemplate();
            var uri = API.GradingTemplate.GetGradingTemplateById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                gradingTemplate = EntityMapper<string, GradingTemplate>.MapFromJson(jsonDataProviders);
            }
            return gradingTemplate;
        }

        public IEnumerable<GradingTemplate> GetGradtingTemplates(int schoolId)
        {
            int languageId = LocalizationHelper.CurrentSystemLanguageId;
            var uri = API.GradingTemplate.GetGradingTemplates(_path, languageId, schoolId);
            IEnumerable<GradingTemplate> gradingTemplates = new List<GradingTemplate>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                gradingTemplates = EntityMapper<string, IEnumerable<GradingTemplate>>.MapFromJson(jsonDataProviders);
            }
            return gradingTemplates;
        }

        public int UpdateGradingTemplate(GradingTemplateEdit model)
        {

            int result = 0;
            var uri = string.Empty;
            var sourceModel = new GradingTemplate();
            EntityMapper<GradingTemplateEdit, GradingTemplate>.Map(model, sourceModel);
            if (model.IsAddMode)
                uri = API.GradingTemplate.InsertGradingTemplate(_path);
            else
                uri = API.GradingTemplate.UpdateGradingTemplate(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        #endregion


    }
}