﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class GradingTemplateItemService : IGradingTemplateItemService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/gradingtemplateitem";
        #endregion

        public GradingTemplateItemService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        public IEnumerable<GradingTemplateItem> GetGradingTemplateItems(int templateId, int SystemLanguageId)
        {
            var uri = API.GradingTemplateItem.GetGradingTemplateItems(_path, templateId, SystemLanguageId);
            IEnumerable<GradingTemplateItem> gradingtemplateitems = new List<GradingTemplateItem>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                gradingtemplateitems = EntityMapper<string, IEnumerable<GradingTemplateItem>>.MapFromJson(jsonDataProviders);
            }
            return gradingtemplateitems;
        }

        public GradingTemplateItem GetGradingTemplateItemById(int id, int SystemLanguageId)
        {
            var gradingtemplateitem = new GradingTemplateItem();
            var uri = API.GradingTemplateItem.GetGradingTemplateItemById(_path, id, SystemLanguageId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                gradingtemplateitem = EntityMapper<string, GradingTemplateItem>.MapFromJson(jsonDataProviders);
            }
            return gradingtemplateitem;
        }

        public int UpdateGradingTemplateItemData(GradingTemplateItemEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new GradingTemplateItem();
            EntityMapper<GradingTemplateItemEdit, GradingTemplateItem>.Map(model, sourceModel);
            if (model.IsAddMode)
                uri = API.GradingTemplateItem.InsertGradingTemplateItem(_path);
            else
                uri = API.GradingTemplateItem.UpdateGradingTemplateItem(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public bool DeleteGradingTemplateItemData(int id)
        {
            var uri = API.GradingTemplateItem.DeleteGradingTemplateItem(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }
        
        #endregion
    }
}