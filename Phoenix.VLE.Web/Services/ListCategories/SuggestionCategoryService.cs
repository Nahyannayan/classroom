﻿//using DevExpress.Utils;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Phoenix.Common.Localization;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Models;

namespace Phoenix.VLE.Web.Services
{
    public class SuggestionCategoryService : ISuggestionCategoryService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/SuggestionCategory";
        #endregion

        public SuggestionCategoryService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);

            }
        }

        #region Methods
        public IEnumerable<SuggestionCategory> GetSuggestionCategory(int schoolId)
        {
            int languageId = LocalizationHelper.CurrentSystemLanguageId;
            var uri = API.SuggestionCategory.GetSuggestionCategory(_path, languageId, schoolId);
            IEnumerable<SuggestionCategory> categories = new List<SuggestionCategory>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                categories = EntityMapper<string, IEnumerable<SuggestionCategory>>.MapFromJson(jsonDataProviders);
            }
            return categories;
        }

        public SuggestionCategory GetSuggestionCategoryById(int id)
        {
            var category = new SuggestionCategory();
            var uri = API.SuggestionCategory.GetSuggestionCategoryById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                category = EntityMapper<string, SuggestionCategory>.MapFromJson(jsonDataProviders);
            }
            return category;
        }

        public int UpdateSuggestionCategoryData(SuggestionCategoryEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new SuggestionCategory();
            EntityMapper<SuggestionCategoryEdit, SuggestionCategory>.Map(model, sourceModel);
            sourceModel.CreatedById = (int)SessionHelper.CurrentSession.Id;
            sourceModel.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            if (model.IsAddMode)
                uri = API.SuggestionCategory.InsertSuggestionCategory(_path);
            else
                uri = API.SuggestionCategory.UpdateSuggestionCategory(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        public int DeleteSuggestionCategoryData(int id)
        {
            var uri = API.SuggestionCategory.DeleteSuggestionCategory(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        #endregion
    }
}