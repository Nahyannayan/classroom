﻿
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class SubjectService : ISubjectService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/subject";
        #endregion

        public SubjectService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        public IEnumerable<Subject> GetSubjects(int schoolId)
        {
            int languageId = LocalizationHelper.CurrentSystemLanguageId;
            var uri = API.Subject.GetSubjects(_path, languageId, schoolId);
            IEnumerable<Subject> subjects = new List<Subject>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<Subject>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }

        public Subject GetSubjectById(int id)
        {
            var subject = new Subject();
            var uri = API.Subject.GetSubjectById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subject = EntityMapper<string, Subject>.MapFromJson(jsonDataProviders);
            }
            return subject;
        }

        public IEnumerable<Objective> GetLessonDetailsById(int id)
        {
            IEnumerable<Objective> objective = new List<Objective>();
            var uri = API.Subject.GetLessonsByTopicId(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objective = EntityMapper<string, IEnumerable<Objective>>.MapFromJson(jsonDataProviders);
            }
            return objective;
        }

        public IEnumerable<GroupUnit> GetSelectedUnitByGroupId(string id)
        {
            IEnumerable<GroupUnit> objective = new List<GroupUnit>();
            var uri = API.Subject.GetSelectedUnitByGroupId(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objective = EntityMapper<string, IEnumerable<GroupUnit>>.MapFromJson(jsonDataProviders);
            }
            return objective;
        }

        public bool UpdateSubjectData(SubjectEdit model)
        {
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new Subject();
            EntityMapper<SubjectEdit, Subject>.Map(model, sourceModel);
            sourceModel.CreatedById = (int)SessionHelper.CurrentSession.Id;
            if (model.IsAddMode)
                uri = API.Subject.InsertSubject(_path);
            else
                uri = API.Subject.UpdateSubject(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteSubjectData(int id)
        {
            var uri = API.Subject.DeleteSubject(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<TopicSubTopicStructure> GetTopicSubTopicStructure(int subjectId)
        {
            var uri = API.Subject.GetTopicSubTopicStructure(_path, subjectId);
            IEnumerable<TopicSubTopicStructure> lstSubtopics = new List<TopicSubTopicStructure>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSubtopics = EntityMapper<string, IEnumerable<TopicSubTopicStructure>>.MapFromJson(jsonDataProviders);
            }
            return lstSubtopics;
        }

        public IEnumerable<TopicSubTopicStructure> GetUnitStructure(string courseIds)
        {
            var uri = API.Subject.GetUnitStructure(_path, courseIds);
            IEnumerable<TopicSubTopicStructure> lstSubtopics = new List<TopicSubTopicStructure>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSubtopics = EntityMapper<string, IEnumerable<TopicSubTopicStructure>>.MapFromJson(jsonDataProviders);
            }
            return lstSubtopics;
        }

        public IEnumerable<Objective> GetSubTopicObjective(int id, bool isMainSyllabus, int subjectId, bool isLesson)
        {
            var uri = API.Subject.GetSubTopicObjective(_path, id, isMainSyllabus, subjectId, isLesson);
            IEnumerable<Objective> lstObjectives = new List<Objective>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObjectives = EntityMapper<string, IEnumerable<Objective>>.MapFromJson(jsonDataProviders);
            }
            return lstObjectives;
        }

        public IEnumerable<Objective> GetSubtopicObjectivesWithMultipleSubjects(int id, bool isMainSyllabus, string subjectIds, bool isLesson)
        {
            var uri = API.Subject.GetSubtopicObjectivesWithMultipleSubjects(_path, id, isMainSyllabus, subjectIds, isLesson);
            IEnumerable<Objective> lstObjectives = new List<Objective>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObjectives = EntityMapper<string, IEnumerable<Objective>>.MapFromJson(jsonDataProviders);
            }
            return lstObjectives;
        }
        public IEnumerable<Objective> GetAssignmentObjective(int assignmentId)
        {
            var uri = API.Subject.GetAssignmentObjective(_path, assignmentId);
            IEnumerable<Objective> lstObjectives = new List<Objective>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObjectives = EntityMapper<string, IEnumerable<Objective>>.MapFromJson(jsonDataProviders);
            }
            return lstObjectives;
        }

        public IEnumerable<CourseUnit> GetAssignmentUnit(int assignmentId)
        {
            var uri = API.Subject.GetAssignmentUnit(_path, assignmentId);
            IEnumerable<CourseUnit> lstCourseUnit = new List<CourseUnit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstCourseUnit = EntityMapper<string, IEnumerable<CourseUnit>>.MapFromJson(jsonDataProviders);
            }
            return lstCourseUnit;
        }

        public IEnumerable<GroupUnit> GetAssignmentCourseTopicUnit(int assignmentId)
        {
            var uri = API.Subject.GetAssignmentCourseTopicUnit(_path, assignmentId);
            IEnumerable<GroupUnit> lstGroupTopicUnit = new List<GroupUnit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstGroupTopicUnit = EntityMapper<string, IEnumerable<GroupUnit>>.MapFromJson(jsonDataProviders);
            }
            return lstGroupTopicUnit;
        }

        public IEnumerable<TopicSubTopicStructure> GetTopicSubTopicStructureByMultipleSubjects(string subjectIds)
        {
            var uri = API.Subject.GetTopicSubTopicStructureByMultipleSubjectIds(_path, subjectIds);
            IEnumerable<TopicSubTopicStructure> lstSubtopics = new List<TopicSubTopicStructure>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSubtopics = EntityMapper<string, IEnumerable<TopicSubTopicStructure>>.MapFromJson(jsonDataProviders);
            }
            return lstSubtopics;
        }


        #endregion
    }
}
