﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Models.Entities;

namespace Phoenix.VLE.Web.Services
{
    public class MarkingSchemeService : IMarkingSchemeService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/markingscheme";
        #endregion

        public MarkingSchemeService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        public bool DeleteMarkingScheme(int id)
        {
            var uri = API.MarkingScheme.DeleteMarkingScheme(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public MarkingScheme GetMarkingScheme(int id)
        {
            var markingScheme = new MarkingScheme();
            var uri = API.MarkingScheme.GetMarkingSchemeById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                markingScheme = EntityMapper<string, MarkingScheme>.MapFromJson(jsonDataProviders);
            }
            return markingScheme;
        }

        public IEnumerable<MarkingScheme> GetMarkingSchemes(long schoolId)
        {
            var uri = API.MarkingScheme.GetMarkingSchemes(_path, schoolId);
            IEnumerable<MarkingScheme> markingSchemes = new List<MarkingScheme>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                markingSchemes = EntityMapper<string, IEnumerable<MarkingScheme>>.MapFromJson(jsonDataProviders);
            }
            return markingSchemes;
        }

        public int UpdateMarkingScheme(MarkingSchemeEdit model)
        {
            var result = 0;
            var uri = string.Empty;
            var sourceModel = new MarkingScheme();
            EntityMapper<MarkingSchemeEdit, MarkingScheme>.Map(model, sourceModel);
            if (model.IsAddMode)
                uri = API.MarkingScheme.InsertMarkingScheme(_path);
            else
                uri = API.MarkingScheme.UpdateMarkingScheme(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        #endregion

    }
}