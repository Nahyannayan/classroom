﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System.Collections.Generic;

namespace Phoenix.VLE.Web.Services
{
    public interface IGradingTemplateService
    {
        IEnumerable<GradingTemplate> GetGradtingTemplates(int schoolId);
        GradingTemplate GetGradingTemplateById(int id);
        int UpdateGradingTemplate(GradingTemplateEdit model);
        int DeleteGradingTemplate(int id);
    }
}
