﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISubjectService
    {
        IEnumerable<Subject> GetSubjects(int schoolId);
        Subject GetSubjectById(int id);
        IEnumerable<Objective> GetLessonDetailsById(int id);
        IEnumerable<GroupUnit> GetSelectedUnitByGroupId(string id);
        bool UpdateSubjectData(SubjectEdit model);
        bool DeleteSubjectData(int id);
        IEnumerable<TopicSubTopicStructure> GetTopicSubTopicStructure(int subjectId);
        IEnumerable<TopicSubTopicStructure> GetUnitStructure(string courseIds);
        IEnumerable<TopicSubTopicStructure> GetTopicSubTopicStructureByMultipleSubjects(string subjectIds);
        IEnumerable<Objective> GetSubTopicObjective(int id, bool isMainSyllabus, int subjectId, bool isLesson);
        IEnumerable<Objective> GetSubtopicObjectivesWithMultipleSubjects(int id, bool isMainSyllabus, string subjectIds, bool isLesson);
        IEnumerable<Objective> GetAssignmentObjective(int assignmentId);
        IEnumerable<CourseUnit> GetAssignmentUnit(int assignmentId);
        IEnumerable<GroupUnit> GetAssignmentCourseTopicUnit(int assignmentId);

    }
}
