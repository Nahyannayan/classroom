﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISuggestionCategoryService
    {
        IEnumerable<SuggestionCategory> GetSuggestionCategory(int schoolId);
        SuggestionCategory GetSuggestionCategoryById(int id);
        int UpdateSuggestionCategoryData(SuggestionCategoryEdit model);
        int DeleteSuggestionCategoryData(int id);
    }
}
