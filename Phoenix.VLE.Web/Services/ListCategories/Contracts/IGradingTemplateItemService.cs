﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IGradingTemplateItemService
    {
        IEnumerable<GradingTemplateItem> GetGradingTemplateItems(int templateId,int SystemLanguageId);
        GradingTemplateItem GetGradingTemplateItemById(int id, int SystemLanguageId);
        int UpdateGradingTemplateItemData(GradingTemplateItemEdit model);
        bool DeleteGradingTemplateItemData(int id);
    }
}
