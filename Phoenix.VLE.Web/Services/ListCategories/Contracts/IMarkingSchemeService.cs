﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IMarkingSchemeService
    {
        IEnumerable<MarkingScheme> GetMarkingSchemes(long schoolId);
        MarkingScheme GetMarkingScheme(int id);
        int UpdateMarkingScheme(MarkingSchemeEdit model);
        bool DeleteMarkingScheme(int id);
    }
}
