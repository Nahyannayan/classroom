﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class EventCategoryService : IEventCategoryService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/EventCategory";

        readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];
        #endregion


        public EventCategoryService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        /// <summary>
        /// Author : Rohit Patil
        /// Created Date : 27-JUNE-2019
        /// Description : To fetch all EventCategory
        /// </summary>
        /// <param name="id">EventCategory id</param>
        /// <returns></returns>
        public IEnumerable<EventCategoryView> GetEventCategory(int id)
        {
            var uri = API.EventCategory.GetEventCategory(_path, id);
            IEnumerable<EventCategoryView> EventCategory = new List<EventCategoryView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                EventCategory = EntityMapper<string, IEnumerable<EventCategoryView>>.MapFromJson(jsonDataProviders);
            }
            return EventCategory;
        }

        /// <summary>
        /// Author : Rohit Patil
        /// Created Date : 27-JUNE-2019
        /// Description : To fetch all EventCategory
        /// </summary>
        /// <param name="id">EventCategory id</param>
        /// <returns></returns>
        public IEnumerable<EventCategoryView> GetAllEventCategoryBySchool()
        {
            var uri = API.EventCategory.GetAllEventCategoryBySchool(_path, SessionHelper.CurrentSession.SchoolId);
            IEnumerable<EventCategoryView> EventCategory = new List<EventCategoryView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                EventCategory = EntityMapper<string, IEnumerable<EventCategoryView>>.MapFromJson(jsonDataProviders);
            }
            return EventCategory;
        }

        /// <summary>
        /// Author : Mukund Patil
        /// Created Date : 05-FEB-2020
        /// Description : To fetch all EventCategories by SchoolId
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EventCategoryView> GetEventCategoriesBySchoolId()
        {
            var uri = API.EventCategory.GetEventCategoriesBySchoolId(_path, SessionHelper.CurrentSession.SchoolId);
            IEnumerable<EventCategoryView> EventCategory = new List<EventCategoryView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                EventCategory = EntityMapper<string, IEnumerable<EventCategoryView>>.MapFromJson(jsonDataProviders);
            }
            return EventCategory;
        }

        public EventCategory GetEventCategoryById(int id)
        {
            var category = new EventCategory();
            var uri = API.EventCategory.GetEventCategoryById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                category = EntityMapper<string, EventCategory>.MapFromJson(jsonDataProviders);
            }
            return category;
        }

        public int UpdateEventCategoryData(EventCategoryEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new EventCategory();
            EntityMapper<EventCategoryEdit, EventCategory>.Map(model, sourceModel);
            sourceModel.CreatedBy = (int)SessionHelper.CurrentSession.Id;
            sourceModel.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            if (model.IsAddMode)
                uri = API.EventCategory.InsertEventCategory(_path);
            else
                uri = API.EventCategory.UpdateEventCategory(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int DeleteEventCategoryData(int id)
        {
            var uri = API.EventCategory.DeleteEventCategory(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public bool CheckEventCategoryAvailable(string categoryName, int id)
        {
            var result = false;
            var uri = API.EventCategory.CheckEventCategoryAvailable(_path, categoryName, id, SessionHelper.CurrentSession.SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToBoolean(jsonDataProviders);
            }
            return result;
        }

        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
            //    _client.DefaultRequestHeaders.Accept.Clear();
            //    var uri = tokenAPI;
            //    string _ContentType = "application/x-www-form-urlencoded";
            //    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //    var nvc = new List<KeyValuePair<string, string>>();
            //    nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //    nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //    nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //    nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //    using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //    {
            //        using (var result = _client.SendAsync(test).Result)
            //        {
            //            if (result.IsSuccessStatusCode)
            //            {
            //                var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                try
            //                {
            //                    JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                }
            //                finally
            //                {
            //                    result.Dispose();
            //                }
            //            }
            //        }
            //    }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}
            return JsonDeserilize;
        }
    }
}