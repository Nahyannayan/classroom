﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public class EventService : IEventService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Events";
        #endregion


        public EventService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public bool EventCUD(SchoolEventEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new EventView();
            EntityMapper<SchoolEventEdit, EventView>.Map(model, sourceModel);
            if (model.EventId>0)
                uri = API.Event.EventUpdate(_path);
            else
                uri = API.Event.EventInsert(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public int CancelEvent(int eventId, long userId, string onlineMeetingId)
        {
            var uri = string.Empty;
            
            uri = API.Event.CancelEvent(_path, eventId, onlineMeetingId, userId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, eventId).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;

        }

        public EditModels.EventUser GeEventUserDetails(int eventId, long userId)
        {
            var uri = API.Event.GetEventUserDetails(_path, eventId, userId);
            EditModels.EventUser eventUserDetails = new EditModels.EventUser();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                eventUserDetails = EntityMapper<string, EditModels.EventUser>.MapFromJson(jsonDataProviders);
            }
            return eventUserDetails;
        }

        public bool DeleteEventFile(int eventId)
        {
            var uri = string.Empty;

            uri = API.Event.DeleteEventFile(_path, eventId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, eventId).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<SchoolEventEdit> GetSchoolEvent(int id)
        {
            var uri = API.Event.GetEvent(_path, id);
            List<SchoolEventEdit> SchoolEvent = new List<SchoolEventEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
               var sourceModel = EntityMapper<string, IEnumerable<EventView>>.MapFromJson(jsonDataProviders);
                if(sourceModel!=null)
                {
                    foreach(EventView source in sourceModel)
                    {
                        var destination = new SchoolEventEdit();
                        EntityMapper<EventView,SchoolEventEdit>.Map(source, destination);
                        SchoolEvent.Add(destination);
                    }
                }
            }
            return SchoolEvent;
        }

        public int CheckTimetableEventExists(string title, string meetingPassword, DateTime startDate, string startTime, long userId)
        {
            var uri = API.Event.CheckTimetableEventExists(_path, title,meetingPassword,startDate,startTime,userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
    }
}