﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class DurationService:IDurationService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Duration";
        #endregion


        public DurationService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        /// <summary>
        /// Author : Rohit Patil
        /// Created Date : 27-JUNE-2019
        /// Description : To fetch all Duration
        /// </summary>
        /// <param name="id">Duration id</param>
        /// <returns></returns>
        public IEnumerable<DurationView> GetDuration(int id)
        {
            var uri = API.Duration.GetDuration(_path, id);
            IEnumerable<DurationView> Duration = new List<DurationView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Duration = EntityMapper<string, IEnumerable<DurationView>>.MapFromJson(jsonDataProviders);
            }
            return Duration;
        }


    }
}