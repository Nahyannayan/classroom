﻿using Phoenix.Common.Models;
using Phoenix.Common.Models.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IEventCategoryService
    {
        IEnumerable<EventCategoryView> GetEventCategory(int id);
        IEnumerable<EventCategoryView> GetAllEventCategoryBySchool();
        IEnumerable<EventCategoryView> GetEventCategoriesBySchoolId();
        EventCategory GetEventCategoryById(int id);
        int UpdateEventCategoryData(EventCategoryEdit model);
        int DeleteEventCategoryData(int id);
        bool CheckEventCategoryAvailable(string categoryName, int id);
    }
}
