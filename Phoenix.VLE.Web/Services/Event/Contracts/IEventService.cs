﻿using Phoenix.Common.Models;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IEventService
    {
        bool EventCUD(SchoolEventEdit eventView);
        IEnumerable<SchoolEventEdit> GetSchoolEvent(int id);
        int CancelEvent(int eventId, long userId, string onlineMeetingId = "");
        bool DeleteEventFile(int eventId);
        EditModels.EventUser GeEventUserDetails(int eventId, long userId);
        int CheckTimetableEventExists(string title, string meetingPassword, DateTime startDate, string startTime, long userId);

    }
}
