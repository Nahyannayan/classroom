﻿using Phoenix.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IDurationService
    {
        IEnumerable<DurationView> GetDuration(int id);

    }
}
