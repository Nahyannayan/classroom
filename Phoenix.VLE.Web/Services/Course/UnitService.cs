﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System.Text;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers.Extensions;

namespace Phoenix.VLE.Web.Services
{

    public class UnitService : IUnitService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Unit";


        #endregion
        public UnitService()
        {

            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool AddEditUnitDetailsType(List<UnitDetailsTypeEdit> objlstUnitType, long schoolId, string standardIds, string assessmentEvidenceIds, string assessmentLearningIds, string lessonIdsToDelete,long Createdby)
        {
            var uri = API.Course.AddEditUnitDetailsType(_path, schoolId, standardIds, assessmentEvidenceIds, assessmentLearningIds, lessonIdsToDelete, Createdby);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, objlstUnitType).Result;
            return response.IsSuccessStatusCode;
        }

        public bool AddEditUnitMaster(UnitMasterEdit objUnitMaster, TransactionModes mode, string createdBy)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Course.AddEditUnitMaster(_path, mode, createdBy), objUnitMaster).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<UnitDetailsTypeEdit> GetUnitDetailsType(long unitId)
        {
            var uri = API.Course.GetUnitDetailsType(_path, unitId);
            IEnumerable<UnitDetailsTypeEdit> objUnitDetailsTypeList = new List<UnitDetailsTypeEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objUnitDetailsTypeList = EntityMapper<string, IEnumerable<UnitDetailsTypeEdit>>.MapFromJson(jsonDataProviders);
            }
            return objUnitDetailsTypeList;
        }

        public IEnumerable<UnitMasterEdit> GetUnitMasterDetails()
        {
            var uri = API.Course.GetUnitMasterDetails(_path);
            IEnumerable<UnitMasterEdit> objUnitMaterList = new List<UnitMasterEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objUnitMaterList = EntityMapper<string, IEnumerable<UnitMasterEdit>>.MapFromJson(jsonDataProviders);
            }
            return objUnitMaterList;
        }

        public IEnumerable<UnitMasterEdit> GetUnitMasterDetailsByCourseId(long courseId)
        {
            var uri = API.Course.GetUnitMasterDetailsByCourseId(_path, courseId);
            IEnumerable<UnitMasterEdit> objUnitMaterList = new List<UnitMasterEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objUnitMaterList = EntityMapper<string, IEnumerable<UnitMasterEdit>>.MapFromJson(jsonDataProviders);
            }
            return objUnitMaterList;
        }

        public UnitMasterEdit GetUnitMasterDetailsByUnitId(long Id)
        {
            var uri = API.Course.GetUnitMasterDetailsByUnitId(_path, Id);
            UnitMasterEdit objUnitMaterList = new UnitMasterEdit();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objUnitMaterList = EntityMapper<string, UnitMasterEdit>.MapFromJson(jsonDataProviders);
            }
            return objUnitMaterList;
        }

        public IEnumerable<UnitTopic> GetUnitTopicList(long MainSyllabusId)
        {
            var uri = API.Course.GetUnitTopicList(_path, MainSyllabusId);
            IEnumerable<UnitTopic> result = new List<UnitTopic>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<UnitTopic>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }


        public IEnumerable<UnitTopicStandardDetails> GetUnitTopicStandardDetails(long UnitMasterId, long GroupId, long UnitId, long StandardBankId, int PageNumber, string SearchString, long SchoolId)
        {
            var uri = API.Course.GetUnitTopicStandardDetails(_path, UnitMasterId, GroupId, UnitId, StandardBankId, PageNumber, SearchString, SchoolId);
            IEnumerable<UnitTopicStandardDetails> result = new List<UnitTopicStandardDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<UnitTopicStandardDetails>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<UnitWeek> GetUnitWeekList(long SchoolId)
        {
            var uri = API.Course.GetUnitWeekList(_path, SchoolId);
            IEnumerable<UnitWeek> result = new List<UnitWeek>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<UnitWeek>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public UnitCalendarEdit GetUnitCalendarByCourseId(long SchoolId, long courseId)
        {
            var uri = API.Course.GetUnitCalendarByCourseId(_path, SchoolId, courseId);
            UnitCalendarEdit objUnitMaterList = new UnitCalendarEdit();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objUnitMaterList = EntityMapper<string, UnitCalendarEdit>.MapFromJson(jsonDataProviders);
            }
            return objUnitMaterList;
        }

        public IEnumerable<TopicTreeView> GetStandardDetailsById(long unitId)

        {
            var uri = API.Course.GetStandardDetailsById(_path, unitId);
            IEnumerable<TopicTreeView> result = new List<TopicTreeView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<TopicTreeView>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public OperationDetails DeleteUnitStandardDetailsById(long Id)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Course.DeleteUnitStandardDetailsById(_path, Id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }
        public IEnumerable<UnitMasterEdit> GetCourseUnitMasterByCourseId(long schoolId, long courseId)
        {
            var uri = API.Course.GetCourseUnitMasterByCourseId(_path, schoolId, courseId);
            IEnumerable<UnitMasterEdit> result = new List<UnitMasterEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<UnitMasterEdit>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<UnitDetailsTypeEdit> GetUnitDetailsByCourseId(long courseId)
        {
            var uri = API.Course.GetUnitDetailsByCourseId(_path, courseId);
            IEnumerable<UnitDetailsTypeEdit> result = new List<UnitDetailsTypeEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<UnitDetailsTypeEdit>>.MapFromJson(jsonDataProviders);
            }
            return result;

        }

        public IEnumerable<Unit> GetUnitMasterByGroupId(long groupId)
        {
            var result = new List<Unit>();
            var uri = API.Course.GetUnitMasterByGroupId(_path, groupId);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Unit>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<Unit> GetCourseDetailsAndUnitList(long courseId, long teacherId)
        {
            HttpResponseMessage response = _client.GetAsync(API.Course.GetCourseDetailsAndUnitList(_path, courseId, teacherId)).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<Unit>>() : default(IEnumerable<Unit>);
        }

        public IEnumerable<UnitMasterEdit> GetUnitsByGroup(long Id)
        {
            var uri = API.Course.GetUnitsByGroup(_path, Id);

            IEnumerable<UnitMasterEdit> result = new List<UnitMasterEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<UnitMasterEdit>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<UnitMasterEdit> GetSubUnitsById(long Id)
        {
            var uri = API.Course.GetSubUnitsById(_path, Id);

            IEnumerable<UnitMasterEdit> result = new List<UnitMasterEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<UnitMasterEdit>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<Attachment> GetAttachmentDetailByUnitId(long UnitId)
        {
            HttpResponseMessage response = _client.GetAsync(API.Course.GetAttachmentDetailByUnitId(_path, UnitId)).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<Attachment>>() : new List<Attachment>();
        }
    }
}