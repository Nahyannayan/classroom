﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class CourseService :ICourseService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Course";
        #endregion

        #region Constructor
        public CourseService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        #endregion

        #region Public methods
        public OperationDetails CourseCU(Course course)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.Course.CourseCU(_path), course).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<OperationDetails>() : default(OperationDetails);
        }

        public Course GetCourseDetails(long courseId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Course.GetCourseDetails(_path, courseId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<Course>() : default(Course);
        }

        public IEnumerable<Course> GetCourses(long curriculumId, long schoolId, int PageSize, int PageNumber, string SearchString)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Course.GetCourses(_path, curriculumId, schoolId,  PageSize,  PageNumber, SearchString)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<Course>>() : default(IEnumerable<Course>);
        }

        public bool IsCourseTitleUnique(string title)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Course.IsCourseTitleUnique(_path, title)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }

        public bool CourseMappingCUD(CourseMapping course)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.Course.CourseMappingCUD(_path), course).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }
        public IEnumerable<CourseMapping> GetCourseMappings(long schoolId, long courseId, int pageNum = 1, int pageSize = 6, string searchString = "")
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Course.GetCourseMappings(_path, schoolId, courseId, pageNum, pageSize, searchString)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<CourseMapping>>() : default(IEnumerable<CourseMapping>);
        }
        public CourseMapping GetCourseMappingDetail(long courseGroupId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Course.GetCourseMappingDetail(_path, courseGroupId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<CourseMapping>() : default(CourseMapping);
        }
        public IEnumerable<SchoolWiseGrade> GetGradeListBySchoolId(string SchoolIds, long TeacherId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Course.GetGradeListBySchoolId(_path, SchoolIds, TeacherId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<SchoolWiseGrade>>() : default(IEnumerable<SchoolWiseGrade>);
        }
        public bool SaveCrossSchoolPermission(CrossSchoolPermission crossSchoolPermission)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.Course.SaveCrossSchoolPermission(_path), crossSchoolPermission).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }
        public IEnumerable<CrossSchoolPermission> GetCrossSchoolPermissionList(long TeacherId, long SchoolId, long TCS_ID =0)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Course.GetCrossSchoolPermissionList(_path, TeacherId, SchoolId, TCS_ID)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<CrossSchoolPermission>>() : default(IEnumerable<CrossSchoolPermission>);
        }

        public IEnumerable<SchoolList> GetSchoolListByTeacherId(string TeacherId)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.Course.GetSchoolListByTeacherId(_path), new SchoolWiseGrade { TeacherId = TeacherId }).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<SchoolList>>() : default(IEnumerable<SchoolList>);
        }
        public IEnumerable<GroupTeacher> GetSchoolTeachersBySchoolId(string SchoolId,string TeacherId)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.Course.GetSchoolTeachersBySchoolId(_path),new SchoolWiseGrade { SchoolId = SchoolId,TeacherId = TeacherId } ).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GroupTeacher>>() : default(IEnumerable<GroupTeacher>);
        }
        public bool IsGroupNameUnique(string title)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Course.IsGroupNameUnique(_path, title)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToBoolean() : default(bool);
        }
        #endregion

    }
}