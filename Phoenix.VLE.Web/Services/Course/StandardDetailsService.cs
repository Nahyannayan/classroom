﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public class StandardDetailsService : IStandardDetailsService
    {

        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/StandardDetails";
        #endregion

        public StandardDetailsService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public Phoenix.Models.Entities.StandardDetails GetStandardDetailsById(Int64? StandardDetailsID)
        {
            var uri = API.StandardDetails.GetStandardDetailsById(_path, StandardDetailsID);
            Phoenix.Models.Entities.StandardDetails model = new Phoenix.Models.Entities.StandardDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                model = EntityMapper<string, Phoenix.Models.Entities.StandardDetails>.MapFromJson(jsonDataProviders);
            }
            return model;
        }
        public OperationDetails SaveUpdateStandardDetails(StandardDetailsEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new Phoenix.Models.Entities.StandardDetails();
            OperationDetails op = new OperationDetails();

            EntityMapper<StandardDetailsEdit, StandardDetails>.Map(model, sourceModel);
            uri = API.StandardDetails.SaveUpdateStandardDetails(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;

            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }
        public IEnumerable<StandardDetails> GetStandardDetailsList()
        {
            var uri = API.StandardDetails.GetStandardDetailsList(_path);
            IEnumerable<StandardDetails> lstStandardDetails = new List<StandardDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStandardDetails = EntityMapper<string, IEnumerable<StandardDetails>>.MapFromJson(jsonDataProviders);
            }
            return lstStandardDetails;
        }

        public OperationDetails DeleteStandardDetails(StandardDetails standardDetails)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.StandardDetails.DeleteStandardDetails(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, standardDetails).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }
    }
}