﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class AttachmentService : IAttachmentService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Attachment";
        #endregion

        #region Constructor
        public AttachmentService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        #endregion
        public bool DeleteAttachment(long id)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.AttachmentAPI.DeleteAttachment(_path, id)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }

        public Phoenix.Models.Entities.Attachment GetAttachment(long masterId = 0, string masterKey = "", long? attachmentId = null)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.AttachmentAPI.GetAttachment(_path, masterId, masterKey, attachmentId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<Phoenix.Models.Entities.Attachment>() : default(Phoenix.Models.Entities.Attachment);
        }
    }
}