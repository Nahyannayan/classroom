﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public class StandardService : IStandardService
    {

        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Standard";
        #endregion

        public StandardService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public Standard GetStandardMasterDetailsById(Int64? StandardID)
        {
            var uri = API.Standard.GetStandardMasterDetailsById(_path, StandardID);
            Standard model = new Standard();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                model = EntityMapper<string, Standard>.MapFromJson(jsonDataProviders);
            }
            return model;
        }

        public OperationDetails SaveUpdateStandardMasterDetails(StandardEdit model)
        {
            var uri = string.Empty;
            var standard = new Standard();
            OperationDetails op = new OperationDetails();
            EntityMapper<StandardEdit, Standard>.Map(model, standard);

            uri = API.Standard.SaveUpdateStandardMasterDetails(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, standard).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }

        public IEnumerable<MainSyllabusIdList> MainSyllabusIdList()
        {
            var uri = API.Standard.MainSyllabusIdList(_path);
            IEnumerable<MainSyllabusIdList> lstStandard = new List<MainSyllabusIdList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStandard = EntityMapper<string, IEnumerable<MainSyllabusIdList>>.MapFromJson(jsonDataProviders);
            }
            return lstStandard;
        }
        public IEnumerable<Standard> GetStandardMasterList(int Acd_Id)
        {
            var uri = API.Standard.GetStandardMasterList(_path, Acd_Id);
            IEnumerable<Standard> lstStandard = new List<Standard>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStandard = EntityMapper<string, IEnumerable<Standard>>.MapFromJson(jsonDataProviders);
            }
            return lstStandard;
        }
        public IEnumerable<ParentDetails> GetParentList(Int64? SId)
        {
            var uri = API.Standard.GetParentList(_path, SId);
            IEnumerable<ParentDetails> lstStandard = new List<ParentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStandard = EntityMapper<string, IEnumerable<ParentDetails>>.MapFromJson(jsonDataProviders);
            }
            return lstStandard;
        }
        
        public OperationDetails DeleteStandardMaster(Standard standard)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Standard.DeleteStandardMaster(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, standard).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }

        public IEnumerable<Standard> GetStandardGroupName(long UnitId,long SchoolId)
        {
            var uri = API.Standard.GetStandardGroupName(_path,UnitId, SchoolId);
            IEnumerable<Standard> lstStandard = new List<Standard>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStandard = EntityMapper<string, IEnumerable<Standard>>.MapFromJson(jsonDataProviders);
            }
            return lstStandard;
        }

        public IEnumerable<SchoolTermList> GetSchoolTerm(int acdid)
        {
            var uri = API.Standard.GetSchoolTerm(_path, acdid);
            IEnumerable<SchoolTermList> lstStandard = new List<SchoolTermList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStandard = EntityMapper<string, IEnumerable<SchoolTermList>>.MapFromJson(jsonDataProviders);
            }
            return lstStandard;
        }

        public bool BulkStandardUpload(StandardUploadModel standardUploadModel)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Standard.BulkStandardUpload(_path), standardUploadModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool BulkStandardBankUpload(StandardBankUploadModel standardUploadModel)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Standard.BulkStandardBankUpload(_path), standardUploadModel).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<StandardBankExcelModel> GetStandardBankList(long SchoolId,long SB_Id=0)
        {

            var uri = API.Standard.GetStandardBankList(_path, SchoolId,SB_Id);
            IEnumerable<StandardBankExcelModel> lstStandard = new List<StandardBankExcelModel>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStandard = EntityMapper<string, IEnumerable<StandardBankExcelModel>>.MapFromJson(jsonDataProviders);
            }
            return lstStandard;
        }

        public bool AddEditStandardBank(string TransMode, string EditType,StandardBankExcelModel standardBankModel)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Standard.AddEditStandardBank(_path, TransMode,  EditType), standardBankModel).Result;
            return response.IsSuccessStatusCode;
        }

    }
}