﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class CourseCatalogueService : ICourseCatalogueService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/CourseCatalogue";
        #endregion

        #region Constructor
        public CourseCatalogueService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        #endregion

        #region Public Methods
        public GroupCatalogue GetCatalogueInfoUsingGroupId(long groupId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.CourseCatalogue.GetCatalogueInfoUsingGroupId(_path, groupId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<GroupCatalogue>() : default(GroupCatalogue);
        }

        public IEnumerable<CourseCatalogue> GetCourseCatalogueInformationByTeacher(long memberId, bool isStudent)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.CourseCatalogue.GetCourseCatalogueInformationByTeacher(_path, memberId, isStudent)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<CourseCatalogue>>() : default(IEnumerable<CourseCatalogue>);
        }
        public IEnumerable<CatalogueUnits> GetCatalogueUnits(long courseId, long memberId, bool isStudent)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.CourseCatalogue.GetCatalogueUnits(_path, courseId, memberId, isStudent)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<CatalogueUnits>>() : new List<CatalogueUnits>();
        }
        #endregion
    }
}