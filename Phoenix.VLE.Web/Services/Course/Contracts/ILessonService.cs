﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.Course.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Phoenix.VLE.Web.Services
{
    public interface ILessonService
    {
        IEnumerable<UnitDetails> GetUnitDetails();
        IEnumerable<StandardDetailsValue> GetStandardDetails();
        IEnumerable<Lesson> GetLessonDetails();
        IEnumerable<LessonCourse> GetTopicLessonDetail(long CourseId);
        IEnumerable<UnitDetails> GetUnitBasedonCourse(long CourseId);
        

        bool AddEditLesson(Lesson model);
        #region School Unit Detail type Mapping
        bool SchoolUnitDetailsTypeCD(SchoolUnitDetailTypeMapping typeMapping);
        IEnumerable<SchoolUnitDetailTypeMapping> GetSchoolUnitDetailTypes(long schoolId, int divisionId);
        IEnumerable<SchoolUnitDetailType> GetSchoolUnitDetailType(SchoolUnitDetailType schoolUnit);
        bool SaveSchoolUnitDetailType(SchoolUnitDetailType schoolUnit);
        #endregion

    }
}
