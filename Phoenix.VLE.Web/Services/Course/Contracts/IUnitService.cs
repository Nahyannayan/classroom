﻿using Phoenix.Common.Enums;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.Course.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public interface IUnitService
    {
        bool AddEditUnitMaster(UnitMasterEdit objUnitMaster, TransactionModes mode, string createdBy);
        IEnumerable<UnitMasterEdit> GetUnitMasterDetails();
        UnitMasterEdit GetUnitMasterDetailsByUnitId(long Id);
        IEnumerable<UnitMasterEdit> GetUnitMasterDetailsByCourseId(long courseId);
        IEnumerable<UnitDetailsTypeEdit> GetUnitDetailsType(long unitId);

        bool AddEditUnitDetailsType(List<UnitDetailsTypeEdit> objlstUnitType,long schoolId, string standardIds,string assessmentEvidenceIds, string assessmentLearningIds,string lessonIdsToDelete, long CreatedBy);

        IEnumerable<UnitTopic> GetUnitTopicList(long MainSyllabusId);
        IEnumerable<UnitTopicStandardDetails> GetUnitTopicStandardDetails(long UnitMasterId,long GroupId, long UnitId, long StandardBankId,int PageNumber,string SearchString,long SchoolId);

        IEnumerable<UnitWeek> GetUnitWeekList(long SchoolId);

        UnitCalendarEdit GetUnitCalendarByCourseId(long SchoolId, long courseId);
        IEnumerable<TopicTreeView> GetStandardDetailsById(long unitId);

        OperationDetails DeleteUnitStandardDetailsById(long Id);

        IEnumerable<UnitMasterEdit> GetCourseUnitMasterByCourseId(long schoolId,long courseId);
        IEnumerable<UnitDetailsTypeEdit> GetUnitDetailsByCourseId(long courseId);

        IEnumerable<Unit> GetUnitMasterByGroupId(long groupId);
        IEnumerable<Unit> GetCourseDetailsAndUnitList(long courseId, long teacherId);

        IEnumerable<UnitMasterEdit> GetUnitsByGroup(long Id);
        IEnumerable<UnitMasterEdit> GetSubUnitsById(long Id);
        IEnumerable<Attachment> GetAttachmentDetailByUnitId(long UnitId);
    }
}
