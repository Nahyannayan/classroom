﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ICourseCatalogueService
    {
        GroupCatalogue GetCatalogueInfoUsingGroupId(long groupId);
        IEnumerable<CourseCatalogue> GetCourseCatalogueInformationByTeacher(long memberId, bool isStudent);
        IEnumerable<CatalogueUnits> GetCatalogueUnits(long courseId, long memberId, bool isStudent);
    }
}
