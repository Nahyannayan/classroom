﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IStandardDetailsService
    {
        StandardDetails GetStandardDetailsById(Int64? StandardDetailsID);
        OperationDetails SaveUpdateStandardDetails(StandardDetailsEdit model);
        IEnumerable<StandardDetails> GetStandardDetailsList();
        OperationDetails DeleteStandardDetails(StandardDetails standardDetails);
    }
}
