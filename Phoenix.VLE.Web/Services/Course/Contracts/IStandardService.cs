﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
   public interface IStandardService
    {
        Standard GetStandardMasterDetailsById(Int64? StandardDetailsID);
        OperationDetails SaveUpdateStandardMasterDetails(StandardEdit model);
        IEnumerable<Standard> GetStandardMasterList(int Acd_Id);
       
        
        IEnumerable<ParentDetails> GetParentList(Int64? SId);
        IEnumerable<MainSyllabusIdList> MainSyllabusIdList();
        
        OperationDetails DeleteStandardMaster(Standard standard);
        IEnumerable<Standard> GetStandardGroupName(long UnitId,long SchoolId);

        IEnumerable<SchoolTermList> GetSchoolTerm(int acdid);
        bool BulkStandardUpload(StandardUploadModel standardUploadModel);

        bool BulkStandardBankUpload(StandardBankUploadModel standardUploadModel);
        IEnumerable<StandardBankExcelModel> GetStandardBankList(long SchoolId,long SB_Id=0);

        bool AddEditStandardBank(string TransMode, string EditType,StandardBankExcelModel standardBankModel);
        
    }
}
