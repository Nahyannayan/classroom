﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using System.Collections.Generic;

namespace Phoenix.VLE.Web.Services
{
    public interface ICourseService
    {
        OperationDetails CourseCU(Course course);
        IEnumerable<Course> GetCourses(long curriculumId, long schoolId, int PageSize, int PageNumber, string SearchString);
        Course GetCourseDetails(long courseId);
        bool IsCourseTitleUnique(string title);
        bool CourseMappingCUD(CourseMapping course);
        IEnumerable<CourseMapping> GetCourseMappings(long schoolId, long courseId, int pageNum = 1, int pageSize = 6, string searchString = "");
        CourseMapping GetCourseMappingDetail(long courseGroupId);
        IEnumerable<SchoolWiseGrade> GetGradeListBySchoolId(string SchoolIds, long TeacherId);
        bool SaveCrossSchoolPermission(CrossSchoolPermission crossSchoolPermission);
        IEnumerable<CrossSchoolPermission> GetCrossSchoolPermissionList(long TeacherId, long SchoolId, long TCS_ID = 0);
        IEnumerable<SchoolList> GetSchoolListByTeacherId(string TeacherId);
        IEnumerable<GroupTeacher> GetSchoolTeachersBySchoolId(string SchoolId, string TeacherId);
        bool IsGroupNameUnique(string title);
    }
}
