﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
   public interface IAssessmentLearningService
    {
        OperationDetails SaveUpdateAssessmentLearningDetails(AssessmentLearningEdit model,long UserId);
        IEnumerable<AssessmentLearningEdit> GetAssessmentLearningList(long UnitId);

        AssessmentLearning GetAssessmentLearningDetailsById(long AssessmentId);
    }
}
