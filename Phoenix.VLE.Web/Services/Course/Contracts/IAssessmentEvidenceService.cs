﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
   public interface IAssessmentEvidenceService
    {
        OperationDetails SaveUpdateAssessmentEvidenceDetails(AssessmentEvidenceEdit model,long UserId);
        IEnumerable<AssessmentEvidenceEdit> GetAssessmentEvidenceList(long UnitId);

        AssessmentEvidence GetAssessmentEvidenceDetailsById(long AssessmentId);
    }
}
