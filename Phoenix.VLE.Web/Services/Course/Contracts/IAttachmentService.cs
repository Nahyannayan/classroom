﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IAttachmentService
    {
        bool DeleteAttachment(long id);
        Phoenix.Models.Entities.Attachment GetAttachment(long masterId = 0, string masterKey = "", long? attachmentId = null);
    }
}
