﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public class AssessmentEvidenceService : IAssessmentEvidenceService
    {

        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/AssessmentEvidence";
        #endregion

        public AssessmentEvidenceService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

       public OperationDetails SaveUpdateAssessmentEvidenceDetails(AssessmentEvidenceEdit model,long UserId)
        {
            var uri = string.Empty;
            var assessmentEvidence = new AssessmentEvidence();
            OperationDetails op = new OperationDetails();
            EntityMapper<AssessmentEvidenceEdit, AssessmentEvidence>.Map(model, assessmentEvidence);

            uri = API.AssessmentEvidence.SaveUpdateAssessmentEvidenceDetails(_path,UserId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, assessmentEvidence).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }


        public IEnumerable<AssessmentEvidenceEdit> GetAssessmentEvidenceList(long UnitId)
        {

            var uri = API.AssessmentEvidence.GetAssessmentEvidenceList(_path, UnitId);
            IEnumerable<AssessmentEvidenceEdit> result = new List<AssessmentEvidenceEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<AssessmentEvidenceEdit>>.MapFromJson(jsonDataProviders);
            }
            return result;

           
        }
        public AssessmentEvidence GetAssessmentEvidenceDetailsById(long AssessmentId)
        {
            var uri = API.AssessmentEvidence.GetAssessmentEvidenceDetailsById(_path, AssessmentId);
            AssessmentEvidence result = new AssessmentEvidence();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, AssessmentEvidence>.MapFromJson(jsonDataProviders);
            }
            return result;

        }

    }
}