﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public class AssessmentLearningService : IAssessmentLearningService
    {

        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/AssessmentLearning";
        #endregion

        public AssessmentLearningService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

       public OperationDetails SaveUpdateAssessmentLearningDetails(AssessmentLearningEdit model,long UserId)
        {
            var uri = string.Empty;
            var assessmentLearning = new AssessmentLearning();
            OperationDetails op = new OperationDetails();
            EntityMapper<AssessmentLearningEdit, AssessmentLearning>.Map(model, assessmentLearning);

            uri = API.AssessmentLearning.SaveUpdateAssessmentLearningDetails(_path,UserId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, assessmentLearning).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }


        public IEnumerable<AssessmentLearningEdit> GetAssessmentLearningList(long UnitId)
        {

            var uri = API.AssessmentLearning.GetAssessmentLearningList(_path, UnitId);
            IEnumerable<AssessmentLearningEdit> result = new List<AssessmentLearningEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<AssessmentLearningEdit>>.MapFromJson(jsonDataProviders);
            }
            return result;

           
        }
        public AssessmentLearning GetAssessmentLearningDetailsById(long AssessmentId)
        {
            var uri = API.AssessmentLearning.GetAssessmentLearningDetailsById(_path, AssessmentId);
            AssessmentLearning result = new AssessmentLearning();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, AssessmentLearning>.MapFromJson(jsonDataProviders);
            }
            return result;

        }

    }
}