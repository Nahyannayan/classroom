﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System.Text;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.Common.Helpers.Extensions;

namespace Phoenix.VLE.Web.Services
{
    public class LessonService : ILessonService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Lesson";
        #endregion
        public LessonService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public IEnumerable<UnitDetails> GetUnitDetails()
        {
            var uri = API.Lesson.GetUnitDetails(_path);
            IEnumerable<UnitDetails> Duration = new List<UnitDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Duration = EntityMapper<string, IEnumerable<UnitDetails>>.MapFromJson(jsonDataProviders);
            }
            return Duration;
        }
        public IEnumerable<StandardDetailsValue> GetStandardDetails()
        {
            var uri = API.Lesson.GetStandardDetails(_path);
            IEnumerable<StandardDetailsValue> Duration = new List<StandardDetailsValue>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Duration = EntityMapper<string, IEnumerable<StandardDetailsValue>>.MapFromJson(jsonDataProviders);
            }
            return Duration;
        }

        public bool AddEditLesson(Lesson model)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Lesson.AddEditLesson(_path), model).Result;
            return response.IsSuccessStatusCode;

        }
        public IEnumerable<Lesson> GetLessonDetails()
        {
            var uri = API.Lesson.GetLessonDetails(_path);
            IEnumerable<Lesson> Lesson = new List<Lesson>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Lesson = EntityMapper<string, IEnumerable<Lesson>>.MapFromJson(jsonDataProviders);
            }
            return Lesson;
        }
        public IEnumerable<LessonCourse> GetTopicLessonDetail(long CourseId)
        {
            var uri = API.Lesson.GetTopicLessonDetail(_path, CourseId);
            IEnumerable<LessonCourse> Lesson = new List<LessonCourse>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Lesson = EntityMapper<string, IEnumerable<LessonCourse>>.MapFromJson(jsonDataProviders);
            }
            return Lesson;
        }

        public IEnumerable<UnitDetails> GetUnitBasedonCourse(long CourseId)
        {
            var uri = API.Lesson.GetUnitBasedonCourse(_path, CourseId);
            IEnumerable<UnitDetails> Lesson = new List<UnitDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Lesson = EntityMapper<string, IEnumerable<UnitDetails>>.MapFromJson(jsonDataProviders);
            }
            return Lesson;
        }
        #region School Unit Detail type Mapping
        public bool SchoolUnitDetailsTypeCD(SchoolUnitDetailTypeMapping typeMapping)
        {
            var response = _client.PostAsJsonAsync(API.Lesson.SchoolUnitDetailsTypeCD(_path), typeMapping).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }
        public IEnumerable<SchoolUnitDetailTypeMapping> GetSchoolUnitDetailTypes(long schoolId, int divisionId)
        {
            var response = _client.GetAsync(API.Lesson.GetSchoolUnitDetailTypes(_path, schoolId, divisionId)).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<SchoolUnitDetailTypeMapping>>() : new List<SchoolUnitDetailTypeMapping>();
        }

        public IEnumerable<SchoolUnitDetailType> GetSchoolUnitDetailType(SchoolUnitDetailType schoolUnit)
        {
            var response = _client.PostAsJsonAsync(API.Lesson.GetSchoolUnitDetailType(_path), schoolUnit).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<SchoolUnitDetailType>>() : new List<SchoolUnitDetailType>();
        }

        public bool SaveSchoolUnitDetailType(SchoolUnitDetailType schoolUnit)
        {
            var response = _client.PostAsJsonAsync(API.Lesson.SaveSchoolUnitDetailType(_path), schoolUnit).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.ToBoolean() : false;
        }
        #endregion

    }
}