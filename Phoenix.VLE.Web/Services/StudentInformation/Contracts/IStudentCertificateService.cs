﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public interface IStudentCertificateService
    {
        IList<StudentCertificate> GetAllStudentCertificates(long id);
        bool InsertCertificateFile(CertificateFile model);
        bool DeleteUserCertificate(StudentCertificate model);
        bool UpdateCertificateStatus(StudentCertificate model);
        bool ApproveCertificateStatus(StudentCertificate model);
        IList<Certificate> GetStudentCertificates(long id, int isTeacher);
        CertificateEdit GetStudentCertificatesById(long id);
         int InsertCertificate(CertificateEdit model);
       
    }
}
