﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.VLE.Web.Services
{
    public interface IStudentAcademicService
    {
        StudentAcademic GetStudentAcademicData(long id);
        bool UpdateAssignmentStatus(AssignmentStudentDetails model);
        bool UpdateAllAssignmentStatus(List<AssignmentStudentDetails> lstModel);
        bool InsertAcademicFile(File file, long userId);
        bool DeleteAcademicFile(AcademicDocuments document);
        bool ToggleDashboardAcademicFiles(AcademicDocuments documentModel);
        bool UpdateAssessmentReportStatus(List<StudentAssessmentReportView> studentAssessment);
        bool DeleteAcademicAssignment(AssignmentStudentDetails model);
        AcademicDocuments GetStudentAcademicDetailById(int assignmentId, long userId);
        IEnumerable<AssignmentFile> GetStudentAcademicAssignments(long id, string searchString);
    }
}
