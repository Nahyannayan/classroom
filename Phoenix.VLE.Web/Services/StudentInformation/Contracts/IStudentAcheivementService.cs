﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using System.Collections.Generic;

namespace Phoenix.VLE.Web.Services
{
    public interface IStudentAcheivementService
    {
        List<StudentIncident> GetStudentAchievements(long userId, BehaviourCategoryTypes type, int pageIndex, int pageSize, string searchString);
        List<StudentAchievement> GetStudentAchievementsWithFiles(long userId, int pageIndex, int pageSize, string searchString);
        bool InsertStudentAchievement(StudentAchievement model);
        bool UpdateAchievementsPortfolioStatus(List<StudentAchievement> lstModel);
        bool UpdateAcheivementDashboardStatus(StudentIncident model);
        bool DeleteAcheivementFile(AchievementFiles acheivementFile);
        bool InsertAchievementFile(AchievementFiles acheivementFile);
        StudentAchievement GetStudentachevementById(long acheivementId);
        bool DeleteAcheivement(StudentAchievement model);
        bool UpdateAcheivementApprovalStatus(StudentAchievement model);
    }
}
