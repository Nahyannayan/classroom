﻿using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
   public interface IStudentListService
    {
        IEnumerable<Student> GetStudentsInGroup(int groupId);
        IEnumerable<Student> GetStudentsNotInGroup(int schoolId, int groupId);
        IEnumerable<Student> GetStudentsInSelectedGroups(string groupIds);
        IEnumerable<Student> GetStudentDetailsByIds(string studentIds);
        Student GetStudentDetailsByStudentId(int studentId);
        IEnumerable<StudentWithSection> GetStudentByGradeIds(string gradeIds);
        IEnumerable<Course> GetStudentCourses(long studentId);
        IEnumerable<StudentWithSection> GetStudentByGradeSection(long gradeId, long sectionId);
        IEnumerable<Student> GetStudentDetailsByIdsPaginate(string studentIds,int page,int size);
        IEnumerable<UserEmailAccountView> GetParentsByStudentIds(string v);
        IEnumerable<Student> GetStudentsWithStaffInGroup(int groupId);
    }
}
