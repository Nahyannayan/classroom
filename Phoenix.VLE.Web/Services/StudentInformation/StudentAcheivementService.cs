﻿using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class StudentAcheivementService : IStudentAcheivementService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/StudentAchievements";
        #endregion


        public StudentAcheivementService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public bool InsertStudentAchievement(StudentAchievement model)
        {
            var url = API.StudentInformation.InsertStudentAchievement(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(url, model).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        public List<StudentIncident> GetStudentAchievements(long userId, BehaviourCategoryTypes type, int pageIndex = 1, int pageSize = 5, string searchString = "")
        {
            var lst = new List<StudentIncident>();
            var uri = API.StudentInformation.GetStudentAchievements(_path, userId, pageIndex, pageSize, searchString, (short)type);
            HttpResponseMessage responseMessage = _client.GetAsync(uri).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = responseMessage.Content.ReadAsStringAsync().Result;
                lst = EntityMapper<string, List<StudentIncident>>.MapFromJson(jsonDataProviders);
            }
            return lst;
        }

        public List<StudentAchievement> GetStudentAchievementsWithFiles(long userId, int pageIndex = 1, int pageSize = 5, string searchString = "")
        {
            var lst = new List<StudentAchievement>();
            var uri = API.StudentInformation.GetStudentAchievementsWithFiles(_path, userId, pageIndex, pageSize, searchString, SessionHelper.CurrentSession.Id);
            HttpResponseMessage responseMessage = _client.GetAsync(uri).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = responseMessage.Content.ReadAsStringAsync().Result;
                lst = EntityMapper<string, List<StudentAchievement>>.MapFromJson(jsonDataProviders);
            }
            return lst;
        }
        public bool UpdateAchievementsPortfolioStatus(List<StudentAchievement> lstModel)
        {
            var uri = API.StudentInformation.UpdateAchievementsPortfolioStatus(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, lstModel).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        public bool UpdateAcheivementDashboardStatus(StudentIncident model)
        {
            var uri = API.StudentInformation.UpdateAcheivementDashboardStatus(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, model).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public bool DeleteAcheivementFile(AchievementFiles acheivementFile)
        {
            var uri = API.StudentInformation.DeleteAcheivementFile(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, acheivementFile).Result;
            return response.IsSuccessStatusCode;
        }

        public bool InsertAchievementFile(AchievementFiles acheivementFile)
        {
            var uri = API.StudentInformation.InsertAchievementFile(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, acheivementFile).Result;
            return response.IsSuccessStatusCode;
        }

        public StudentAchievement GetStudentachevementById(long acheivementId)
        {
            var studentAcheivement = new StudentAchievement();
            var uri = API.StudentInformation.GetStudentachevementById(_path, acheivementId);
            HttpResponseMessage responseMessage = _client.GetAsync(uri).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = responseMessage.Content.ReadAsStringAsync().Result;
                studentAcheivement = EntityMapper<string, StudentAchievement>.MapFromJson(jsonDataProviders);
            }
            return studentAcheivement;
        }

        public bool DeleteAcheivement(StudentAchievement model)
        {
            var uri = API.StudentInformation.DeleteAcheivement(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateAcheivementApprovalStatus(StudentAchievement model)
        {
            var uri = API.StudentInformation.UpdateAcheivementApprovalStatus(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }
    }
}