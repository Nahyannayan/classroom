﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Newtonsoft.Json.Linq;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class StudentListService : IStudentListService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/StudentList";
        #endregion

        public StudentListService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        #endregion
        public IEnumerable<Student> GetStudentsInGroup(int groupId)
        {
            var uri = API.StudentInformation.GetStudentsInGroup(_path, groupId);
            IEnumerable<Student> lstStudents = new List<Student>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudents = EntityMapper<string, IEnumerable<Student>>.MapFromJson(jsonDataProviders);
            }
            return lstStudents;
        }

        public IEnumerable<Student> GetStudentsNotInGroup(int schoolId,int groupId)
        {
            var uri = API.StudentInformation.GetStudentsNotInGroup(_path, schoolId,groupId);
            IEnumerable<Student> lstStudents = new List<Student>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudents = EntityMapper<string, IEnumerable<Student>>.MapFromJson(jsonDataProviders);
            }
            return lstStudents;
        }

        public IEnumerable<Student> GetStudentsInSelectedGroups(string groupIds)
        {
            var uri = API.StudentInformation.GetStudentsInSelectedGroups(_path);
            IEnumerable<Student> lstStudents = new List<Student>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, groupIds).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudents = EntityMapper<string, IEnumerable<Student>>.MapFromJson(jsonDataProviders);
            }
            return lstStudents;
        }
        public IEnumerable<Course> GetStudentCourses(long studentId)
        {
            var uri = API.StudentInformation.GetStudentCourses(_path);
            IEnumerable<Course> lstCourses = new List<Course>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentId).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstCourses = EntityMapper<string, IEnumerable<Course>>.MapFromJson(jsonDataProviders);
            }
            return lstCourses;
        }
        public IEnumerable<Student> GetStudentDetailsByIds(string studentIds)
        {
            var uri = API.StudentInformation.GetStudentDetailsByIds(_path);
            IEnumerable<Student> lstStudents = new List<Student>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri,studentIds).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudents = EntityMapper<string, IEnumerable<Student>>.MapFromJson(jsonDataProviders);
            }
            return lstStudents;
        }

        public Student GetStudentDetailsByStudentId(int studentId)
        {
            var uri = API.StudentInformation.GetStudentDetailsByStudentId(_path, studentId);
            Student student = new Student();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                student = EntityMapper<string, Student>.MapFromJson(jsonDataProviders);
            }
            return student;
        }
        public IEnumerable<StudentWithSection> GetStudentByGradeIds(string gradeIds)
        {
            var uri = API.StudentInformation.GetStudentByGradeIds(_path, gradeIds);            
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<StudentWithSection>>() : default(IEnumerable<StudentWithSection>);
        }
        public IEnumerable<StudentWithSection> GetStudentByGradeSection(long gradeId, long sectionId)
        {
            var uri = API.StudentInformation.GetStudentByGradeSection(_path, gradeId, sectionId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<StudentWithSection>>() : default(IEnumerable<StudentWithSection>);
        }
        public IEnumerable<Student> GetStudentDetailsByIdsPaginate(string studentIds, int page, int size)
        {
            var uri = API.StudentInformation.GetStudentDetailsByIdsPaginate(_path, page, size);
            IEnumerable<Student> lstStudents = new List<Student>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentIds).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudents = EntityMapper<string, IEnumerable<Student>>.MapFromJson(jsonDataProviders);
            }
            return lstStudents;
        }

        public IEnumerable<UserEmailAccountView> GetParentsByStudentIds(string studentIds)
        {
            IEnumerable<UserEmailAccountView> parentDetails = new List<UserEmailAccountView>();
            var uri = API.StudentInformation.GetParentsByStudents(_path, studentIds);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                parentDetails = EntityMapper<string, IEnumerable<UserEmailAccountView>>.MapFromJson(jsonDataProviders);
            }
            return parentDetails;
        }

        public IEnumerable<Student> GetStudentsWithStaffInGroup(int groupId)
        {
            var uri = API.StudentInformation.GetStudentsWithStaffInGroup(_path, groupId);
            IEnumerable<Student> lstStudents = new List<Student>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudents = EntityMapper<string, IEnumerable<Student>>.MapFromJson(jsonDataProviders);
            }
            return lstStudents;
        }
    }
}