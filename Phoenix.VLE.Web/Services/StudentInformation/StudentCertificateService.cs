﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class StudentCertificateService : IStudentCertificateService
    {

        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/StudentCertificates";
        #endregion

        public StudentCertificateService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool DeleteUserCertificate(StudentCertificate model)
        {
            var uri = API.StudentInformation.UpdateDeleteUserCertificate(_path, 'D');
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }


        public bool UpdateCertificateStatus(StudentCertificate model)
        {
            var uri = API.StudentInformation.UpdateDeleteUserCertificate(_path, 'U');
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public IList<StudentCertificate> GetAllStudentCertificates(long id)
        {
            IEnumerable<StudentCertificate> lstStudentCertificates = new List<StudentCertificate>();
            var uri = API.StudentInformation.GetAllStudentCertificates(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudentCertificates = EntityMapper<string, IEnumerable<StudentCertificate>>.MapFromJson(jsonDataProviders);
            }
            return lstStudentCertificates.ToList();

        }

        public bool InsertCertificateFile(CertificateFile model)
        {
            var uri = API.StudentInformation.InsertCertificateFile(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;

        }
        public IList<Certificate> GetStudentCertificates(long id,int isTeacher)
        {
            IEnumerable<Certificate> lstStudentCertificates = new List<Certificate>();
            var uri = API.StudentInformation.GetStudentCertificates(_path, id, isTeacher);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudentCertificates = EntityMapper<string, IEnumerable<Certificate>>.MapFromJson(jsonDataProviders);
            }
            return lstStudentCertificates.ToList();

        }
        public CertificateEdit GetStudentCertificatesById(long id)
        {
           Certificate lstStudentCertificates = new Certificate();
            CertificateEdit certificate = new CertificateEdit();
            var uri = API.StudentInformation.GetStudentCertificateById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudentCertificates = EntityMapper<string, Certificate>.MapFromJson(jsonDataProviders);
                if (lstStudentCertificates != null)
                {
                    EntityMapper<Certificate, CertificateEdit>.Map(lstStudentCertificates, certificate);
                }
               
            }
            return certificate;

           

        }

        public int InsertCertificate(CertificateEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new Certificate();
            EntityMapper<CertificateEdit, Certificate>.Map(model, sourceModel);
            sourceModel.CreatedBy = (int)SessionHelper.CurrentSession.Id;

            if (model.IsAddMode)
                uri = API.StudentInformation.InsertCertificate(_path);
            else
                uri = API.StudentInformation.UpdateCertificate(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public bool ApproveCertificateStatus(StudentCertificate model)
        {
            var uri = API.StudentInformation.UpdateDeleteUserCertificate(_path, 'A');
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }
    }
}