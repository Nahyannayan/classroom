﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class StudentAcademicService : IStudentAcademicService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/StudentAcademic";
        #endregion

        public StudentAcademicService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool DeleteAcademicFile(AcademicDocuments document)
        {
            var uri = API.StudentInformation.DeleteAcademicFile(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, document).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public StudentAcademic GetStudentAcademicData(long id)
        {
            var uri = API.StudentInformation.GetStudentAcademicData(_path, id);
            var studentAcademicData = new StudentAcademic();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentAcademicData = EntityMapper<string, StudentAcademic>.MapFromJson(jsonDataProviders);
            }
            return studentAcademicData;
        }

        public bool InsertAcademicFile(File file, long userId)
        {
            var uri = API.StudentInformation.InsertAcademicFile(_path, userId);
            var studentAcademicData = new StudentAcademic();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, file).Result;
            return response.IsSuccessStatusCode;
        }

        public bool ToggleDashboardAcademicFiles(AcademicDocuments documentModel)
        {
            var uri = API.StudentInformation.UpdateAcademicFileDashboardStatus(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, documentModel).Result;
            return response.IsSuccessStatusCode; ;
        }

        public bool UpdateAssignmentStatus(AssignmentStudentDetails model)
        {
            var uri = API.StudentInformation.UpdateAssignmentStatus(_path);
            var studentAcademicData = new StudentAcademic();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateAllAssignmentStatus(List<AssignmentStudentDetails> lstModel)
        {
            var uri = API.StudentInformation.UpdateAllAssignmentStatus(_path);
            var studentAcademicData = new StudentAcademic();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, lstModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateAssessmentReportStatus(List<StudentAssessmentReportView> studentAssessment)
        {
            var uri = API.StudentInformation.UpdateAssessmentReportStatus(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentAssessment).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteAcademicAssignment(AssignmentStudentDetails model)
        {
            var uri = API.StudentInformation.DeleteAcademicAssignment(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public AcademicDocuments GetStudentAcademicDetailById(int assignmentId, long userId)
        {
            AcademicDocuments model = new AcademicDocuments();
            var uri = API.StudentInformation.GetStudentAcademicDetailById(_path, assignmentId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                model = EntityMapper<string, AcademicDocuments>.MapFromJson(jsonDataProviders);
            }
            return model;
        }

        public IEnumerable<AssignmentFile> GetStudentAcademicAssignments(long id, string searchString)
        {
            IEnumerable<AssignmentFile> lst = new List<AssignmentFile>();
            var uri = API.StudentInformation.GetStudentAcademicAssignments(_path, id, searchString);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lst = EntityMapper<string, IEnumerable<AssignmentFile>>.MapFromJson(jsonDataProviders);
            }
            return lst;
        }
    }
}