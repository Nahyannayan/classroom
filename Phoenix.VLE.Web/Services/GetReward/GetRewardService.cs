﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class GetRewardService : IGetRewardService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        //readonly string _path = "api/v1/TimeTable";
        //readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        //readonly string allLeaveDetailsAPI = ConfigurationManager.AppSettings["GetLeaveDetailsAPI"];
        //readonly string addLeaveRequestAPI = ConfigurationManager.AppSettings["AddLeaveRequestAPI"];
        //readonly string GetLeaveTypeAPI = ConfigurationManager.AppSettings["GetLeaveTypeAPI"];
        //readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        //readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        //readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        //readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];
        readonly string GET_REWARD_REDEMPTION_DETAILS = ConfigurationManager.AppSettings["GET_REWARD_REDEMPTION_DETAILS"];
        readonly string POST_REWARD_REDEMPTION_DETAILS = ConfigurationManager.AppSettings["POST_REWARD_REDEMPTION_DETAILS"];
        readonly string POST_REWARD_REDEEM = ConfigurationManager.AppSettings["POST_REWARD_REDEEM"];
        readonly string GET_REDEMPTION_HISTORY = ConfigurationManager.AppSettings["GET_REDEMPTION_HISTORY"];
        private ILoggerClient _loggerClient;
        private readonly IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        public GetRewardService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                // _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion
        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
            //    _client.DefaultRequestHeaders.Accept.Clear();
            //    var uri = tokenAPI;
            //    string _ContentType = "application/x-www-form-urlencoded";
            //    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //    var nvc = new List<KeyValuePair<string, string>>();
            //    nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //    nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //    nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //    nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //    using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //    {
            //        using (var result = _client.SendAsync(test).Result)
            //        {
            //            if (result.IsSuccessStatusCode)
            //            {
            //                var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                try
            //                {
            //                    JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                }
            //                finally
            //                {
            //                    result.Dispose();
            //                }
            //            }
            //        }
            //    }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}
            return JsonDeserilize;
        }

        public RewardRedemption GetRewardRedemptions(string stuId, string isPaySibling, string paymentTo)
        {
            string json = "";
            RewardRedemption getRewardRedemptionList = new RewardRedemption();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GET_REWARD_REDEMPTION_DETAILS);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                httpRequestMessage.Headers.Add("stuId", stuId);
                httpRequestMessage.Headers.Add("isPaySibling", Convert.ToString(isPaySibling));
                httpRequestMessage.Headers.Add("paymentTo", paymentTo);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        getRewardRedemptionList = new JavaScriptSerializer().Deserialize<RewardRedemption>(json);
                    }
                }
                return getRewardRedemptionList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in GetRewardService.GetRewardRedemptions()" + ex.Message);
                return getRewardRedemptionList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, getRewardRedemptionList, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public IEnumerable<RedemptionHistory> GetRedemptionHistory(string stuId, string parentID)
        {
            string json = string.Empty;
            List<RedemptionHistory> redHistList = new List<RedemptionHistory>();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GET_REDEMPTION_HISTORY);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                httpRequestMessage.Headers.Add("stuId", stuId);
                httpRequestMessage.Headers.Add("parentID", parentID);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        redHistList = new JavaScriptSerializer().Deserialize<List<RedemptionHistory>>(json);
                    }
                }
                return redHistList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in GetRewardService.GetRedemptionHistory()" + ex.Message);
                return redHistList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, redHistList, response, SessionHelper.CurrentSession.UserName);
            }
        }

        public RedemptionResponse PostRewardRedemptions(RewardRedemption rewardRedemption, string paymentTo)
        {
            string json = "";
            RedemptionResponse obj = new RedemptionResponse();
            var payload = string.Empty;
            var startStr = string.Empty;
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, POST_REWARD_REDEMPTION_DETAILS);
            try
            {
                foreach (var item in rewardRedemption.FeeDetails)
                {
                    string jsonFeeDetails = JsonConvert.SerializeObject(item.StudFeeDetails.Select(m => new { m.BlockPayNow, m.AdvancePaymentAvailable, m.AdvanceDetails, m.DiscAmount, m.DueAmount, m.PayAmount, m.OriginalAmount, m.FeeDescription, m.FeeID }));
                    string jsonDiscountDetails = JsonConvert.SerializeObject(item.DiscountDetails);
                    jsonFeeDetails = jsonFeeDetails == "null" ? "[]" : jsonFeeDetails;
                    jsonDiscountDetails = jsonDiscountDetails == "null" ? "[]" : jsonDiscountDetails;

                    payload += "," + "{\"STU_NO\":\"" + item.STU_NO + "\",\"OnlinePaymentAllowed\":\"" + item.OnlinePaymentAllowed
                            + "\",\"UserMessageforOnlinePaymentBlock\":\"" + item.UserMessageforOnlinePaymentBlock
                            + "\",\"IpAddress\":\"" + item.IpAddress + "\",\"PaymentTypeID\":\"" + item.PaymentTypeID
                            + "\",\"PaymentProcessingCharge\":\"" + item.PaymentProcessingCharge
                            + "\",\"PayingAmount\":\"" + item.PayingAmount + "\",\"PayMode\":\"" + item.PayMode
                            + "\",\"FeeDetail\":" + Convert.ToString(jsonFeeDetails) + ",\"DiscountDetails\":" + Convert.ToString(jsonDiscountDetails) + "}";
                }
                payload = payload.Substring(1);
                payload = string.Format("[{0}]", payload);

                startStr = "{\"persistentId\":\"" + rewardRedemption.persistentId + "\",\"logUserName\":\"" + rewardRedemption.logUserName
                         + "\",\"studentsFeeDetails\":" + payload + ",\"available_points\":\"" + rewardRedemption.available_points
                         + "\",\"redeemable_points\":\"" + rewardRedemption.redeemable_points
                         + "\",\"redeemable_amount\":\"" + rewardRedemption.redeemable_amount + "\"}";

                HttpContent content = new StringContent(startStr, Encoding.UTF8, "application/json");

                httpRequestMessage.Content = content;
                httpRequestMessage.Headers.Add("paymentTo", paymentTo);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        obj = new JavaScriptSerializer().Deserialize<RedemptionResponse>(json);
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in GetRewardService.PostRewardRedemptions()" + ex.Message);
                return obj;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, obj, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public string PostRewardRedeem(string paymentTo, string redemptionRefNo, double totalAmount)
        {
            string json = "";
            RewardRedemption getRewardRedemptionList = new RewardRedemption();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, POST_REWARD_REDEEM);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                httpRequestMessage.Headers.Add("paymentTo", paymentTo);
                httpRequestMessage.Headers.Add("redemptionRefNo", redemptionRefNo);
                httpRequestMessage.Headers.Add("totalAmount", Convert.ToString(totalAmount));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in GetRewardService.PostRewardRedeem()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }
        }
    }
}
