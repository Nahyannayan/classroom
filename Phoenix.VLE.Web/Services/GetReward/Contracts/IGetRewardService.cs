﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IGetRewardService
    {
        RewardRedemption GetRewardRedemptions(string stuId, string isPaySibling, string paymentTo);
        RedemptionResponse PostRewardRedemptions(RewardRedemption rewardRedemption, string paymentTo);
        string PostRewardRedeem(string paymentTo, string redemptionRefNo, double totalAmount);
        IEnumerable<RedemptionHistory> GetRedemptionHistory(string stuId, string parentID);
    }
}
