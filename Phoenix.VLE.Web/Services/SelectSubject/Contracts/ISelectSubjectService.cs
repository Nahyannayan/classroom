﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services.SelectSubject.Contracts
{
    public interface ISelectSubjectService
    {
        List<GetOption> GetCourseWithCategoryList(string StuNumber);
        string PostCourseSelection(SaveCourseModel SaveCourseModel);

    }
}
