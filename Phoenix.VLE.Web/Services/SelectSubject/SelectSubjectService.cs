﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.SelectSubject.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services.SelectSubject
{
    public class SelectSubjectService : ISelectSubjectService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.SIMSApiUrl;
        readonly string _path = "api/v1/TimeTable";
        private IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
       

        private ILoggerClient _loggerClient;
        public SelectSubjectService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion
        
        public List<GetOption> GetCourseWithCategoryList(string studId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            GetOptionRoot getOptionRoot = new GetOptionRoot();
            List<GetOption> GetOptionList = new List<GetOption>();
            var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
            try
            {
                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;
                        httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GetCoursesWithCatgoryList);


                        httpRequestMessage.Headers.Add("StudentID", studId.ToString());
                        httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                        using (response = _client.SendAsync(httpRequestMessage).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                JavaScriptSerializer se = new JavaScriptSerializer();
                                json = response.Content.ReadAsStringAsync().Result;

                                getOptionRoot = new JavaScriptSerializer().Deserialize<GetOptionRoot>(json);

                                if (getOptionRoot.data != null && getOptionRoot.data.Count > 0)
                                    GetOptionList = getOptionRoot.data;
                            }
                        }

                    }
                }
                return GetOptionList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in SelectSubjectService.GetCourseWithCategoryList()" + ex.Message);
                return GetOptionList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, GetOptionList, response, SessionHelper.CurrentSession.UserName);
            }
        }


        public string PostCourseSelection(SaveCourseModel SaveCourseModel)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
            //bool Json = false;
            string Json = string.Empty;
            try
            {
                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;

                        using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.SaveCourseSelection))
                        {
                            var data = JsonConvert.SerializeObject(SaveCourseModel);
                            HttpContent content = new StringContent(data, Encoding.UTF8, "application/json");

                            httpRequestMessage.Content = content;
                            httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                            using (response = _client.SendAsync(httpRequestMessage).Result)
                            {
                                if (response.IsSuccessStatusCode)
                                {
                                    JavaScriptSerializer se = new JavaScriptSerializer();
                                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                                    JasonResultFileter result = new JavaScriptSerializer().Deserialize<JasonResultFileter>(jsonDataProviders);
                                    //Json = Convert.ToBoolean(result.success);
                                    Json = jsonDataProviders;
                                }

                            }
                        }
                    }
                }
                return Json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in SelectSubjectService.PostCourseSelection()" + ex.Message);
                return Json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, Json, response, SessionHelper.CurrentSession.UserName);
            }
        }

    }

}