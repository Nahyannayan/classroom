﻿using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IAssignmentService
    {
        Assignment GetAssignments(int teacherId);
        IEnumerable<AssignmentStudentDetails> GetAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "");
        IEnumerable<AssignmentStudentDetails> GetArchivedAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "");

        IEnumerable<AssignmentStudentDetails> GeStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "", string filterVal = "");
        IEnumerable<AssignmentStudentDetails> GetArchivedStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "", string filterVal = "");
        IEnumerable<AssignmentStudentDetails> GetAssignmentByStudentIdWithoutPagination(long studentId, string assignmentType = "");
        IEnumerable<AssignmentStudentDetails> GetStudentAssignmentByUserId(int pageNumber, int pageSize, long userId, string searchString = "");
        IEnumerable<AssignmentStudentDetails> GetAssignmentByStudentIdWithDateRange(int PageNumber, int PageSize, long studentId, DateTime startDate, DateTime endDate, string SearchString = "", string assignmentType = "");
        IEnumerable<AssignmentStudentDetails> GetAssignmentByStudentIdAndTeacherIdWithDateRange(int PageNumber, int PageSize, long studentId, long teacherId, DateTime startDate, DateTime endDate, string SearchString = "", string assignmentType = "");
        Assignment GetAssignmentById(int id);

        AssignmentStudent GetAssignmentSubmitMarksByAssignmentStudentId(int assignmentStudentId);

        StudentTask GetTaskSubmitMarksByTaskId(int TaskId);

        OperationDetails GetAssignmentPermissions(int assignmentId, long UserId);

        OperationDetails AssignmentUnArchive(int assignmentId);
        OperationDetails AddEditAssignmentCategory(int AssignmentCategoryId, bool IsActive,long SchoolId);
        OperationDetails AddAssignmentCategoryMaster(string AssignmentCategoryTitle,string AssignmentCategoryDesc,int CategoryId, bool IsActive, long SchoolId);
        AssignmentCategoryDetails GetAssignmentCategoryMasterById(int id);
        IEnumerable<AssignmentCategoryDetails> GetAssignmentCategoryBySchoolId(long SchoolId);
        GroupQuiz GetAssignmentQuizDetailsByTaskId(int taskId);

        OperationDetails SeenStudentAssignmentComment(AssignmentComment objAssignmentComment);
        Assignment GetExcelReportDataAssignmentById(int id);
        OperationDetails UpdateAssignmentData(AssignmentEdit model, List<Objective> lstObjective, List<CourseUnit> lstCourseUnit,List<GroupUnit> lstGroupTopicUnit);
        OperationDetails SaveAssignmentData(AssignmentEdit model);
        bool SaveAssignmentFiles(List<AssignmentFile> lstModel);
        OperationDetails DeleteAssignmentData(Assignment assignment);

        OperationDetails DeleteAssignmentComment(AssignmentComment assignmentcomment);

        IEnumerable<AssignmentStudent> GetAssignmentStudent();

        IEnumerable<AssignmentStudentDetails> GetAssignmentStudentDetails(long TeacherId, AssignmentFilter loadAssignment);
        IEnumerable<AssignmentStudentDetails> GetAdminAssignmentStudentDetails(AssignmentFilter loadAssignment);

        IEnumerable<AssignmentStudentDetails> GetFilterSchoolGroupsById(long TeacherId, AssignmentFilter loadAssignment);

        IEnumerable<AssignmentStudentDetails> GetArchivedFilterSchoolGroupsById(long TeacherId, AssignmentFilter loadAssignment);

        IEnumerable<AssignmentStudentDetails> GetAssignmentStudentDetailsById(int Id);
        IEnumerable<AssignmentTask> GetAssignmentTasks(int assignmentId);
        IEnumerable<AssignmentReport> GetAssignmentTaskQuizByAssignmentId(int assignmentId);
        bool AssignGradeToStudentAssignment(int studentAssignmentId, int gradingTemplateItemId, int GradedBy);
        IEnumerable<AssignmentFile> GetAssignmentFiles(int assignmentId);
        IEnumerable<TaskFile> GetTaskFilesByTaskId(int taskId);
        AssignmentStudent GetStudentAsgHomeworkDetailsById(int assignmentStudentId, int IsSeenByStudent = 0);
        AssignmentTask GetAssignmentTaskbyTaskId(int assignmentId);
        IEnumerable<SchoolGroup> GetSelectedSchoolGroupsByAssignmentID(int assignmentId);
        IEnumerable<Course> GetSelectegCoursesByAssignmentId(int assignmentId);
        IEnumerable<AssignmentStudent> GetStudentsByAssignmentId(int assignmentId, int groupId);
        IEnumerable<AssignmentStudent> GetAssignmentCompletedStudentsByAssignmentId(int assignmentId);
        AssignmentReport GetExcelReportDataStudentsByAssignmentId(int assignmentId);
        bool MarkAsComplete(int assignmentStudentId, bool isTeacher, int CompletedBy);
        GradingTemplateItem SubmitAssignmentMarks(int assignmentStudentId,decimal submitMarks,int CompletedBy,int gradingTemplateId,int SystemLanguageId);
        bool UploadStudentAssignmentFile(List<StudentAssignmentFile> lstFiles, int studentId, int assignmentId, string IpDetails);
        IEnumerable<StudentAssignmentFile> GetStudentAssignmentFiles(int studentId, int assignmentId);
        IEnumerable<AssignmentFile> GetSavedAssignmentFiles(int assignmentId, long userId);
        IEnumerable<AssignmentFeedbackFiles> GetAssignmentFeedbackFiles(int assignmentStudentId, int assignmentCommentId);
        bool MarkAsIncompleteAssignment(int studentId, int assignmentId, bool isReSubmitAssignnment);
        //added for the student task file
        bool UploadStudentTaskFiles(List<StudentTaskFile> lststudentTaskFiles, int studentId, int taskId, string IpDetails);

        IEnumerable<StudentTaskFile> GetStudentTaskFiles(int studentId, int taskid);
        StudentTask GetStudentTaskDetails(int taskId, int studentId);
        AssignmentTask GetStudentTaskDetailById(int taskId, int studentId, int AssignmentStudentId, long StudentTaskId);
        bool InsertAssignmentComment(AssignmentComment entity);
        IEnumerable<AssignmentComment> GetAssignmentComments(int assignmentStudentId);
        bool MarkAsCompleteTask(long studentTaskId, bool isByteacher, int CompletedBy);
        GradingTemplateItem SubmitTaskMarks(int studentTaskId,int StudentAssignmentId, decimal submitMarks,int CompletedBy,int gradingTemplateId,int languageId);

        bool AssignGradeToStudentTask(int studentTaskId, int gradeId, int GradedBy);
        bool AssignGradeToStudentObjective(int studentObjectiveId, int gradeId);
        IEnumerable<MyFilesTreeItem> GetMyFiles(long userId);
        IEnumerable<File> GetStudentAssignmentObjectiveAudiofeedback(int studentObjectiveId);
        bool InsertTaskFeedback(AssignmentTask assignmentTask);
        bool InsertStudentObjectiveFeedback(AssignmentStudentObjective studentObjective);
        IEnumerable<AssignmentFile> GetAssignmentMyFiles(long fileId, bool isFolder);
        bool DeleteUploadedFiles(int FileId, int AssignmentFileId, long UserId);
        bool DeleteStudentAssignmentFile(int FileId, long UserId, bool IsTaskFile);
        File GetAssignmentFilebyAssignmentFileId(long id);
        File GetStudentTaskFile(long id);
        File GetTaskFile(long id);
        File GetStudentAssignmentFilebyStudentAsgFileId(long id);
        bool UpdateActiveAssignmets(long assignmentId);
        IEnumerable<AssignmentStudentDetails> GetAssignmentListForReport(int pageNumber, int pageSize, string searchString, long userId, long schoolId, int schoolGroupId, DateTime startDate, DateTime endDate, string userType);
        IEnumerable<AssignmentStudentDetails> GetArchivedAssignmentStudentDetails(long TeacherId, AssignmentFilter loadAssignment);
        IEnumerable<AssignmentStudentDetails> GetAdminArchivedAssignmentStudentDetails(AssignmentFilter loadAssignment);
        OperationDetails AddUpdatePeermarking(List<AssignmentPeerReview> lstPeerReviewMapping);
        IEnumerable<AssignmentPeerReview> GetPeerAssignmentDetails(long assignmentId, long userId);
        IEnumerable<AssignmentPeerReview> GetPeerMappingDetails(long assignmentId);
        OperationDetails AddUpdateDocumentReviewDetails(List<DocumentReviewdetails> lstDocumentReviewdetails);
        IEnumerable<DocumentReviewdetails> GetReviewedDocumentFiles(long peerReviewId);
        DocumentReviewdetails GetDocumentFileByStudAsgFileId(long stdAsgFileId);
        bool MarkAsCompleteReview(long peerReviewId, long reviewedBy);
        bool MarkAsCompleteTaskReview(long peerReviewId, long reviewedBy);
        AssignmentPeerReview GetPeerAssignmentDetail(long peerReviewId);
        AssignmentPeerReview GetPeerAssignmentDetailByAssignmentStudentId(long assignmentStudentId);
        OperationDetails AddUpdateTaskDocumentReviewDetails(List<TaskDocumentReviewdetails> lstDocumentReviewdetails);
        IEnumerable<TaskDocumentReviewdetails> GetTaskReviewedDocumentFiles(long peerReviewId);
        IEnumerable<TaskPeerReview> GetPeerTaskDetails(long assignmentId, long studentId);
        TaskPeerReview GetPeerTaskDetail(long studentId, long taskId);
        TaskDocumentReviewdetails GetTaskDocumentFileByStudTaskFileId(long studentTaskFileId);
        IEnumerable<Assignment> GetSchoolGroupAssignments(long schoolGroupid);
        IEnumerable<AssignmentStudent> GetStudentSchoolGroupAssignments(long schoolGroupid, long UserId);
        SendEmailNotificationView GenerateEmailTemplateForAssignmentComment(string assignmentTitle, string comment, string studentInternalIds);
        SendEmailNotificationView GenerateEmailTemplateForAssignmentMarkAsComplete(string assignmentTitle, string studentInternalIds);
        SendEmailNotificationView GenerateEmailTemplateForUpdateAssignmentGrade(string assignmentTitle, string studentInternalIds);
        AssignmentDetails GetStudentAssignmentDetailsById(int assignmentStudentId);
        IEnumerable<Assignment> GetDashboardAssignmentOverview(long userId);
        IEnumerable<AssignmentStudentObjective> GetStudentAssignmentObjectives(long assignmentStudentId);
        bool UploadStudentSharedCopyFiles(List<AssignmentStudentSharedFiles> copiedFiles);
        OperationDetails ShareAssignment(int assignmentId, string teacherIds);
        string GetAssignmentsForMyPlanner(long studentId, string startDate, string endDate, string assignmentType = "");
        byte[] GenerateAssignmentReportXLSFile(string path, Assignment assignmentReports, List<SchoolGroup> lstSchoolGroups, List<AssignmentTask> Tasks, AssignmentReport reportData);
        bool UndoAssignmentsArchive(ArchiveAssignment model);
    }
}
