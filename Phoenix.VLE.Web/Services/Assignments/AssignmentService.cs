﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class AssignmentService : IAssignmentService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/assignments";
        #endregion

        public AssignmentService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public OperationDetails DeleteAssignmentData(Assignment assignment)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Assignment.DeleteAssignment(_path);
            var deleteData = JsonConvert.SerializeObject(assignment);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, assignment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }


        public OperationDetails DeleteAssignmentComment(AssignmentComment assignmentComment)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Assignment.DeleteAssignmentComment(_path);
            var deleteData = JsonConvert.SerializeObject(assignmentComment);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, assignmentComment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public Assignment GetAssignments(int teacherId)
        {
            var uri = API.Assignment.GetAssignments(_path, teacherId);
            Assignment assignments = new Assignment();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, Assignment>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public Assignment GetAssignmentById(int id)
        {
            var assignment = new Assignment();
            var uri = API.Assignment.GetAssignmentById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignment = EntityMapper<string, Assignment>.MapFromJson(jsonDataProviders);
            }
            return assignment;
        }


        public AssignmentStudent GetAssignmentSubmitMarksByAssignmentStudentId(int assignmentStudentId)
        {
            var assignmentStudent = new AssignmentStudent();
            var uri = API.Assignment.GetAssignmentSubmitMarksByAssignmentStudentId(_path, assignmentStudentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, AssignmentStudent>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }

        public StudentTask GetTaskSubmitMarksByTaskId(int TaskId)
        {
            var studentTask = new StudentTask();
            var uri = API.Assignment.GetTaskSubmitMarksByTaskId(_path, TaskId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentTask = EntityMapper<string, StudentTask>.MapFromJson(jsonDataProviders);
            }
            return studentTask;
        }

        public Assignment GetExcelReportDataAssignmentById(int id)
        {
            var assignment = new Assignment();
            var uri = API.Assignment.GetExcelReportDataAssignmentById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignment = EntityMapper<string, Assignment>.MapFromJson(jsonDataProviders);
            }
            return assignment;
        }

        public OperationDetails UpdateAssignmentData(AssignmentEdit model, List<Objective> lstObjectives, List<CourseUnit> lstCourseUnit, List<GroupUnit> lstGroupTopicUnit)
        {
            OperationDetails op = new OperationDetails();
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new Assignment();
            EntityMapper<AssignmentEdit, Assignment>.Map(model, sourceModel);
            sourceModel.lstAssignmentFiles = new List<AssignmentFile>();
            sourceModel.lstObjectives = lstObjectives;
            sourceModel.lstCourseUnit = lstCourseUnit;
            sourceModel.lstGroupTopicUnit = lstGroupTopicUnit;
            if (model.SchoolGroupIds != null)
            {
                sourceModel.SchoolGroups = string.Join(",", model.SchoolGroupIds);
            }
            if (model.StudentIds != null)
            {
                sourceModel.StudentIdsToAdd = string.Join(",", model.StudentIds);
            }
            if (model.CourseIds != null)
            {
                sourceModel.Courses = string.Join(",", model.CourseIds);
            }

            foreach (var item in model.lstAssignmentTasks)
            {
                if (item.CourseIds != null)
                {
                    item.Courses = string.Join(",", item.CourseIds);
                }
            }

            if (model.lstDocuments != null)
            {
                foreach (var item in model.lstDocuments)
                {
                    AssignmentFile objAssignmentFile = new AssignmentFile();
                    objAssignmentFile.AssignmentId = model.AssignmentId;
                    objAssignmentFile.FileName = item.FileName;
                    objAssignmentFile.UploadedFileName = item.UploadedFileName;
                    objAssignmentFile.ResourceFileTypeId = item.ResourceFileTypeId;
                    objAssignmentFile.ShareableLinkFileURL = item.ShareableLinkFileURL;
                    sourceModel.lstAssignmentFiles.Add(objAssignmentFile);
                }
            }

            if (model.IsAddMode)
                uri = API.Assignment.InsertAssignment(_path);
            else
                uri = API.Assignment.UpdateAssignment(_path);
            string data = JsonConvert.SerializeObject(sourceModel);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public OperationDetails SaveAssignmentData(AssignmentEdit model)
        {
            OperationDetails op = new OperationDetails();
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new Assignment();
            EntityMapper<AssignmentEdit, Assignment>.Map(model, sourceModel);

            if (model.SchoolGroupIds != null)
            {
                sourceModel.SchoolGroups = string.Join(",", model.SchoolGroupIds);
            }
            if (model.StudentIds != null)
            {
                sourceModel.StudentIdsToAdd = string.Join(",", model.StudentIds);
            }

            if (model.lstDocuments != null)
            {
                foreach (var item in model.lstDocuments)
                {
                    AssignmentFile objAssignmentFile = new AssignmentFile();
                    objAssignmentFile.AssignmentId = model.AssignmentId;
                    objAssignmentFile.FileName = item.FileName;
                    objAssignmentFile.UploadedFileName = item.UploadedFileName;
                    sourceModel.lstAssignmentFiles.Add(objAssignmentFile);
                }
            }

            if (model.IsAddMode)
                uri = API.Assignment.SaveAssignmentDetails(_path);
            else
                uri = API.Assignment.SaveAssignmentDetails(_path);
            string data = JsonConvert.SerializeObject(sourceModel);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public IEnumerable<AssignmentStudent> GetAssignmentStudent()
        {
            var uri = API.Assignment.GetAssignmentStudent(_path);
            IEnumerable<AssignmentStudent> assignmentStudent = new List<AssignmentStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, IEnumerable<AssignmentStudent>>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }

        public IEnumerable<AssignmentStudentDetails> GetAssignmentStudentDetails(long TeacherId, AssignmentFilter loadAssignment)
        {
            var uri = API.Assignment.GetAssignmentStudentDetails(_path, TeacherId);
            IEnumerable<AssignmentStudentDetails> assignmentStudent = new List<AssignmentStudentDetails>();
            //int[] arr = teacherVal.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, loadAssignment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }

        public IEnumerable<AssignmentStudentDetails> GetAdminAssignmentStudentDetails(AssignmentFilter loadAssignment)
        {
            var uri = API.Assignment.GetAdminAssignmentStudentDetails(_path);
            IEnumerable<AssignmentStudentDetails> assignmentStudent = new List<AssignmentStudentDetails>();
            //int[] arr = teacherVal.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, loadAssignment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }


        public IEnumerable<AssignmentStudentDetails> GetFilterSchoolGroupsById(long TeacherId, AssignmentFilter loadAssignment)
        {
            var uri = API.Assignment.GetFilterSchoolGroupsById(_path, TeacherId);
            IEnumerable<AssignmentStudentDetails> assignmentStudent = new List<AssignmentStudentDetails>();
            //int[] arr = filterVal.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            //int[] arr1 = teacherVal.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, loadAssignment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }



        public IEnumerable<AssignmentStudentDetails> GetArchivedFilterSchoolGroupsById(long TeacherId, AssignmentFilter loadAssignment)
        {
            var uri = API.Assignment.GetArchivedFilterSchoolGroupsById(_path, TeacherId);
            IEnumerable<AssignmentStudentDetails> assignmentStudent = new List<AssignmentStudentDetails>();
            //int[] arr = filterVal.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, loadAssignment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            //int l = uri.Length;
            return assignmentStudent;
        }

        public IEnumerable<AssignmentStudentDetails> GetAssignmentStudentDetailsById(int Id)
        {
            var uri = API.Assignment.GetAssignmentStudentDetailsById(_path, Id);
            IEnumerable<AssignmentStudentDetails> assignmentStudent = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }
        public IEnumerable<AssignmentTask> GetAssignmentTasks(int assignmentId)
        {
            var uri = API.Assignment.GetTasksByAssignmentId(_path, assignmentId);
            IEnumerable<AssignmentTask> assignmentTasks = new List<AssignmentTask>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentTasks = EntityMapper<string, IEnumerable<AssignmentTask>>.MapFromJson(jsonDataProviders);
            }
            return assignmentTasks;
        }

        public IEnumerable<AssignmentStudentDetails> GetAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "")
        {
            var uri = API.Assignment.GetAssignmentByStudentId(_path, PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public IEnumerable<AssignmentStudentDetails> GetArchivedAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "")
        {
            var uri = API.Assignment.GetArchivedAssignmentByStudentId(_path, PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public IEnumerable<AssignmentStudentDetails> GeStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "", string filterVal = "")
        {
            var uri = API.Assignment.GeStudentFilterSchoolGroupsById(_path, PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            int[] arr = filterVal.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, arr).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }


        public IEnumerable<AssignmentStudentDetails> GetArchivedStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "", string filterVal = "")
        {
            var uri = API.Assignment.GetArchivedStudentFilterSchoolGroupsById(_path, PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            int[] arr = filterVal.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, arr).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public IEnumerable<AssignmentStudentDetails> GetAssignmentByStudentIdWithoutPagination(long studentId, string assignmentType = "")
        {
            var uri = API.Assignment.GetAssignmentByStudentIdWithoutPagination(_path, studentId, assignmentType);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public IEnumerable<AssignmentStudentDetails> GetAssignmentByStudentIdWithDateRange(int PageNumber, int PageSize, long studentId, DateTime startDate, DateTime endDate, string SearchString = "", string assignmentType = "")
        {
            var uri = API.Assignment.GetAssignmentByStudentIdWithDateRange(_path, PageNumber, PageSize, studentId, startDate, endDate, SearchString, assignmentType);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public IEnumerable<AssignmentStudentDetails> GetAssignmentByStudentIdAndTeacherIdWithDateRange(int PageNumber, int PageSize, long studentId, long teacherId, DateTime startDate, DateTime endDate, string SearchString = "", string assignmentType = "")
        {
            var uri = API.Assignment.GetAssignmentByStudentIdAndTeacherIdWithDateRange(_path, PageNumber, PageSize, studentId, teacherId, startDate, endDate, SearchString, assignmentType);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }
        public IEnumerable<AssignmentReport> GetAssignmentTaskQuizByAssignmentId(int assignmentId)
        {
            var uri = API.Assignment.GetAssignmentTaskQuizByAssignmentId(_path, assignmentId);
            IEnumerable<AssignmentReport> assignmentReports = new List<AssignmentReport>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentReports = EntityMapper<string, IEnumerable<AssignmentReport>>.MapFromJson(jsonDataProviders);
            }
            return assignmentReports;
        }
        public string GetAssignmentsForMyPlanner(long studentId, string startDate, string endDate, string assignmentType = "")
        {
            DateTime _fromDate = Convert.ToDateTime(startDate);
            DateTime _toDate = Convert.ToDateTime(endDate);
            var jsonDataProviders = String.Empty;
            var uri = API.Assignment.GetAssignmentsForMyPlanner(_path, studentId, _fromDate, _toDate, assignmentType);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            }
            return jsonDataProviders;
        }

        public bool AssignGradeToStudentAssignment(int studentAssignmentId, int gradingTemplateItemId, int GradedBy)
        {
            var uri = API.Assignment.AssignGradeToStudentAssignment(_path, studentAssignmentId, gradingTemplateItemId, GradedBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public OperationDetails GetAssignmentPermissions(int assignmentId, long userId)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Assignment.GetAssignmentPermissions(_path, assignmentId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public OperationDetails AssignmentUnArchive(int assignmentId)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Assignment.AssignmentUnArchive(_path, assignmentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public OperationDetails AddEditAssignmentCategory(int AssignmentCategoryId, bool IsActive, long SchoolId)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Assignment.AddEditAssignmentCategory(_path, AssignmentCategoryId, IsActive, SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public OperationDetails AddAssignmentCategoryMaster(string AssignmentCategoryTitle, string AssignmentCategoryDesc, int CategoryId, bool IsActive, long SchoolId)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Assignment.AddAssignmentCategoryMaster(_path, AssignmentCategoryTitle, AssignmentCategoryDesc, CategoryId, IsActive, SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public AssignmentCategoryDetails GetAssignmentCategoryMasterById(int id)
        {
            var uri = API.Assignment.GetAssignmentCategoryMasterById(_path, id);
            AssignmentCategoryDetails assignmentCategory = new AssignmentCategoryDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentCategory = EntityMapper<string, AssignmentCategoryDetails>.MapFromJson(jsonDataProviders);
            }
            return assignmentCategory;
        }
        public GroupQuiz GetAssignmentQuizDetailsByTaskId(int taskId)
        {
            var uri = API.Assignment.GetAssignmentQuizDetailsByTaskId(_path, taskId);
            GroupQuiz quizDetails = new GroupQuiz();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                quizDetails = EntityMapper<string, GroupQuiz>.MapFromJson(jsonDataProviders);
            }
            return quizDetails;
        }
        public IEnumerable<AssignmentCategoryDetails> GetAssignmentCategoryBySchoolId(long SchoolId)
        {
            var uri = API.Assignment.GetAssignmentCategoryBySchoolId(_path, SchoolId);
            IEnumerable<AssignmentCategoryDetails> assignmentCategories = new List<AssignmentCategoryDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentCategories = EntityMapper<string, IEnumerable<AssignmentCategoryDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignmentCategories;
        }

        public OperationDetails SeenStudentAssignmentComment(AssignmentComment objAssignmentComment)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.Assignment.SeenStudentAssignmentComment(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, objAssignmentComment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public IEnumerable<AssignmentFile> GetAssignmentFiles(int assignmentId)
        {
            var uri = API.Assignment.getAssignmentFilesByAssignmentId(_path, assignmentId);
            IEnumerable<AssignmentFile> assignmentFiles = new List<AssignmentFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentFiles = EntityMapper<string, IEnumerable<AssignmentFile>>.MapFromJson(jsonDataProviders);
            }
            return assignmentFiles;
        }
        public IEnumerable<TaskFile> GetTaskFilesByTaskId(int taskId)
        {
            var uri = API.Assignment.getFilesByTaskId(_path, taskId);
            IEnumerable<TaskFile> taskFiles = new List<TaskFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                taskFiles = EntityMapper<string, IEnumerable<TaskFile>>.MapFromJson(jsonDataProviders);
            }
            return taskFiles;
        }
        public AssignmentStudent GetStudentAsgHomeworkDetailsById(int assignmentStudentId, int IsSeenByStudent = 0)
        {
            var uri = API.Assignment.GetStudentAsgHomeworkDetailsById(_path, assignmentStudentId, IsSeenByStudent);
            AssignmentStudent assignmentStudent = new AssignmentStudent();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, AssignmentStudent>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }

        public AssignmentDetails GetStudentAssignmentDetailsById(int assignmentStudentId)
        {
            var uri = API.Assignment.GetStudentAssignmentDetailsById(_path, assignmentStudentId);
            AssignmentDetails assignmentDetail = new AssignmentDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentDetail = EntityMapper<string, AssignmentDetails>.MapFromJson(jsonDataProviders);
            }
            return assignmentDetail;
        }

        public AssignmentTask GetAssignmentTaskbyTaskId(int taskId)
        {
            var uri = API.Assignment.GetAssignmentTaskbyTaskId(_path, taskId);
            AssignmentTask task = new AssignmentTask();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                task = EntityMapper<string, AssignmentTask>.MapFromJson(jsonDataProviders);
            }
            return task;
        }
        public IEnumerable<SchoolGroup> GetSelectedSchoolGroupsByAssignmentID(int assignmentId)
        {
            var uri = API.Assignment.GetSchoolGroupsByAssignmentId(_path, assignmentId);
            IEnumerable<SchoolGroup> lstSchoolGroups = new List<SchoolGroup>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSchoolGroups = EntityMapper<string, IEnumerable<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return lstSchoolGroups;
        }

        public IEnumerable<Course> GetSelectegCoursesByAssignmentId(int assignmentId)
        {
            var uri = API.Assignment.GetSelectegCoursesByAssignmentId(_path, assignmentId);
            IEnumerable<Course> lstCourses = new List<Course>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstCourses = EntityMapper<string, IEnumerable<Course>>.MapFromJson(jsonDataProviders);
            }
            return lstCourses;
        }


        public IEnumerable<AssignmentStudent> GetStudentsByAssignmentId(int assignmentId, int groupId)
        {
            var uri = API.Assignment.GetStudentsByAssignmentId(_path, assignmentId, groupId);
            IEnumerable<AssignmentStudent> lstAssignmentStudents = new List<AssignmentStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstAssignmentStudents = EntityMapper<string, IEnumerable<AssignmentStudent>>.MapFromJson(jsonDataProviders);
            }
            return lstAssignmentStudents;
        }


        public IEnumerable<AssignmentStudent> GetAssignmentCompletedStudentsByAssignmentId(int assignmentId)
        {
            var uri = API.Assignment.GetAssignmentCompletedStudentsByAssignmentId(_path, assignmentId);
            IEnumerable<AssignmentStudent> lstAssignmentStudents = new List<AssignmentStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstAssignmentStudents = EntityMapper<string, IEnumerable<AssignmentStudent>>.MapFromJson(jsonDataProviders);
            }
            return lstAssignmentStudents;
        }

        public AssignmentReport GetExcelReportDataStudentsByAssignmentId(int assignmentId)
        {
            //var uri = API.Assignment.GetExcelReportDataStudentsByAssignmentId(_path, assignmentId);
            //IEnumerable<AssignmentStudent> lstAssignmentStudents = new List<AssignmentStudent>();
            //HttpResponseMessage response = _client.GetAsync(uri).Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            //    lstAssignmentStudents = EntityMapper<string, IEnumerable<AssignmentStudent>>.MapFromJson(jsonDataProviders);
            //}
            //return lstAssignmentStudents;

            var uri = API.Assignment.GetExcelReportDataStudentsByAssignmentId(_path, assignmentId);
            AssignmentReport reports = new AssignmentReport();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reports = EntityMapper<string, AssignmentReport>.MapFromJson(jsonDataProviders);
            }
            return reports;


        }

        public bool MarkAsComplete(int assignmentStudentId, bool isTeacher, int CompletedBy)
        {
            bool bFlag = false;
            var uri = API.Assignment.MarkAsComplete(_path, assignmentStudentId, isTeacher, CompletedBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }

        public GradingTemplateItem SubmitAssignmentMarks(int assignmentStudentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int SystemLanguageId)
        {
            var gradingtemplateitem = new GradingTemplateItem();
            var uri = API.Assignment.SubmitAssignmentMarks(_path, assignmentStudentId, submitMarks, CompletedBy, gradingTemplateId, SystemLanguageId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                gradingtemplateitem = EntityMapper<string, GradingTemplateItem>.MapFromJson(jsonDataProviders);
            }
            return gradingtemplateitem;
        }

        public bool UploadStudentAssignmentFile(List<StudentAssignmentFile> lstFiles, int studentId, int assignmentId, string IpDetails)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Assignment.UploadStudentAssignmeentFiles(_path, studentId, assignmentId, IpDetails), lstFiles).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<StudentAssignmentFile> GetStudentAssignmentFiles(int studentId, int assignmentId)
        {
            var uri = API.Assignment.GetStudentAssignmentFiles(_path, studentId, assignmentId);
            IEnumerable<StudentAssignmentFile> lstStudentAssignmentFiles = new List<StudentAssignmentFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudentAssignmentFiles = EntityMapper<string, IEnumerable<StudentAssignmentFile>>.MapFromJson(jsonDataProviders);
            }
            return lstStudentAssignmentFiles;
        }

        public IEnumerable<AssignmentFile> GetSavedAssignmentFiles(int assignmentId, long userId)
        {
            var uri = API.Assignment.GetSavedAssignmentFiles(_path, assignmentId, userId);
            IEnumerable<AssignmentFile> lstStudentAssignmentFiles = new List<AssignmentFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudentAssignmentFiles = EntityMapper<string, IEnumerable<AssignmentFile>>.MapFromJson(jsonDataProviders);
            }
            return lstStudentAssignmentFiles;
        }

        public bool SaveAssignmentFiles(List<AssignmentFile> lstModel)
        {
            var uri = API.Assignment.SaveAssignmentFiles(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, lstModel).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<AssignmentFeedbackFiles> GetAssignmentFeedbackFiles(int assignmentStudentId, int assignmentCommentId)
        {
            var uri = API.Assignment.GetAssignmentFeedbackFiles(_path, assignmentStudentId, assignmentCommentId);
            IEnumerable<AssignmentFeedbackFiles> assignmentFeedbackFiles = new List<AssignmentFeedbackFiles>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentFeedbackFiles = EntityMapper<string, IEnumerable<AssignmentFeedbackFiles>>.MapFromJson(jsonDataProviders);
            }
            return assignmentFeedbackFiles;
        }

        public bool MarkAsIncompleteAssignment(int studentId, int assignmentId, bool isReSubmitAssignnment)
        {
            bool bFlag = false;
            var uri = API.Assignment.MarkAsIncomplete(_path, studentId, assignmentId, isReSubmitAssignnment);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }

        //for student task file
        public bool UploadStudentTaskFiles(List<StudentTaskFile> lstFiles, int studentId, int taskfile, string IpDetails)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Assignment.UploadStudentTaskFiles(_path, studentId, taskfile, IpDetails), lstFiles).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<StudentTaskFile> GetStudentTaskFiles(int studentId, int taskid)
        {
            var uri = API.Assignment.GetStudentTaskFiles(_path, studentId, taskid);
            IEnumerable<StudentTaskFile> lstStudentTaskFiles = new List<StudentTaskFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudentTaskFiles = EntityMapper<string, IEnumerable<StudentTaskFile>>.MapFromJson(jsonDataProviders);
            }
            return lstStudentTaskFiles;
        }
        public StudentTask GetStudentTaskDetails(int taskId, int studentId)
        {
            var uri = API.Assignment.GetStudentTaskdetails(_path, taskId, studentId);
            StudentTask studentTaskdetail = new StudentTask();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentTaskdetail = EntityMapper<string, StudentTask>.MapFromJson(jsonDataProviders);
            }
            return studentTaskdetail;
        }

        public AssignmentTask GetStudentTaskDetailById(int taskId, int studentId, int AssignmentStudentId, long StudentTaskId)
        {
            var uri = API.Assignment.GetStudentTaskDetailById(_path, taskId, studentId, AssignmentStudentId, StudentTaskId);
            AssignmentTask studentTaskdetail = new AssignmentTask();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentTaskdetail = EntityMapper<string, AssignmentTask>.MapFromJson(jsonDataProviders);
            }
            return studentTaskdetail;
        }

        public bool InsertAssignmentComment(AssignmentComment entity)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Assignment.InsertAssignmentComment(_path), entity).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<AssignmentComment> GetAssignmentComments(int assignmentStudentId)
        {
            var uri = API.Assignment.GetAssignmentComments(_path, assignmentStudentId);
            IEnumerable<AssignmentComment> assignmentComments = new List<AssignmentComment>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentComments = EntityMapper<string, IEnumerable<AssignmentComment>>.MapFromJson(jsonDataProviders);
            }
            return assignmentComments;
        }

        public bool MarkAsCompleteTask(long studentTaskId, bool isByteacher, int CompletedBy)
        {
            bool bFlag = false;
            var uri = API.Assignment.MarkAsCompleteTask(_path, studentTaskId, isByteacher, CompletedBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }

        public GradingTemplateItem SubmitTaskMarks(int studentTaskId, int StudentAssignmentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int languageId)
        {
            var gradingtemplateitem = new GradingTemplateItem();
            var uri = API.Assignment.SubmitTaskMarks(_path, studentTaskId, StudentAssignmentId, submitMarks, CompletedBy, gradingTemplateId, languageId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                gradingtemplateitem = EntityMapper<string, GradingTemplateItem>.MapFromJson(jsonDataProviders);
            }
            return gradingtemplateitem;
        }

        public bool AssignGradeToStudentTask(int studentTaskId, int gradeId, int GradedBy)
        {
            bool bFlag = false;
            var uri = API.Assignment.AssignGradeToStudentTask(_path, studentTaskId, gradeId, GradedBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }

        public bool AssignGradeToStudentObjective(int studentObjectiveId, int gradeId)
        {
            bool bFlag = false;
            var uri = API.Assignment.AssignGradeToStudentObjective(_path, studentObjectiveId, gradeId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }


        public bool InsertTaskFeedback(AssignmentTask assignmentTask)
        {
            var uri = string.Empty;
            uri = API.Assignment.InsertTaskFeedback(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, assignmentTask).Result;
            return response.IsSuccessStatusCode;

        }
        public bool InsertStudentObjectiveFeedback(AssignmentStudentObjective studentObjective)
        {
            var uri = string.Empty;
            uri = API.Assignment.InsertStudentObjectiveFeedback(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentObjective).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<MyFilesTreeItem> GetMyFiles(long userId)
        {
            var uri = API.Assignment.GetMyFiles(_path, userId);
            IEnumerable<MyFilesTreeItem> lstMyFiles = new List<MyFilesTreeItem>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMyFiles = EntityMapper<string, IEnumerable<MyFilesTreeItem>>.MapFromJson(jsonDataProviders);
            }
            return lstMyFiles;
        }
        public IEnumerable<File> GetStudentAssignmentObjectiveAudiofeedback(int studentObjectiveId)
        {
            var uri = API.Assignment.GetStudentAssignmentObjectiveAudiofeedback(_path, studentObjectiveId);
            IEnumerable<File> lstFiles = new List<File>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstFiles = EntityMapper<string, IEnumerable<File>>.MapFromJson(jsonDataProviders);
            }
            return lstFiles;
        }

        public IEnumerable<AssignmentFile> GetAssignmentMyFiles(long fileId, bool isFolder)
        {
            var uri = API.Assignment.GetAssignmentMyFiles(_path, fileId, isFolder);
            IEnumerable<AssignmentFile> lstMyFiles = new List<AssignmentFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMyFiles = EntityMapper<string, IEnumerable<AssignmentFile>>.MapFromJson(jsonDataProviders);
            }
            return lstMyFiles;
        }

        public bool DeleteUploadedFiles(int FileId, int AssignmentFileId, long UserId)
        {
            var uri = API.Assignment.DeleteUploadedFiles(_path, FileId, AssignmentFileId, UserId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteStudentAssignmentFile(int FileId, long UserId, bool IsTaskFile)
        {
            var uri = API.Assignment.DeleteStudentAssignmentFile(_path, FileId, UserId, IsTaskFile);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }
        public File GetAssignmentFilebyAssignmentFileId(long id)
        {
            var uri = API.Assignment.GetAssignmentFilebyAssignmentFileId(_path, id);
            File lstMyFiles = new File();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMyFiles = EntityMapper<string, File>.MapFromJson(jsonDataProviders);
            }
            return lstMyFiles;
        }
        public File GetStudentTaskFile(long id)
        {
            var uri = API.Assignment.GetStudentTaskFile(_path, id);
            File lstMyFiles = new File();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMyFiles = EntityMapper<string, File>.MapFromJson(jsonDataProviders);
            }
            return lstMyFiles;
        }
        public File GetTaskFile(long id)
        {
            var uri = API.Assignment.GetTaskFile(_path, id);
            File lstMyFiles = new File();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMyFiles = EntityMapper<string, File>.MapFromJson(jsonDataProviders);
            }
            return lstMyFiles;
        }

        public File GetStudentAssignmentFilebyStudentAsgFileId(long id)
        {
            var uri = API.Assignment.GetStudentAssignmentFilebyStudentAsgFileId(_path, id);
            File lstMyFiles = new File();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMyFiles = EntityMapper<string, File>.MapFromJson(jsonDataProviders);
            }
            return lstMyFiles;
        }

        public bool UpdateActiveAssignmets(long assignmentId)
        {
            var uri = API.Assignment.UpdateActiveAssignment(_path, assignmentId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, assignmentId).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<AssignmentStudentDetails> GetAssignmentListForReport(int pageNumber, int pageSize, string searchString, long userId, long schoolId, int schoolGroupId, DateTime startDate, DateTime endDate, string userType)
        {
            var uri = API.Assignment.GetAssignmentListForReport(_path, pageNumber, pageSize, searchString, userId, schoolId, schoolGroupId, startDate, endDate, userType);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public IEnumerable<AssignmentStudentDetails> GetArchivedAssignmentStudentDetails(long TeacherId, AssignmentFilter loadAssignment)
        {
            var uri = API.Assignment.GetArchivedAssignmentStudentDetails(_path, TeacherId);
            IEnumerable<AssignmentStudentDetails> assignmentStudent = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, loadAssignment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }

        public IEnumerable<AssignmentStudentDetails> GetAdminArchivedAssignmentStudentDetails(AssignmentFilter loadAssignment)
        {
            var uri = API.Assignment.GetAdminArchivedAssignmentStudentDetails(_path);
            IEnumerable<AssignmentStudentDetails> assignmentStudent = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, loadAssignment).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentStudent = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignmentStudent;
        }

        public IEnumerable<AssignmentStudentDetails> GetStudentAssignmentByUserId(int pageNumber, int pageSize, long userId, string searchString = "")
        {
            var uri = API.Assignment.GetStudentAssignmentByUserId(_path, pageNumber, pageSize, userId, searchString);
            IEnumerable<AssignmentStudentDetails> assignments = new List<AssignmentStudentDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudentDetails>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public OperationDetails AddUpdatePeermarking(List<AssignmentPeerReview> lstPeerReviewMapping)
        {
            OperationDetails op = new OperationDetails();
            var uri = string.Empty;
            uri = API.Assignment.AddUpdatePeermarking(_path);
            string data = JsonConvert.SerializeObject(lstPeerReviewMapping);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, lstPeerReviewMapping).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op.Success = Convert.ToBoolean(jsonDataProviders);
            }
            return op;
        }
        public IEnumerable<AssignmentPeerReview> GetPeerAssignmentDetails(long assignmentId, long userId)
        {
            var uri = API.Assignment.GetPeerAssignmentDetails(_path, assignmentId, userId);
            IEnumerable<AssignmentPeerReview> lstAssignmentPeers = new List<AssignmentPeerReview>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstAssignmentPeers = EntityMapper<string, IEnumerable<AssignmentPeerReview>>.MapFromJson(jsonDataProviders);
            }
            return lstAssignmentPeers;
        }

        public IEnumerable<AssignmentPeerReview> GetPeerMappingDetails(long assignmentId)
        {
            var uri = API.Assignment.GetPeerMappingDetails(_path, assignmentId);
            IEnumerable<AssignmentPeerReview> lstAssignmentPeers = new List<AssignmentPeerReview>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstAssignmentPeers = EntityMapper<string, IEnumerable<AssignmentPeerReview>>.MapFromJson(jsonDataProviders);
            }
            return lstAssignmentPeers;
        }
        public OperationDetails AddUpdateDocumentReviewDetails(List<DocumentReviewdetails> lstDocumentReviewdetails)
        {
            OperationDetails op = new OperationDetails();
            var uri = string.Empty;
            long userId = SessionHelper.CurrentSession.Id;
            uri = API.Assignment.AddUpdateDocumentReviewDetails(_path, userId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, lstDocumentReviewdetails).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
                // op.Success = Convert.ToBoolean(jsonDataProviders);
            }
            return op;
        }
        public IEnumerable<DocumentReviewdetails> GetReviewedDocumentFiles(long peerReviewId)
        {
            IEnumerable<DocumentReviewdetails> lstReviewedDocs = new List<DocumentReviewdetails>();
            var uri = string.Empty;
            uri = API.Assignment.GetReviewedDocumentFiles(_path, peerReviewId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstReviewedDocs = EntityMapper<string, IEnumerable<DocumentReviewdetails>>.MapFromJson(jsonDataProviders);
            }
            return lstReviewedDocs;
        }
        public DocumentReviewdetails GetDocumentFileByStudAsgFileId(long stdAsgFileId)
        {
            DocumentReviewdetails objReviwedDocument = new DocumentReviewdetails();
            var uri = string.Empty;
            uri = API.Assignment.GetDocumentFileByStudAsgFileId(_path, stdAsgFileId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objReviwedDocument = EntityMapper<string, DocumentReviewdetails>.MapFromJson(jsonDataProviders);
            }
            return objReviwedDocument;
        }
        public bool MarkAsCompleteReview(long peerReviewId, long reviewedBy)
        {
            bool bFlag = false;
            var uri = API.Assignment.MarkAsCompleteReview(_path, peerReviewId, reviewedBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }

        public bool MarkAsCompleteTaskReview(long peerReviewId, long reviewedBy)
        {
            bool bFlag = false;
            var uri = API.Assignment.MarkAsCompleteTaskReview(_path, peerReviewId, reviewedBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                bFlag = Convert.ToBoolean(jsonDataProviders);
            }
            return bFlag;
        }
        public AssignmentPeerReview GetPeerAssignmentDetail(long peerReviewId)
        {
            var uri = API.Assignment.GetPeerAssignmentDetail(_path, peerReviewId);
            AssignmentPeerReview objAssignmentPeerReview = new AssignmentPeerReview();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objAssignmentPeerReview = EntityMapper<string, AssignmentPeerReview>.MapFromJson(jsonDataProviders);
            }
            return objAssignmentPeerReview;
        }
        public AssignmentPeerReview GetPeerAssignmentDetailByAssignmentStudentId(long assignmentStudentId)
        {
            var uri = API.Assignment.GetPeerAssignmentDetailByAssignmentStudentId(_path, assignmentStudentId);
            AssignmentPeerReview objAssignmentPeerReview = new AssignmentPeerReview();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objAssignmentPeerReview = EntityMapper<string, AssignmentPeerReview>.MapFromJson(jsonDataProviders);
            }
            return objAssignmentPeerReview;
        }
        public OperationDetails AddUpdateTaskDocumentReviewDetails(List<TaskDocumentReviewdetails> lstDocumentReviewdetails)
        {
            OperationDetails op = new OperationDetails();
            var uri = string.Empty;
            long userId = SessionHelper.CurrentSession.Id;
            uri = API.Assignment.AddUpdateTaskDocumentReviewDetails(_path, userId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, lstDocumentReviewdetails).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
                // op.Success = Convert.ToBoolean(jsonDataProviders);
            }
            return op;
        }

        public IEnumerable<TaskPeerReview> GetPeerTaskDetails(long assignmentId, long studentId)
        {
            var uri = API.Assignment.GetPeerTaskDetails(_path, assignmentId, studentId);
            IEnumerable<TaskPeerReview> lstTaskPeers = new List<TaskPeerReview>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstTaskPeers = EntityMapper<string, IEnumerable<TaskPeerReview>>.MapFromJson(jsonDataProviders);
            }
            return lstTaskPeers;
        }

        public TaskPeerReview GetPeerTaskDetail(long studentId, long taskId)
        {
            var uri = API.Assignment.GetPeerTaskDetail(_path, studentId, taskId);
            TaskPeerReview taskPeer = new TaskPeerReview();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                taskPeer = EntityMapper<string, TaskPeerReview>.MapFromJson(jsonDataProviders);
            }
            return taskPeer;
        }

        public TaskDocumentReviewdetails GetTaskDocumentFileByStudTaskFileId(long studentTaskFileId)
        {
            var uri = API.Assignment.GetTaskDocumentFileByStudTaskFileId(_path, studentTaskFileId);
            TaskDocumentReviewdetails reiewedTaskFile = new TaskDocumentReviewdetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reiewedTaskFile = EntityMapper<string, TaskDocumentReviewdetails>.MapFromJson(jsonDataProviders);
            }
            return reiewedTaskFile;
        }

        public IEnumerable<TaskDocumentReviewdetails> GetTaskReviewedDocumentFiles(long studentTaskFileId)
        {
            var uri = API.Assignment.GetTaskReviewedDocumentFiles(_path, studentTaskFileId);
            IEnumerable<TaskDocumentReviewdetails> reiewedTaskFiles = new List<TaskDocumentReviewdetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reiewedTaskFiles = EntityMapper<string, IEnumerable<TaskDocumentReviewdetails>>.MapFromJson(jsonDataProviders);
            }
            return reiewedTaskFiles;
        }

        public IEnumerable<Assignment> GetSchoolGroupAssignments(long schoolGroupid)
        {
            IEnumerable<Assignment> assignments = new List<Assignment>();
            var uri = string.Empty;
            uri = API.Assignment.GetSchoolGroupAssignment(_path, schoolGroupid);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<Assignment>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }

        public IEnumerable<AssignmentStudent> GetStudentSchoolGroupAssignments(long schoolGroupid, long UserId)
        {
            IEnumerable<AssignmentStudent> assignments = new List<AssignmentStudent>();
            var uri = string.Empty;
            uri = API.Assignment.GetStudentGroupAssignments(_path, schoolGroupid, UserId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignments = EntityMapper<string, IEnumerable<AssignmentStudent>>.MapFromJson(jsonDataProviders);
            }
            return assignments;
        }


        public IEnumerable<Assignment> GetDashboardAssignmentOverview(long userId)
        {
            IEnumerable<Assignment> studentAssignments = new List<Assignment>();
            var uri = API.Assignment.GetDashboardAssignmentOverview(_path, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentAssignments = EntityMapper<string, IEnumerable<Assignment>>.MapFromJson(jsonDataProviders);
            }
            return studentAssignments;
        }

        public IEnumerable<AssignmentStudentObjective> GetStudentAssignmentObjectives(long assignmentStudentId)
        {
            IEnumerable<AssignmentStudentObjective> studentAsgObjectives = new List<AssignmentStudentObjective>();
            var uri = API.Assignment.GetStudentAssignmentObjectives(_path, assignmentStudentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentAsgObjectives = EntityMapper<string, IEnumerable<AssignmentStudentObjective>>.MapFromJson(jsonDataProviders);
            }
            return studentAsgObjectives;
        }
        #region Methods
        #endregion

        #region Generate Email Template Code For Assignment
        public SendEmailNotificationView GenerateEmailTemplateForAssignmentComment(string assignmentTitle, string comment, string studentInternalIds)
        {
            string EmailBody = "";
            SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
            sendEmailNotificationView.FromMail = Constants.NoReplyMail;
            sendEmailNotificationView.LogType = "AssignmentNotification";
            sendEmailNotificationView.Subject = Constants.AssignmentCommentNotification;

            System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(Constants.AssignmentCommentNotificationTemplate), Encoding.UTF8);
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
            EmailBody = EmailBody.Replace("@@TeacherName", SessionHelper.CurrentSession.FullName);
            EmailBody = EmailBody.Replace("@@AssignmentName", assignmentTitle);
            EmailBody = EmailBody.Replace("@@Comment", comment);
            sendEmailNotificationView.StudentId = studentInternalIds;
            sendEmailNotificationView.Message = EmailBody;
            sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;

            return sendEmailNotificationView;
        }

        public SendEmailNotificationView GenerateEmailTemplateForAssignmentMarkAsComplete(string assignmentTitle, string studentInternalIds)
        {
            string EmailBody = "";
            string Message = "";
            SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
            sendEmailNotificationView.FromMail = Constants.NoReplyMail;
            sendEmailNotificationView.LogType = "AssignmentNotification";
            sendEmailNotificationView.Subject = Constants.AssignmentMarkAsCompleted;
            Message = Constants.AssignmentCompletionMessage;

            System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(Constants.AssignmentMarkAsCompleteNotificationTemplate), Encoding.UTF8);
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
            EmailBody = EmailBody.Replace("@@Message", Message);
            EmailBody = EmailBody.Replace("@@AssignmentName", assignmentTitle);
            sendEmailNotificationView.StudentId = studentInternalIds;
            sendEmailNotificationView.Message = EmailBody;
            sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;

            return sendEmailNotificationView;
        }

        public SendEmailNotificationView GenerateEmailTemplateForUpdateAssignmentGrade(string assignmentTitle, string studentInternalIds)
        {
            string EmailBody = "";
            SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
            sendEmailNotificationView.FromMail = Constants.NoReplyMail;
            sendEmailNotificationView.LogType = "AssignmentNotification";
            sendEmailNotificationView.Subject = Constants.AssignmentGradeNotification;

            System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(Constants.AssignmentGradeNotificationTemplate), Encoding.UTF8);
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
            EmailBody = EmailBody.Replace("@@TeacherName", SessionHelper.CurrentSession.FullName);
            EmailBody = EmailBody.Replace("@@AssignmentName", assignmentTitle);
            sendEmailNotificationView.StudentId = studentInternalIds;
            sendEmailNotificationView.Message = EmailBody;
            sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;

            return sendEmailNotificationView;
        }

        public bool UploadStudentSharedCopyFiles(List<AssignmentStudentSharedFiles> copiedFiles)
        {
            var uri = API.Assignment.UploadStudentSharedCopyFiles(_path);
            HttpResponseMessage httpResponse = _client.PostAsJsonAsync(uri, copiedFiles).Result;
            return httpResponse.IsSuccessStatusCode;
        }



        public OperationDetails ShareAssignment(int assignmentId, string teacherIds)
        {
            OperationDetails op = new OperationDetails();
            bool result = false;
            var uri = string.Empty;
            uri = API.Assignment.ShareAssignment(_path, assignmentId, teacherIds);

            HttpResponseMessage response = _client.GetAsync(API.Assignment.ShareAssignment(_path, assignmentId, teacherIds)).Result;
            // return response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            }
            return op;
        }

        public byte[] GenerateAssignmentReportXLSFile(string path, Assignment assignmentReports, List<SchoolGroup> lstSchoolGroups, List<AssignmentTask> Tasks, AssignmentReport reportData)
        {

            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {

                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();
                    uint sheetId = 1;



                    workbookPart.Workbook.Sheets = new Sheets();
                    //Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                    Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();

                    workbookPart.Workbook.Save();


                    if (Tasks.Any())
                    {
                        foreach (var task in Tasks)
                        {
                            WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                            worksheetPart.Worksheet = new Worksheet();

                            Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = sheetId, Name = task.TaskTitle };

                            sheets.Append(sheet);

                            SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());
                            var studentData = reportData.AssignmentStudentData.Where(x => x.TaskId == task.TaskId);
                            var questionAnswers = reportData.QuizReportAnswers.Where(x => x.ResourseId == task.TaskId);
                            var questionList = GetQuizQuestionsByTaskId(task.TaskId);
                            //var questions = 
                            // filter questions as per task 
                            // Constructing header
                            Row row = new Row();

                            row.Append(
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentName"), CellValues.String),
                                 ConstructCell(ResourceManager.GetString("Assignment.Assignment.Task"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.GroupName"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.TeacherName"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentSDate"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentDDate"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentName"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentYear"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentSection"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.QuizName"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.QuizScore"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentCompleted"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentSubmissionDate"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentGradingDone"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentGrade"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentFeedbackXls"), CellValues.String),
                                ConstructCell(ResourceManager.GetString("Assignment.Assignment.TaskCompleted"), CellValues.String));
                            foreach (var i in questionList)
                            {
                                var itemName = i.QuestionText == null ? String.Empty : Regex.Replace(i.QuestionText, @"<[^>]+>|&nbsp;", String.Empty).Trim();
                                itemName = i.QuestionText == null ? String.Empty : Regex.Replace(itemName, @"\s{2,}", String.Empty);
                                row.Append(
                                    ConstructCell(itemName, CellValues.String)
                                    );
                            }
                            //if (assignmentReports.TaskCount > 0)
                            //{
                            //    for (int j = 1; j <= assignmentReports.TaskCount; j++)
                            //    {
                            //        row.Append(ConstructCell(ResourceManager.GetString("Assignment.Assignment.TaskCompleted") + j, CellValues.String));
                            //    }
                            //}
                            //else
                            //    row.Append(ConstructCell(ResourceManager.GetString("Assignment.Assignment.TaskCompleted") + "1", CellValues.String));


                            // Insert the header row to the Sheet Data
                            sheetData.AppendChild(row);
                            foreach (var item in studentData)
                            {

                                string groupName = string.Empty;
                                if (lstSchoolGroups.Count > 0)
                                {
                                    foreach (var SchoolGroup in lstSchoolGroups)
                                    {
                                        if (groupName == "")
                                        {
                                            groupName = SchoolGroup.SchoolGroupName;
                                        }
                                        else
                                        {
                                            groupName += "," + SchoolGroup.SchoolGroupName;
                                        }
                                    }

                                }

                                int currentAssignmentId = 0;
                                int counter = 0;
                                string rawComment = string.Empty;
                                row = new Row();
                                if (item.Comment != null)
                                {
                                    rawComment = System.Text.RegularExpressions.Regex.Replace(item.Comment, "<.*?>", String.Empty);
                                }
                                else
                                {
                                    rawComment = null;
                                }

                                row.Append(
                                    ConstructCell(assignmentReports.AssignmentTitle, CellValues.String),
                                    ConstructCell(task.TaskTitle, CellValues.String),
                                    ConstructCell(groupName, CellValues.String), //GroupName
                                    ConstructCell(assignmentReports.TeacherName, CellValues.String), //TeacherName
                                    ConstructCell(Convert.ToString(assignmentReports.StartDate.ToString("MMM-dd-yyyy")), CellValues.String),
                                    ConstructCell(assignmentReports.DueDate == null ? String.Empty : Convert.ToString(assignmentReports.DueDate.Value.ToString("MMM-dd-yyyy hh:mm tt")), CellValues.String),
                                    //ConstructCell(Convert.ToString(assignmentReports.DueDate), CellValues.String),
                                    ConstructCell(item.StudentName, CellValues.String),
                                    ConstructCell(item.DisplayAcademicYear, CellValues.String),
                                    ConstructCell(item.SectionName, CellValues.String),
                                     ConstructCell(item.QuizName, CellValues.String),
                                    ConstructCell(item.QuizScore, CellValues.String),
                                    ConstructCell(item.CompleteMarkByTeacher == true ? "Yes" : "No", CellValues.String),
                                    ConstructCell(Convert.ToString(item.CompletedOn) != "1/1/0001 12:00:00 AM" ? Convert.ToString(item.CompletedOn.ToString("MMM-dd-yyyy hh:mm tt")) : "", CellValues.String), // submissionDate
                                    ConstructCell(item.GradingTemplateItemId != 0 ? "Yes" : "No", CellValues.String),
                                    ConstructCell(item.GradingTemplateItemId != 0 ? item.ShortLabel : "", CellValues.String),
                                    ConstructCell(rawComment, CellValues.String),
                                    ConstructCell(item.IsTaskCompleted == true ? "Yes" : "No", CellValues.String));
                                foreach (var q in questionList)
                                {
                                    var isExist = questionAnswers.Where(x => x.QuizQuestionId == q.QuizQuestionId && x.ResourseId == item.TaskId).Any();
                                    if (!isExist)
                                    {
                                        row.Append(
                                      ConstructCell("0.00", CellValues.String)
                                      );
                                    }
                                    else
                                    {
                                        foreach (var i in questionAnswers)
                                        {

                                            if (item.TaskId == i.ResourseId && q.QuizQuestionId == i.QuizQuestionId && i.UserId == item.StudentId)
                                            {
                                                row.Append(
                                              ConstructCell(Convert.ToString(i.ObtainedMarks), CellValues.String)
                                              );
                                            }
                                        }
                                    }
                                }
                                sheetData.AppendChild(row);
                                currentAssignmentId = item.AssignmentStudentId;
                            }
                            worksheetPart.Worksheet.Save();
                            sheetId++;
                        }
                    }
                    else
                    {
                        WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                        worksheetPart.Worksheet = new Worksheet();

                        Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                        sheets.Append(sheet);

                        SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());
                        Row row = new Row();

                        row.Append(
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentName"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.GroupName"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.TeacherName"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentSDate"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentDDate"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentName"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentYear"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentSection"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentCompleted"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentSubmissionDate"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentGradingDone"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentGrade"), CellValues.String),
                            ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentFeedbackXls"), CellValues.String));

                        // Insert the header row to the Sheet Data
                        sheetData.AppendChild(row);
                        foreach (var item in reportData.AssignmentStudentData)
                        {

                            string groupName = string.Empty;
                            if (lstSchoolGroups.Count > 0)
                            {
                                foreach (var SchoolGroup in lstSchoolGroups)
                                {
                                    if (groupName == "")
                                    {
                                        groupName = SchoolGroup.SchoolGroupName;
                                    }
                                    else
                                    {
                                        groupName += "," + SchoolGroup.SchoolGroupName;
                                    }
                                }

                            }

                            int currentAssignmentId = 0;
                            int counter = 0;
                            string rawComment = string.Empty;

                            counter = 1;
                            row = new Row();
                            if (item.Comment != null)
                            {
                                rawComment = System.Text.RegularExpressions.Regex.Replace(item.Comment, "<.*?>", String.Empty);
                            }
                            else
                            {
                                rawComment = null;
                            }

                            row.Append(
                                ConstructCell(assignmentReports.AssignmentTitle, CellValues.String),
                                ConstructCell(groupName, CellValues.String), //GroupName
                                ConstructCell(assignmentReports.TeacherName, CellValues.String), //TeacherName
                                ConstructCell(Convert.ToString(assignmentReports.StartDate.ToString("MMM-dd-yyyy")), CellValues.String),
                                ConstructCell(assignmentReports.DueDate == null ? String.Empty : Convert.ToString(assignmentReports.DueDate.Value.ToString("MMM-dd-yyyy hh:mm tt")), CellValues.String),
                                //ConstructCell(Convert.ToString(assignmentReports.DueDate), CellValues.String),
                                ConstructCell(item.StudentName, CellValues.String),
                                ConstructCell(item.DisplayAcademicYear, CellValues.String),
                                ConstructCell(item.SectionName, CellValues.String),
                                ConstructCell(item.CompleteMarkByTeacher == true ? "Yes" : "No", CellValues.String),
                                ConstructCell(Convert.ToString(item.CompletedOn) != "1/1/0001 12:00:00 AM" ? Convert.ToString(item.CompletedOn.ToString("MMM-dd-yyyy hh:mm tt")) : "", CellValues.String), // submissionDate
                                ConstructCell(item.GradingTemplateItemId != 0 ? "Yes" : "No", CellValues.String),
                                ConstructCell(item.GradingTemplateItemId != 0 ? item.ShortLabel : "", CellValues.String),
                                ConstructCell(rawComment, CellValues.String));
                            sheetData.AppendChild(row);
                            currentAssignmentId = item.AssignmentStudentId;

                        }
                        worksheetPart.Worksheet.Save();
                    }





                }



                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch (Exception)
            {

                throw;
            }




            //try
            //{
            //    using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
            //    {

            //        WorkbookPart workbookPart = document.AddWorkbookPart();
            //        workbookPart.Workbook = new Workbook();


            //        WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            //        worksheetPart.Worksheet = new Worksheet();

            //        Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

            //        Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

            //        sheets.Append(sheet);

            //        workbookPart.Workbook.Save();

            //        SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());

            //        int pgcounter = 1;
            //        if (assignmentReports.TaskCount > 1)
            //        {
            //            if (pgcounter == 1)
            //            {
            //                for (int i = 1; i <= assignmentReports.TaskCount ; i++)
            //                {
            //                    pgcounter = pgcounter + 1;



            //                    // Constructing header
            //                    Row row = new Row();

            //                    row.Append(
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentName"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.GroupName"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.TeacherName"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentSDate"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentDDate"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentName"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentYear"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.StudentSection"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.QuizScore"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentCompleted"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentSubmissionDate"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentGradingDone"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentGrade"), CellValues.String),
            //                        ConstructCell(ResourceManager.GetString("Assignment.Assignment.AssignmentFeedbackXls"), CellValues.String));
            //                    if (assignmentReports.TaskCount > 0)
            //                    {
            //                        for (int j = 1; j <= assignmentReports.TaskCount; j++)
            //                        {
            //                            row.Append(ConstructCell(ResourceManager.GetString("Assignment.Assignment.TaskCompleted") + j, CellValues.String));
            //                        }
            //                    }
            //                    else
            //                        row.Append(ConstructCell(ResourceManager.GetString("Assignment.Assignment.TaskCompleted") + "1", CellValues.String));


            //                    // Insert the header row to the Sheet Data
            //                    sheetData.AppendChild(row);

            //                    string groupName = string.Empty;
            //                    if (lstSchoolGroups.Count > 0)
            //                    {
            //                        foreach (var item in lstSchoolGroups)
            //                        {
            //                            if (groupName == "")
            //                            {
            //                                groupName = item.SchoolGroupName;
            //                            }
            //                            else
            //                            {
            //                                groupName += "," + item.SchoolGroupName;
            //                            }
            //                        }

            //                    }

            //                    if (assignmentReports.lstAssignmentStudent.Count > 0)
            //                    {

            //                        int currentAssignmentId = 0;
            //                        int counter = 0;
            //                        string rawComment = string.Empty;
            //                        foreach (var item in assignmentReports.lstAssignmentStudent)
            //                        {
            //                            if (currentAssignmentId != item.AssignmentStudentId)
            //                            {
            //                                counter = 1;
            //                                row = new Row();
            //                                if (item.Comment != null)
            //                                {
            //                                    rawComment = System.Text.RegularExpressions.Regex.Replace(item.Comment, "<.*?>", String.Empty);
            //                                }
            //                                else
            //                                {
            //                                    rawComment = null;
            //                                }

            //                                row.Append(
            //                                    ConstructCell(assignmentReports.AssignmentTitle, CellValues.String),
            //                                    ConstructCell(groupName, CellValues.String), //GroupName
            //                                    ConstructCell(assignmentReports.TeacherName, CellValues.String), //TeacherName
            //                                    ConstructCell(Convert.ToString(assignmentReports.StartDate.ToString("MMM-dd-yyyy")), CellValues.String),
            //                                    ConstructCell(assignmentReports.DueDate == null ? String.Empty : Convert.ToString(assignmentReports.DueDate.Value.ToString("MMM-dd-yyyy hh:mm tt")), CellValues.String),
            //                                    //ConstructCell(Convert.ToString(assignmentReports.DueDate), CellValues.String),
            //                                    ConstructCell(item.StudentName, CellValues.String),
            //                                    ConstructCell(item.DisplayAcademicYear, CellValues.String),
            //                                    ConstructCell(item.SectionName, CellValues.String),
            //                                    ConstructCell(item.QuizScore, CellValues.String),
            //                                    ConstructCell(item.CompleteMarkByTeacher == true ? "Yes" : "No", CellValues.String),
            //                                    ConstructCell(Convert.ToString(item.CompletedOn) != "1/1/0001 12:00:00 AM" ? Convert.ToString(item.CompletedOn.ToString("MMM-dd-yyyy hh:mm tt")) : "", CellValues.String), // submissionDate
            //                                    ConstructCell(item.GradingTemplateItemId != 0 ? "Yes" : "No", CellValues.String),
            //                                    ConstructCell(item.GradingTemplateItemId != 0 ? item.ShortLabel : "", CellValues.String),
            //                                    ConstructCell(rawComment, CellValues.String),
            //                                    ConstructCell(item.IsTaskCompleted == true ? "Yes" : "No", CellValues.String));
            //                                sheetData.AppendChild(row);
            //                                currentAssignmentId = item.AssignmentStudentId;
            //                            }
            //                            else
            //                            {
            //                                if (assignmentReports.TaskCount > 1)
            //                                {
            //                                    if (counter == 1)
            //                                    {
            //                                        for (int k = 1; k <= assignmentReports.TaskCount - 1; k++)
            //                                        {
            //                                            counter = counter + 1;
            //                                            row.Append(ConstructCell(item.IsTaskCompleted == true ? "Yes" : "No", CellValues.String));
            //                                        }
            //                                    }
            //                                }
            //                            }
            //                        }

            //                    }

            //                    worksheetPart.Worksheet.Save();
            //                }
            //            }
            //        }

            //    }



            //    var data = System.IO.File.ReadAllBytes(path);

            //    System.IO.File.Delete(path);

            //    return data;
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
        }

        private IEnumerable<QuizQuestionsEdit> GetQuizQuestionsByTaskId(int taskId)
        {
            var uri = API.Assignment.GetQuizQuestionsByTaskId(_path, taskId);
            List<QuizQuestionsEdit> quiz = new List<QuizQuestionsEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, IEnumerable<QuizQuestionsView>>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    foreach (QuizQuestionsView source in sourceModel)
                    {
                        var destination = new QuizQuestionsEdit();
                        EntityMapper<QuizQuestionsView, QuizQuestionsEdit>.Map(source, destination);
                        quiz.Add(destination);
                    }
                }
            }
            return quiz;
        }

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }

        public bool UndoAssignmentsArchive(ArchiveAssignment model)
        {
            var uri = API.Assignment.UndoAssignmentsArchive(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }
        #endregion
    }
}