﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolSpaceService : ISchoolSpaceService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/schoolspace";
        #endregion

        public SchoolSpaceService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                 _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool AddOrUpdateSchoolSpaceData(SchoolSpaceEdit model)
        {
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new SchoolSpace();
            EntityMapper<SchoolSpaceEdit, SchoolSpace>.Map(model, sourceModel);
            if (model.SpaceId == 0)
                uri = API.SchoolSpace.AddSchoolSpace(_path);
            else
                uri = API.SchoolSpace.UpdateSchoolSpace(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteSchoolSpaceData(int id)
        {
            var uri = API.SchoolSpace.DeleteSchoolSpace(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public SchoolSpace GetSchoolSpaceById(int id)
        {
            var result = new SchoolSpace();
            var uri = API.SchoolSpace.GetSchoolSpaceById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, SchoolSpace>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SchoolSpace> GetSchoolSpaceBySchoolId(int schoolId)
        {
            var result = new List<SchoolSpace>();
            var uri = API.SchoolSpace.GetSchoolSpaceBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolSpace>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SchoolSpace> GetSchoolSpaces()
        {
            var result = new List<SchoolSpace>();
            var uri = API.SchoolSpace.GetSchoolSpaces(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolSpace>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
    }
}