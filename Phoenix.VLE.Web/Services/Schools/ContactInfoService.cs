﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;


namespace Phoenix.VLE.Web.Services
{
    public class ContactInfoService : IContactInfoService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/contactinfo";
        #endregion

        public ContactInfoService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool DeleteContactInfoData(int id)
        {
            var uri = API.ContactInfo.DeleteContactInfo(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public ContactInfo GetContactInfoById(int id)
        {
            var result = new ContactInfo();
            var uri = API.ContactInfo.GetContactInfoById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, ContactInfo>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<ContactInfo> GetContactInfoBySchoolId(int schoolId)
        {
            var result = new List<ContactInfo>();
            var uri = API.ContactInfo.GetContactInfoBySchoolId(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<ContactInfo>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<ContactInfo> GetContactInfos()
        {
            var result = new List<ContactInfo>();
            var uri = API.ContactInfo.GetContactInfos(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<ContactInfo>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool AddOrUpdateContactInfoData(ContactInfoEdit model)
        {
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new ContactInfo();
            EntityMapper<ContactInfoEdit, ContactInfo>.Map(model, sourceModel);
            if (model.ContactInfoId == 0)
                uri = API.ContactInfo.AddContactInfo(_path);
            else
                uri = API.ContactInfo.UpdateContactInfo(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }
    }
}