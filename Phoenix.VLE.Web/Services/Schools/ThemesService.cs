﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class ThemesService : IThemesService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/themes";
        #endregion

        public ThemesService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public IEnumerable<Themes> GetAllSchoolThemes(int? schoolId = null, short languageId=0)
        {
            var uri = API.Themes.GetAllSchoolThemes(_path, schoolId, languageId);
            IEnumerable<Themes> Themes = new List<Themes>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Themes = EntityMapper<string, IEnumerable<Themes>>.MapFromJson(jsonDataProviders);
            }
            return Themes;
        }

        public IEnumerable<Themes> GetAllSchoolThemes(int schoolId, int? curriculumId,short languageId=0)
        {
            var uri = API.Themes.GetAllSchoolThemes(_path, schoolId, curriculumId, languageId);
            IEnumerable<Themes> Themes = new List<Themes>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Themes = EntityMapper<string, IEnumerable<Themes>>.MapFromJson(jsonDataProviders);
            }
            return Themes;
        }

        public SchoolThemeEdit GetSchoolThemeById(int themeId)
        {
            var uri = API.Themes.GetSchoolThemeById(_path, themeId);
            var destination = new SchoolThemeEdit();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var sourceModel = EntityMapper<string, Themes>.MapFromJson(jsonDataProviders);
                if (sourceModel != null)
                {
                    EntityMapper<Themes, SchoolThemeEdit>.Map(sourceModel, destination);
                }
            }
            return destination;
        }

        public IEnumerable<Themes> GetStudentGradeTheme(long userId)
        {
            var uri = API.Themes.GetStudentGradeTheme(_path, userId);
            IEnumerable<Themes> themes = new List<Themes>();

            //To set authorization header, to service called before setting login cookie
            var token = Helpers.CommonHelper.GetCookieValue("st", "tk");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                themes = EntityMapper<string, IEnumerable<Themes>>.MapFromJson(jsonDataProviders);
            }
            return themes;
        }

        public bool SchoolThemeCUD(SchoolThemeEdit model)
        {
            var uri = string.Empty;
            var sourceModel = new Themes();
            EntityMapper<SchoolThemeEdit, Themes>.Map(model, sourceModel);

            if (model.IsAddMode)
            {
                sourceModel.SchoolGradeIds = string.Join(",", model.SchoolGradeIdList);
                uri = API.Themes.InsertSchoolTheme(_path);
            }
            else
            {
                sourceModel.SchoolGradeIds = model.SchoolGradeId.ToString();
                uri = API.Themes.UpdateSchoolTheme(_path);
            }
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }

    }
}