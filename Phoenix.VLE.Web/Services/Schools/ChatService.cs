﻿using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class ChatService : IChatService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/chat";
        readonly string _chatpath = "api/v1/ChatterPermission";
        List<User> lstuser= new List<User>();
        #endregion
        public ChatService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public bool Delete(int id)
        {
            var uri = API.Blog.DeleteBlog(_path, id, SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public List<User> GetAllUser()
        {
            var result = new List<User>();
            lstuser.Add(new User { FirstName = "Arvind" });
            lstuser.Add(new User { FirstName = "Aman" });
            lstuser.Add(new User { FirstName = "Vivek" });
          //  result = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            return lstuser;
        }

        public IEnumerable<Chat> GetChatHistorybyUserId(int schoolId, int userId, int otherUser,int groupId,int pageNum)
        {
            var result = new List<Chat>();
            var uri = API.Chat.GetChatHistoryByUserId(_path,schoolId,userId,otherUser,groupId,pageNum);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Chat>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Chat> GetRecentChatUserList(int schoolId, int userId)
        {
            var result = new List<Chat>();
            var uri = API.Chat.GetRecentChatUserList(_path, schoolId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Chat>>.MapFromJson(jsonDataProviders);
            }
            return result;
            throw new NotImplementedException();
        }

        public IEnumerable<RecentUserActivity> GetRecentUserActivity(int schoolId, int userId, bool enableChat, long? groupId = 0 )
        {
            var result = new List<RecentUserActivity>();
            var uri = API.Chat.GetRecentUserActivity(_path, schoolId,userId, enableChat, groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<RecentUserActivity>>.MapFromJson(jsonDataProviders);
            }
            return result;
            throw new NotImplementedException();
        }


        public IEnumerable<Chat> GetSearchInChat(int schoolId, string searchText,int userTypeId,int userId)
        {
            var result = new List<Chat>();
            var uri = API.Chat.GetSearchInChat(_path, schoolId,userId, userTypeId, searchText);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Chat>>.MapFromJson(jsonDataProviders);
            }
            return result;
            throw new NotImplementedException();
        }
        public IEnumerable<LogInUser> GetMyContactList(int schoolId, int userId)
        {
            var result = new List<LogInUser>();
            var uri = API.Chat.GetMyContactsList(_path, schoolId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<LogInUser>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public int GetGroupLatest()
        {
            var result = 0;
            var uri = API.Chat.GetLatestGroup(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result =Convert.ToInt32(jsonDataProviders);
            }
            return result;
            throw new NotImplementedException();
        }
        public IEnumerable<InviteChat> GetGroupMemberList(int groupId, int schoolId)
        {
            var result = new List<InviteChat>();
            var uri = API.Chat.GetGroupMemberList(_path, groupId, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<InviteChat>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public string GetGroupName(int groupId)
        {
            var result ="";
            var uri = API.Chat.GetGroupName(_path, groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = jsonDataProviders.ToString();
            }
            return result;
        }
        public IEnumerable<UserChatPermission> GetUserPermissionChat(int schoolId, int userId)
        {
            var result = new List<UserChatPermission>();
            var uri = API.ChatterPermission.GetChatPermmissionByUser(_chatpath, schoolId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<UserChatPermission>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public int UpdateInviteChat(InviteChat inviteChatModel)
        {
            var result = 0;
            try
            {
                var uri = API.Chat.UpdateInviteChatByUserId(_path);

                HttpResponseMessage response = _client.PostAsJsonAsync(uri, inviteChatModel).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                    result = Convert.ToInt32(jsonDataProviders);
                }
                return result;
            }
            catch (Exception ex)
            {

                return 0;
            }
            throw new NotImplementedException();
        }
        public string UpdateInviteGroupChatName(InviteChat inviteChatModel)
        {
            var result = "";
            try
            {
                var uri = API.Chat.UpdateInviteChatGroupName(_path);

                HttpResponseMessage response = _client.PostAsJsonAsync(uri, inviteChatModel).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                    result = jsonDataProviders;
                }
                return result;
            }
            catch (Exception ex)
            {

                return result;
            }
            throw new NotImplementedException();
        }

        public int DeleteChatConversation(long schoolId, long groupId, long fromUser, long toUser)
        {
            var result = 0;
            try
            {
                var uri = API.Chat.DeleteChatConversation(_path, schoolId, groupId, fromUser, toUser);

                HttpResponseMessage response = _client.GetAsync(uri).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                    result = Convert.ToInt32(jsonDataProviders);
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
    }
}