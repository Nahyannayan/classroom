﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class FeePaymentsService : IFeePaymentsService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        readonly string _path = "api/v1/TimeTable";

        //readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        readonly string GetFeeDetailsAPI = ConfigurationManager.AppSettings["GetFeeDetailsAPI"];
        readonly string GetPaymentTypeAPI = ConfigurationManager.AppSettings["GetPaymentTypeAPI"];
        readonly string GetDiscountAPI = ConfigurationManager.AppSettings["GetDiscountAPI"];
        readonly string GetFeeScheduleAPI = ConfigurationManager.AppSettings["GetFeeScheduleAPI"];
        readonly string AddFeeDetailsAPI = ConfigurationManager.AppSettings["AddFeeDetailsAPI"];
        readonly string GetOutstandingAmountAPI = ConfigurationManager.AppSettings["GetOutstandingAmountAPI"];
        //readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        //readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        //readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        //readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];
        private ILoggerClient _loggerClient;
        private IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        public FeePaymentsService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion

        public IEnumerable<PaymentType> GetPaymentTypes(string studId, string source)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            PaymentTypeRoot paymentTypes = new PaymentTypeRoot();
            List<PaymentType> paymentTypList = new List<PaymentType>();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //    if (token != null)
                //    {
                //        if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //        {
                //            string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetPaymentTypeAPI);
                httpRequestMessage.Headers.Add("source", source);
                httpRequestMessage.Headers.Add("stuId", Convert.ToString(studId));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);


                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        paymentTypes = new JavaScriptSerializer().Deserialize<PaymentTypeRoot>(json);

                        if (paymentTypes.data != null && paymentTypes.data.Count > 0)
                            paymentTypList = paymentTypes.data;
                    }
                }

                //    }
                //}
                return paymentTypList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in FeePaymentsService.GetPaymentTypes()" + ex.Message);
                return paymentTypList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, paymentTypList, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public IEnumerable<FeeSchedule> GetFeeSchedule(string studId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            FeeScheduleRoot feeDetails = new FeeScheduleRoot();
            List<FeeSchedule> feeDetlsList = new List<FeeSchedule>();
            var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetFeeScheduleAPI);
                httpRequestMessage.Headers.Add("stuId", Convert.ToString(studId));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        feeDetails = new JavaScriptSerializer().Deserialize<FeeScheduleRoot>(json);

                        if (feeDetails.data != null && feeDetails.data.Count > 0)
                            feeDetlsList = feeDetails.data;
                    }
                }

                //    }
                //}
                return feeDetlsList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in FeePaymentsService.GetFeeSchedule()" + ex.Message);
                return feeDetlsList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, feeDetlsList, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public IEnumerable<StudentsFeeDetails> GetFeeDetails(string studId, string paymentTo, int providerTypeID, string isPaySibling)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            FeeDetailsRoot feeDetails = new FeeDetailsRoot();
            IEnumerable<StudentsFeeDetails> feeDetlsList = new List<StudentsFeeDetails>();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetFeeDetailsAPI);
                httpRequestMessage.Headers.Add("isPaySibling", isPaySibling);
                httpRequestMessage.Headers.Add("stuId", Convert.ToString(studId));
                httpRequestMessage.Headers.Add("paymentTo", Convert.ToString(paymentTo).ToUpper());
                httpRequestMessage.Headers.Add("providerTypeID", Convert.ToString(providerTypeID));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        feeDetails = new JavaScriptSerializer().Deserialize<FeeDetailsRoot>(json);

                        if (feeDetails.data != null && feeDetails.data.Count() > 0)
                            feeDetlsList = feeDetails.data;
                    }
                }

                //    }
                //}
                return feeDetlsList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in FeePaymentsService.GetFeeDetails()" + ex.Message);
                return feeDetlsList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, feeDetlsList, response, SessionHelper.CurrentSession.UserName);
            }

            //return json;
        }
        public string GetDiscountDetails(string studId, double amount, int paymentTypeID, int feeID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            DiscountDetailsRoot discDetails = new DiscountDetailsRoot();
            List<DiscountDetails> disclsList = new List<DiscountDetails>();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetDiscountAPI);
                httpRequestMessage.Headers.Add("feeID", Convert.ToString(feeID));
                httpRequestMessage.Headers.Add("stuId", Convert.ToString(studId));
                httpRequestMessage.Headers.Add("amount", Convert.ToString(amount));
                httpRequestMessage.Headers.Add("paymentTypeID", Convert.ToString(paymentTypeID));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        //discDetails = new JavaScriptSerializer().Deserialize<DiscountDetailsRoot>(json);

                        //if (discDetails.data != null && discDetails.data.Count > 0)
                        //    disclsList = discDetails.data;
                    }
                }
                //    }
                //}
                //return disclsList;
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in FeePaymentsService.GetDiscountDetails()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public OnlinePayResponse SubmitOnlinePaymentRequest(IEnumerable<StudentsFeeDetails> obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = ""; string apdId = string.Empty;
            OnlinePayResponse onlPayResponse = new OnlinePayResponse();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                var paymentOrigin = "CLASSROOM_WEB"; // Global
                var payload = string.Empty; string source = obj.Select(m => m.Source).FirstOrDefault();
                foreach (var item in obj)
                {
                    string jsonFeeDetails = JsonConvert.SerializeObject(item.FeeDetail.Select(m => new { m.BlockPayNow, m.AdvancePaymentAvailable, m.AdvanceDetails, m.DiscAmount, m.DueAmount, m.PayAmount, m.OriginalAmount, m.FeeDescription, m.FeeID }));
                    string jsonDiscountDetails = JsonConvert.SerializeObject(item.DiscountDetails);

                    payload += "," + "{\"STU_NO\":\"" + item.STU_NO + "\",\"OnlinePaymentAllowed\":\"" + item.OnlinePaymentAllowed
                            + "\",\"UserMessageforOnlinePaymentBlock\":\"" + item.UserMessageforOnlinePaymentBlock
                            + "\",\"IpAddress\":\"" + item.IpAddress + "\",\"PaymentTypeID\":\"" + item.PaymentTypeID
                            + "\",\"PaymentProcessingCharge\":\"" + item.PaymentProcessingCharge
                            + "\",\"PayingAmount\":\"" + item.PayingAmount + "\",\"PayMode\":\"" + item.PayMode
                            + "\",\"FeeDetail\":" + jsonFeeDetails + ",\"DiscountDetails\":" + jsonDiscountDetails + "}";
                }
                payload = payload.Substring(1);
                payload = string.Format("[{0}]", payload);

                //JSON.stringify([{ "STU_NO":"12300400127471","OnlinePaymentAllowed":true,"UserMessageforOnlinePaymentBlock":""
                //,"PaymentTypeID":779,"PayingAmount":100,"IpAddress":"123","PaymentProcessingCharge":0
                //,"FeeDetail":[{"BlockPayNow":false,"AdvanceDetails":[],"AdvancePaymentAvailable":false,"FeeID":5
                //,"FeeDescription":"TUITION FEE","DueAmount":-1082,"PayAmount":100,"OriginalAmount":0,"DiscAmount":0}]
                //,"DiscountDetails":[],"PayMode":""},{"STU_NO":"12300400132237","OnlinePaymentAllowed":true
                //,"UserMessageforOnlinePaymentBlock":"","PaymentTypeID":779,"PayingAmount":100,"IpAddress":"123","PaymentProcessingCharge":0,"FeeDetail":[{"BlockPayNow":false,"AdvanceDetails":[],"AdvancePaymentAvailable":false,"FeeID":5,"FeeDescription":"TUITION FEE","DueAmount":-1082,"PayAmount":100,"OriginalAmount":0,"DiscAmount":0}],"DiscountDetails":[],"PayMode":""}])

                //string AuthorizedToken = token.token_type + " " + token.access_token;
                using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, AddFeeDetailsAPI))
                {
                    HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
                    httpRequestMessage.Content = content;
                    httpRequestMessage.Headers.Add("paymentTo", source);
                    httpRequestMessage.Headers.Add("paymentOrigin", paymentOrigin);
                    httpRequestMessage.Headers.Add("apdId", apdId);
                    httpRequestMessage.Headers.Add("data", payload);
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                    //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                    using (response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            json = response.Content.ReadAsStringAsync().Result;
                            onlPayResponse = new JavaScriptSerializer().Deserialize<OnlinePayResponse>(json);
                        }
                    }
                }
                //    }
                //}
                return onlPayResponse;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in FeePaymentsService.SubmitOnlinePaymentRequest()" + ex.Message);
                return onlPayResponse;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, onlPayResponse, response, SessionHelper.CurrentSession.UserName);
            }

        }

        public IEnumerable<FeeDetail> GetTotalOutstanding(string studId, string source, bool isDetailed)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            FeeDetailRoot feeOutstRoot = new FeeDetailRoot();
            List<FeeDetail> feeOutstanding = new List<FeeDetail>();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetOutstandingAmountAPI);
                httpRequestMessage.Headers.Add("source", source);
                httpRequestMessage.Headers.Add("stuId", Convert.ToString(studId));
                httpRequestMessage.Headers.Add("isdetailed", Convert.ToString(isDetailed));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        feeOutstRoot = new JavaScriptSerializer().Deserialize<FeeDetailRoot>(json);

                        if (feeOutstRoot.data != null && feeOutstRoot.data.Count > 0)
                            feeOutstanding = feeOutstRoot.data;
                    }
                }

                //    }
                //}
                return feeOutstanding;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in FeePaymentsService.GetTotalOutstanding()" + ex.Message);
                return feeOutstanding;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, feeOutstanding, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
            //    _client.DefaultRequestHeaders.Accept.Clear();
            //    var uri = tokenAPI;
            //    string _ContentType = "application/x-www-form-urlencoded";
            //    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //    var nvc = new List<KeyValuePair<string, string>>();
            //    nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //    nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //    nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //    nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //    using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //    {
            //        using (var result = _client.SendAsync(test).Result)
            //        {
            //            if (result.IsSuccessStatusCode)
            //            {
            //                var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                try
            //                {
            //                    JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                }
            //                finally
            //                {
            //                    result.Dispose();
            //                }
            //            }
            //        }
            //    }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}
            return JsonDeserilize;
        }
    }
}