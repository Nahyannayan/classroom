﻿using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class LeaveService : ILeaveService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        readonly string _path = "api/v1/TimeTable";

        //readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        readonly string allLeaveDetailsAPI = ConfigurationManager.AppSettings["GetLeaveDetailsAPI"];
        readonly string addLeaveRequestAPI = ConfigurationManager.AppSettings["AddLeaveRequestAPI"];
        readonly string GetLeaveTypeAPI = ConfigurationManager.AppSettings["GetLeaveTypeAPI"];
        //readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        //readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        //readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        //readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"]; 
        readonly string LeaveRequestWithAttachment = ConfigurationManager.AppSettings["LeaveRequestWithAttachment"];
        readonly string GET_LIST_OF_ACTIVITIES = ConfigurationManager.AppSettings["GET_LIST_OF_ACTIVITIES"];
        private ILoggerClient _loggerClient;
        private readonly IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        public LeaveService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion
        public IEnumerable<LeaveRequest> GetLeaveRequestsByStudentlId(string studId, string username, string language)
        {
            string json = "";
            List<LeaveRequest> leaveReqList = new List<LeaveRequest>();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, allLeaveDetailsAPI);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                httpRequestMessage.Headers.Add("username", username);
                httpRequestMessage.Headers.Add("language", language);
                httpRequestMessage.Headers.Add("studentid", Convert.ToString(studId));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        var data = new JavaScriptSerializer().Deserialize<List<LeaveRequest>>(json);

                        if (data != null && data.Count > 0)
                            leaveReqList = data;
                    }
                }
                return leaveReqList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in LeaveService.GetLeaveRequestsByStudentlId()" + ex.Message);
                return leaveReqList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, leaveReqList, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public IEnumerable<LeaveType> GetLeaveTypes(string master, int bsuid, int acdid)
        {
            string json = "";
            LeaveTypeRoot leaveTypes = new LeaveTypeRoot();
            List<LeaveType> leaveReqList = new List<LeaveType>();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetLeaveTypeAPI);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                httpRequestMessage.Headers.Add("master", master);
                httpRequestMessage.Headers.Add("bsuidbsuid", Convert.ToString(bsuid));
                httpRequestMessage.Headers.Add("acdid", Convert.ToString(acdid));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        leaveTypes = new JavaScriptSerializer().Deserialize<LeaveTypeRoot>(json);

                        if (leaveTypes.data != null && leaveTypes.data.Count > 0)
                            leaveReqList = leaveTypes.data;

                    }
                }
                return leaveReqList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in LeaveService.GetLeaveTypes()" + ex.Message);
                return leaveReqList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, leaveReqList, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public string EditLeaveRequest(LeaveApplication obj)
        {
            string json = "";
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, addLeaveRequestAPI);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var payload = "{\"SLA_ID\":\"" + Convert.ToString(obj.SLA_Id) + "\",\"STU_ID\":\"" + obj.StudentNo + "\",\"FROMDT\":\"" + obj.FromDT
                   + "\",\"TODT\":\"" + obj.ToDT + "\",\"REMARK\":\"" + obj.Remark
                   + "\",\"APD_ID\":\"" + Convert.ToString(obj.LeaveReasonId) + "\",\"SLA_TYPE\":\"" + obj.SLA_Type + "\",\"bEDIT\":\"" + Convert.ToString(obj.BEdit) + "\"}";
                HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
                httpRequestMessage.Content = content;
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in LeaveService.GetLeaveTypes()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }
        }

        public string LeaveRequestSubmit(HttpFileCollectionBase formFile, LeaveApplication obj)
        {
            string json = "";
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, LeaveRequestWithAttachment);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var data = "{\"SLA_ID\":\"" + Convert.ToString(obj.SLA_Id) + "\",\"STU_ID\":\"" + obj.StudentNo + "\",\"FROMDT\":\"" + obj.FromDT
                    + "\",\"TODT\":\"" + obj.ToDT + "\",\"REMARK\":\"" + obj.Remark
                    + "\",\"APD_ID\":\"" + Convert.ToString(obj.LeaveReasonId) + "\",\"SLA_TYPE\":\"" + obj.SLA_Type
                    + "\",\"bEDIT\":\"" + Convert.ToString(obj.BEdit) + "\"}"; // + "\",\"attachmentStatus\":\"" + obj.AttachmentStatus

                byte[] content = Encoding.ASCII.GetBytes(data);
                var bytes = new ByteArrayContent(content);
                MultipartFormDataContent multiContent = new MultipartFormDataContent();
                multiContent.Add(bytes, "data");
                byte[] imageData = null;
                if (formFile != null && formFile.Count > 0)
                {
                    using (var br = new BinaryReader(formFile[0].InputStream))
                    {
                        imageData = br.ReadBytes((int)formFile[0].ContentLength);
                        ByteArrayContent filebytes = new ByteArrayContent(imageData);
                        multiContent.Add(filebytes, "file", formFile[0].FileName);
                    }

                }
                httpRequestMessage.Content = multiContent;
                httpRequestMessage.Headers.Add("attachmentStatus", obj.AttachmentStatus);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in LeaveService.GetLeaveTypes()" + ex.Message);
                return json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, json, response, SessionHelper.CurrentSession.UserName);
            }
        }

        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
            //    _client.DefaultRequestHeaders.Accept.Clear();
            //    var uri = tokenAPI;
            //    string _ContentType = "application/x-www-form-urlencoded";
            //    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //    var nvc = new List<KeyValuePair<string, string>>();
            //    nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //    nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //    nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //    nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //    using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //    {
            //        using (var result = _client.SendAsync(test).Result)
            //        {
            //            if (result.IsSuccessStatusCode)
            //            {
            //                var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                try
            //                {
            //                    JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                }
            //                finally
            //                {
            //                    result.Dispose();
            //                }
            //            }
            //        }
            //    }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}
            return JsonDeserilize;
        }
        public IEnumerable<EnrolllActivityModel> GetListOfActivities(string studId)
        {
            string json = "";
            EnrolllActivityRoot enrolllactivityRoot = new EnrolllActivityRoot();
            HttpResponseMessage response = new HttpResponseMessage();
            List<EnrolllActivityModel> EnrolllActivityList = new List<EnrolllActivityModel>();
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GET_LIST_OF_ACTIVITIES);
            try
            {
                httpRequestMessage.Headers.Add("stuno", Convert.ToString(studId));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        enrolllactivityRoot = new JavaScriptSerializer().Deserialize<EnrolllActivityRoot>(json);

                        if (enrolllactivityRoot.data != null && enrolllactivityRoot.data.Count > 0)
                            EnrolllActivityList = enrolllactivityRoot.data;
                    }
                }
                return EnrolllActivityList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in LeaveService.GetLeaveTypes()" + ex.Message);
                return EnrolllActivityList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, EnrolllActivityList, response, SessionHelper.CurrentSession.UserName);
            }
        }
    }
}