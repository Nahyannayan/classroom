﻿using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class BlogService : IBlogService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/blog";
        readonly string _ExemplarPath = "api/v1/exemplarwall";
        #endregion
        public BlogService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public bool Delete(int id)
        {
            var uri = API.Blog.DeleteBlog(_path, id, SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public Blog Get(int id)
        {
            var result = new Blog();
            var uri = API.Blog.GetBlogById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, Blog>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public ExemplarWallModel GetExemplar(int id)
        {
            var result = new ExemplarWallModel();
            var uri = API.ExemplarWall.GetExemplarWallById(_ExemplarPath, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, ExemplarWallModel>.MapFromJson(jsonDataProviders);
            }
            return result;
        }


        public IEnumerable<Blog> GetAll()
        {
            var result = new List<Blog>();
            var uri = API.Blog.GetAllBlogs(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<BlogCategory> GetBlogCategories(long? schoolId)
        {
            var result = new List<BlogCategory>();
            var uri = API.Blog.GetBlogCategories(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogCategory>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Blog> GetBlogsByCategoryId(int schoolId, int categoryId, bool? isPublish)
        {

            var result = new List<Blog>();
            var uri = API.Blog.GetBlogsByCategoryId(_path, schoolId, categoryId, isPublish);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Blog> GetBlogsBySchoolId(int schoolId, bool? isPublish)
        {
            var result = new List<Blog>();
            var uri = API.Blog.GetBlogsBySchoolId(_path, schoolId, isPublish);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int Insert(Blog entity)
        {
            int result = 0;
            var uri = string.Empty;

            uri = API.Blog.AddBlog(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? 0 : Convert.ToInt32(jsonDataProviders);
            }
            return result;
        }

        public int InsertExemplar(ExemplarWallModel entity)
        {
            int result = 0;
            var uri = string.Empty;

            uri = API.ExemplarWall.AddExemplarWallModel(_ExemplarPath);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? 0 : 1;
            }
            return result;
        }
        public int InsertMap(SchoolGroup_StudentMapping mapModal)
        {
            int result = 0;
            var uri = string.Empty;

            uri = API.Blog.MapGroupAndStudent(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, mapModal).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? 0 : Convert.ToInt32(jsonDataProviders);
            }
            return result;
        }




        public bool Update(Blog entityToUpdate)
        {
            bool result = false;
            var uri = string.Empty;
            //var sourceModel = new Blog();
            //EntityMapper<SchoolSpaceEdit, SchoolSpace>.Map(model, sourceModel);
            uri = API.Blog.UpdateBlog(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            return response.IsSuccessStatusCode;
        }
        public bool UpdateDiscloseDate(int id)
        {
            var uri = API.BlogComment.UpdateDiscloseDate(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<Blog> GetTeacherBlogs(int schoolId, int pageNumber, int pageSize)
        {
            var result = new List<Blog>();
            var uri = API.Blog.GetTeacherBlogs(_path, schoolId, pageNumber, pageSize);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool PublishBlogById(int blogId, bool isPublish, int publishBy)
        {
            var uri = string.Empty;

            uri = API.Blog.PublishBlogById(_path, blogId, isPublish, publishBy);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<Blog> GetBlogsByGroupId(int groupId, int schoolId, int? userId, bool? isPublish)
        {
            var result = new List<Blog>();
            var uri = API.Blog.GetBlogByGroupId(_path, groupId, schoolId, userId, isPublish);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Blog> GetBlogsByBlogTypeId(int blogTypeId, bool? isPublish)
        {
            var result = new List<Blog>();
            var uri = API.Blog.GetBlogByBlogTypeId(_path, blogTypeId, isPublish);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Blog> GetBlogsByGroupNBlotTypeId(int groupId, int blogTypeId, bool? isPublish)
        {
            var result = new List<Blog>();
            var uri = API.Blog.GetBlogByGroupNBlogTypeId(_path, groupId, blogTypeId, isPublish);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public bool InsertLike(BlogLike blogLike)
        {
            var uri = string.Empty;

            uri = API.Blog.AddBlogLike(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, blogLike).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdatedLike(BlogLike blogLike)
        {
            var uri = string.Empty;

            uri = API.Blog.UpdateBlogLike(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, blogLike).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteLike(int id)
        {
            var uri = API.Blog.DeleteBlogLike(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<BlogLike> GetBlogLikes(int? blogId, int? blogLikeId, int? likedBy, bool? isLike)
        {
            var result = new List<BlogLike>();
            var uri = API.Blog.GetBlogLikes(_path, blogId, likedBy, isLike, blogLikeId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogLike>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<BlogTypes> GetBlogTypes(long? schoolId, short languageId = 0)
        {
            var result = new List<BlogTypes>();
            var uri = API.Blog.GetBlogTypes(_path, schoolId, languageId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogTypes>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int DeleteExemplar(ExemplarWallModel entity)
        {
            int result = 0;
            var uri = string.Empty;

            uri = API.ExemplarWall.DeleteExemplar(_ExemplarPath);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? 0 : 1;
            }
            return result;
        }

        public bool UpdateWallSortOrder(ExemplarWallOrder model)
        {
            var uri = string.Empty;
            uri = API.Blog.UpdateWallSortOrder(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;

        }
        /// <summary>
        /// Added by Arvind 30-06-2020
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int ShareBlogsWithGroup(ShareBlogs model)
        {
            int result = 0;
            var uri = string.Empty;
            uri = API.Blog.ShareBlogWithGroups(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = 1;
            }
            return result;

        }

        #region Generate Email Template Code

        public SendEmailNotificationView GenerateEmailTemplateForNewChatterNotification(string title, string studentInternalIds)
        {
            string EmailBody = "";
            SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
            sendEmailNotificationView.FromMail = Constants.NoReplyMail;
            sendEmailNotificationView.LogType = "ChatterNotification";
            sendEmailNotificationView.Subject = Constants.ChatterNotification;

            System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(Constants.ChatterNotificationTemplate), Encoding.UTF8);
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
            EmailBody = EmailBody.Replace("@@TeacherName", SessionHelper.CurrentSession.FullName);
            EmailBody = EmailBody.Replace("@@BlogName", title);
            sendEmailNotificationView.StudentId = studentInternalIds;
            sendEmailNotificationView.Message = EmailBody;
            sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;

            return sendEmailNotificationView;
        }

        #endregion

        public IEnumerable<Chatter> GetChatterWithComments(int? blogId, int schoolId, int? categoryId, int groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? publishBy, bool? isCommentPublish)
        {
            var result = new List<Chatter>();
            var uri = API.Blog.GetChatterWithComment(_path, blogId, schoolId, categoryId, groupId, userId, isTeacher, isPublish, commentId, publishBy, isCommentPublish);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Chatter>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Blog> GetUserExemplerWallData(long userId, int pageIndex, int pageSize)
        {
            IEnumerable<Blog> blogsList = new List<Blog>();
            var uri = API.Blog.GetUserExemplerWallData(_path, userId, pageIndex, pageSize);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                blogsList = EntityMapper<string, List<Blog>>.MapFromJson(jsonDataProviders);
            }
            return blogsList;
        }
        /// <summary>
        /// Get My wall data- Addded by arvind 02-07-2020
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="schoolId"></param>
        /// <param name="categoryId"></param>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="isPublish"></param>
        /// <param name="commentId"></param>
        /// <param name="publishBy"></param>
        /// <param name="isCommentPublish"></param>
        /// <returns></returns>
        public IEnumerable<Chatter> GetMyWallData(int? blogId, int schoolId, int? categoryId, int? groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? publishBy, bool? isCommentPublish,int? LogedInUserId,int? PageNum)
        {
            var result = new List<Chatter>();
            var uri = API.Blog.GetWallWithComment(_path, blogId, schoolId, categoryId, groupId, userId, isTeacher, isPublish, commentId, publishBy, isCommentPublish, LogedInUserId,PageNum);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Chatter>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Chatter> GetMyWallDataOnSearch(long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId, bool isTeacher, bool? isPublish, int? commentId, int? publishBy, bool? isCommentPublish, int? LogedInUserId,int? pageNum)
        {
            var result = new List<Chatter>();
            var uri = API.Blog.GetWallWithCommentOnSearch(_path, blogId, schoolId, categoryId, blogTypeId, groupId, userId, isTeacher, isPublish, commentId, publishBy, isCommentPublish, LogedInUserId, pageNum);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Chatter>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<SearchMyWall> GetSearchInBlog(long schoolId, string searchString,long userId, bool isTeacher)
        {
            var result = new List<SearchMyWall>();
            var uri = API.Blog.GetSearchInBlog(_path, schoolId, searchString, userId, isTeacher);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SearchMyWall>>.MapFromJson(jsonDataProviders);
            }
            return result;
            throw new NotImplementedException();
        }
        public IEnumerable<SearchMyWall> GetGroupBySearchInBlog(long schoolId, string searchString, long userId, bool isTeacher,long groupId)
        {
            var result = new List<SearchMyWall>();
            var uri = API.Blog.GetGroupBySearchInBlog(_path, schoolId, searchString, userId, isTeacher, groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SearchMyWall>>.MapFromJson(jsonDataProviders);
            }
            return result;
            throw new NotImplementedException();
        }

        public List<BlogStudentDash> GetStudentChatterBlogs(long schoolId,long userId)
        {
            var result = new List<BlogStudentDash>();
            var uri = API.Blog.GetStudentChatterBlogs(_path, schoolId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogStudentDash>>.MapFromJson(jsonDataProviders);
            }
            return result;
            throw new NotImplementedException();
        }


        public List<BlogTeacherDash> GetTeacherChatterBlogs(long schoolId, long userId)
        {
            var result = new List<BlogTeacherDash>();
            var uri = API.Blog.GetTeacherChatterBlogs(_path, schoolId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogTeacherDash>>.MapFromJson(jsonDataProviders);
            }
            return result;
            throw new NotImplementedException();
        }

        public List<PostAndCommentLikeUserList> ViewPostCommentUserList(long BlogId, long CommentSectioId, bool IsCommentDetailDisplay,long UserId) {
            var result = new List<PostAndCommentLikeUserList>();
            var uri = API.Blog.ViewPostCommentUserList(_path, BlogId, CommentSectioId, IsCommentDetailDisplay, UserId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<PostAndCommentLikeUserList>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
    }
}