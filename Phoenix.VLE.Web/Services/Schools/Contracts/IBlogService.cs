﻿using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;

namespace Phoenix.VLE.Web.Services
{
    public interface IBlogService
    {
        IEnumerable<Blog> GetBlogsBySchoolId(int schoolId, bool? isPublish);
        IEnumerable<Blog> GetBlogsByCategoryId(int schoolId, int categoryId, bool? isPublish);
        IEnumerable<Blog> GetAll();
        Blog Get(int id);
        ExemplarWallModel GetExemplar(int id);
        int Insert(Blog entity);
        int InsertExemplar(ExemplarWallModel entity);
        int InsertMap(SchoolGroup_StudentMapping mapModal);
        int DeleteExemplar(ExemplarWallModel entity);
        bool Delete(int id);
        bool Update(Blog entityToUpdate);
        bool UpdateDiscloseDate(int id);
        List<PostAndCommentLikeUserList> ViewPostCommentUserList(long BlogId, long CommentSectioId, bool IsCommentDetailDisplay,long UserId);
        IEnumerable<BlogCategory> GetBlogCategories(long? schoolId);
        IEnumerable<Blog> GetTeacherBlogs(int schoolId, int pageNumber, int pageSize);
        bool PublishBlogById(int blogId, bool isPublish, int publishBy);

        IEnumerable<Blog> GetBlogsByGroupId(int groupId,int schoolId, int? userId, bool? isPublish);
        IEnumerable<Blog> GetBlogsByBlogTypeId(int blogTypeId, bool? isPublish);
        IEnumerable<Blog> GetBlogsByGroupNBlotTypeId(int groupId, int blogTypeId, bool? isPublish);
        IEnumerable<Chatter> GetChatterWithComments(int? blogId, int schoolId, int? categoryId,int groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId,int? publishBy, bool? isCommentPublish);

        bool InsertLike(BlogLike blogLike);
        bool UpdatedLike(BlogLike blogLike);
        bool DeleteLike(int id);
        IEnumerable<BlogLike> GetBlogLikes(int? blogId, int? blogLikeId, int? likedBy, bool? isLike);
        IEnumerable<BlogTypes> GetBlogTypes(long? schoolId, short languageId = 0);
        bool UpdateWallSortOrder(ExemplarWallOrder model);
        SendEmailNotificationView GenerateEmailTemplateForNewChatterNotification(string title, string studentInternalIds);
        IEnumerable<Blog> GetUserExemplerWallData(long id, int pageIndex, int pageSize);

        int ShareBlogsWithGroup(ShareBlogs model);
        IEnumerable<Chatter> GetMyWallData(int? blogId, int schoolId, int? categoryId, int? groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? publishBy, bool? isCommentPublish,int? LogedInUserId,int? PageNum);
        IEnumerable<Chatter> GetMyWallDataOnSearch(long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId, bool isTeacher, bool? isPublish, int? commentId, int? publishBy, bool? isCommentPublish, int? LogedInUserId,int? PageNum = 1);
        IEnumerable<SearchMyWall> GetSearchInBlog(long schoolId, string searchString, long userId, bool isTeacher);

        List<BlogStudentDash> GetStudentChatterBlogs(long schoolId, long userId);
        List<BlogTeacherDash> GetTeacherChatterBlogs(long schoolId, long userId);
        IEnumerable<SearchMyWall> GetGroupBySearchInBlog(long schoolId, string searchString, long userId, bool isTeacher, long groupId);
    }
}
