﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models;
using System.Collections.Generic;

namespace Phoenix.VLE.Web.Services
{
    public interface IDivisionService
    {
        OperationDetails SaveDivisionDetails(DivisionDetails DivisionDetails, string DATAMODE);
        IEnumerable<DivisionDetails> GetDivisionDetails(long BSU_ID, int CurriculumId);
        IEnumerable<ReportingTermModel> GetReportingTermDetail(long ReportingTermId, long SchoolId);
        IEnumerable<ReportingSubTermModel> GetReportingSubTermDetail(long ReportingTermId, long ReportingSubTermId);
        bool SaveReportingTermDetail(ReportingTermModel reportingTermModel);
        bool LockUnlockTerm(long ReportingTermId, bool IsLock);
        bool SaveReportingSubTermDetail(ReportingSubTermModel reportingSubTermModel);
        bool LockUnlockSubTerm(long ReportingSubTermId, bool IsLock);
    }
}
