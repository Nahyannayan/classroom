﻿using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public interface ILeaveService
    {
        IEnumerable<LeaveRequest> GetLeaveRequestsByStudentlId(string studentId, string username, string language);
        IEnumerable<LeaveType> GetLeaveTypes(string master, int bsuid, int acdid);
        IEnumerable<EnrolllActivityModel> GetListOfActivities(string studId);
        string EditLeaveRequest(LeaveApplication obj);
        TokenResult GetAuthorizationTokenAsync();
        string LeaveRequestSubmit( HttpFileCollectionBase files,LeaveApplication obj);
    }
}
