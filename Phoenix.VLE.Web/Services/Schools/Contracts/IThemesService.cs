﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IThemesService
    {
        IEnumerable<Themes> GetAllSchoolThemes(int? schoolId = null, short languageId = 0);
        IEnumerable<Themes> GetStudentGradeTheme(long userId);
        bool SchoolThemeCUD(SchoolThemeEdit model);
        SchoolThemeEdit GetSchoolThemeById(int themeId);
        IEnumerable<Themes> GetAllSchoolThemes(int schoolId, int? curriculumId, short languageId = 0);
    }
}
