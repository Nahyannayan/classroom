﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IBlogCommentService
    {
        IEnumerable<BlogComment> GetBlogCommentByBlogId(int blogId, bool? isPublish);

        IEnumerable<BlogComment> GetBlogCommentByPublishBy(int publishId, bool? isPublish);
        IEnumerable<BlogComment> GetAll();
        BlogComment Get(int id);
        int Insert(BlogComment entity);
        bool Delete(int id);
        
        bool Update(BlogComment entityToUpdate);

        bool InsertCommentLike(BlogCommentLike blogCommentLike);
        bool UpdatedCommentLike(BlogCommentLike blogCommentLike);
        bool DeleteCommentLike(int id);
        IEnumerable<BlogCommentLike> GetBlogCommentLikes(int? commentId, int? likedBy);
    }
}
