﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System.Collections.Generic;


namespace Phoenix.VLE.Web.Services
{
    public interface IContactInfoService
    {
        IEnumerable<ContactInfo> GetContactInfos();
        IEnumerable<ContactInfo> GetContactInfoBySchoolId(int schoolId);
        ContactInfo GetContactInfoById(int id);
        bool AddOrUpdateContactInfoData(ContactInfoEdit model);
        bool DeleteContactInfoData(int id);
    }
}
