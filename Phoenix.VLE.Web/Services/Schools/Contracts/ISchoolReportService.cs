﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISchoolReportService
    {
        bool DeleteSchoolReport(int id, long schoolId);
        int SchoolReportCU(SchoolReports schoolReports);
        IEnumerable<ReportListModel> GetReportLists(long schoolId);
        ReportDetailModel GetReportDetail(long reportDetailId, bool isCommonReport, long schoolId, bool isPreview = false);
        IEnumerable<ReportFilterModel> GetReportFilter(string filterCode, string filterParameter);
        ReportDataModel GetReportData(ReportParameterRequestModel requestModel);
    }
}
