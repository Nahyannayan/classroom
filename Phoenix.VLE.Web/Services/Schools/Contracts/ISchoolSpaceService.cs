﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System.Collections.Generic;

namespace Phoenix.VLE.Web.Services
{
    public interface ISchoolSpaceService
    {
        IEnumerable<SchoolSpace> GetSchoolSpaces();
        IEnumerable<SchoolSpace> GetSchoolSpaceBySchoolId(int schoolId);
        SchoolSpace GetSchoolSpaceById(int id);
        bool AddOrUpdateSchoolSpaceData(SchoolSpaceEdit model);
        bool DeleteSchoolSpaceData(int id);
    }
}
