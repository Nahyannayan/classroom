﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IFeePaymentsService
    {
        
        IEnumerable<PaymentType> GetPaymentTypes(string studentId,string source);
        IEnumerable<FeeSchedule> GetFeeSchedule(string studentId);
        string GetDiscountDetails(string studId, double amount, int paymentTypeID, int feeID);
        IEnumerable<StudentsFeeDetails> GetFeeDetails(string studId, string paymentTo, int providerTypeID, string isPaySibling);
        OnlinePayResponse SubmitOnlinePaymentRequest(IEnumerable<StudentsFeeDetails> ob);
        IEnumerable<FeeDetail> GetTotalOutstanding(string studId, string source, bool isDetailed);
    }
}
