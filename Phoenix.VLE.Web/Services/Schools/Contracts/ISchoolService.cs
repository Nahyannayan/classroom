﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISchoolService
    {
        IEnumerable<SchoolInformation> GetSchoolList();
        IEnumerable<SchoolInformation> GetAdminSchoolList();
        SchoolInformation GetSchoolById(int id);
        List<Student> GetStudentForTeacher(long id, string ids = "");
        List<Student> GetStudentForTeacherBySchoolGroup(int id, int pageNumber, int pageSize, string schoolGroupIds, string searchString = "", string sortBy = "");
        FileDownload GetModuleFileDetails(int id, string filetype);
        ReportDashboard GetReportDashboard(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId);
        IEnumerable<SchoolInformation> GetAllBusinesUnitSchools(int pageIndex, int pageSize, string searchString);
        IEnumerable<ChildQuizReportData> GetChildQuizReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        IEnumerable<TeacherList> GetSchoolTeachersBySchoolId(long schoolId);
        IEnumerable<QuizReportData> GetQuizReportDataWithDateRange(long? userId, long? schoolId, DateTime startDate, DateTime endDate);

        GetReportDetail GetReportDetails(DateTime startDate, DateTime endDate);
        bool UpdateSchoolClassroomStatus(long schoolId);
        byte[] GenerateAssignmentsByGroupReportXLSFile(string path, IEnumerable<TableData> groupAssignments);
        byte[] GenerateChattersByGroupReportXLSFile(string path, IEnumerable<ChatterTableData> groupChatters);
        byte[] GenerateAssignmentsByTeacherReportXLSFile(string path, IEnumerable<TeacherAssignment> teacherAssignments);
        byte[] GenerateAssignmentsByStudentReportXLSFile(string path, IEnumerable<StudentAssignment> studentAssignments);
        byte[] GenerateAbuseReportXLSFile(string path, IEnumerable<Data> abuseReport);
        byte[] GenerateUsersLoginReportXLSFile(string path, List<Phoenix.Models.User> loginUsers);
        IEnumerable<TableData> GetSchoolGroupAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        SessionConfiguration GetSchoolSessionStatus(long schoolId);
        IEnumerable<ChatterTableData> GetGroupChatterReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        IEnumerable<TeacherAssignment> GetTeacherAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        IEnumerable<StudentAssignment> GetStudentAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        IEnumerable<Data> GetSchoolAbuseReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        LoginDetailData GetSchoolLoginReport(long userId, string startDate, string endDate, long schoolId);
        bool SaveSchoolSessionStatus(SessionConfiguration model);
        AssignmentReportCard GetAssignmentReportCardData(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId);

        #region
        bool DepartmentCourseCU(Department model);
        IEnumerable<Department> GetDepartmentCourse(long schoolId, long curriculumId, long? departmentId);
        bool CanDeleteDepartment(long departmentId);
        byte[] GenerateStudentVaultReport(string filePath);
        #endregion
    }
}
