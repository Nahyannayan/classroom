﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IChatService
    {
        List<User> GetAllUser();
        IEnumerable<Chat> GetChatHistorybyUserId(int schoolId, int userId, int otherUser,int groupId,int pageNum);
        IEnumerable<Chat> GetRecentChatUserList(int schoolId, int userId);
        IEnumerable<RecentUserActivity> GetRecentUserActivity(int schoolId, int userId, bool enableChat, long? groupId=0);
        IEnumerable<Chat> GetSearchInChat(int schoolId, string searchText,int userTypeId, int userId);
        IEnumerable<LogInUser> GetMyContactList(int schoolId, int userId);
        int GetGroupLatest();
        IEnumerable<InviteChat> GetGroupMemberList(int groupId, int schoolId);
        int UpdateInviteChat(InviteChat inviteChatModel);
        string UpdateInviteGroupChatName(InviteChat inviteChatModel);
        IEnumerable<UserChatPermission> GetUserPermissionChat(int schoolId, int userId);
        string GetGroupName(int groupId);
        int DeleteChatConversation(long schoolId, long groupId, long fromUser, long toUser);
    }
}
