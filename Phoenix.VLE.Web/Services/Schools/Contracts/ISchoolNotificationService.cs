﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public interface ISchoolNotificationService
    {
        List<DeploymentNotification> GetSchoolDeploymentNotificationList();
        bool SaveDeploymentNotificationData(DeploymentNotificationEdit editModel);
    }
}
