﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class DivisionService : IDivisionService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Division";
        #endregion
        public DivisionService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }
        public OperationDetails SaveDivisionDetails(DivisionDetails DivisionDetails, string DATAMODE)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.Division.SaveDivisionDetails(_path, DATAMODE), DivisionDetails).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<OperationDetails>() : default(OperationDetails);
        }
        public IEnumerable<DivisionDetails> GetDivisionDetails(long BSU_ID, int CurriculumId)
        {
            var uri = API.Division.GetDivisionDetails(_path, BSU_ID, CurriculumId);
            IEnumerable<DivisionDetails> Details = new List<DivisionDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Details = EntityMapper<string, IEnumerable<DivisionDetails>>.MapFromJson(jsonDataProviders);
            }
            return Details;
        }
        public IEnumerable<ReportingTermModel> GetReportingTermDetail(long ReportingTermId, long SchoolId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Division.GetReportingTermDetail(_path, ReportingTermId, SchoolId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<ReportingTermModel>>() : new List<ReportingTermModel>();
        }
        public IEnumerable<ReportingSubTermModel> GetReportingSubTermDetail(long ReportingTermId, long ReportingSubTermId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Division.GetReportingSubTermDetail(_path, ReportingTermId, ReportingSubTermId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<ReportingSubTermModel>>() : new List<ReportingSubTermModel>();
        }
        public bool SaveReportingTermDetail(ReportingTermModel reportingTermModel)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.Division.SaveReportingTermDetail(_path), reportingTermModel).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        public bool LockUnlockTerm(long ReportingTermId, bool IsLock)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Division.LockUnlockTerm(_path, ReportingTermId,IsLock)).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        public bool SaveReportingSubTermDetail(ReportingSubTermModel reportingSubTermModel)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.Division.SaveReportingSubTermDetail(_path), reportingSubTermModel).Result;
            return responseMessage.IsSuccessStatusCode;
        }
        public bool LockUnlockSubTerm(long ReportingSubTermId, bool IsLock)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Division.LockUnlockSubTerm(_path, ReportingSubTermId, IsLock)).Result;
            return responseMessage.IsSuccessStatusCode;
        }
    }
}