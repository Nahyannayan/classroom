﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolNotificationService : ISchoolNotificationService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/SchoolNotification";
        #endregion
        public SchoolNotificationService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public List<DeploymentNotification> GetSchoolDeploymentNotificationList()
        {
            var uri = API.SchoolNotification.GetSchoolDeploymentNotificationList(_path);
            List<DeploymentNotification> lst = new List<DeploymentNotification>();
            HttpResponseMessage responseMessage = _client.GetAsync(uri).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = responseMessage.Content.ReadAsStringAsync().Result;
                lst = EntityMapper<string, List<DeploymentNotification>>.MapFromJson(jsonDataProviders);
            }
            return lst;
        }

        public bool SaveDeploymentNotificationData(DeploymentNotificationEdit editModel)
        {
            var model = new DeploymentNotification();
            EntityMapper<DeploymentNotificationEdit, DeploymentNotification>.Map(editModel, model);
            var uri = API.SchoolNotification.SaveDeploymentNotificationData(_path);
            HttpResponseMessage httpResponse = _client.PostAsJsonAsync(uri, model).Result;
            return httpResponse.IsSuccessStatusCode;
        }
    }
}