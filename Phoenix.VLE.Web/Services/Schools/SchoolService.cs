﻿
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolService : ISchoolService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/school";
        #endregion

        public SchoolService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        public IEnumerable<SchoolInformation> GetSchoolList()
        {
            var uri = API.School.GetSchoolList(_path);
            IEnumerable<SchoolInformation> schools = new List<SchoolInformation>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schools = EntityMapper<string, IEnumerable<SchoolInformation>>.MapFromJson(jsonDataProviders);
            }
            return schools;
        }
        public IEnumerable<SchoolInformation> GetAdminSchoolList()
        {
            var uri = API.School.GetAdminSchoolList(_path);
            IEnumerable<SchoolInformation> schools = new List<SchoolInformation>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schools = EntityMapper<string, IEnumerable<SchoolInformation>>.MapFromJson(jsonDataProviders);
            }
            return schools;
        }

        public SchoolInformation GetSchoolById(int id)
        {
            var schools = new SchoolInformation();
            var uri = API.School.GetSchoolById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schools = EntityMapper<string, SchoolInformation>.MapFromJson(jsonDataProviders);
            }
            return schools;
        }

        public List<Student> GetStudentForTeacher(long id, string ids = "")
        {
            var students = new List<Student>();
            var uri = API.School.GetStudentForTeacher(_path, id, ids);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                students = EntityMapper<string, List<Student>>.MapFromJson(jsonDataProviders);
            }
            return students;
        }

        public FileDownload GetModuleFileDetails(int id, string filetype)
        {
            var fileDetails = new FileDownload();
            var uri = API.School.GetModuleFileDetails(_path, id, filetype);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                fileDetails = EntityMapper<string, FileDownload>.MapFromJson(jsonDataProviders);
            }
            return fileDetails;
        }

        public ReportDashboard GetReportDashboard(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId)
        {
            var reportDashboards = new ReportDashboard();
            var uri = API.School.GetReportDashboard(_path, schoolId, startDate, endDate, userId, groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reportDashboards = EntityMapper<string, ReportDashboard>.MapFromJson(jsonDataProviders);
            }
            return reportDashboards;
        }

        public AssignmentReportCard GetAssignmentReportCardData(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId)
        {
            var assignmentReportData = new AssignmentReportCard();
            var uri = API.School.GetAssignmentReportData(_path, schoolId, startDate, endDate, userId, groupId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentReportData = EntityMapper<string, AssignmentReportCard>.MapFromJson(jsonDataProviders);
            }
            return assignmentReportData;
        }

        public List<Student> GetStudentForTeacherBySchoolGroup(int id, int pageNumber, int pageSize, string schoolGroupIds, string searchString = "", string sortBy = "")
        {
            var students = new List<Student>();
            var uri = API.School.GetStudentForTeacherBySchoolGroup(_path, id, pageNumber, pageSize, schoolGroupIds, searchString, sortBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                students = EntityMapper<string, List<Student>>.MapFromJson(jsonDataProviders);
            }
            return students;
        }

        public IEnumerable<ChildQuizReportData> GetChildQuizReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            var uri = API.School.GetChildQuizReportData(_path, schoolId, startDate, endDate, userId);
            IEnumerable<ChildQuizReportData> reportData = new List<ChildQuizReportData>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reportData = EntityMapper<string, IEnumerable<ChildQuizReportData>>.MapFromJson(jsonDataProviders);
            }
            return reportData;
        }

        public IEnumerable<TeacherList> GetSchoolTeachersBySchoolId(long schoolId)
        {
            var uri = API.School.GetSchoolTeachersBySchoolId(_path, schoolId);
            IEnumerable<TeacherList> teacherList = new List<TeacherList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                teacherList = EntityMapper<string, IEnumerable<TeacherList>>.MapFromJson(jsonDataProviders);
            }
            return teacherList;
        }

        public IEnumerable<QuizReportData> GetQuizReportDataWithDateRange(long? userId, long? schoolId, DateTime startDate, DateTime endDate)
        {
            var uri = API.School.GetQuizReportDataWithDateRange(_path, userId, schoolId, startDate, endDate);
            IEnumerable<QuizReportData> reportData = new List<QuizReportData>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reportData = EntityMapper<string, IEnumerable<QuizReportData>>.MapFromJson(jsonDataProviders);
            }
            return reportData;
        }
        public GetReportDetail GetReportDetails(DateTime startDate, DateTime endDate)
        {
            var GetReportDetail = new GetReportDetail();
            var uri = API.School.GetReportDetails(_path, startDate, endDate);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                GetReportDetail = EntityMapper<string, GetReportDetail>.MapFromJson(jsonDataProviders);
            }
            return GetReportDetail;
        }

        public IEnumerable<SchoolInformation> GetAllBusinesUnitSchools(int pageIndex, int pageSize, string searchString)
        {
            IEnumerable<SchoolInformation> schoolList = new List<SchoolInformation>();
            var uri = API.School.GetAllBusinesUnitSchools(_path, pageIndex, pageSize, searchString);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schoolList = EntityMapper<string, IEnumerable<SchoolInformation>>.MapFromJson(jsonDataProviders);
            }
            return schoolList;
        }

        public bool UpdateSchoolClassroomStatus(long schoolId)
        {
            var schoolInfo = new SchoolInformation() { SchoolId = schoolId };
            var uri = API.School.UpdateSchoolClassroomStatus(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, schoolInfo).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public IEnumerable<TableData> GetSchoolGroupAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            var uri = API.School.GetSchoolGroupAssignmentReport(_path, schoolId, startDate, endDate, userId);
            IEnumerable<TableData> groupAssignments = new List<TableData>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                groupAssignments = EntityMapper<string, IEnumerable<TableData>>.MapFromJson(jsonDataProviders);
            }
            return groupAssignments;
        }

        public IEnumerable<ChatterTableData> GetGroupChatterReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            var uri = API.School.GetGroupChatterReport(_path, schoolId, startDate, endDate, userId);
            IEnumerable<ChatterTableData> groupChatters = new List<ChatterTableData>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                groupChatters = EntityMapper<string, IEnumerable<ChatterTableData>>.MapFromJson(jsonDataProviders);
            }
            return groupChatters;
        }

        public IEnumerable<TeacherAssignment> GetTeacherAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            var uri = API.School.GetTeacherAssignmentReport(_path, schoolId, startDate, endDate, userId);
            IEnumerable<TeacherAssignment> teacherAssignments = new List<TeacherAssignment>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                teacherAssignments = EntityMapper<string, IEnumerable<TeacherAssignment>>.MapFromJson(jsonDataProviders);
            }
            return teacherAssignments;
        }

        public IEnumerable<StudentAssignment> GetStudentAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            var uri = API.School.GetStudentAssignmentReport(_path, schoolId, startDate, endDate, userId);
            IEnumerable<StudentAssignment> studentAssignments = new List<StudentAssignment>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentAssignments = EntityMapper<string, IEnumerable<StudentAssignment>>.MapFromJson(jsonDataProviders);
            }
            return studentAssignments;
        }

        public IEnumerable<Data> GetSchoolAbuseReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            var uri = API.School.GetSchoolAbuseReport(_path, schoolId, startDate, endDate, userId);
            IEnumerable<Data> abuseReportData = new List<Data>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                abuseReportData = EntityMapper<string, IEnumerable<Data>>.MapFromJson(jsonDataProviders);
            }
            return abuseReportData;
        }

        public byte[] GenerateAssignmentsByGroupReportXLSFile(string path, IEnumerable<TableData> groupAssignments)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell(ResourceManager.GetString("School.YoYoBlog.Groups"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.Assignments"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.FilesAttached"), CellValues.String));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var assignments in groupAssignments)
                    {
                        row = new Row();

                        row.Append(
                            ConstructCell(assignments.SchoolGroupName.Replace("_", " /"), CellValues.String),
                            ConstructCell(Convert.ToString(assignments.TotalAssignments), CellValues.String),
                            ConstructCell(Convert.ToString(assignments.TotalFiles), CellValues.String));

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        public byte[] GenerateChattersByGroupReportXLSFile(string path, IEnumerable<ChatterTableData> groupChatters)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell(ResourceManager.GetString("School.YoYoBlog.Groups"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.Chatters"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.FilesAttached"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.Comments"), CellValues.String));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var chatter in groupChatters)
                    {
                        row = new Row();

                        row.Append(
                            ConstructCell(chatter.SchoolGroupName.Replace("_", " /"), CellValues.String),
                            ConstructCell(Convert.ToString(chatter.TotalChatters), CellValues.String),
                            ConstructCell(Convert.ToString(chatter.TotalFiles), CellValues.String),
                            ConstructCell(Convert.ToString(chatter.TotalComments), CellValues.String));

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        public byte[] GenerateAssignmentsByTeacherReportXLSFile(string path, IEnumerable<TeacherAssignment> teacherAssignments)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell(ResourceManager.GetString("StudentInformation.Endorsement.Teacher"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.Assignments"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.FilesAttached"), CellValues.String));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var teacherAssignment in teacherAssignments)
                    {
                        row = new Row();

                        row.Append(
                            ConstructCell(teacherAssignment.Teacher, CellValues.String),
                            ConstructCell(Convert.ToString(teacherAssignment.TotalAssignments), CellValues.String),
                            ConstructCell(Convert.ToString(teacherAssignment.TotalFiles), CellValues.String));

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        public byte[] GenerateAssignmentsByStudentReportXLSFile(string path, IEnumerable<StudentAssignment> studentAssignments)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell(ResourceManager.GetString("Shared.Labels.Student"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.Assignments"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.FilesAttached"), CellValues.String));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var item in studentAssignments)
                    {
                        row = new Row();

                        row.Append(
                            ConstructCell(item.Student, CellValues.String),
                            ConstructCell(Convert.ToString(item.TotalAssignments), CellValues.String),
                            ConstructCell(Convert.ToString(item.TotalFiles), CellValues.String));

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        public byte[] GenerateAbuseReportXLSFile(string path, IEnumerable<Data> abuseReport)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell(ResourceManager.GetString("School.Blog.Categories"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("School.Blog.count"), CellValues.String));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var item in abuseReport)
                    {
                        row = new Row();

                        row.Append(
                            ConstructCell(item.Name, CellValues.String),
                            ConstructCell(Convert.ToString(item.Value), CellValues.String));

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        public byte[] GenerateUsersLoginReportXLSFile(string path, List<Phoenix.Models.User> loginUsers)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());


                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell(ResourceManager.GetString("Users.UserList.UserDisplayName"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Users.UserList.UserName"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Users.UserList.UserTypeName"), CellValues.String),
                        ConstructCell(ResourceManager.GetString("Users.UserList.LastLoginTime"), CellValues.String));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var item in loginUsers)
                    {
                        row = new Row();

                        row.Append(
                            ConstructCell(item.UserDisplayName, CellValues.String),
                            ConstructCell(item.UserName, CellValues.String),
                            ConstructCell(item.UserTypeName, CellValues.String),
                            ConstructCell(Convert.ToString(item.LastLoginTime), CellValues.String));

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }

        public LoginDetailData GetSchoolLoginReport(long userId, string startDate, string endDate, long schoolId)
        {
            LoginDetailData userLoginInfo = new LoginDetailData();
            var uri = API.School.GetSchoolLoginReport(_path, userId, schoolId, startDate, endDate);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                userLoginInfo = EntityMapper<string, LoginDetailData>.MapFromJson(jsonDataProviders);
            }
            return userLoginInfo;
        }
        #endregion

        #region Live Session Configuration

        public SessionConfiguration GetSchoolSessionStatus(long schoolId)
        {
            var model = new SessionConfiguration();
            var uri = API.School.GetSchoolSessionStatus(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                model = EntityMapper<string, SessionConfiguration>.MapFromJson(jsonDataProviders);
            }
            return model;
        }

        public bool SaveSchoolSessionStatus(SessionConfiguration model)
        {
            var uri = API.School.SaveSchoolSessionStatus(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        #endregion
        #region FOR DEPARTMENT
        public bool DepartmentCourseCU(Department model)
        {
            var uri = API.School.DepartmentCourseCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<Department> GetDepartmentCourse(long schoolId, long curriculumId, long? departmentId)
        {
            var uri = API.School.GetDepartmentCourse(_path, schoolId, curriculumId, departmentId);
            IEnumerable<Department> reportData = new List<Department>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                reportData = EntityMapper<string, IEnumerable<Department>>.MapFromJson(jsonDataProviders);
            }
            return reportData;
        }
        public bool CanDeleteDepartment(long departmentId)
        {
            bool CanDelete = false;
            var uri = API.School.CanDeleteDepartment(_path, departmentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var res = response.Content.ReadAsStringAsync().Result;
                CanDelete = JsonConvert.DeserializeObject<bool>(res);
            }
            return CanDelete;
        }

        public IEnumerable<Student> GetStudentVaultReportData()
        {
            IEnumerable<Student> lst = new List<Student>();
            var uri = API.School.GetStudentVaultReportData(_path, SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var res = response.Content.ReadAsStringAsync().Result;
                lst = JsonConvert.DeserializeObject<IEnumerable<Student>>(res);
            }
            return lst;
        }

        public byte[] GenerateStudentVaultReport(string filePath)
        {
            var reportData = GetStudentVaultReportData();
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();
                    WorkbookStylesPart stylesPart = workbookPart.AddNewPart<WorkbookStylesPart>();
                    stylesPart.Stylesheet = GetBoldHeaderStyles();
                    stylesPart.Stylesheet.Save();
                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();
                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Vault Report" };
                    sheets.Append(sheet);
                    workbookPart.Workbook.Save();
                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());
                    Row row = new Row();
                    row.Append(
                       new Cell() { CellValue = new CellValue("Student Name"), DataType = CellValues.String, StyleIndex = 1 },
                       new Cell() { CellValue = new CellValue("Student Number"), DataType = CellValues.String, StyleIndex = 1 },
                       new Cell() { CellValue = new CellValue("Grade"), DataType = CellValues.String, StyleIndex = 1 },
                       new Cell() { CellValue = new CellValue("Total Files"), DataType = CellValues.String, StyleIndex = 1 }
                    );
                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);
                    // Inserting each value
                    foreach (var r in reportData)
                    {
                        row = new Row();
                        row.Append(
                                                    new Cell() { CellValue = new CellValue(r.StudentName), DataType = CellValues.String },
                                                    new Cell() { CellValue = new CellValue(r.StudentNumber), DataType = CellValues.String },
                                                    new Cell() { CellValue = new CellValue(r.GradeDisplay), DataType = CellValues.String },
                                                    new Cell() { CellValue = new CellValue(r.TotalCount.ToString()), DataType = CellValues.String }
                                                 );

                        sheetData.AppendChild(row);
                    }
                    worksheetPart.Worksheet.Save();
                }
                var data = System.IO.File.ReadAllBytes(filePath);
                System.IO.File.Delete(filePath);

                return data;
            }
            catch
            {
                throw;
            }
        }

        private static Stylesheet GetBoldHeaderStyles()
        {
            return new Stylesheet(
                new Fonts(
                    new Font(                                                               // Index 0 - The default font.
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 1 - The bold font.
                        new Bold(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 2 - The Italic font.
                        new Italic(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(
                        new FontSize() { Val = 16 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Times New Roman" })
                ),
                new Fills(
                    new Fill(
                        new PatternFill() { PatternType = PatternValues.None }),
                    new Fill(
                        new PatternFill() { PatternType = PatternValues.Gray125 }),
                    new Fill(
                        new PatternFill(
                            new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "FFFFFF00" } }
                        )
                        { PatternType = PatternValues.Solid })
                ),
                new Borders(
                    new Border(                                                         // Index 0 - The default border.
                        new LeftBorder(),
                        new RightBorder(),
                        new TopBorder(),
                        new BottomBorder(),
                        new DiagonalBorder()),
                    new Border(                                                         // Index 1 - Applies a Left, Right, Top, Bottom border to a cell
                        new LeftBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new RightBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new TopBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new BottomBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                ),
                new CellFormats(
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 0 },                          // Index 0 - The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                    new CellFormat() { FontId = 1, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 1 - Bold 
                    new CellFormat() { FontId = 2, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 2 - Italic
                    new CellFormat() { FontId = 3, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 3 - Times Roman
                    new CellFormat() { FontId = 0, FillId = 2, BorderId = 0, ApplyFill = true },       // Index 4 - Yellow Fill
                    new CellFormat(                                                                   // Index 5 - Alignment
                        new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }
                    )
                    { FontId = 0, FillId = 0, BorderId = 0, ApplyAlignment = true },
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }      // Index 6 - Border
                )
            );
        }

        #endregion
    }
}
