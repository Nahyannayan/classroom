﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolReportService : ISchoolReportService
    {
        #region private variables
        readonly static HttpClient _client;
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/SchoolReport";
        #endregion
        static SchoolReportService()
        {
            if (_client == null)
                _client = new HttpClient();
        }
        public SchoolReportService()
        {            
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        public int SchoolReportCU(SchoolReports schoolReports)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.SchoolReports.SchoolReportCU(_path), schoolReports).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.ToInt32() : 0;
        }
        public IEnumerable<ReportListModel> GetReportLists(long schoolId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.SchoolReports.GetReportLists(_path, schoolId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<ReportListModel>>() : new List<ReportListModel>();
        }
        public ReportDetailModel GetReportDetail(long reportDetailId, bool isCommonReport, long schoolId, bool isPreview = false)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.SchoolReports.GetReportDetail(_path, reportDetailId,  isCommonReport,  schoolId, isPreview)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<ReportDetailModel>() : new ReportDetailModel();
        }
        public IEnumerable<ReportFilterModel> GetReportFilter(string filterCode, string filterParameter)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.SchoolReports.GetReportFilter(_path), new ReportFilterModel { Name = filterCode, value = filterParameter }).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<ReportFilterModel>>() : new List<ReportFilterModel>();
        }
        public ReportDataModel GetReportData(ReportParameterRequestModel requestModel)
        {
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(API.SchoolReports.GetReportData(_path), requestModel).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<ReportDataModel>() : new ReportDataModel();
        }

        public bool DeleteSchoolReport(int id, long schoolId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.SchoolReports.DeleteSchoolReport(_path,id, schoolId)).Result;
            return responseMessage.IsSuccessStatusCode;
        }
    }
}