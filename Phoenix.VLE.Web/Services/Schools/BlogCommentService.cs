﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Phoenix.VLE.Web.Services
{
    public class BlogCommentService: IBlogCommentService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/blogcomment";
        #endregion
        public BlogCommentService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool Delete(int id)
        {
            var uri = API.BlogComment.DeleteBlogComment(_path, id, SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }
        
        public IEnumerable<BlogComment> GetAll()
        {
            var result = new List<BlogComment>();
            var uri = API.BlogComment.GetAllBlogComments(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogComment>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<BlogComment> GetBlogCommentByBlogId(int blogId, bool? isPublish)
        {
            var result = new List<BlogComment>();
            var uri = API.BlogComment.GetBlogCommentsByBlogId(_path, blogId, isPublish);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogComment>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<BlogComment> GetBlogCommentByPublishBy(int publishId, bool? isPublish)
        {
            var result = new List<BlogComment>();
            var uri = API.BlogComment.GetBlogCommentsByPublishBy(_path, publishId, isPublish);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogComment>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public BlogComment Get(int id)
        {
            var result = new BlogComment();
            var uri = API.BlogComment.GetBlogCommentById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, BlogComment>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int Insert(BlogComment entity)
        {

            int result = 0;
            var uri = string.Empty;
            //var sourceModel = new Blog();
            //EntityMapper<SchoolSpaceEdit, SchoolSpace>.Map(model, sourceModel);
            uri = API.BlogComment.AddBlogComment(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = string.IsNullOrWhiteSpace(jsonDataProviders) ? 0 : Convert.ToInt32(jsonDataProviders);
            }
            return result;
           
        }

        public bool Update(BlogComment entityToUpdate)
        {
            bool result = false;
            var uri = string.Empty;
            //var sourceModel = new Blog();
            //EntityMapper<SchoolSpaceEdit, SchoolSpace>.Map(model, sourceModel);
            uri = API.BlogComment.UpdateBlogComment(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            return response.IsSuccessStatusCode;
        }

        public bool InsertCommentLike(BlogCommentLike blogCommentLike)
        {
            var uri = string.Empty;
            uri = API.BlogComment.AddBlogCommentLike(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, blogCommentLike).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdatedCommentLike(BlogCommentLike blogCommentLike)
        {
            var uri = string.Empty;
            
            uri = API.BlogComment.UpdateBlogCommentLike(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, blogCommentLike).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteCommentLike(int id)
        {
            var uri = API.BlogComment.DeleteBlogCommentLike(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<BlogCommentLike> GetBlogCommentLikes(int? commentId,  int? likedBy)
        {
            var result = new List<BlogCommentLike>();
            var uri = API.BlogComment.GetBlogCommentLikes(_path, commentId, null, likedBy, null);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<BlogCommentLike>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
    }
}