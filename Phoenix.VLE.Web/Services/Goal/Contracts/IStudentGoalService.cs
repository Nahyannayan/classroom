﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IStudentGoalService
    {
        IEnumerable<StudentGoal> GetAllStudentGoals(int pageIndex, int v, long userId, long currentUserId, string searchString = "");
        bool UpdateDashboardGoalsStatus(StudentGoal goalModel);
        bool UpdateAllDashboardGoalsStatus(List<StudentGoal> lastGoalModel);
        bool InsertStudentGoal(StudentGoalEdit editModel);
        bool ApproveStudentGoal(StudentGoal model);
        bool DeleteStudentGoal(long goalId);
        StudentGoal GetStudentGoalById(long value);
    }
}
