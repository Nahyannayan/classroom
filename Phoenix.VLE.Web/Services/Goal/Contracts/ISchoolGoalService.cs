﻿using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISchoolGoalService
    {
        IEnumerable<SchoolGoal> GetSchoolGoals();
        SchoolGoal GetSchoolGoalById(int id);
        int UpdateSchoolGoal(SchoolGoalEdit model);
        int DeleteSchoolGoal(int id);
    }
}
