﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class StudentGoalService : IStudentGoalService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/StudentGoals";
        #endregion

        public StudentGoalService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool ApproveStudentGoal(StudentGoal model)
        {
            var uri = API.StudentInformation.ApproveStudentGoal(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<StudentGoal> GetAllStudentGoals(int pageIndex, int pageSize, long userId, long currentUserId,string searchString = "")
        {
            var uri = API.StudentInformation.GetAllStudentGoals(_path, userId, pageSize, pageIndex, searchString, currentUserId);
            IEnumerable<StudentGoal> studentGoals = new List<StudentGoal>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentGoals = EntityMapper<string, IEnumerable<StudentGoal>>.MapFromJson(jsonDataProviders);
            }
            return studentGoals;
        }

        public bool InsertStudentGoal(StudentGoalEdit editModel)
        {
            StudentGoal model = new StudentGoal();
            EntityMapper<StudentGoalEdit, StudentGoal>.Map(editModel, model);
            var url = API.StudentInformation.InsertStudentGoals(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(url, model).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public bool UpdateDashboardGoalsStatus(StudentGoal goalModel)
        {
            var uri = API.StudentInformation.UpdateDashboardGoalsStatus(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, goalModel).Result;
            return response.IsSuccessStatusCode;
        }
        public bool UpdateAllDashboardGoalsStatus(List<StudentGoal> lstGoalModel)
        {
            var uri = API.StudentInformation.UpdateAllDashboardGoalsStatus(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, lstGoalModel).Result;
            return response.IsSuccessStatusCode;
        }

        public bool DeleteStudentGoal(long goalId)
        {
            StudentGoal model = new StudentGoal();
            model.GoalId = goalId;
            model.UserId = SessionHelper.CurrentSession.Id;
            var uri = API.SchoolGoal.DeleteStudentGoal(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public StudentGoal GetStudentGoalById(long goalId)
        {
            var studentGoal = new StudentGoal();
            var uri = API.SchoolGoal.GetStudentGoalById(_path,goalId);
            HttpResponseMessage responseMessage = _client.GetAsync(uri).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = responseMessage.Content.ReadAsStringAsync().Result;
                studentGoal = EntityMapper<string, StudentGoal>.MapFromJson(jsonDataProviders);
            }
            return studentGoal;
        }
    }
}