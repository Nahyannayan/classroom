﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolGoalService : ISchoolGoalService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/SchoolGoal";
        #endregion

        public SchoolGoalService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods

        public IEnumerable<SchoolGoal> GetSchoolGoals()
        {
            int languageId = LocalizationHelper.CurrentSystemLanguageId;
            var uri = "";//API.SchoolGoal.GetSchoolGoal(_path, languageId);
            IEnumerable<SchoolGoal> categories = new List<SchoolGoal>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                categories = EntityMapper<string, IEnumerable<SchoolGoal>>.MapFromJson(jsonDataProviders);
            }
            return categories;
        }

        public SchoolGoal GetSchoolGoalById(int id)
        {
            var category = new SchoolGoal();
            var uri = API.SchoolGoal.GetSchoolGoalById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                category = EntityMapper<string, SchoolGoal>.MapFromJson(jsonDataProviders);
            }
            return category;
        }

        public int UpdateSchoolGoal(SchoolGoalEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new SchoolGoal();
            EntityMapper<SchoolGoalEdit, SchoolGoal>.Map(model, sourceModel);
            sourceModel.CreatedBy = (int)SessionHelper.CurrentSession.Id;

            if (model.IsAddMode)
                uri = API.SchoolGoal.InsertSchoolGoal(_path);
            else
                uri = API.SchoolGoal.UpdateSchoolGoal(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int DeleteSchoolGoal(int id)
        {
            var uri = API.SchoolGoal.DeleteSchoolGoal(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        #endregion
    }
}