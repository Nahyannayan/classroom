﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using Phoenix.Common.ViewModels;
using CommonHelper = Phoenix.VLE.Web.Helpers.CommonHelper;
using Phoenix.Common.Models.ViewModels;

namespace Phoenix.VLE.Web.Services
{
    public class CalendarService : ICalendarService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        //readonly string _path = "api/v1/TimeTable";

        readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        readonly string timeTableAPI = ConfigurationManager.AppSettings["TimeTableAPI"];
        readonly string circularAPI = ConfigurationManager.AppSettings["CircularAPI"];
        readonly string resourcesCategoryAPI = ConfigurationManager.AppSettings["ResourcesCategoryAPI"];
        readonly string resourcesAPI = ConfigurationManager.AppSettings["ResourcesAPI"];
        readonly string eventsAPI = ConfigurationManager.AppSettings["EventsAPI"];
        readonly string galleryAPI = ConfigurationManager.AppSettings["GalleryAPI"];
        readonly string circularListAPI = ConfigurationManager.AppSettings["CircularListAPI"];
        readonly string PostCircularDetail = ConfigurationManager.AppSettings["circularIsReadtAPI"];
        readonly string circularDetailsAPI = ConfigurationManager.AppSettings["CircularDetailsAPI"];
        readonly string studentEventsAPI = ConfigurationManager.AppSettings["StudentEventsAPI"];
        readonly string GetEventCategoryAPI = ConfigurationManager.AppSettings["GetEventCategoriesAPI"];

        readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];

        private ILoggerClient _loggerClient;

        public CalendarService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                   _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                _loggerClient = LoggerClient.Instance;
            }
        }



        #endregion
        public CalendarEvent GetCalendarEvent(long id, string date)
        {
            CalendarEvent objCalendarEvent = new CalendarEvent();
            //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });
            //if (token != null)
            //{
            //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
            //    {
            //        string AuthorizedToken = token.token_type + " " + token.access_token;
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, timeTableAPI);
            httpRequestMessage.Headers.Add("stuid", id.ToString());
            httpRequestMessage.Headers.Add("asondate", date);
            //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

            using (var response = _client.SendAsync(httpRequestMessage).Result)
            {
                if (response.IsSuccessStatusCode)
                {
                    JavaScriptSerializer se = new JavaScriptSerializer();
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;

                    objCalendarEvent = new JavaScriptSerializer().Deserialize<CalendarEvent>(jsonDataProviders);
                }
            }
            // }
            //}
            return objCalendarEvent;
        }

        /// <summary>
        /// author : vishal dhabade
        /// date : 29 july 2019
        /// desc : api to fetch the ciculars and newsletter for given parent
        /// </summary>
        /// <param name="parent_Id"></param>
        /// <param name="SearchTextValue"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="Language"></param>
        /// <returns></returns>
        public List<Circular> GetCircularsAndNewsLetters(string parent_Id, string teacher_Id, string SearchTextValue, string FromDate, string ToDate, string Language, string isStaff)
        {
            Circulars circulars = new Circulars();
            //_loggerClient = LoggerClient.Instance;

            //_loggerClient.LogException("token request sent----");
            //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });
            ////_loggerClient.LogException("token generated----");

            //if (token != null)
            //{
            //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
            //    {
            //        string AuthorizedToken = token.token_type + " " + token.access_token;
            //        //_loggerClient.LogException("token ----" + AuthorizedToken);


            using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, circularAPI))
            {
                //HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, circularAPI);
                httpRequestMessage.Headers.Add("Parent_ID", parent_Id);
                httpRequestMessage.Headers.Add("Teacher_ID", teacher_Id);
                httpRequestMessage.Headers.Add("IsStaff", isStaff);
                httpRequestMessage.Headers.Add("SearchTextValue", SearchTextValue);
                httpRequestMessage.Headers.Add("FromDate", FromDate);
                httpRequestMessage.Headers.Add("ToDate", ToDate);
                httpRequestMessage.Headers.Add("Language", Language);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
                _loggerClient.LogException("token addition----");
                //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);

                using (var response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        //JavaScriptSerializer se = new JavaScriptSerializer();
                        var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                        if (!string.IsNullOrEmpty(jsonDataProviders))
                            circulars = LoadFromXMLString(jsonDataProviders);
                    }
                }

            }
            // }
            //}
            //_loggerClient.LogException("API request ended----");
            //_loggerClient.LogException("--------------------------------------------");
            return circulars.CircularList;
        }

        public List<ResourceCategory> GetResourceCategories(string parent_Id, string Language, string isStaff, string staffID)
        {
            ResourceCategoryRoot category = new ResourceCategoryRoot();
            List<ResourceCategory> resCategories = new List<ResourceCategory>();
            //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });

            //if (token != null)
            //{
            //    string AuthorizedToken = token.token_type + " " + token.access_token;

            //    using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, resourcesCategoryAPI))
            //    {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, resourcesCategoryAPI);
            httpRequestMessage.Headers.Add("parentID", parent_Id);
            httpRequestMessage.Headers.Add("isStaff", isStaff);
            httpRequestMessage.Headers.Add("staffID", staffID);
            httpRequestMessage.Headers.Add("source", "SCHOOL");
            httpRequestMessage.Headers.Add("Language", Language);
            //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
            //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
            using (var response = _client.SendAsync(httpRequestMessage).Result)
            {
                if (response.IsSuccessStatusCode)
                {
                    JavaScriptSerializer se = new JavaScriptSerializer();
                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;

                    category = new JavaScriptSerializer().Deserialize<ResourceCategoryRoot>(jsonDataProviders);
                    resCategories = category.data;
                }
            }

            //    }
            //}

            return resCategories;
        }

        public List<Resource> GetResources(string parent_Id, string schoolId, string categoryId, string filterText, string language, string isStaff, string staffID)
        {
            List<Resource> resources = new List<Resource>();

            //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });

            //if (token != null)
            //{
            //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
            //    {
            //        string AuthorizedToken = token.token_type + " " + token.access_token;

            using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, resourcesAPI))
            {
                var payload = "{\"ParentID\":\"" + parent_Id + "\",\"Language\":\"" + language
                    + "\",\"Source\":\"SCHOOL\",\"SchoolID\":\"" + schoolId + "\",\"StaffID\":\"" + staffID
                    + "\",\"CategoryID\":\"" + categoryId + "\",\"IsStaff\":\"" + isStaff
                    + "\",\"FilterText\":\"" + filterText + "\"}";

                HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");

                httpRequestMessage.Content = content;
                //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                using (var response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                        dynamic json = Json.Decode(jsonDataProviders);
                        if (json.data != null && json.data[0].categories[0] != null && json.data[0].categories[0].resources != null)
                        {
                            foreach (dynamic item in json.data[0].categories[0].resources)
                            {
                                if (item != null)
                                {
                                    resources.Add(new Resource()
                                    {
                                        resourceAttachmentUrl = item.resourceAttachmentUrl,
                                        resourceDateTime = Convert.ToDateTime(item.resourceDateTime),
                                        resourceDescription = item.resourceDescription,
                                        resourceID = item.resourceID,
                                        resourceThumbNailUrl = item.resourceThumbNailUrl,
                                        resourceTitle = item.resourceTitle
                                    });
                                }
                            }
                        }

                    }
                }
            }
            // }
            // }

            return resources;
        }
        public bool updateCircularDetails(string payload)
        {
            string json = "";

            //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });

            //if (token != null)
            //{
            //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
            //    {
            //        string AuthorizedToken = token.token_type + " " + token.access_token;
            using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, PostCircularDetail))
            {
                //var payload = "{\"CircularID\":\"" + CircularId + "\",\"Key\":\"" + key
                //    + "\",\"Identifier\":\"" + Identifier + "\",\"ISRead\":\"" + isRead + "\",\"IsFavorite\":\"" + IsFavorite + "\"}";

                HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
                httpRequestMessage.Content = content;
                //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                using (var response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                    }
                }

            }

            //    }
            //}
            return true;
        }

        public string GetEvents(string parent_Id, string studentId, string IsDateRange, string FromDate, string ToDate, string language, string isStaff, string staffID)
        {
            string json = "";

            //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });

            //if (token != null)
            //{
            //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
            //    {
            //        string AuthorizedToken = token.token_type + " " + token.access_token;

            //using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, eventsAPI))
            //{
            //    var payload = "{\"ParentID\":\"" + parent_Id + "\",\"Language\":\"" + language
            //        + "\",\"Source\":\"SCHOOL\",\"StudentID\":\"" + studentId + "\",\"StaffID\":\"" + staffID
            //        + "\",\"IsDateRange\":\"" + IsDateRange + "\",\"IsStaff\":\"" + isStaff
            //       + "\",\"ToDate\":\"" + ToDate + "\",\"FromDate\":\"" + FromDate + "\"}";

            Phoenix.VLE.Web.Models.SchoolEventParameter schoolEventParameter = new Models.SchoolEventParameter()
            {
                FromDate = FromDate,
                ToDate = ToDate,
                Language = language,
                IsDateRange = IsDateRange,
                Source= "SCHOOL",
                IsStaff = isStaff,
                ParentID = parent_Id,
                StaffID = staffID,
                StudentID = studentId,
                
            };

            //HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");

            //httpRequestMessage.Content = content;
            //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);

            // httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
            var response = _client.PostAsJsonAsyncExt(eventsAPI, schoolEventParameter, SessionHelper.CurrentSession.PhoenixAccessToken).Result;
                //{
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                    }
                //}
                //}

                return json;
            }

            public string GetStudentEvents(string studentId, string FromDate, string ToDate)
            {
                string json = "";

                //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });

                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;

                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, studentEventsAPI))
                {
                    httpRequestMessage.Headers.Add("stuId", studentId);
                    httpRequestMessage.Headers.Add("fromDate", FromDate);
                    httpRequestMessage.Headers.Add("toDate", ToDate);
                    //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                    using (var response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            json = response.Content.ReadAsStringAsync().Result;
                        }
                    }

                }

                //    }
                //}

                return json;
            }

            public List<GalleryFile> GetGallery(string parent_Id, string FromDate, string ToDate, string language, string isStaff, string staffID, string fileParentID, string filterText)
            {
                List<GalleryFile> files = new List<GalleryFile>();

                //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });

                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;

                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, galleryAPI))
                {
                    var payload = "{\"ParentID\":\"" + parent_Id + "\",\"Language\":\"" + language
                        + "\",\"Source\":\"SCHOOL\",\"StaffID\":\"" + staffID + "\",\"FileParentID\":\"" + fileParentID
                        + "\",\"IsStaff\":\"" + isStaff + "\",\"FilterText\":\"" + filterText
                        + "\",\"ToDate\":\"" + ToDate + "\",\"FromDate\":\"" + FromDate + "\"}";

                    HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");

                    httpRequestMessage.Content = content;
                    //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                    using (var response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                            dynamic json = Json.Decode(jsonDataProviders);
                            if (json.data != null)
                            {
                                foreach (dynamic item in json.data)
                                {
                                    if (item != null)
                                    {
                                        files.Add(new GalleryFile()
                                        {
                                            FileID = item.fileID,
                                            FileName = item.fileName,
                                            CreatedDate = Convert.ToDateTime(item.fileCreatedDT),
                                            FileExtention = item.fileExtention,
                                            FilePath = item.filePath,
                                            IsDirectory = item.isDirectory,
                                            SchoolId = item.schoolID
                                        });
                                    }
                                }
                            }

                        }

                    }
                }
                //    }
                //}
                return files;
            }


            public List<Circular> GetCircularList(string Identifier, string Key, string SearchTextValue, string FromDate, string ToDate, string Language)
            {
                List<Circular> circularList = new List<Circular>();
                Circulars circulars = new Circulars();
                RootObject obj = new RootObject();
                //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });

                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;

                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, circularListAPI))
                {
                    var payload = "{\"Key\":\"" + Key + "\",\"Identifier\": \"" + Identifier + "\",\"Language\": \"en\",\"FromDate\":\"" + FromDate + "\",\"ToDate\":\"" + ToDate + "\",\"FilterText\": \"\"}";

                    HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");

                    httpRequestMessage.Content = content;
                    //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                    using (var response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                            if (!string.IsNullOrEmpty(jsonDataProviders))
                            {
                                obj = new JavaScriptSerializer().Deserialize<RootObject>(jsonDataProviders);
                                if (obj != null && obj.data != null)
                                {
                                    foreach (var a in obj.data)
                                    {
                                        circularList.Add(new Circular()
                                        {
                                            CircularId = a.circularId,
                                            Date = a.date,
                                            Title = a.title,
                                            isRead = a.IsRead,
                                            ThmubnailUrl = a.thmubnailUrl,
                                            AttachmentCount = a.attachmentCount,
                                            Type = a.type
                                        });
                                    }
                                    circulars.CircularList = circularList;
                                }
                            }
                        }

                    }
                }
                //    }
                //}

                return circulars.CircularList;
            }

            public Circular GetCircularDetails(string circularId, string language)
            {
                RootObject obj = new RootObject();
                Circular circulars = new Circular();

                //var token = GetAuthorizationTokenAsync(new APIAuthorization() { });

                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;

                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, circularDetailsAPI))
                {
                    httpRequestMessage.Headers.Add("circularID", circularId);
                    httpRequestMessage.Headers.Add("lang", language);
                    //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);

                    using (var response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                        if (!string.IsNullOrEmpty(jsonDataProviders))
                        {
                            dynamic json = Json.Decode(jsonDataProviders);
                            if (json.data != null)
                            {
                                circulars.Title = json.data.title;
                                circulars.CircularId = json.data.circularId;
                                circulars.Body = json.data.body;
                                circulars.BannerURL = json.data.bannerURL;
                                circulars.ThmubnailUrl = json.data.thmubnailUrl;
                                circulars.Date = Convert.ToDateTime(json.data.CircularDate);
                                List<Attachment> attachments = new List<Attachment>();
                                foreach (dynamic a in json.data.attachments)
                                {
                                    attachments.Add(new Attachment()
                                    {
                                        fileName = a.fileName,
                                        fileURL = a.fileURL,
                                    });
                                    circulars.attachments = attachments;
                                }
                            }
                        }
                    }

                }
                //    }
                //}

                return circulars;
            }

            //public TokenResult GetAuthorizationTokenAsync(APIAuthorization calendarAuthorization)
            //{
            //    //_loggerClient.LogWarning("GetAuthorizationToken");
            //    TokenResult JsonDeserilize = new TokenResult();
            //    if (HttpContext.Current.Session["AccessToken"] != null)
            //    {
            //        JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //    }
            //    else
            //    {
            //        //added httpclient in scope for Authorization Token By Rohan P-27/08/2020
            //        HttpClient _phoneixapiclient = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
            //        _phoneixapiclient.DefaultRequestHeaders.Accept.Clear();
            //        var uri = tokenAPI;
            //        string _ContentType = "application/x-www-form-urlencoded";
            //        _phoneixapiclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //        var nvc = new List<KeyValuePair<string, string>>();
            //        nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //        nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //        nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //        nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //        using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //        {
            //            using (var result = _phoneixapiclient.SendAsync(test).Result)
            //            {
            //                if (result.IsSuccessStatusCode)
            //                {
            //                    var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                    try
            //                    {
            //                        JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                    }
            //                    finally
            //                    {
            //                        result.Dispose();
            //                    }
            //                }
            //            }
            //        }

            //        HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //    }
            //    return JsonDeserilize;
            //}

            //public TokenResult GetAuthorizationTokenAsync(APIAuthorization calendarAuthorization)
            //{
            //    //_loggerClient.LogWarning("GetAuthorizationToken");
            //    TokenResult JsonDeserilize = new TokenResult();
            //    if (HttpContext.Current.Session["AccessToken"] != null)
            //    {
            //        JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //    }
            //    else
            //    {
            //        //_client.DefaultRequestHeaders.Accept.Clear();
            //        var uri = tokenAPI;
            //        //string _ContentType = "application/x-www-form-urlencoded";
            //        //_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //        var nvc = new List<KeyValuePair<string, string>>();
            //        nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //        nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //        nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //        nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //        using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //        {
            //            using (var result = _client.SendAsync(test).Result)
            //            {
            //                if (result.IsSuccessStatusCode)
            //                {
            //                    var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                    try
            //                    {
            //                        JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                    }
            //                    finally
            //                    {
            //                        result.Dispose();
            //                    }
            //                }
            //            }
            //        }

            //        HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //    }
            //    return JsonDeserilize;
            //}

            public string GetQueryString(object obj)
            {
                var properties = from p in obj.GetType().GetProperties()
                                 where p.GetValue(obj, null) != null
                                 select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

                return String.Join("&", properties.ToArray());
            }

            string ICalendarService.GetAuthorizationToken(APIAuthorization ca)
        {
                throw new NotImplementedException();
            }

            public string ToXML()
            {
                using (var stringwriter = new System.IO.StringWriter())
                {
                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(stringwriter, this);
                    return stringwriter.ToString();
                }
            }

            public static Circulars LoadFromXMLString(string xmlText)
            {
                using (var stringReader = new System.IO.StringReader(xmlText))
                {
                    var serializer = new XmlSerializer(typeof(Circulars));
                    return serializer.Deserialize(stringReader) as Circulars;
                }
            }

            public IEnumerable<EventCategories> GetAllEventCategories()
            {
                string json = "";
                EventCategoriesRoot categories = new EventCategoriesRoot();
                List<EventCategories> leaveReqList = new List<EventCategories>();
                //var token = GetAuthorizationTokenAsync();

                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {


                HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetEventCategoryAPI);

                // httpRequestMessage.Headers.Add("Authorization", SessionHelper.CurrentSession.PhoenixAccessToken);

                using (var response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        categories = new JavaScriptSerializer().Deserialize<EventCategoriesRoot>(json);

                        if (categories.data != null && categories.data.Count > 0)
                            leaveReqList = categories.data;
                    }
                }


                //    }
                //}
                return leaveReqList;
            }
        }

        public class Datum
        {
            public int circularId { get; set; }
            public string title { get; set; }
            public string body { get; set; }
            public DateTime date { get; set; }
            public string bannerURL { get; set; }
            public string thmubnailUrl { get; set; }
            public bool IsRead { get; set; }
            public List<Attachment> attachments { get; set; }
            public short attachmentCount { get; set; }
            public string type { get; set; }
        }

        public class Attachment
        {
            public string fileURL { get; set; }
            public string fileName { get; set; }
        }

        public class RootObject
        {
            public string cmd { get; set; }
            public string success { get; set; }
            public object responseCode { get; set; }
            public object message { get; set; }
            public List<Datum> data { get; set; }
        }

        [XmlRoot(ElementName = "EMAIL")]
        public class Circular
        {
            [XmlElement(ElementName = "LOG_UNIQUE_ID")]
            public string LOG_UNIQUE_ID { get; set; }
            [XmlElement(ElementName = "LOG_ENTRY_DATE")]
            public string LOG_ENTRY_DATE { get; set; }
            [XmlElement(ElementName = "LOG_EML_ID")]
            public string LOG_EML_ID { get; set; }
            [XmlElement(ElementName = "EML_TITLE")]
            public string EML_TITLE { get; set; }
            [XmlElement(ElementName = "EML_BODY")]
            public string EML_BODY { get; set; }
            [XmlElement(ElementName = "bShow")]
            public string BShow { get; set; }
            [XmlElement(ElementName = "tempview")]
            public string Tempview { get; set; }
            [XmlElement(ElementName = "NEW")]
            public string NEW { get; set; }
            [XmlElement(ElementName = "LOG_EMAIL_ID")]
            public string LOG_EMAIL_ID { get; set; }
            [XmlElement(ElementName = "ENTRY_DATE")]
            public string ENTRY_DATE { get; set; }
            [XmlElement(ElementName = "EmailType")]
            public string EmailType { get; set; }
            [XmlElement(ElementName = "status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "EML_BANNER")]
            public string EML_BANNER { get; set; }
            [XmlElement(ElementName = "EML_THUMBNAIL")]
            public string EML_THUMBNAIL { get; set; }
            [XmlElement(ElementName = "IsFavorite")]
            public string IsFavorite { get; set; }
            [XmlElement(ElementName = "LOG_ISREAD")]
            public string LOG_ISREAD { get; set; }
            [XmlElement(ElementName = "BSU_SHORTNAME")]
            public string BSU_SHORTNAME { get; set; }
            [XmlElement(ElementName = "ATTACHMENTS")]
            public ATTACHMENTS ATTACHMENTS { get; set; }
            public DateTime Date { get; set; }
            public int CircularId { get; set; }
            public short AttachmentCount { get; set; }
            public string Type { get; set; }
            public string Title { get; set; }
            public string Body { get; set; }
            public string BannerURL { get; set; }
            public bool isRead { get; set; }
            public string ThmubnailUrl { get; set; }
            public List<Attachment> attachments { get; set; }
        }

        [XmlRoot(ElementName = "EMAILS")]
        public class Circulars
        {
            public string cmd { get; set; }
            public string success { get; set; }
            public object responseCode { get; set; }
            public object message { get; set; }

            [XmlElement(ElementName = "EMAIL")]
            public List<Circular> CircularList { get; set; }
        }

        [XmlRoot(ElementName = "ATTACHMENTS")]
        public class ATTACHMENTS
        {
            [XmlElement(ElementName = "ATTACHMENT")]
            public List<string> ATTACHMENT { get; set; }
        }
    }