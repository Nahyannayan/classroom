﻿using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ICalendarService
    {
        CalendarEvent GetCalendarEvent(long id, string date);
        string GetAuthorizationToken(APIAuthorization ca);
        List<Circular> GetCircularsAndNewsLetters(string parent_Id, string teacher_Id, string SearchTextValue, string FromDate, string ToDate, string Language, string isStaff);
        List<ResourceCategory> GetResourceCategories(string parent_Id, string Language, string isStaff, string staffID);
        List<Resource> GetResources(string parent_Id, string schoolId, string categoryId, string filterText, string language, string isStaff, string staffID);
        bool updateCircularDetails(string payload);
        string GetEvents(string parent_Id, string studentId, string IsDateRange, string FromDate, string ToDate, string language, string isStaff, string staffID);
        string GetStudentEvents(string studentId, string FromDate, string ToDate);
        List<GalleryFile> GetGallery(string parent_Id, string FromDate, string ToDate, string language, string isStaff, string staffID, string fileParentID, string filterText);
        List<Circular> GetCircularList(string Identifier, string Key, string SearchTextValue, string FromDate, string ToDate, string Language);
        Circular GetCircularDetails(string circularId, string language);
        ////TokenResult GetAuthorizationTokenAsync(APIAuthorization calendarAuthorization);
        string GetQueryString(object obj);
        IEnumerable<EventCategories> GetAllEventCategories();

    }
}
