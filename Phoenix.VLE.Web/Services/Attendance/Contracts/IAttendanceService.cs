﻿using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Phoenix.VLE.Web.Services
{
   public interface IAttendanceService
    {
        IEnumerable<Attendance> GetAttendanceByIdAndDate(long acd_id, int ttm_id, string username, string entrydate, string grade = null, string section = null, string AttendanceType = "");
        bool InsertAttendanceDetails(string student_xml, string entry_date, string username, int alg_id, int ttm_id = 0, string sct_id = "", string GRD_ID = "", long ACD_ID = 0, long BSU_ID = 0, long SHF_ID = 0, long STM_ID = 0, string AttendanceType="");

        IEnumerable<ATTENDENCE_ANALYSIS> Get_ATTENDENCE_ANALYSIS(string STU_ID);

        IEnumerable<AttendenceBySession> Get_AttendenceBySession(string STU_ID,DateTime EndDate);
        IEnumerable<AttendenceSessionCode> Get_AttendenceSessionCode(string STU_ID, DateTime EndDate);

        IEnumerable<AttendanceChart> Get_AttendanceChartMain(string STU_ID);
         
       
        IEnumerable<RoomAttendance> GetRoomAttendanceDetails( long Coursegroupid, string entryDate);

        IEnumerable<RoomAttendanceHeader> GetRoomAttendanceHeader(long SGR_ID, DateTime ENTRY_DATE);

        bool InsertUpdateRoomAttendance(string SchoolId, string UserId, long acd_id, int isDailyWeekly, long schoolGroupId, long teacherId, List<StudentRoomAttendance> objStudentRoomAttendance,DateTime entryDate);

        IEnumerable<StudentRoomAttendance> GetRoomAttendanceRemarksList(string entryDate, string schoolId, string UserId, int GroupId);

        IEnumerable<GradeSectionAccess> GetGradeSectionAccesses(long schoolId, long academicYear, string userName, string IsSuperUser, int gradeAccess, string gradeId = "");

        IEnumerable<AttendanceSessionType> GetAttendanceTypeByEntryDate(int acdId, string schoolId, DateTime AttendanceDt, string GrdId);

        IEnumerable<AuthorizedStaffDetails> GetGradeSectionByUserId(long Id, long currentAcademicYearId);

        IEnumerable<AttendanceStudent> GetDailyAttendanceDetails(long gradeId, long sectionId, long sessionTypeId, DateTime asOnDate, long schoolId);
        bool SaveDailyAttendance(List<AttendanceStudent> objAttendanceStudent, long userId, int IsMobile);
        IEnumerable<ClassAttendance> GetClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate);

        IEnumerable<ClassAttendanceDetails> GetClassAttendanceDetails(long schoolGroupId, DateTime entryDate);
        IEnumerable<AttendanceWeekend> GetWeekEndBySchoolId(long schoolId=0, long academicyearId=0, long schoolgradeId=0, long courseId = 0, DateTime? date = null);
        IEnumerable<GradeSessionType> GetGradeSessionByGradeId(long schoolGradeId);
        IEnumerable<StaffCurriculumn> GetCurriculumByAuthorizedStaffId(long staffId);
        IEnumerable<TimeTableGroup> GetGroupsfromTimeTable(long teacherId, DateTime courseDate);
        IEnumerable<ClassAttendance> GetPreviousClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate);
        IEnumerable<ClassAttendanceDetails> GetPreviousClassAttendanceDetails(long schoolGroupId, DateTime entryDate);
        IEnumerable<ParameterMappingList> GetParameterMappingByAcademicId(long academicYearId);
        MergeServiceModelForGradeSection GetRequiredParamBySchoolUserId(long schoolId, long id, long academicYearId);
        ClassAttendanceHeaderNDetails GetClassAttendanceHeaderNDetails(long schoolId, long schoolGroupId, DateTime entryDate);
    }
}
