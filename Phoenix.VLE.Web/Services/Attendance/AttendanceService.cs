﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System.Text;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public class AttendanceService : IAttendanceService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Attendance";
        #endregion


        /// <summary>
        /// Author : Fraz Ahmed
        /// Created Date : 11-Jun-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public AttendanceService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }


        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 May 2019
        /// Description : This method is use fetch Room Attendance  List for  particular grade on a particular date through Api
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        #region Methods
        public IEnumerable<Attendance> GetAttendanceByIdAndDate(long acd_id,int ttm_id, string username, string entrydate, string grade = null, string section = null, string AttendanceType = "")
        {
            var uri = API.Attendance.GetAttendanceByIdAndDate(_path, acd_id, ttm_id, username, entrydate, grade, section, AttendanceType);
            IEnumerable<Attendance> attendances = new List<Attendance>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<Attendance>>.MapFromJson(jsonDataProviders);
            }
            return attendances;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 6 May 2019
        /// Description : This method is insert or update the attendance details of a grade by grd id or tt id
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool InsertAttendanceDetails(string student_xml, string entry_date, string username, int alg_id, int ttm_id = 0, string sct_id = "", string GRD_ID = "", long ACD_ID = 0, long BSU_ID = 0, long SHF_ID = 0, long STM_ID = 0, string AttendanceType = "")
        {
            
            var uri = API.Attendance.InsertAttendanceDetails(_path, entry_date, username, alg_id, ttm_id , sct_id, GRD_ID, ACD_ID, BSU_ID, SHF_ID, STM_ID, AttendanceType);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, student_xml).Result;
            return response.IsSuccessStatusCode;
        }


        /// <summary>
        /// Author :Hrushikesh
        /// Created At: 8-Aug-2019
        /// Description : This method is use fetch Attendence analysis by student id
        /// </summary>
        /// <param name="STU_ID"></param>
        /// <returns></returns>
       
        public IEnumerable<ATTENDENCE_ANALYSIS> Get_ATTENDENCE_ANALYSIS(string STU_ID)
        {
            var uri = API.Attendance.Get_ATTENDENCE_ANALYSIS(_path,STU_ID);
            IEnumerable<ATTENDENCE_ANALYSIS> attendances = new List<ATTENDENCE_ANALYSIS>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<ATTENDENCE_ANALYSIS>>.MapFromJson(jsonDataProviders);
            }
            return attendances;
        }

        public IEnumerable<AttendenceBySession> Get_AttendenceBySession(string STU_ID,DateTime EndDate)
        {
            var uri = API.Attendance.Get_AttendenceBySession(_path, STU_ID, EndDate);
            IEnumerable<AttendenceBySession> attendances = new List<AttendenceBySession>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<AttendenceBySession>>.MapFromJson(jsonDataProviders);
            }
            return attendances;
        }


        public IEnumerable<AttendenceSessionCode> Get_AttendenceSessionCode(string STU_ID,DateTime EndDate)
        {
            var uri = API.Attendance.Get_AttendenceSessionCode(_path, STU_ID, EndDate);
            IEnumerable<AttendenceSessionCode> attendances = new List<AttendenceSessionCode>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<AttendenceSessionCode>>.MapFromJson(jsonDataProviders);
            }
            return attendances;
        }
        public IEnumerable<AttendanceChart> Get_AttendanceChartMain(string STU_ID)
        {
            var uri = API.Attendance.Get_AttendanceChartMain(_path, STU_ID);
            IEnumerable<AttendanceChart> attendances = new List<AttendanceChart>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<AttendanceChart>>.MapFromJson(jsonDataProviders);
            }
            return attendances;
        }



        
        public IEnumerable<RoomAttendance> GetRoomAttendanceDetails( long Coursegroupid,string entryDate)
        {
            var uri = API.Attendance.GetRoomAttendanceDetails(_path,  Coursegroupid, entryDate);
            IEnumerable<RoomAttendance> attendances = new List<RoomAttendance>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<RoomAttendance>>.MapFromJson(jsonDataProviders);
            }
            return attendances;
        }

        public IEnumerable<RoomAttendanceHeader> GetRoomAttendanceHeader(long SGR_ID, DateTime ENTRY_DATE)
        {
            var uri = API.Attendance.GetRoomAttendanceHeader(_path, SGR_ID, ENTRY_DATE);
            IEnumerable<RoomAttendanceHeader> attendances = new List<RoomAttendanceHeader>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<RoomAttendanceHeader>>.MapFromJson(jsonDataProviders);
            }
            return attendances;
        }

        public bool InsertUpdateRoomAttendance(string SchoolId, string UserId, long acd_id, int isDailyWeekly, long schoolGroupId, long teacherId, List<StudentRoomAttendance> objStudentRoomAttendance,DateTime entryDate)
        {
            var uri = API.Attendance.InsertUpdateRoomAttendance(_path, SchoolId,  UserId, acd_id,  isDailyWeekly,  schoolGroupId,  teacherId, entryDate);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, objStudentRoomAttendance).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<StudentRoomAttendance> GetRoomAttendanceRemarksList(string entryDate, string schoolId, string UserId, int GroupId)
        {
            var uri = API.Attendance.GetRoomAttendanceRemarksList(_path,  entryDate,  schoolId,  UserId,  GroupId);
            IEnumerable<StudentRoomAttendance> attendances = new List<StudentRoomAttendance>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendances = EntityMapper<string, IEnumerable<StudentRoomAttendance>>.MapFromJson(jsonDataProviders);
            }

            return attendances;
        }


        #endregion

        public IEnumerable<GradeSectionAccess> GetGradeSectionAccesses(long schoolId, long academicYear, string userName, string IsSuperUser, int gradeAccess, string gradeId = "")
        {
            var uri = API.Attendance.GetGradeSectionAccesses(_path, schoolId, academicYear, userName, IsSuperUser, gradeAccess, gradeId);
            IEnumerable<GradeSectionAccess> gradeSection = new List<GradeSectionAccess>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                gradeSection = EntityMapper<string, IEnumerable<GradeSectionAccess>>.MapFromJson(jsonDataProviders);
            }

            return gradeSection;
        }

        public IEnumerable<AttendanceSessionType> GetAttendanceTypeByEntryDate(int acdId, string schoolId, DateTime AttendanceDt, string GrdId)
        {
            var uri = API.Attendance.GetAttendanceTypeByEntryDate(_path, acdId, schoolId, AttendanceDt, GrdId);
            IEnumerable<AttendanceSessionType> attendancesType = new List<AttendanceSessionType>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendancesType = EntityMapper<string, IEnumerable<AttendanceSessionType>>.MapFromJson(jsonDataProviders);
            }

            return attendancesType;
        }

        public IEnumerable<AuthorizedStaffDetails> GetGradeSectionByUserId(long Id, long currentAcademicYearId)
        {
            var uri = API.Attendance.GetGradeSectionByUserId(_path, Id, currentAcademicYearId);
            IEnumerable<AuthorizedStaffDetails> staffDetails = new List<AuthorizedStaffDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                staffDetails = EntityMapper<string, IEnumerable<AuthorizedStaffDetails>>.MapFromJson(jsonDataProviders);
            }
            return staffDetails;
        }

        public IEnumerable<AttendanceStudent> GetDailyAttendanceDetails(long gradeId, long sectionId, long sessionTypeId, DateTime asOnDate, long schoolId)
        {
            var uri = API.Attendance.GetDailyAttendanceDetails(_path, gradeId,  sectionId,  sessionTypeId,  asOnDate, schoolId);
            IEnumerable<AttendanceStudent> attendanceStudentDetails = new List<AttendanceStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attendanceStudentDetails = EntityMapper<string, IEnumerable<AttendanceStudent>>.MapFromJson(jsonDataProviders);
            }
            return attendanceStudentDetails;
        }

        public bool SaveDailyAttendance(List<AttendanceStudent> objAttendanceStudent, long userId, int IsMobile)
        {
            var uri = API.Attendance.SaveDailyAttendance(_path, userId, IsMobile);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, objAttendanceStudent).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<ClassAttendance> GetClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            var uri = API.Attendance.GetClassAttendanceHeader(_path, schoolId, schoolGroupId, entryDate);
            IEnumerable<ClassAttendance> classAttendanceHeader = new List<ClassAttendance>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classAttendanceHeader = EntityMapper<string, IEnumerable<ClassAttendance>>.MapFromJson(jsonDataProviders);
            }
            return classAttendanceHeader;
        }

        public IEnumerable<ClassAttendanceDetails> GetClassAttendanceDetails(long schoolGroupId, DateTime entryDate)
        {
            var uri = API.Attendance.GetClassAttendanceDetails(_path, schoolGroupId, entryDate);
            IEnumerable<ClassAttendanceDetails> classAttendanceDetails = new List<ClassAttendanceDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classAttendanceDetails = EntityMapper<string, IEnumerable<ClassAttendanceDetails>>.MapFromJson(jsonDataProviders);
            }
            return classAttendanceDetails;
        }

        public IEnumerable<AttendanceWeekend> GetWeekEndBySchoolId(long schoolId=0, long academicyearId=0, long schoolgradeId=0, long courseId = 0, DateTime? date = null)
        {
            var uri = API.Attendance.GetWeekEndBySchoolId(_path, schoolId,  academicyearId,  schoolgradeId, courseId,  date.Value);
            IEnumerable<AttendanceWeekend> objAttendanceWeekend = new List<AttendanceWeekend>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objAttendanceWeekend = EntityMapper<string,IEnumerable<AttendanceWeekend>>.MapFromJson(jsonDataProviders);
            }
            return objAttendanceWeekend;
        }

        public IEnumerable<GradeSessionType> GetGradeSessionByGradeId(long schoolGradeId)
        {
            var uri = API.Attendance.GetGradeSessionByGradeId(_path, schoolGradeId);
            IEnumerable<GradeSessionType> objAttendanceGradeSessionType = new List<GradeSessionType>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objAttendanceGradeSessionType = EntityMapper<string, IEnumerable<GradeSessionType>>.MapFromJson(jsonDataProviders);
            }
            return objAttendanceGradeSessionType;
        }

        public IEnumerable<StaffCurriculumn> GetCurriculumByAuthorizedStaffId(long staffId)
        {
            var uri = API.Attendance.GetCurriculumByAuthorizedStaffId(_path, staffId);
            IEnumerable<StaffCurriculumn> objCurriculumByAuthorizedStaffId = new List<StaffCurriculumn>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objCurriculumByAuthorizedStaffId = EntityMapper<string, IEnumerable<StaffCurriculumn>>.MapFromJson(jsonDataProviders);
            }
            return objCurriculumByAuthorizedStaffId;
        }

        public IEnumerable<TimeTableGroup> GetGroupsfromTimeTable(long teacherId, DateTime courseDate)
        {
            var uri = API.Attendance.GetGroupsfromTimeTable(_path, teacherId, courseDate);
            IEnumerable<TimeTableGroup> objlistofGroup = new List<TimeTableGroup>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objlistofGroup = EntityMapper<string, IEnumerable<TimeTableGroup>>.MapFromJson(jsonDataProviders);
            }
            return objlistofGroup;
        }

        public IEnumerable<ClassAttendance> GetPreviousClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            var uri = API.Attendance.GetPreviousClassAttendanceHeader(_path, schoolId, schoolGroupId, entryDate);
            IEnumerable<ClassAttendance> classprvAttendanceHeader = new List<ClassAttendance>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classprvAttendanceHeader = EntityMapper<string, IEnumerable<ClassAttendance>>.MapFromJson(jsonDataProviders);
            }
            return classprvAttendanceHeader;
        }

        public IEnumerable<ClassAttendanceDetails> GetPreviousClassAttendanceDetails(long schoolGroupId, DateTime entryDate)
        {
            var uri = API.Attendance.GetPreviousClassAttendanceDetails(_path, schoolGroupId, entryDate);
            IEnumerable<ClassAttendanceDetails> classprvAttendanceDetails = new List<ClassAttendanceDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classprvAttendanceDetails = EntityMapper<string, IEnumerable<ClassAttendanceDetails>>.MapFromJson(jsonDataProviders);
            }
            return classprvAttendanceDetails;
        }

        public IEnumerable<ParameterMappingList> GetParameterMappingByAcademicId(long academicYearId)
        {
            var uri = API.Attendance.GetParameterMappingByAcademicId(_path, academicYearId);
            IEnumerable<ParameterMappingList> parametermappingList = new List<ParameterMappingList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                parametermappingList = EntityMapper<string, IEnumerable<ParameterMappingList>>.MapFromJson(jsonDataProviders);
            }
            return parametermappingList;
        }

        public MergeServiceModelForGradeSection GetRequiredParamBySchoolUserId(long schoolId, long id, long academicYearId)
        {
            var uri = API.Attendance.GetRequiredParamBySchoolUserId(_path,  schoolId,  id,  academicYearId);
            MergeServiceModelForGradeSection objMergeServiceModelForGradeSection = new MergeServiceModelForGradeSection();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objMergeServiceModelForGradeSection = EntityMapper<string, MergeServiceModelForGradeSection>.MapFromJson(jsonDataProviders);
            }
            return objMergeServiceModelForGradeSection;
        }

        public ClassAttendanceHeaderNDetails GetClassAttendanceHeaderNDetails(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            var uri = API.Attendance.GetClassAttendanceHeaderNDetails(_path, schoolId, schoolGroupId, entryDate);
            ClassAttendanceHeaderNDetails objClassAttendanceHeaderNDetails = new ClassAttendanceHeaderNDetails();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objClassAttendanceHeaderNDetails = EntityMapper<string, ClassAttendanceHeaderNDetails>.MapFromJson(jsonDataProviders);
            }
            return objClassAttendanceHeaderNDetails;
        }
    }
}