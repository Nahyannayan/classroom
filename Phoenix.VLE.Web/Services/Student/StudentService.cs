﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Phoenix.VLE.Web;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;

using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Common.Helpers;
using System.Web;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Models.Entities;
using Phoenix.Common.Models.ViewModels;

namespace Phoenix.VLE.Web.Services
{
    public class StudentService : IStudentService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });

        // TODO: put this is a base class if needed
        readonly string _baseUrl = Constants.PhoenixAPIUrl;

        readonly string _path = "api/v1/student";

        //readonly string _configurationPath = "api/v1/configuration";
        #endregion

        public StudentService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods

        #region TODO: Evaluate Async options
        public Task<IEnumerable<StudentView>> GetStudentsAsync()
        {
            throw new NotImplementedException();
        }

        public Task<StudentView> GetStudentAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<StudentView> AddStudentAsync(StudentView s)
        {
            throw new NotImplementedException();
        }

        public Task<StudentView> UpdateStudentAsync(StudentView sp)
        {
            throw new NotImplementedException();
        }

        public bool DeleteStudentAsync(int id)
        {
            throw new NotImplementedException();
        }
        #endregion


        public StudentView GetStudent(int id)
        {
            var uri = API.Student.GetStudent(_path, id);
            var student = new StudentView();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonData = response.Content.ReadAsStringAsync().Result;
                student = JsonConvert.DeserializeObject<StudentView>(jsonData);
            }
            return student;
        }

        public StudentView AddStudent(StudentView p)
        {
            var uri = API.Student.AddStudent(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, p).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                p = JsonConvert.DeserializeObject<StudentView>(jsonDataStatus);
            }
            return p;
        }

        public StudentView UpdateStudent(StudentView p)
        {
            var uri = API.Student.UpdateStudent(_path, p.StudentId);
            HttpResponseMessage response = _client.PutAsJsonAsync(uri, p).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                p = JsonConvert.DeserializeObject<StudentView>(jsonDataStatus);
            }

            return p;
        }

        public bool DeleteStudent(int id)
        {
            var uri = API.Student.DeleteStudent(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.StatusCode == System.Net.HttpStatusCode.NoContent ? true : false;
        }

        public StudentDetail GetStudentByUserId(long id)
        {
            var studentDetail = new StudentDetail();
            var uri = API.Student.GetStudentByUserId(_path, id);

            //To set authorization header, to service called before setting login cookie
            var token = Helpers.CommonHelper.GetCookieValue("st", "tk");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                studentDetail = JsonConvert.DeserializeObject<StudentDetail>(jsonDataStatus);
            }

            return studentDetail;
        }

        public IEnumerable<StudentDetail> GetStudentByFamily(long id)
        {
            var studentDetail = new List<StudentDetail>();
            var uri = API.Student.GetStudentByFamily(_path, id);

            //To set authorization header, to service called before setting login cookie
            var token = Helpers.CommonHelper.GetCookieValue("st", "tk");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                studentDetail = JsonConvert.DeserializeObject<List<StudentDetail>>(jsonDataStatus);
            }

            return studentDetail;
        }

        #endregion




        public StudentDashboard GetStudentDashboard(long id)
        {
            var uri = API.Student.GetStudentDashboard(_path, id);
            StudentDashboard schools = new StudentDashboard();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schools = EntityMapper<string, StudentDashboard>.MapFromJson(jsonDataProviders);
            }
            return schools;
        }

        public StudentPortfolio GetStudentPortfolioInformation(long userId)
        {
            var uri = API.StudentInformation.GetStudentPortfolioInformation(_path, userId);
            StudentPortfolio studentPortfolio = new StudentPortfolio();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentPortfolio = EntityMapper<string, StudentPortfolio>.MapFromJson(jsonDataProviders);
            }
            return studentPortfolio;
        }

        public bool SaveStudentDescription(StudentEdit editModel)
        {
            var uri = API.StudentInformation.SaveStudentDescription(_path);
            var model = new Student();
            EntityMapper<StudentEdit, Student>.Map(editModel, model);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;

        }

        ////deependra
        public IEnumerable<Student> GetAll()
        {
            var uri = API.Student.GetAllStudents(_path);
            List<Student> studentList = new List<Student>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentList = EntityMapper<string, List<Student>>.MapFromJson(jsonDataProviders);
            }
            return studentList;
        }

        public bool SaveStudentAboutMe(SchoolBadge model)
        {
            var uri = API.StudentInformation.SaveStudentAboutMe(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;

        }

        public IEnumerable<UserActivityPointsView> GetStudentTotalActivePoints(long id)
        {
            var uri = Constants.PhoenixActiveKidsApiUrl + API.StudentInformation.GetStudentActivePoints("/Information", id);

            IEnumerable<UserActivityPointsView> lstUserActivityPointsView = new List<UserActivityPointsView>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataPoints = response.Content.ReadAsStringAsync().Result;
                lstUserActivityPointsView = EntityMapper<string, List<UserActivityPointsView>>.MapFromJson(jsonDataPoints);
            }

            return lstUserActivityPointsView;
        }

        public StudentProfileDetail GetStudentProfileDetails(long userId)
        {
            StudentProfileDetail studentDetails = new StudentProfileDetail();
            var uri = API.Student.GetStudentProfileDetails(_path, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataPoints = response.Content.ReadAsStringAsync().Result;
                studentDetails = EntityMapper<string, StudentProfileDetail>.MapFromJson(jsonDataPoints);
            }
            return studentDetails;
        }

        public bool SaveStudentProfileImages(Student model)
        {
            var uri = API.Student.SaveStudentProfileImages(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public bool UpdateStudentPortfolioSectionDetails(StudentProfileSections model)
        {
            var uri = API.Student.UpdateStudentPortfolioSectionDetails(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<LeadBoard> GetLeaderBoard(long? schoolId, long? studentId, int? range, int? gradeId, DateTime? date)
        {
            var uri = Constants.PhoenixActiveKidsApiUrl + API.StudentInformation.GetLeaderBoard("/Information", schoolId, studentId, range, gradeId, date);

            IEnumerable<LeadBoard> lstLeaderBoard = new List<LeadBoard>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataPoints = response.Content.ReadAsStringAsync().Result;
                lstLeaderBoard = EntityMapper<string, List<LeadBoard>>.MapFromJson(jsonDataPoints);
            }

            return lstLeaderBoard;
        }

        public int CheckIfTheResourceDeleted(int sourceId, string notificationType)
        {
            var uri = API.Student.CheckIfTheResourceDeleted(_path, sourceId, notificationType);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
    }
}
