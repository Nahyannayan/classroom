﻿using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IStudentService
    {
        //Task<IEnumerable<StudentView>> GetStudents();

        Task<IEnumerable<StudentView>> GetStudentsAsync();
        Task<StudentView> GetStudentAsync(int id);
        Task<StudentView> AddStudentAsync(StudentView s);
        Task<StudentView> UpdateStudentAsync(StudentView sp);
        bool DeleteStudentAsync(int id);
        StudentView GetStudent(int id);
        StudentView AddStudent(StudentView s);
        StudentView UpdateStudent(StudentView s);
        bool DeleteStudent(int id);
        StudentDetail GetStudentByUserId(long id);
        IEnumerable<StudentDetail> GetStudentByFamily(long id);

        StudentDashboard GetStudentDashboard(long id);
        StudentPortfolio GetStudentPortfolioInformation(long userId);
        bool SaveStudentDescription(StudentEdit editModel);

        //deependra
        IEnumerable<Student> GetAll();
        bool SaveStudentAboutMe(SchoolBadge model);

        IEnumerable<UserActivityPointsView> GetStudentTotalActivePoints(long id);
        StudentProfileDetail GetStudentProfileDetails(long userId);
        bool SaveStudentProfileImages(Student model);
        bool UpdateStudentPortfolioSectionDetails(StudentProfileSections model);

        IEnumerable<LeadBoard> GetLeaderBoard(long? schoolId, long? studentId, int? range, int? gradeId, DateTime? date);
        int CheckIfTheResourceDeleted(int sourceId, string notificationType);
    }
}
