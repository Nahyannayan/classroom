﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class IncidentService : IIncidentService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Incident";
        #endregion
        public IncidentService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }

        }
        public IncidentDashBoardModel GetIncidentList(long schoolId, long curriculumId, int month, bool isFA = true)
        {
            var uri = API.Incident.GetIncidentList(_path, schoolId, curriculumId, month, isFA);
            IncidentDashBoardModel classLists = new IncidentDashBoardModel();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classLists = EntityMapper<string, IncidentDashBoardModel>.MapFromJson(jsonDataProviders);
            }
            return classLists;
        }
        public IncidentModel GetIncident(long IncidentId)
        {
            var uri = API.Incident.GetIncident(_path, IncidentId);
            IncidentModel classLists = new IncidentModel();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classLists = EntityMapper<string, IncidentModel>.MapFromJson(jsonDataProviders);
            }
            return classLists;
        }

        public IEnumerable<ChartModel> GetIncidentChartByCategory(long schoolId, long academicYearId, int month, bool isCategory)
        {
            var uri = API.Incident.GetIncidentChart(_path, schoolId, academicYearId, month, isCategory);
            IEnumerable<ChartModel> classLists = new List<ChartModel>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classLists = EntityMapper<string, IEnumerable<ChartModel>>.MapFromJson(jsonDataProviders);
            }
            return classLists;
        }

        public IEnumerable<IncidentStudentList> GetStudentByIncidentId(long incidentId)
        {
            var uri = API.Incident.GetStudentByIncidentId(_path, incidentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode ?
                response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<IncidentStudentList>>()
                : default(IEnumerable<IncidentStudentList>);
        }
        public string IncidentEntryCUD(IncidentEntry incidentEntry)
        {
            var uri = API.Incident.GetIncidentEntryCUD(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, incidentEntry).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return jsonDataProviders;
            }
            return string.Empty;
        }

        #region Incident Action
        public ActionModel GetBehaviourAction(long incidentId, long studentId)
        {
            var uri = API.Incident.GetBehaviourAction(_path, incidentId, studentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode ?
                response.Content.ReadAsStringAsync().Result.FromJsonToType<ActionModel>()
                : default(ActionModel);
        }
        public IEnumerable<BehaviourActionFollowup> GetBehaviourActionFollowups(long incidentId, long actionId)
        {
            var uri = API.Incident.GetBehaviourActionFollowups(_path, incidentId, actionId);
            IEnumerable<BehaviourActionFollowup> followUpList = new List<BehaviourActionFollowup>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                followUpList = EntityMapper<string, IEnumerable<BehaviourActionFollowup>>.MapFromJson(jsonDataProviders);
            }
            return followUpList;
        }
        public IEnumerable<FollowUpDesignation> GetFollowUpDesignations(long schoolId, long incidentId, long UserId)
        {
            var uri = API.Incident.GetFollowUpDesignations(_path, schoolId, incidentId,UserId);
            IEnumerable<FollowUpDesignation> designationList = new List<FollowUpDesignation>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                designationList = EntityMapper<string, IEnumerable<FollowUpDesignation>>.MapFromJson(jsonDataProviders);
            }
            return designationList;
        }
        public IEnumerable<FollowUpStaff> GetFollowUpStaffs(long schoolId, long designationId)
        {
            var uri = API.Incident.GetFollowUpStaffs(_path, schoolId, designationId);
            IEnumerable<FollowUpStaff> staffList = new List<FollowUpStaff>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                staffList = EntityMapper<string, IEnumerable<FollowUpStaff>>.MapFromJson(jsonDataProviders);
            }
            return staffList;
        }

        public bool ActionCUD(ActionModel behaviourAction)
        {
            var uri = API.Incident.ActionCUD(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, behaviourAction).Result;
            return response.IsSuccessStatusCode;
        }
        public bool ActionFollowUpCUD(BehaviourActionFollowup behaviourActionsForwaredList)
        {
            var uri = API.Incident.ActionFollowUpCUD(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, behaviourActionsForwaredList).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<GroupTeacher> GetSchoolTeachersBySchoolId(long schoolId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Incident.GetSchoolTeachersBySchoolId(_path, schoolId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<GroupTeacher>>() : new List<GroupTeacher>();
        }
        #endregion
    }
}