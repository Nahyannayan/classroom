﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class BehaviourSetupService : IBehaviourSetupService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/BehaviourSetup";
        #endregion

        public BehaviourSetupService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }
        public IEnumerable<BehaviourSetup> GetSubCategoryList(long MainCategoryId, long SchoolId)
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.BehaviourSetup.GetSubCategoryList(_path, MainCategoryId, SchoolId)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<BehaviourSetup>>() : default(IEnumerable<BehaviourSetup>);
        }
        public bool SaveSubCategory(BehaviourSetup behaviourSetup, string DATAMODE)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.BehaviourSetup.SaveSubCategory(_path, DATAMODE), behaviourSetup).Result;
            return response.IsSuccessStatusCode;
        }
        #region Action Hierarchy
        public IEnumerable<Designations> GetDesignations(long schoolId)
        {
            var uri = API.BehaviourSetup.GetDesignations(_path, schoolId);
            IEnumerable<Designations> list = new List<Designations>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<Designations>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public IEnumerable<DesignationsRouting> GetDesignationsRoutings(long schoolId, long? designationFrom = null)
        {
            var uri = API.BehaviourSetup.GetDesignationsRoutings(_path, schoolId, designationFrom);
            IEnumerable<DesignationsRouting> list = new List<DesignationsRouting>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<DesignationsRouting>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public IEnumerable<DesignationsRouting> DesignationBySchoolCUD(DesignationsRoutingCUD designationsRouting)
        {
            var uri = API.BehaviourSetup.DesignationBySchoolCUD(_path);
            IEnumerable<DesignationsRouting> list = new List<DesignationsRouting>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, designationsRouting).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<DesignationsRouting>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }
        #endregion

        #region Certificate Schedule
        public IEnumerable<CertificateScheduling> GetCertificateSchedulings(long? CertificateSchedulingId, long? curriculumId, long? schoolId, int? scheduleType = null)
        {
            var uri = API.BehaviourSetup.GetCertificateSchedulings(_path, CertificateSchedulingId, curriculumId, schoolId, scheduleType);
            IEnumerable<CertificateScheduling> list = new List<CertificateScheduling>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<CertificateScheduling>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public long CertificateSchedulingCUD(CertificateScheduling certificateScheduling)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.BehaviourSetup.CertificateSchedulingCUD(_path), certificateScheduling).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt64(jsonDataProviders);
            }
            return 0;
        }
        #endregion
    }
}