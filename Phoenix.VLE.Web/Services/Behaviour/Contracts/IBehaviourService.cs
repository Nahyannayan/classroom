﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using SMS.Web.Areas.SMS.Model;
using SMS.Web.Areas.SMS.ViewModels;

namespace SMS.Web.Services
{
   public  interface IBehaviourService
    {
        IEnumerable<ClassList> GetStudentList(string username, int tt_id = 0, string grade = null, string section = null);
        IEnumerable<BehaviourModel> LoadBehaviourByStudentId(string stu_id);
        IEnumerable<BehaviourDetails> GetBehaviourById(int id);
        bool InsertBehaviourDetails(BehaviourDetails entity, string bsu_id, string mode = "ADD");

        IEnumerable<StudentBehaviour> GetListOfStudentBehaviour();
        bool InsertUpdateStudentBehavior(List<StudentBehaviourFiles> studentBehaviourFiles,long studentId = 0, int behaviorId = 0, string behaviourComment = "");

        IEnumerable<StudentBehaviour> GetStudentBehaviorByStudentId(long studentId = 0);
        IEnumerable<CourseGroupModel> GetCourseGroupList(long TeacherId, long SchoolId);
        bool InsertBulkStudentBehaviour(List<StudentBehaviourFiles> studentBehaviourFiles,string bulkStudentds = "", int behaviourId = 0, string behaviourComment = "");

        bool UpdateBehaviourTypes(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0);

        IEnumerable<StudentBehaviourMerit> GetFileDetailsByStudentId(long studentId = 0);

        IEnumerable<ClassList> GetBehaviourClassList(string username, int tt_id = 0, string grade = null, string section = null, long GroupId = 0, bool IsFilterByGroup = false);
        IEnumerable<ClassList> GetBehaviourStudentListByGroupId(long groupId);
        IEnumerable<SubCategories> GetSubCategoriesByCategoryId(long categoryId, string BSU_ID, string GRD_ID, long GroupId);

        bool DeleteStudentBehaviourMapping(long studentId = 0, int behaviourId = 0);

        IEnumerable<StudentBehaviourMerit> GetMeritDetails(long meritId);
        IEnumerable<SubCategories> GetMeritCategoryByStudent(long schoolId, long studentId);
        bool InsertMeritDemerit(string schoolId, int academicId,DateTime? IncidentDate, MeritDemerit objMeritDemerit);

        #region Student Point Category
        IEnumerable<StudentPointCategory> GetStudentPointCategory(string sectionId, int? scheduleType = null, long? CertificateScheduleId = null, string date = null, long? studentId = null);
        IEnumerable<StudentPointCategory> GetStudentPointCategory(long schoolId, long academicYearId, int scheduleType);
        bool CertificateProcessLogCU(List<CertificateProcessLog> processLogs);
        SendEmailNotificationView GenerateEmailTemplateForBehaviorCertificate(string title, string message, string studentInternalIds, string notifyTo, string attachmentFiles);
        #endregion

        #region StudentOnReport
        IEnumerable<StudentOnReportMaster> GetStudentOnReportMDetail(long studentId);
        long StudentOnReportMCU(StudentOnReportMaster studentOnReport);

        IEnumerable<StudentOnReportDetail> GetStudentOnReportDetails(StudentOnReportDetailsParameter detailsParameter);
        long StudentOnReportDetailsCU(StudentOnReportDetail studentOnReport);
        #endregion
    }
}
