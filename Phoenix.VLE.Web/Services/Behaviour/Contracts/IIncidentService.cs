﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IIncidentService
    {
        IncidentDashBoardModel GetIncidentList(long schoolId, long curriculumId, int month, bool isFA = true);
        IncidentModel GetIncident(long IncidentId);
        IEnumerable<ChartModel> GetIncidentChartByCategory(long schoolId, long academicYearId, int month, bool isCategory);
        IEnumerable<IncidentStudentList> GetStudentByIncidentId(long incidentId);
        string IncidentEntryCUD(IncidentEntry incidentEntry);
        #region Incident Action
        ActionModel GetBehaviourAction(long incidentId, long studentId);
        IEnumerable<BehaviourActionFollowup> GetBehaviourActionFollowups(long incidentId, long actionId);
        IEnumerable<FollowUpDesignation> GetFollowUpDesignations(long schoolId, long incidentId, long UserId);
        IEnumerable<FollowUpStaff> GetFollowUpStaffs(long schoolId, long designationId);
        bool ActionCUD(ActionModel behaviourAction);
        bool ActionFollowUpCUD(BehaviourActionFollowup behaviourActionsForwaredList);
        IEnumerable<GroupTeacher> GetSchoolTeachersBySchoolId(long schoolId);
        #endregion
    }
}
