﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models.Entities;

namespace Phoenix.VLE.Web.Services
{
    public interface IBehaviourSetupService
    {
        IEnumerable<BehaviourSetup> GetSubCategoryList(long MainCategoryId, long SchoolId);
        bool SaveSubCategory(BehaviourSetup behaviourSetup, string DATAMODE);

        #region Action Hierarchy
        IEnumerable<Designations> GetDesignations(long schoolId);
        IEnumerable<DesignationsRouting> GetDesignationsRoutings(long schoolId, long? designationFrom = null);
        IEnumerable<DesignationsRouting> DesignationBySchoolCUD(DesignationsRoutingCUD designationsRouting);
        #endregion

        #region Certificate Schedule
        IEnumerable<CertificateScheduling> GetCertificateSchedulings(long? CertificateSchedulingId, long? curriculumId, long? schoolId, int? scheduleType = null);
        long CertificateSchedulingCUD(CertificateScheduling certificateScheduling);
        #endregion
    }
}
