﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using SMS.Web.Areas.SMS.Model;
using SMS.Web.Areas.SMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Phoenix.VLE.Web;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models.Entities;
using System.Web;
using System.Text;
using Phoenix.Common.ViewModels;

namespace SMS.Web.Services
{
    public class BehaviourService : IBehaviourService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Behaviour";
        #endregion

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created Date : 11-Jun-2019
        /// Description : 
        /// </summary>
        /// <returns></returns>

        public BehaviourService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }

        }
        #region Methods

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 11 July 2019
        /// Description : This method is use fetch Student List  for  particular grade or TimeTable ID  through Api
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public IEnumerable<ClassList> GetStudentList(string username, int tt_id = 0, string grade = null, string section = null)
        {
            var uri = API.Behaviour.GetStudentList(_path, username, tt_id, grade, section);
            IEnumerable<ClassList> studentlist = new List<ClassList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentlist = EntityMapper<string, IEnumerable<ClassList>>.MapFromJson(jsonDataProviders);
            }
            return studentlist;
        }


        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 14 July 2019
        /// Description : This method is use fetch the behaviour details of student(s) through Api
        /// </summary>
        /// <param name="stu_id"></param>
        /// <returns></returns>
        public IEnumerable<BehaviourModel> LoadBehaviourByStudentId(string stu_id)
        {
            var uri = API.Behaviour.LoadBehaviourByStudentId(_path, stu_id);
            IEnumerable<BehaviourModel> behaviours = new List<BehaviourModel>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviours = EntityMapper<string, IEnumerable<BehaviourModel>>.MapFromJson(jsonDataProviders);
            }
            return behaviours;
        }

        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 15 July 2019
        /// Description : This method is use fetch the behaviour details by behaviour ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<BehaviourDetails> GetBehaviourById(int id)
        {
            var uri = API.Behaviour.GetBehaviourById(_path, id);
            IEnumerable<BehaviourDetails> behaviourDetails = new List<BehaviourDetails>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviourDetails = EntityMapper<string, IEnumerable<BehaviourDetails>>.MapFromJson(jsonDataProviders);
            }
            return behaviourDetails;
        }
        /// <summary>
        /// Author : Fraz Ahmed
        /// Created At: 17 July 2019
        /// Description : This method is used to insert/update behaviour details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool InsertBehaviourDetails(BehaviourDetails model, string bsu_id, string mode = "ADD")
        {
            var uri = API.Behaviour.InsertBehaviourDetails(_path, bsu_id, mode);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<StudentBehaviour> GetListOfStudentBehaviour()
        {
            var uri = API.Behaviour.GetListOfStudentBehaviour(_path);
            IEnumerable<StudentBehaviour> behaviourDetails = new List<StudentBehaviour>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviourDetails = EntityMapper<string, IEnumerable<StudentBehaviour>>.MapFromJson(jsonDataProviders);
            }
            return behaviourDetails;
        }

        public bool InsertUpdateStudentBehavior(List<StudentBehaviourFiles> studentBehaviourFiles, long studentId = 0, int behaviorId = 0, string behaviourComment = "")
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Behaviour.InsertUpdateStudentBehavior(_path, studentId, behaviorId, behaviourComment), studentBehaviourFiles).Result;
            return response.IsSuccessStatusCode;

        }

        public IEnumerable<StudentBehaviour> GetStudentBehaviorByStudentId(long studentId = 0)
        {
            var uri = API.Behaviour.GetStudentBehaviorByStudentId(_path, studentId);
            IEnumerable<StudentBehaviour> behaviourDetails = new List<StudentBehaviour>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviourDetails = EntityMapper<string, IEnumerable<StudentBehaviour>>.MapFromJson(jsonDataProviders);
            }
            return behaviourDetails;
        }

        public bool InsertBulkStudentBehaviour(List<StudentBehaviourFiles> studentBehaviourFiles, string bulkStudentds = "", int behaviourId = 0, string behaviourComment = "")
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Behaviour.InsertBulkStudentBehaviour(_path, bulkStudentds, behaviourId, behaviourComment), studentBehaviourFiles).Result;
            return response.IsSuccessStatusCode;

        }

        public bool UpdateBehaviourTypes(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0)
        {
            var uri = API.Behaviour.UpdateBehaviourTypes(_path, behaviourId, behaviourType, behaviourPoint, categoryId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<StudentBehaviourMerit> GetFileDetailsByStudentId(long studentId = 0)
        {
            var uri = API.Behaviour.GetFileDetailsByStudentId(_path, studentId);
            IEnumerable<StudentBehaviourMerit> behaviourFileDetails = new List<StudentBehaviourMerit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                behaviourFileDetails = EntityMapper<string, IEnumerable<StudentBehaviourMerit>>.MapFromJson(jsonDataProviders);
            }
            return behaviourFileDetails;
        }

        public IEnumerable<ClassList> GetBehaviourClassList(string username, int tt_id = 0, string grade = null, string section = null, long GroupId = 0, bool IsFilterByGroup = false)
        {
            var uri = API.Behaviour.GetBehaviourClassList(_path, username, tt_id, grade, section, GroupId, IsFilterByGroup);
            IEnumerable<ClassList> classLists = new List<ClassList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classLists = EntityMapper<string, IEnumerable<ClassList>>.MapFromJson(jsonDataProviders);
            }
            return classLists;
        }
        public IEnumerable<ClassList> GetBehaviourStudentListByGroupId(long groupId)
        {
            HttpResponseMessage response = _client.GetAsync(API.Behaviour.GetBehaviourStudentListByGroupId(_path, groupId)).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<ClassList>>() : default(IEnumerable<ClassList>);
        }
        public IEnumerable<CourseGroupModel> GetCourseGroupList(long TeacherId, long SchoolId)
        {
            HttpResponseMessage response = _client.GetAsync(API.Behaviour.GetCourseGroupList(_path, TeacherId, SchoolId)).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<CourseGroupModel>>() : new List<CourseGroupModel>();
        }
        public bool DeleteStudentBehaviourMapping(long studentId = 0, int behaviourId = 0)
        {
            var uri = API.Behaviour.DeleteStudentBehaviourMapping(_path, studentId, behaviourId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;


        }
        public IEnumerable<SubCategories> GetSubCategoriesByCategoryId(long categoryId, string BSU_ID, string GRD_ID, long GroupId)
        {
            var uri = API.Behaviour.GetSubCategoriesByCategoryId(_path, categoryId, BSU_ID, GRD_ID, GroupId);
            IEnumerable<SubCategories> classLists = new List<SubCategories>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                classLists = EntityMapper<string, IEnumerable<SubCategories>>.MapFromJson(jsonDataProviders);
            }
            return classLists;
        }


        public IEnumerable<StudentBehaviourMerit> GetMeritDetails(long meritId)
        {
            var uri = API.Behaviour.GetMeritDetails(_path, meritId);
            IEnumerable<StudentBehaviourMerit> objStudentBehaviourMerit = new List<StudentBehaviourMerit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objStudentBehaviourMerit = EntityMapper<string, IEnumerable<StudentBehaviourMerit>>.MapFromJson(jsonDataProviders);
            }
            return objStudentBehaviourMerit;
        }
        public IEnumerable<SubCategories> GetMeritCategoryByStudent(long schoolId, long studentId)
        {
            var uri = API.Behaviour.GetMeritCategoryByStudent(_path, schoolId, studentId);
            IEnumerable<SubCategories> objStudentBehaviourMerit = new List<SubCategories>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objStudentBehaviourMerit = EntityMapper<string, IEnumerable<SubCategories>>.MapFromJson(jsonDataProviders);
            }
            return objStudentBehaviourMerit;
        }
        public bool InsertMeritDemerit(string schoolId, int academicId, DateTime? IncidentDate, MeritDemerit objMeritDemerit)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Behaviour.InsertMeritDemerit(_path, schoolId, academicId, IncidentDate), objMeritDemerit).Result;
            return response.IsSuccessStatusCode;
        }

        #endregion

        #region Student Point Category
        public IEnumerable<StudentPointCategory> GetStudentPointCategory(long schoolId, long academicYearId, int scheduleType)
        {
            var uri = API.Behaviour.GetStudentPointCategory(_path, schoolId, academicYearId, scheduleType);
            IEnumerable<StudentPointCategory> objStudentBehaviourMerit = new List<StudentPointCategory>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objStudentBehaviourMerit = EntityMapper<string, IEnumerable<StudentPointCategory>>.MapFromJson(jsonDataProviders);
            }
            return objStudentBehaviourMerit;
        }
        public IEnumerable<StudentPointCategory> GetStudentPointCategory(string sectionId, int? scheduleType = null, long? CertificateScheduleId= null, string date = null, long? studentId = null)
        {
            HttpResponseMessage response = _client.GetAsync(API.Behaviour.GetStudentPointCategory(_path, sectionId, scheduleType, CertificateScheduleId, date, studentId)).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<List<StudentPointCategory>>() : new List<StudentPointCategory>();
        }
        public bool CertificateProcessLogCU(List<CertificateProcessLog> processLogs)
        {
            var uri = API.Behaviour.CertificateProcessLogCU(_path);
            IEnumerable<StudentPointCategory> objStudentBehaviourMerit = new List<StudentPointCategory>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, processLogs).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt64(jsonDataProviders) > 0;
            }
            return false;
        }

        public SendEmailNotificationView GenerateEmailTemplateForBehaviorCertificate(string title, string message, string studentInternalIds, string notifyTo, string attachmentFiles)
        {
            string EmailBody = "";
            System.IO.StreamReader reader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(Constants.BehaviorCertificateEmailBody), Encoding.UTF8);
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
            EmailBody = EmailBody.Replace("@@Attachments", attachmentFiles);

            SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
            sendEmailNotificationView.FromMail = Constants.NoReplyMail;
            sendEmailNotificationView.LogType = "SendGroupMessage";
            sendEmailNotificationView.StudentId = studentInternalIds;
            sendEmailNotificationView.Subject = title;
            sendEmailNotificationView.Message = EmailBody;
            sendEmailNotificationView.NotifyTo = "STUDENT";
            sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;

            return sendEmailNotificationView;
        }
        #endregion

        #region StudentOnReportMaster
        public IEnumerable<StudentOnReportMaster> GetStudentOnReportMDetail(long studentId)
        {
            var uri = API.Behaviour.GetStudentOnReportMDetail(_path, studentId);
            IEnumerable<StudentOnReportMaster> studentOnReport = new List<StudentOnReportMaster>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentOnReport = EntityMapper<string, IEnumerable<StudentOnReportMaster>>.MapFromJson(jsonDataProviders);
            }
            return studentOnReport;
        }
        public long StudentOnReportMCU(StudentOnReportMaster studentOnReport)
        {
            var uri = API.Behaviour.StudentOnReportMCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentOnReport).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt64(jsonDataProviders);
            }
            return 0;
        }
        public IEnumerable<StudentOnReportDetail> GetStudentOnReportDetails(StudentOnReportDetailsParameter detailsParameter)
        {
            var uri = API.Behaviour.GetStudentOnReportDetails(_path);
            IEnumerable<StudentOnReportDetail> studentOnReport = new List<StudentOnReportDetail>();
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, detailsParameter).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentOnReport = EntityMapper<string, IEnumerable<StudentOnReportDetail>>.MapFromJson(jsonDataProviders);
            }
            return studentOnReport;
        }
        public long StudentOnReportDetailsCU(StudentOnReportDetail studentOnReport)
        {
            var uri = API.Behaviour.StudentOnReportDetailsCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentOnReport).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return Convert.ToInt64(jsonDataProviders);
            }
            return 0;
        }
        #endregion
    }
}