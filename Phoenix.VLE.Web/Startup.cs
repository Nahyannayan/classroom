﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Phoenix.Web.Startup))]

namespace Phoenix.Web
{

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
    //public partial class Startup
    //{
    //    // The Client ID is used by the application to uniquely identify itself to Azure AD.
    //    string clientId = System.Configuration.ConfigurationManager.AppSettings["ClientId"];

    //    // RedirectUri is the URL where the user will be redirected to after they sign in.
    //    string redirectUri = System.Configuration.ConfigurationManager.AppSettings["RedirectUri"];

    //    // Tenant is the tenant ID (e.g. contoso.onmicrosoft.com, or 'common' for multi-tenant)
    //    static string tenant = System.Configuration.ConfigurationManager.AppSettings["Tenant"];

    //    string graphScopes = System.Configuration.ConfigurationManager.AppSettings["ida:GraphScopes"];

    //    // Authority is the URL for authority, composed by Microsoft identity platform endpoint and the tenant name (e.g. https://login.microsoftonline.com/contoso.onmicrosoft.com/v2.0)
    //    string authority = "https://login.microsoftonline.com/common/v2.0";

    //    public void Configuration(IAppBuilder app)
    //    {
    //        app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
    //        app.UseCookieAuthentication(new CookieAuthenticationOptions());
    //        app.UseOpenIdConnectAuthentication(
    //        new OpenIdConnectAuthenticationOptions
    //        {
    //            // Sets the ClientId, authority, RedirectUri as obtained from web.config
    //            ClientId = clientId,
    //            Authority = authority,
    //            RedirectUri = redirectUri,
    //            // PostLogoutRedirectUri is the page that users will be redirected to after sign-out. In this case, it is using the home page
    //            PostLogoutRedirectUri = redirectUri,
    //            Scope = $"openid email profile offline_access {graphScopes}",
    //            // ResponseType is set to request the id_token - which contains basic information about the signed-in user
    //            ResponseType = OpenIdConnectResponseType.IdToken,
    //            // ValidateIssuer set to false to allow personal and work accounts from any organization to sign in to your application
    //            // To only allow users from a single organizations, set ValidateIssuer to true and 'tenant' setting in web.config to the tenant name
    //            // To allow users from only a list of specific organizations, set ValidateIssuer to true and use ValidIssuers parameter 
    //            TokenValidationParameters = new TokenValidationParameters()
    //            {
    //                ValidateIssuer = false
    //            },
    //            // OpenIdConnectAuthenticationNotifications configures OWIN to send notification of failed authentications to OnAuthenticationFailed method
    //            Notifications = new OpenIdConnectAuthenticationNotifications
    //            {
    //                AuthenticationFailed = OnAuthenticationFailed
    //            }
    //        }
    //    );
    //        //JwtSecurityTokenHandler.DefaultInboundClaimTypeMap = new Dictionary<string, string>();
    //        ////AntiForgeryConfig.UniqueClaimTypeIdentifier = "sub";

    //        //app.UseCookieAuthentication(new CookieAuthenticationOptions
    //        //{
    //        //    AuthenticationType = "Cookies",
    //        //    ExpireTimeSpan = TimeSpan.FromMinutes(120),
    //        //    SlidingExpiration = true

    //        //});

    //        //app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions {
    //        //    AuthenticationType = "oidc",
    //        //    Authority = Constants.IdentityAPIUrl,
    //        //    ClientId = Constants.ClientId,
    //        //    ClientSecret = Constants.ClientSecret,
    //        //    RedirectUri = Constants.RedirectUri,
    //        //    PostLogoutRedirectUri = Constants.PostLogoutRedirectUri,
    //        //    ResponseType = "code id_token",
    //        //    Scope = "openid profile offline_access phoenix hse",
    //        //    RequireHttpsMetadata=false,

    //        //    UseTokenLifetime = false,               
    //        //    SignInAsAuthenticationType = "Cookies",

    //        //});
    //    }

    //    private Task OnAuthenticationFailed(AuthenticationFailedNotification<OpenIdConnectMessage, OpenIdConnectAuthenticationOptions> context)
    //    {
    //        context.HandleResponse();
    //        context.Response.Redirect("/?errormessage=" + context.Exception.Message);
    //        return Task.FromResult(0);
    //    }

    //    //app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
    //    //{
    //    //    ClientId = "phoenix.Identity",
    //    //    ClientSecret = "secret",
    //    //    Authority = Constants.IdentityAPIUrl,
    //    //    RedirectUri = Constants.PhoenixUrl,
    //    //    PostLogoutRedirectUri = Constants.PhoenixUrl,
    //    //    ResponseType = "id_token",                
    //    //    Scope = "openid profile offline_access phoenix hse",
    //    //    RequireHttpsMetadata = false,
    //    //    UseTokenLifetime = false,

    //    //    //TokenValidationParameters = new TokenValidationParameters
    //    //    //{
    //    //    //    NameClaimType = "name"
    //    //    //},


    //    //    SignInAsAuthenticationType = "Cookies",

    //    //    //Notifications = new OpenIdConnectAuthenticationNotifications
    //    //    //{
    //    //    //    AuthorizationCodeReceived = async n =>
    //    //    //    {
    //    //    //        // use the code to get the access and refresh token
    //    //    //        var tokenClient = new TokenClient(
    //    //    //            Constants.TokenEndpoint,
    //    //    //            "phoenix.Identity",
    //    //    //            "secret");

    //    //    //        var tokenResponse = await tokenClient.RequestAuthorizationCodeAsync(
    //    //    //            n.Code, n.RedirectUri);

    //    //    //        if (tokenResponse.IsError)
    //    //    //        {
    //    //    //            throw new Exception(tokenResponse.Error);
    //    //    //        }

    //    //    //        // use the access token to retrieve claims from userinfo
    //    //    //        //var userInfoClient = new UserInfoClient(Constants.UserInfoEndpoint);



    //    //    //        //var userInfoResponse = await userInfoClient.GetAsync(tokenResponse.AccessToken);

    //    //    //        // create new identity
    //    //    //        //var id = new ClaimsIdentity(n.AuthenticationTicket.Identity.AuthenticationType);
    //    //    //        //id.AddClaims(userInfoResponse.Claims);

    //    //    //        //id.AddClaim(new Claim("access_token", tokenResponse.AccessToken));
    //    //    //        //id.AddClaim(new Claim("expires_at", DateTime.Now.AddSeconds(tokenResponse.ExpiresIn).ToLocalTime().ToString()));
    //    //    //        //id.AddClaim(new Claim("refresh_token", tokenResponse.RefreshToken));
    //    //    //        //id.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
    //    //    //        //id.AddClaim(new Claim("sid", n.AuthenticationTicket.Identity.FindFirst("sid").Value));

    //    //    //        //n.AuthenticationTicket = new AuthenticationTicket(
    //    //    //        //    new ClaimsIdentity(id.Claims.ToString(), n.AuthenticationTicket.Identity.AuthenticationType, "name"),
    //    //    //        //    n.AuthenticationTicket.Properties);
    //    //    //    },

    //    //    //    RedirectToIdentityProvider = n =>
    //    //    //    {
    //    //    //        // if signing out, add the id_token_hint
    //    //    //        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.Logout)
    //    //    //        {
    //    //    //            var idTokenHint = n.OwinContext.Authentication.User.FindFirst("id_token");

    //    //    //            if (idTokenHint != null)
    //    //    //            {
    //    //    //                n.ProtocolMessage.IdTokenHint = idTokenHint.Value;
    //    //    //            }

    //    //    //        }

    //    //    //        return Task.FromResult(0);
    //    //    //    }
    //    //    //}
    //    //});
    //    //}
    //}
}