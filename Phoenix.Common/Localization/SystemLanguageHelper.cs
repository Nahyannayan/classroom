﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Phoenix.Common.DataAccess;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Services;
using Phoenix.Common.ViewModels;

namespace Phoenix.Common.Helpers
{
    public class SystemLanguageHelper
    {
        private static IEnumerable<SystemLanguage> _languages = null;

        public static IEnumerable<SystemLanguage> SystemLanguages
        {
            get
            {
                if (_languages == null)
                {
                    ICommonService _commonService = new CommonService();
                    _languages = _commonService.GetSystemLanguages();
                    //if _languages is null return static list,added to avoid errors in other applications(HSE,SIMS)
                    if (_languages.Count() <= 0)
                    {
                        var langList = new List<SystemLanguage>();
                        langList.Add(new SystemLanguage { SystemLanguageId = 1, SystemLanguageCode = "EN", SystemLanguageName = "English", CultureString = "en-GB", IsEnabled = true });
                        langList.Add(new SystemLanguage { SystemLanguageId = 2, SystemLanguageCode = "AR", SystemLanguageName = "العربية", CultureString = "ar-AE", IsEnabled = true, IsRightToLeft = true });
                        _languages = langList;
                    }
                    return _languages;

                }
                return _languages;
            }
        }
        public static SystemLanguage GetSchoolCurrentLanguage()
        {
            //DataHelper _datahelper = new DataHelper();
            //DataParameter parameter = new DataParameter("@SchoolId", SqlDbType.BigInt, PhoenixConfiguration.Instance.DefaultSchoolId);
            //string query = "SELECT TOP 1 SystemLanguageId FROM Admin.SchoolSettings WHERE SchoolId = @SchoolId";
            //int currentLanguageId = _datahelper.GetScalarValue(query, CommandType.Text, parameter).ToInteger();
            //if(currentLanguageId > 0)
            //    return SystemLanguages.FirstOrDefault(x => x.SystemLanguageId == currentLanguageId);
            //else
            ICommonService _commonService = new CommonService();
            var currentLanguage = _commonService.GetSchoolCurrentLanguage(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId));
            if (currentLanguage != null)
                return currentLanguage;
            else
                //default english
                return SystemLanguages.FirstOrDefault(x => x.SystemLanguageId == 1);
        }

        public static bool SetSchoolCurrentLanguage(int languageId)
        {
            //DataHelper _datahelper = new DataHelper();
            //IList<DataParameter> parameters = new List<DataParameter>();
            //parameters.Add(new DataParameter("@SchoolId", SqlDbType.BigInt, PhoenixConfiguration.Instance.DefaultSchoolId));
            //parameters.Add(new DataParameter("@SystemLanguageId", SqlDbType.Int, languageId));
            //return _datahelper.ExecuteNonQuery("Admin.SetSchoolCurrentLanguage", CommandType.StoredProcedure, parameters).ToInteger() > 0;
            ICommonService _commonService = new CommonService();
            return _commonService.SetSchoolCurrentLanguage(languageId, Convert.ToInt32(SessionHelper.CurrentSession.SchoolId));
            //return true;
        }

        public static SystemLanguage GetUserCurrentLanguage()
        {
            ICommonService _commonService = new CommonService();
            var currentLanguage = _commonService.GetUserCurrentLanguage(SessionHelper.CurrentSession.LanguageId);
            if (currentLanguage.SystemLanguageId != 0)
                return currentLanguage;
            else
                //default english
                return SystemLanguages.FirstOrDefault(x => x.SystemLanguageId == 1);
        }
        public static void SetCurrentSystemLanguageCookie(string value)
        {
            HttpCookie cookie = new HttpCookie("SystemLanguageCookie");
            cookie.HttpOnly = true;
            cookie.Values.Add("SystemLanguageId", value);
            cookie.Expires = DateTime.Now.AddYears(5); // === Need to update as per web.config method
            HttpContext.Current.Response.AppendCookie(cookie);
        }
        public static SystemLanguage GetCurrentSystemLanguageByCookie()
        {
            HttpCookie LangCookie = HttpContext.Current.Request.Cookies.Get("SystemLanguageCookie");
            if (LangCookie != null)
            {
                var langId = LangCookie["SystemLanguageId"].ToInteger();
                langId = langId > 0 ? langId : 1;
                return SystemLanguages.FirstOrDefault(x => x.SystemLanguageId == langId);
            }
            else
                //default english
                return SystemLanguages.FirstOrDefault(x => x.SystemLanguageId == 1);
        }
    }
}
