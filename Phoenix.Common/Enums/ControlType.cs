﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum ControlType
    {
        Mark = 0,
        Grade = 1,
        Comment = 2
    }
}
