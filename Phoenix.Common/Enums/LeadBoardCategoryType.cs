﻿using Phoenix.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum LeadBoardCategoryType
    {
        [StringValue("Overall performer")]
        OverAllPerformer,
        [StringValue("Physical Activity")]
        PhysicalActivity,
        [StringValue("Performing Arts & Fine Arts")]
        ArtsAndFineArts
    }
}
