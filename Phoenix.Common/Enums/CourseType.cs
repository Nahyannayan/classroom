﻿using Phoenix.Common.Helpers;

namespace Phoenix.Common.Enums
{
    public enum CourseType
    {
        //if new course type is added , add the same in _AddEditCourse.cshtml and Course.xml
        [StringValue("Termly")]
        Termly = 1,
        [StringValue("Yearly")]
        Yearly = 2
    }
    public enum AssessmentType
    {
        //if new course type is added , add the same in _AssessmentEvidence.cshtml and Course.xml
        [StringValue("Formative")]
        Formative = 1,
        [StringValue("Summative")]
        Summative = 2
    }

    public enum AssementDeliveryMode
    {
        //if new course type is added , add the same in _AssesmentLearning.cshtml and Course.xml
        [StringValue("Face to Face")]
        FacetoFace = 1,
        [StringValue("Synchronous")]
        Synchronous = 2,
        [StringValue("Asynchronous")]
        Asynchronous = 3,
        [StringValue("Flipped Activity")]
        FlippedActivity = 4
    }
    public enum UnitControlType
    {
        [StringValue("Assessment Learning")]
        AssessmentLearning = 1,
        [StringValue("Assessments")]
        Assessments = 2,
        [StringValue("Future Fluencies")]
        FutureFluencies = 3,
        [StringValue("TextArea")]
        TextArea = 4,
        [StringValue("Standards/Objectives")]
        Standards = 5
    }
}