﻿namespace Phoenix.Common.Enums
{
    public enum AssignmentStatusEnum
    {
        Completed = 'C',
        Overdue = 'O',
        Pending = 'P',
        New = 'N'
    }
}
