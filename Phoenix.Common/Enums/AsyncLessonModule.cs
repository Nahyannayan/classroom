﻿using Phoenix.Common.Helpers;

namespace Phoenix.Common.Enums
{
    public enum AsyncLessonModule
    {
        [StringValue("Resources")]
        Resources = 1,
        [StringValue("Assignment")]
        Assignment = 2,
        [StringValue("Quiz")]
        Quiz = 3,
        [StringValue("PreRecordedSession")]
        PreRecordedSession = 4
    }

    public enum AsyncLessonStatus
    {
        [StringValue("Not Viewabel Locked")]
        NotViewablLocked = 0,
        [StringValue("Viewable Not Locked")]
        ViewableNotLocked = 1,
        [StringValue("Viewable Locked")]
        ViewableLocked = 2
    }

    public enum StudentMappingStatus
    {
        [StringValue("Not Started")]
        NotStarted = 0,
        [StringValue("In-Process")]
        InProcess = 1,
        [StringValue("Completed")]
        Completed = 2

    }
}
