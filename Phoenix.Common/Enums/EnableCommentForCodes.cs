﻿using Phoenix.Common.Helpers;

namespace Phoenix.Common.Enums
{
    public enum EnableCommentForCodes
    {
        [StringValue("None")]
        None,
        [StringValue("Student")]
        Student,
        [StringValue("Parent")]
        Parent,
        [StringValue("All")]
        All
    }
}
