﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum ReminderEvents
    {
        EverydayTillCompletion=1,
        EverydayBeforeDuedate=2,
        BeforeDuedate=3,
        AfterDuedate=4,
        BeforeStartdate=5
    }
}
