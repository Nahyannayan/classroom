﻿using Phoenix.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public class SchoolReport
    {
    }
    public enum ReportModule
    {
        [StringValue("Attendance")]
        Attendance,
        [StringValue("Grade book")]
        Gradebook,
        [StringValue("Behavior")]
        Behavior
    }
    public enum ScheduleType
    {
        [StringValue("Weekly")]
        Weekly,
        [StringValue("Monthly")]
        Monthly,
        [StringValue("Yearly")]
        Yearly
    }
}
