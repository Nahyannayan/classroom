﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum TemplateColumnTypes
    {
        Certificate = 1,
        LessonPlan = 2
    }
}
