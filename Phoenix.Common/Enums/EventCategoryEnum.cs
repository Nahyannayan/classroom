﻿using Phoenix.Common.Helpers;

namespace Phoenix.Common.Enums
{
    public enum EventCategoryEnum
    {
        [StringValue("Teams Meeting")]
        TeamsMeeting = 1,
        [StringValue("Zoom Meeting")]
        ZoomMeeting = 2,
        [StringValue("Sports Day")]
        SportsDay = 3,
        [StringValue("Staff Meeting")]
        StaffMeeting = 4,
        [StringValue("WebRTC")]
        WebRTC = 5
    }
}
