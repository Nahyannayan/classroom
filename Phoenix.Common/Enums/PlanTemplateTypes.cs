﻿using Phoenix.Common.Helpers;

namespace Phoenix.Common.Enums
{
    public enum PlanTemplateTypes
    {
        [StringValue("Scheme of Work")]
        SOW,
        [StringValue("Lesson Plan")]
        Plan,
        [StringValue("Certificate")]
        Certificate
    }

    public enum PlanPeriod
    {
        [StringValue("Daily")]
        Daily,
        [StringValue("Weekly")]
        Weekly,
        [StringValue("Monthly")]
        Monthly,
        [StringValue("Yearly")]
        Yearly
    }

    public enum TemplateStatus
    {
        [StringValue("New")]
        New,
        [StringValue("Approved")]
        Approved,
        [StringValue("Pending For Approval")]
        Pending,
        [StringValue("Rejected")]
        Rejected
    }
}
