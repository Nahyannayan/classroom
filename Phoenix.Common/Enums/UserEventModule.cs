﻿using Phoenix.Common.Helpers;

namespace Phoenix.Common.Enums
{
    public enum UserEventModule
    {
        [StringValue("None")]
        None,
        [StringValue("Incident")]
        Incident,
        [StringValue("Task")]
        Task,
        [StringValue("ScoreCard")]
        ScoreCard,
        [StringValue("CreateAudit")]
        CreateAudit,
        [StringValue("AuditSeverity")]
        AuditSeverity,
        [StringValue("AuditAmmend")]
        AuditAmmend,
        [StringValue("AuditSection")]
        AuditSection,
        [StringValue("AuditQuestionAnswer")]        
        AuditQuestionAnswer,
        [StringValue("System")]
        System,
        [StringValue("UserRole")]
        UserRole,
        [StringValue("UserRoleMapping")]
        UserRoleMapping,
        [StringValue("NotificationProfile")]
        NotificationProfile,
        [StringValue("ClosureConfiguration")]
        ClosureConfiguration,
        [StringValue("BusinessUnit")]
        BusinessUnit,
        [StringValue("IncidentCategory")]
        IncidentCategory,
        [StringValue("IncidentSubCategory")]
        IncidentSubCategory,
        [StringValue("IncidentSection")]
        IncidentSection,
        [StringValue("UserList")]
        UserList,

    }
}
