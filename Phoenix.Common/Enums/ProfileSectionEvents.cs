﻿using Phoenix.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum ProfileSectionEvents
    {
        [StringValue("None")]
        None = 0,
        [StringValue("Enabled by student")]
        EnabledByStudent = 1,
        [StringValue("Disabled by Student")]
        DisabledByStudent = 2,
        [StringValue("Enabled by teacher")]
        EnabledByTeacher = 3,
        [StringValue("Disabled by teacher")]
        DisabledByTeacher = 4
    }
}
