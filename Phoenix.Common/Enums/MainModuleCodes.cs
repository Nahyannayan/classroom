﻿using Phoenix.Common.Helpers;

namespace Phoenix.Common.Enums
{
    public enum MainModuleCodes
    {
        [StringValue("Admin")]
        AdminPanel,
        [StringValue("HSE")]
        HSE,
        [StringValue("ReportList")]
        ReportList,
        [StringValue("VLE")]
        Classroom,
        [StringValue("SIMS")]
        SIMS,
        [StringValue("HSEAdmin")]
        HSEAdmin

    }
}
