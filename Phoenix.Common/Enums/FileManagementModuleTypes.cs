﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum FileManagementModuleTypes
    {
        MyFiles = 1,
        Groups = 2,
        SchoolSpaces = 3,
        Lockers = 4,
        Assignment = 5,
        Chatter = 6,
        Certificates = 7,
        Academics = 8,
        Feedback = 9,
        TaskFeedback = 10,
        ChatterComment = 12
    }
}
