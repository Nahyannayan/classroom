﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum ResourceFileTypes
    {
        GoogleDrive = 1,
        OneDrive = 2,
        SharePoint = 3,
        OneNote = 4,
        NormalFile=0
    }

    public enum FileUploadModuleType
    {
        Assignment = 1,
        Task = 2,
        MyFile = 3,
        Blogs = 4,
        Observation = 5
    }
}
