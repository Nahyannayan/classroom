﻿using Phoenix.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum GradeBookAssessmentType
    {
        [StringValue("Assignment")]
        Assignment = 1,
        [StringValue("Quiz")]
        Quiz = 2,        
        [StringValue("Internal Assessment")]
        InternalAssessment = 3,
        [StringValue("Standardized Assessment")]
        StandardizedAssessment = 4,
        [StringValue("External Examination")]
        ExternalExamination = 5,
        [StringValue("Progress Tracker")]
        ProgressTracker = 6,
        [StringValue("Student demographic")]
        StudentDemographic = 7

    }
    public enum GradeBookProgressTracker
    {
        [StringValue("Progress Tracking Attainment")]
        Attainment = 1,
        [StringValue("Progress Tracking Progress")]
        Progress = 2
    }
    public enum GradebookCalculationType
    {
        [StringValue("Average")]
        Average = 1,
        [StringValue("Mode")]
        Mod = 2
    }
    public enum RuleSetupCalculationType
    {
        [StringValue("Average")]
        Average = 1,
        [StringValue("Sum")]
        Sum = 2,
        [StringValue("Best Of")]
        BestOf = 3,
        [StringValue("Weighted average")]
        WeightedAverage = 4,
    }
    public enum InternalAssessmentRowType
    {
        [StringValue("Mark")]
        Mark = 1,
        [StringValue("Grade")]
        Grade = 2,
        [StringValue("Comment")]
        Comment = 3,
    }
}
