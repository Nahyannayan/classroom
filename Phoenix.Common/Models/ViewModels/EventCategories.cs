﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Models.ViewModels
{
    public class EventCategories
    {
        public int categoryID { get; set; }
        public string categoryDescription { get; set; }
        public string categoryThumbNailUrl { get; set; }
        public string categoryColorCode { get; set; }

    }
    public class EventCategoriesRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<EventCategories> data { get; set; }
    }
}
