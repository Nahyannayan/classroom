﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Phoenix.Common.ViewModels
{
    public class TreeItemView
    {
        public int? id { get; set; }
        public int? parentId { get; set; }
        public string name { get; set; }
        public bool checkbox { get; set; }
        public string url { get; set; }
        public string ModuleCode { get; set; }
    }
    public class ComboTreeView
    {
        public ComboTreeView()
        {
            children = new List<ComboTreeView>();
        }
        public dynamic id { get; set; }
        public string text { get; set; }
        public bool IsParent { get; set; }
        public dynamic ParentId { get; set; }
        public string iconCls { get; set; }
        public string state { get; set; }
        public bool _checked { get; set; }
        public List<ComboTreeView> children { get; set; }
    }
    public class TopicTreeItemView
    {
        public TopicTreeItemView()
        {
            children = new List<TopicTreeItemView>();
        }
        public dynamic id { get; set; }
        public string text { get; set; }
        public bool IsParent { get; set; }
        public string iconCls { get; set; }
        public string state { get; set; }
        public List<TopicTreeItemView> children { get; set; }
    }


    public class TopicTreeItem
    {
        public string id { get; set; }
        public string parentId { get; set; }
        public string name { get; set; }
        public bool checkbox { get; set; }
        public string url { get; set; }
        public string ModuleCode { get; set; }
        public bool isLesson { get; set; }
        public int SubjectId { get; set; }
    }

    public class ParentTree
    {
        public int id { get; set; }
        public string text { get; set; }
        public bool @checked { get; set; }
        public TreeAttributes attributes { get; set; }
        public List<ParentTree> children { get; set; }
    }

    public class TreeAttributes
    {
        public int HeaderRowNum { get; set; }
        public short LevelNo { get; set; }
    }

    public class FileTreeItem
    {
        public string id { get; set; }
        public long parentId { get; set; }
        public string name { get; set; }
        public bool checkbox { get; set; }
        public string url { get; set; }
        public string ModuleCode { get; set; }
        public bool isFolder { get; set; }
    }
    public class TopicTreeView
    {
        public dynamic scmid { get; set; }
        public dynamic id { get; set; }
        [AllowHtml]
        public string text { get; set; }
        [AllowHtml]
        public string subtopic { get; set; }
        [AllowHtml]
        public string maintopic { get; set; }
        public string standardcode { get; set; }

    }

}
