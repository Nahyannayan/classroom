﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Models.ViewModels
{
   public class SystemImageViewModel
    {
        public long ImageId { get; set; }
        public string ImageTitle { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
    }
}
