﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Models.ViewModels
{
    public class UserActivityPointsView
    {
        public int Points { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string SummaryType { get; set; }
    }
}
