﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.Common.ViewModels
{
    public class HtmlHelperModel
    {
        public TagBuilder Tag { get; set; }
    }
    public class FileUploadHelperMode : HtmlHelperModel
    {
        public IEnumerable<Attachment> Attachments { get; set; }
    }
}