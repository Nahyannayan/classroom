﻿using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ParentView : User
    {
        public ParentView()
        {
            ChildIds = new List<int>();
            ChildNames = new List<string>();
            Childs = new List<Student>();
            StudentDetails = new List<StudentDetail>();
            studentDetail = new StudentDetail();
        }
        public User User { get; set; }
        public List<StudentDetail> StudentDetails { get; set; }
        public List<SchoolBadge> Badges { get; set; }
        public List<Student> Childs { get; set; }
        public List<int> ChildIds { get; set; }
        public List<string> ChildNames { get; set; }
        public List<AssignmentStudentDetails> Assignments { get; set; }
        public List<SchoolGroup> SchoolGroups { get; set; }
        public int PendingAssignments { get; set; }
        public int CompletedAssignments { get; set; }
        public StudentDetail studentDetail { get; set; }
        public string encriptedStudentId { get; set; }
       
        public List<StudentNotification> Notifications { get; set; }
        public List<SchoolBanner> SchoolBanners { get; set; }
        public UserProfileView UserFeelingStatus { get; set; }

        public int TotalActivePoints { get; set; }
    }
}
