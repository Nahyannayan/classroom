﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Models
{
    public class ContentView
    {
        public long ContentId { get; set; }
        public string ContentName { get; set; }
        public string Description { get; set; }
        public int ContentType { get; set; }
        public string RedirectUrl { get; set; }
        public string ContentImage { get; set; }
        public int SubjectId { get; set; }
        public int SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string Subject { get; set; }
        public bool IsApproved { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public bool IsAssign { get; set; }
        public int TotalCount { get; set; }
        public string FileExtenstion { get; set; }
        public bool IsSharepointFile { get; set; }
        public string PhysicalPath { get; set; }
    }
}
