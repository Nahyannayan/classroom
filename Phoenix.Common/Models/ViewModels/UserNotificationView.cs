﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Models
{
    public class UserNotificationView
    {
        public DateTime Date { get; set; }
        public string Notification { get; set; }
        public long SourceID { get; set; }
        public string ModuleCode { get; set; }
        public string NotificationModuleName { get; set; }
        public string NotificationType { get; set; }
        public string ModuleIconCssClass { get; set; }
        public string NotificationMessage { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsRead { get; set; }
    }

    public class GetNotificationParams
    {
        public string Key { get; set; }
        public string Identifier { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
    }

    public class StudentNotificationRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<StudentNotification> data { get; set; }
    }

    public class StudentNotification
    {
        public string NotificationID { get; set; }
        public DateTime DateTime { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public bool IsFavorite { get; set; }
        public string Category { get; set; }
        public string referenceID { get; set; }
        public string StudentId { get; set; }

    }

    public class PostNotificationParams
    {
        public string Key { get; set; }
        public string Identifier { get; set; }
        public string Source { get; set; }
        public List<NotificationDetails> NotificationDetails { get; set; }
    }

    public class NotificationDetails
    {
        public long NotificationID { get; set; }
        public int ReadFlag { get; set; }
        public int FavoriteFlag { get; set; }
        public int DeleteFlag { get; set; }
    }
}
