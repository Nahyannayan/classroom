﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix.Common.Models
{
    public class SubjectListItem
    {
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class SchoolGroupListModel
    {
        public long SelectedDropdownItem { get; set; }
        public IEnumerable<SelectListItem> DropdownList { get; set; }
    }
    public class AcademicYearList
    {
        public int AcademicYearId { get; set; }
        public string AcademicYear { get; set; }
    }
    public class AcademicYearListModel
    {
        public long SelectedDropdownItem { get; set; }
        public IEnumerable<AcademicYearList> DropdownList { get; set; }
    }
    public class AssignmentCategory
    {
        public int AssigntmentCatgoryId { get; set; }
        [Required(ErrorMessage = "Mandatory Field")]
        public string CategoryTitle { get; set; }
        [Required(ErrorMessage = "Mandatory Field")]
        public string CategoryDescription { get; set; }
        public long SchoolId { get; set; }
        public bool IsActive { get; set; }
    }
}
