﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Models
{
    public class SchoolLevels
    {
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
    }
}
