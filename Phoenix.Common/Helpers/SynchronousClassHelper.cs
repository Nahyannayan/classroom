﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.Models;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Helpers
{
    public static class SynchronousClassHelper
    {
        public static HttpClient httpClient;
        public static readonly JsonSerializerSettings jsonSettings =
       new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };

        public static string zoomMeetingEndPoint;

        static SynchronousClassHelper()
        {
            zoomMeetingEndPoint = "https://api.zoom.us/v2/users";
            httpClient = new HttpClient();
        }
        public static string CreateAndRetreiveZoomMeetingPayload(ZoomMeetingView model)
        {

            var settingObj = new object();
            var recurrenceObj = new object();
            var payloadObj = new object();
            if (model.IsRecurringMeeting)
            {
                recurrenceObj = new
                {
                    type = 2,
                    weekly_days = model.RecurringDays,
                    end_date_time = model.MeetingEndDateTime
                };
            }
            if (model.IsSSOEnabled)
            {
                settingObj = new
                {
                    contact_name = model.ContactName
                };
            }
            else
            {
                settingObj = new
                {
                    contact_name = model.ContactName,
                    registration_type = 2,
                    approval_type = 0,
                    registrants_email_notification = false
                    //authentication_domains = "gemseducation.com,gemsedu.com,gemselearning.com"
                };
            }
            if (model.IsRecurringMeeting)
            {
                payloadObj = new
                {
                    topic = model.MeetingName,
                    type = model.IsRecurringMeeting ? 8 : 2,
                    duration = model.MeetingDuration,
                    password = model.MeetingPassword,
                    timezone = "UTC",
                    settings = settingObj,
                    recurrence = recurrenceObj,
                    start_time = model.FormattedMeetingDateTime,
                };
            }
            else
            {
                payloadObj = new
                {
                    topic = model.MeetingName,
                    type = model.IsRecurringMeeting ? 8 : 2,
                    duration = model.MeetingDuration,
                    password = model.MeetingPassword,
                    timezone = "UTC",
                    settings = settingObj,
                    start_time = model.FormattedMeetingDateTime,
                };
            }
            
            return JsonConvert.SerializeObject(payloadObj);
        }

        private static string CreateZoomMeetingParticipantsPayload(GroupMemberMapping userDetails)
        {
            var payloads = new
            {
                email = userDetails.Email,
                first_name = userDetails.FirstName,
                last_name = userDetails.LastName
            };
            return JsonConvert.SerializeObject(payloads);
        }

        public static async Task<ZoomMeetingResponse> CallZoomApiMethod(HttpMethod method, string uri, object body = null, string accessToken = "")
        {
            string bodyString;

            if (body is string)
                bodyString = body as string;
            else
                bodyString = JsonConvert.SerializeObject(body, jsonSettings);
            var apiUrl = uri.ToLower().Contains("http") ? uri : zoomMeetingEndPoint + uri;
            var request = new HttpRequestMessage(method, apiUrl);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            if (!string.IsNullOrEmpty(accessToken))
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            if (method != HttpMethod.Get && method != HttpMethod.Delete)
                request.Content = new StringContent(bodyString, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await httpClient.SendAsync(request);
            string responseBody = await response.Content.ReadAsStringAsync();

            //if (!response.IsSuccessStatusCode)
            //{
            //    return new ZoomMeetingResponse();
            //}
            return JsonConvert.DeserializeObject<ZoomMeetingResponse>(responseBody);
        }

        public static async Task<ZoomMeetingResponse> CreateZoomMeeting(ZoomMeetingView model)
        {
            return await CallZoomApiMethod(HttpMethod.Post, $"/{model.ZoomEmail}/meetings", CreateAndRetreiveZoomMeetingPayload(model), model.AccessToken);
        }

        public static async Task<ZoomMeetingResponse> GenerateZoomUserAccessToken(string email, string accessToken)
        {
            return await CallZoomApiMethod(HttpMethod.Get, $"{zoomMeetingEndPoint}/{email}/token?type=zak&access_token={accessToken}", null);
        }

        public static async Task<ZoomMeetingResponse> UpdateZoomMeeting(ZoomMeetingView model)
        {
            return await CallZoomApiMethod(new HttpMethod("PATCH"), $"{zoomMeetingEndPoint.Replace("/users", "")}/meetings/{model.ZoomMeetingId}", CreateAndRetreiveZoomMeetingPayload(model), model.AccessToken);
        }

        public static async Task<ZoomMeetingResponse> AddZoomMeetingRegistrants(GroupMemberMapping studentDetails, string meetingId, string accessToken)
        {
            return await CallZoomApiMethod(HttpMethod.Post, $"{zoomMeetingEndPoint.Replace("/users", string.Empty)}/meetings/{meetingId}/registrants", CreateZoomMeetingParticipantsPayload(studentDetails), accessToken);
        }


        //public static async Task<ZoomMeetingResponse> ValidateZoomUser(string userEmail)
        //{
        //    var user = await CallZoomApiMethod(HttpMethod.Get, $"{zoomMeetingEndPoint}/{userEmail}");
        //    if (!string.IsNullOrEmpty(user.email) && user.email.Equals(SessionHelper.CurrentSession.Email, StringComparison.CurrentCultureIgnoreCase))
        //        return user;
        //    else
        //        return await AddZoomUser(userEmail);
        //}

        public static async Task<ZoomMeetingResponse> AddZoomUser(ZoomMeetingJoinView model, string zoomAccessToken)
        {
            var payloadObj = new
            {
                action = "ssoCreate",
                user_info = new
                {
                    email = model.Email,
                    type = model.UserLicenceType,
                    first_name = model.FirstName,
                    last_name = model.LastName
                }
            };
            return await CallZoomApiMethod(HttpMethod.Post, $"https://api.zoom.us/v2/users", JsonConvert.SerializeObject(payloadObj), zoomAccessToken);
        }

        public static async Task<ZoomMeetingResponse> DeleteMeeting(string onlineMeetingId, string accessToken)
        {
            return await CallZoomApiMethod(HttpMethod.Delete, $"{zoomMeetingEndPoint.Replace("users", "")}/meetings/{onlineMeetingId}", string.Empty, accessToken);
        }
    }
}
