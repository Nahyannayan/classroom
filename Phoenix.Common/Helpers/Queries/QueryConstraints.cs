﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Collections.ObjectModel;
using jQuery.DataTables.Mvc;

namespace Phoenix.Common.Queries
{
    /// <summary>
    /// Typed constraints
    /// </summary>
    /// <typeparam name="T">Model to query</typeparam>
    public class QueryConstraints<T> : IQueryConstraints<T> where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryConstraints{T}"/> class.
        /// </summary>
        /// <remarks>Will per default return the first 50 items</remarks>
        public QueryConstraints()
        {
            ModelType = typeof(T);
            PageSize = 50;
            StartIndex = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryConstraints{T}"/> class.
        /// </summary>
        /// <remarks>Will per default return the first 50 items</remarks>
        public QueryConstraints(JQueryDataTablesModel jQueryDataTablesModel)
        {
            ModelType = typeof(T);
            Page(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength);
            Column(jQueryDataTablesModel.mDataProp_, jQueryDataTablesModel.sSearch_);
            SortBy(jQueryDataTablesModel.GetSortedColumns());
            SearchString = jQueryDataTablesModel.sSearch;
        }

        /// <summary>
        /// Gets model which will be queried.
        /// </summary>
        protected Type ModelType { get; set; }

        #region IQueryConstraints<T> Members

        /// <summary>
        /// Gets the search string
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// Gets number of items per page (when paging is used)
        /// </summary>
        public int PageSize { get; private set; }

        /// <summary>
        /// Gets the start item index
        /// </summary>
        public int StartIndex { get; private set; }
        
        /// <summary>
        /// Gets data columns
        /// </summary>
        public IList<DTColumn> Columns { get; set; }

        /// <summary>
        /// Gets the kind of sort order
        /// </summary>
        public SortOrder SortOrder { get; private set; }

        /// <summary>
        /// Gets property name for the property to sort by.
        /// </summary>
        public string SortPropertyName { get; private set; }

        /// <summary>
        /// Gets sorted columns
        /// </summary>
        public ReadOnlyCollection<SortedColumn> SortedColumns { get; private set; }

        /// <summary>
        /// Gets sorted columns as comma seperated values
        /// </summary>
        public string SortedColumnsString { get; set; }

        public IQueryConstraints<T> Column(ReadOnlyCollection<string> dataColumns, ReadOnlyCollection<string> searchValues)
        {
            Columns = new List<DTColumn>();
            if (dataColumns != null)
            {
                for (int col = 0; col < dataColumns.Count; col++)
                {
                    Columns.Add(new DTColumn(dataColumns[col], searchValues[col]));
                }
            }
            
            return this;
        }

        /// <summary>
        /// Use paging
        /// </summary>
        /// <param name="displayStart">zero record index (one based index)</param>
        /// <param name="pageSize">Number of items per page.</param>
        /// <returns>Current instance</returns>
        public IQueryConstraints<T> Page(int? displayStart, int pageSize)
        {
            if (!displayStart.HasValue || displayStart < 0)
                throw new ArgumentOutOfRangeException("displayStart", "Display start number must be greater or equal to 0");
            if (pageSize < 1)
                pageSize = 10;
            if (pageSize > 500)
                pageSize = 500;

            StartIndex = displayStart.Value;
            PageSize = pageSize;
            
            return this;
        }

        /// <summary>
        /// Sort ascending by a property
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>Current instance</returns>
        public IQueryConstraints<T> SortBy(string propertyName)
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");
            ValidatePropertyName(propertyName);

            SortOrder = SortOrder.Ascending;
            SortPropertyName = propertyName;
            return this;
        }

        /// <summary>
        /// Sort descending by a property.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>Current instance</returns>
        public IQueryConstraints<T> SortByDescending(string propertyName)
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");
            ValidatePropertyName(propertyName);

            SortOrder = SortOrder.Descending;
            SortPropertyName = propertyName;
            return this;
        }

        /// <summary>
        /// Property to sort by (ascending)
        /// </summary>
        /// <param name="property">The property.</param>
        public IQueryConstraints<T> SortBy(Expression<Func<T, object>> property)
        {
            if (property == null) throw new ArgumentNullException("property");
            var expression = property.GetMemberInfo();
            var name = expression.Member.Name;
            SortBy(name);
            return this;
        }

        /// <summary>
        /// Property to sort by (descending)
        /// </summary>
        /// <param name="property">The property</param>
        public IQueryConstraints<T> SortByDescending(Expression<Func<T, object>> property)
        {
            if (property == null) throw new ArgumentNullException("property");
            var expression = property.GetMemberInfo();
            var name = expression.Member.Name;
            SortByDescending(name);
            return this;
        }

        /// <summary>
        /// Sort by columns.
        /// </summary>
        /// <param name="sortedColumns">Sort by columns.</param>
        /// <returns>Current instance</returns>
        public IQueryConstraints<T> SortBy(ReadOnlyCollection<SortedColumn> sortedColumns)
        {
            if (sortedColumns == null) throw new ArgumentNullException("sortedColumns");
            string sortedColumnsString = string.Empty;
            SortedColumns = sortedColumns;
            foreach(var item in sortedColumns)
            {
                sortedColumnsString += item.PropertyName + " " + (item.Direction == SortingDirection.Descending ? "DESC" : "ASC") + ",";
            }
            SortedColumnsString = sortedColumnsString.TrimEnd(',');

            return this;
        }

        #endregion

        /// <summary>
        /// Make sure that the property exists in the model.
        /// </summary>
        /// <param name="name">The name.</param>
        protected virtual void ValidatePropertyName(string name)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (ModelType.GetProperty(name) == null)
            {
                throw new ArgumentException(string.Format("'{0}' is not a public property of '{1}'.", name,
                                                          ModelType.FullName));
            }
        }
    }
}