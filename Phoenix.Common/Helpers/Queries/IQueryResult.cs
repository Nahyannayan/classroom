﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Phoenix.Common.Queries
{
    /// <summary>
    /// Defines minimum parameters for query search results.
    /// </summary>
    /// <typeparam name="T">Type of return model</typeparam>
    public interface IQueryResult<out T> where T : class
    {
        /// <summary>
        /// Gets all matching items
        /// </summary>
        IEnumerable<T> Items { get; }

        /// <summary>
        /// Gets total number of items in a page
        /// </summary>
        int PageSize { get; }

        /// <summary>
        /// Gets total number of items (useful when paging is used)
        /// </summary>
        int TotalCount { get; }

        /// <summary>
        /// Gets total number of items filtered
        /// </summary>
        int FilteredCount { get; }
    }
}
