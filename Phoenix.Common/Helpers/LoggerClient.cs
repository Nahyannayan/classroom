﻿using Phoenix.Common.Logger;

namespace Phoenix.Common.Helpers
{
    public static class LoggerClient
    {
        public static ILoggerClient Instance
        {
            get
            {
                return LoggerFactory.GetLoggerClient(PhoenixConfiguration.Instance.DefaultLogger);
            }
        }
    }
}
