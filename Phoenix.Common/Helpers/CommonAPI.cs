﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Helpers
{
    public static class CommonAPI
    {
        public static class Common
        {
            public static string GetEmailSettings(string baseUri) => $"{baseUri}/getEmailSettings";
            public static string GetSchoolCurrentLanguage(string baseUri, int schoolId) => $"{baseUri}/getSchoolCurrentLanguage?schoolId={schoolId}";
            public static string SetSchoolCurrentLanguage(string baseUri, int languageId, int schoolId) => $"{baseUri}/setSchoolCurrentLanguage?languageId={languageId}&schoolId={schoolId}";
            public static string GetUserCurrentLanguage(string baseUri, int languageId) => $"{baseUri}/getUserCurrentLanguage?languageId={languageId}";
            public static string GetSystemLanguages(string baseUri) => $"{baseUri}/getSystemLanguages";

        }
    }
}
