﻿
using Microsoft.SharePoint.Client;
using Phoenix.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace Phoenix.VLE.Web.Helpers
{
    public class SharePointHelper
    {
        public string SHAREPOINT_URL { get; set; }
        public string SHAREPOINT_USER { get; set; }
        public string SHAREPOINT_PASSWORD { get; set; }

        public ClientContext LoginToSharepoint()
        {
            SHAREPOINT_URL = ConfigurationManager.AppSettings["SharepointURL"].ToString();
            SHAREPOINT_USER = ConfigurationManager.AppSettings["SharepointUser"].ToString();
            SHAREPOINT_PASSWORD = ConfigurationManager.AppSettings["SharepointPassword"].ToString();
            ClientContext cxt = new ClientContext(SHAREPOINT_URL);
            System.Security.SecureString s = new System.Security.SecureString();
            foreach (char c in SHAREPOINT_PASSWORD.ToCharArray())
                s.AppendChar(c);
            cxt.Credentials = new Microsoft.SharePoint.Client.SharePointOnlineCredentials(SHAREPOINT_USER, s);
            Microsoft.SharePoint.Client.Web web = cxt.Web;
            cxt.Load(web);
            cxt.ExecuteQuery();
            string webtitle = web.Title;
            return cxt;
        }
        public static bool CreateFolder(ref ClientContext cxt, ref List list, string FolderNAme)
        {
            bool CreateFolder = false;
            list.EnableFolderCreation = true;
            list.Update();
            cxt.ExecuteQuery();
            ListItemCreationInformation info = new ListItemCreationInformation();
            info.UnderlyingObjectType = FileSystemObjectType.Folder;
            info.LeafName = FolderNAme.Trim(); // Trim for spaces.Just extra check
            ListItem newItem = list.AddItem(info);
            newItem["Title"] = FolderNAme;
            newItem.Update();
            cxt.ExecuteQuery();
            CreateFolder = true;
            return CreateFolder;
        }

        public static bool CreateLibrary(ref ClientContext cxt, string LibraryName)
        {
            bool CreateLibrary = false;
            ListCreationInformation lci = new ListCreationInformation();
            lci.Description = LibraryName;
            lci.Title = LibraryName;
            lci.TemplateType = System.Convert.ToInt32(ListTemplateType.DocumentLibrary);
            List newLib = cxt.Web.Lists.Add(lci);
            cxt.Load(newLib);
            cxt.ExecuteQuery();
            CreateLibrary = true;
            return CreateLibrary;
        }

        public static bool IsLibraryExists(ref ClientContext clientContext, string listTitle)
        {
            bool IsLibraryExists = false;
            var web = clientContext.Web;
            ListCollection lists = web.Lists;
            clientContext.Load(lists);
            clientContext.ExecuteQuery();
            foreach (List L in lists)
            {
                if (L.Title.Equals(listTitle, StringComparison.CurrentCultureIgnoreCase))
                {
                    IsLibraryExists = true;
                    break;
                }
            }
            return IsLibraryExists;
        }

        public static bool CheckFileExists(ref ClientContext ctx, string FilePath)
        {
            bool CheckFileExists = false;
            try
            {
                var web = ctx.Web;
                ctx.Load(web);
                string url = new Uri(FilePath).AbsolutePath;
                Microsoft.SharePoint.Client.File f = web.GetFileByServerRelativeUrl(url);
                ctx.Load(f);
                ctx.ExecuteQuery();
                if (f.Exists)
                    CheckFileExists = true;
            }
            catch (Exception ex)
            {

            }
            return CheckFileExists;
        }

        public static string UploadFilesAsPerModule(ref ClientContext clientContext, string LibraryName, FileModulesConstants ModuleName, HttpPostedFileBase file)
        {
            bool UploadFile = false;
            bool LibraryExists = false;
            bool IsFolderExists = false;
            string ShareableLink = "";
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);

            if (!IsLibraryExists(ref clientContext, LibraryName))
                LibraryExists = CreateLibrary(ref clientContext, LibraryName); // Creates library if not exists
            else
                LibraryExists = true;
            if (LibraryExists)
            {
                List List = clientContext.Web.Lists.GetByTitle(LibraryName);
                clientContext.Load(List);
                clientContext.Load(List.RootFolder);
                clientContext.Load(List.RootFolder.Folders);
                clientContext.ExecuteQuery();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == ModuleNametoUpload)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref clientContext, ref List, ModuleNametoUpload))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    using (var fs = file.InputStream)
                    {
                        string DestinationFileName = "";
                        string ServerDomain = "";
                        ServerDomain = new Uri(clientContext.Url).GetLeftPart(UriPartial.Authority);
                        DestinationFileName = file.FileName;
                        var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + ModuleName, DestinationFileName);
                        while (CheckFileExists(ref clientContext, fileUrl))
                        {
                            var random = new Random(DateTime.Now.Millisecond);
                            int randomNumber = random.Next(1, 500000);
                            DestinationFileName = randomNumber + "_" + DestinationFileName;
                            fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + ModuleName, DestinationFileName);
                        }
                        if ((clientContext.HasPendingRequest))
                            clientContext.ExecuteQuery();
                        Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl + "/" + ModuleName + "/" + DestinationFileName, fs, false);
                        ShareableLink = clientContext.Web.CreateAnonymousLinkForDocument(fileUrl, ExternalSharingDocumentOption.View);
                    }

                }
            }
            return ShareableLink;

        }

        public string APIUploadFilesAsPerModule(string LibraryName, FileModulesConstants ModuleName, HttpPostedFileBase file)
        {
            bool UploadFile = false;
            bool LibraryExists = false;
            bool IsFolderExists = false;
            string ShareableLink = "";
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);

            SHAREPOINT_URL = "https://gemsedu.sharepoint.com/ClassroomSync";
            SHAREPOINT_USER = "ClassroomSync@gemseducation.com";
            SHAREPOINT_PASSWORD = "Classr00m$ync!@";
            ClientContext clientContext = new ClientContext(SHAREPOINT_URL);
            System.Security.SecureString s = new System.Security.SecureString();
            foreach (char c in SHAREPOINT_PASSWORD.ToCharArray())
                s.AppendChar(c);
            clientContext.Credentials = new Microsoft.SharePoint.Client.SharePointOnlineCredentials(SHAREPOINT_USER, s);
            Microsoft.SharePoint.Client.Web web = clientContext.Web;
            clientContext.Load(web);
            clientContext.ExecuteQuery();
            string webtitle = web.Title;

            if (!IsLibraryExists(ref clientContext, LibraryName))
                LibraryExists = CreateLibrary(ref clientContext, LibraryName); // Creates library if not exists
            else
                LibraryExists = true;
            if (LibraryExists)
            {
                List List = clientContext.Web.Lists.GetByTitle(LibraryName);
                clientContext.Load(List);
                clientContext.Load(List.RootFolder);
                clientContext.Load(List.RootFolder.Folders);
                clientContext.ExecuteQuery();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == ModuleNametoUpload)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref clientContext, ref List, ModuleNametoUpload))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {

                    using (var fs = file.InputStream)
                    {
                        string DestinationFileName = "";
                        string ServerDomain = "";
                        ServerDomain = new Uri(clientContext.Url).GetLeftPart(UriPartial.Authority);
                        DestinationFileName = file.FileName;
                        var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + ModuleName, DestinationFileName);
                        while (CheckFileExists(ref clientContext, fileUrl))
                        {
                            var random = new Random(DateTime.Now.Millisecond);
                            int randomNumber = random.Next(1, 500000);
                            DestinationFileName = randomNumber + "_" + DestinationFileName;
                            fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + ModuleName, DestinationFileName);
                        }
                        if ((clientContext.HasPendingRequest))
                            clientContext.ExecuteQuery();
                        Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl + "/" + ModuleName + "/" + DestinationFileName, fs, false);
                        ShareableLink = clientContext.Web.CreateAnonymousLinkForDocument(fileUrl, ExternalSharingDocumentOption.View);
                    }

                    //string DestinationFileName = "";
                    //string ServerDomain = "";
                    //ServerDomain = new Uri(clientContext.Url).GetLeftPart(UriPartial.Authority);
                    //DestinationFileName = file.Name;
                    //var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + ModuleName, DestinationFileName);
                    //while (CheckFileExists(ref clientContext, fileUrl))
                    //{
                    //    var random = new Random(DateTime.Now.Millisecond);
                    //    int randomNumber = random.Next(1, 500000);
                    //    DestinationFileName = randomNumber + "_" + DestinationFileName;
                    //    fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + ModuleName, DestinationFileName);
                    //}
                    //if ((clientContext.HasPendingRequest))
                    //    clientContext.ExecuteQuery();
                    //Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl + "/" + ModuleName + "/" + DestinationFileName, file, false);
                    //ShareableLink = clientContext.Web.CreateAnonymousLinkForDocument(fileUrl, ExternalSharingDocumentOption.View);


                }
            }
            return ShareableLink;

        }

    }
}