﻿using Phoenix.Common.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace Phoenix.Common.Helpers
{
    public static class SessionHelper
    {
        public static UserPrincipal CurrentSession
        {
            get
            {
                //HttpContext.Current.Session.Clear();
                //HttpContext.Current.Session.Abandon();
                //HttpContext.Current.User = null;
                //System.Web.Security.FormsAuthentication.SignOut();
                if (HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var userData = HttpContext.Current.User as UserPrincipal;
                    if (userData != null)
                    {
                        if (userData.IsParent())
                        {

                            if (HttpContext.Current.Session["FamilyStudentList"] == null || HttpContext.Current.Session["CurrentSelectedStudent"] == null)
                            {
                                HttpContext.Current.Response.Redirect("/Account/Signout");
                            }
                            userData.FamilyStudentList = (List<StudentDetail>)HttpContext.Current.Session["FamilyStudentList"];
                            userData.CurrentSelectedStudent = (StudentDetail)HttpContext.Current.Session["CurrentSelectedStudent"];

                        }
                        if (HttpContext.Current.Session != null && HttpContext.Current.Session["BannedWords"] != null)
                        {
                            userData.BannedWords = (List<BannedWord>)HttpContext.Current.Session["BannedWords"];
                        }
                        //Getting token for user to access web api => Deepak Singh 21 August 2019
                        if (HttpContext.Current.Request.Cookies != null && HttpContext.Current.Request.Cookies.Count > 0)
                        {
                            HttpCookie tokenCookie = HttpContext.Current.Request.Cookies.Get("st");
                            userData.Token = tokenCookie != null ? tokenCookie["tk"].ToString() : string.Empty;

                            HttpCookie phtokenCookie = HttpContext.Current.Request.Cookies.Get("phst");
                            userData.PhoenixAccessToken = phtokenCookie != null ? phtokenCookie["tk"].ToString() : string.Empty;

                            HttpCookie smsCookie = HttpContext.Current.Request.Cookies.Get("sms");
                            var isActive = smsCookie != null ? Convert.ToBoolean(smsCookie["act"]) : false;
                            userData.IsSmartSchoolActive = isActive;

                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            HttpCookie userSessionCookie = HttpContext.Current.Request.Cookies.Get("usd");
                            var userSessionEncryptedData = userSessionCookie != null ? userSessionCookie["d"].ToString() : string.Empty;
                            var userSessionData = EncryptDecryptHelper.Decrypt(userSessionEncryptedData);
                            var userSessionModel = serializer.Deserialize<UserSessionModel>(userSessionData);

                            if (userSessionModel != null)
                            {
                                userData.SchoolName = userSessionModel.SchoolName;
                                userData.SchoolEmail = userSessionModel.SchoolEmail;
                                userData.SchoolCode = userSessionModel.SchoolCode;
                                userData.BusinessUnitTypeId = userSessionModel.BusinessUnitTypeId;
                                userData.BusinessUnitType = userSessionModel.BusinessUnitType;
                                userData.UserTypeName = userSessionModel.UserTypeName;
                                userData.RoleId = userSessionModel.RoleId;
                                userData.RoleName = userSessionModel.RoleName;
                                userData.SchoolImage = userSessionModel.SchoolImage;
                                userData.ProfilePhoto = userSessionModel.ProfilePhoto;
                                userData.UserAvatar = userSessionModel.UserAvatar;
                                userData.GoogleDriveIntegrationType = userSessionModel.GoogleDriveIntegrationType;
                                userData.GoogleDriveClientKey = userSessionModel.GoogleDriveClientKey;
                                userData.GoogleDriveSecretKey = userSessionModel.GoogleDriveSecretKey;
                                userData.IsGEMSBU = userSessionModel.IsGEMSBU;
                                userData.IsSSOLogin = userSessionModel.IsSSOLogin;
                                userData.PhoneNumber = userSessionModel.PhoneNumber;
                                userData.ZoomSessionEnabled = userSessionModel.ZoomSessionEnabled;
                                userData.TeamsSessionEnabled = userSessionModel.TeamsSessionEnabled;
                                userData.IsMultilingual = userSessionModel.IsMultilingual;
                            }

                            HttpCookie msTeamsAccessTokenCookie = HttpContext.Current.Request.Cookies.Get("msaccesstoken");
                            userData.MSTeamsAccessToken = msTeamsAccessTokenCookie != null ? msTeamsAccessTokenCookie["msteamsaccesstoken"].ToString() : string.Empty;
                            HttpCookie msTeamsUserCookie = HttpContext.Current.Request.Cookies.Get("msuserid");
                            userData.MSTeamsUserId = msTeamsUserCookie != null ? msTeamsUserCookie["msteamsuserid"].ToString() : string.Empty;
                            //cloud storage cookies
                            HttpCookie oneDriveAccessToken = HttpContext.Current.Request.Cookies.Get("odat");
                            if (oneDriveAccessToken != null)
                            {
                                var tokenView = new MicrosoftResponseTokenView();
                                tokenView.AccessToken = !string.IsNullOrEmpty(oneDriveAccessToken["odatk"]) ? oneDriveAccessToken["odatk"].ToString() : null;
                                tokenView.TokenType = HttpContext.Current.Request.Cookies.Get("odty") != null ? EncryptDecryptHelper.Decrypt(HttpContext.Current.Request.Cookies.Get("odty")["odtokentype"].ToString()) : null;
                                tokenView.RefreshToken = HttpContext.Current.Request.Cookies.Get("odrt") != null ? HttpContext.Current.Request.Cookies.Get("odrt")["odrft"].ToString() : null;
                                tokenView.ExpiresIn = HttpContext.Current.Request.Cookies.Get("odei") != null ? Convert.ToInt32(HttpContext.Current.Request.Cookies.Get("odei")["odtexipry"].ToString()) : 0;
                                if (HttpContext.Current.Request.Cookies.Get("odtdt") != null)
                                    tokenView.AccessDateTime = Convert.ToDateTime(HttpContext.Current.Request.Cookies.Get("odtdt")["oddt"].ToString());
                                tokenView.TokenResourceType = HttpContext.Current.Request.Cookies.Get("odtrt") != null ? HttpContext.Current.Request.Cookies.Get("odtrt")["odtrtype"].ToString() : null;
                                userData.MicrosoftToken = tokenView;
                            }
                            HttpCookie googleDriveAccessToken = HttpContext.Current.Request.Cookies.Get("googledriveaccess");
                            userData.GoogleDriveAccessToken = googleDriveAccessToken != null ? googleDriveAccessToken["gdriveaccess"].ToString() : string.Empty;
                            HttpCookie cloudLoadFileType = HttpContext.Current.Request.Cookies.Get("cloudfileloadertype");
                            userData.ResourceFileLoadType = cloudLoadFileType != null ? Convert.ToInt16(cloudLoadFileType["cloudloadertype"]) : (short)0;
                            HttpCookie gdRedirectUri = HttpContext.Current.Request.Cookies.Get("gdiveredirecturi");
                            userData.GoogleDriveRedirectUri = gdRedirectUri != null ? Convert.ToString(gdRedirectUri["driveredirect"]) : null;
                        }
                        else
                        {
                            userData.IsSmartSchoolActive = false;
                        }
                        return (userData);
                    }
                }
                return new UserPrincipal();
            }
        }
        public static void RemoveKeepAliveCookie()
        {
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie1);

            HttpCookie tokenCookie = new HttpCookie("st", "");
            tokenCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(tokenCookie);

            HttpCookie userSesssionCookie = new HttpCookie("usd", "");
            userSesssionCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(userSesssionCookie);

            HttpCookie phCookie = new HttpCookie("phst", "");
            phCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(phCookie);

            HttpCookie msTeamsTokenCookie = new HttpCookie("msaccesstoken", "");
            msTeamsTokenCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(msTeamsTokenCookie);

            HttpCookie msTeamsUserCookie = new HttpCookie("msuserid", "");
            msTeamsUserCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(msTeamsUserCookie);

            HttpCookie notificationCookie = new HttpCookie("ShowNotification", "");
            notificationCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(notificationCookie);
            HttpCookie notificationExistCookie = new HttpCookie("NotificationExist", "");
            notificationExistCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(notificationExistCookie);

            HttpCookie TimeZone = new HttpCookie("TimeZone", "");
            TimeZone.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(TimeZone);

            SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie2);

            //remove cloud storage cookie
            HttpCookie oneDriveAccessTokens = new HttpCookie("odat", "");
            oneDriveAccessTokens.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(oneDriveAccessTokens);

            HttpCookie odty = new HttpCookie("odty", "");
            odty.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(odty);

            HttpCookie odrt = new HttpCookie("odrt", "");
            odrt.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(odrt);

            HttpCookie odei = new HttpCookie("odei", "");
            odei.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(odei);

            HttpCookie odtdt = new HttpCookie("odtdt", "");
            odtdt.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(odtdt);

            HttpCookie odtrt = new HttpCookie("odtrt", "");
            odtrt.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(odtrt);

            HttpCookie gDriveRedirectUri = new HttpCookie("gdiveredirecturi", "");
            gDriveRedirectUri.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(gDriveRedirectUri);

            HttpCookie gDriveTokenCookie = new HttpCookie("googledriveaccess", "");
            gDriveTokenCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(gDriveTokenCookie);

            HttpCookie aspNetCookies = new HttpCookie(".AspNet.Cookies", "");
            aspNetCookies.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(aspNetCookies);
        }
        public static void LogOffUser()
        {
            RemoveKeepAliveCookie();
            if (HttpContext.Current.Session != null)
            { HttpContext.Current.Session.Clear(); }
            FormsAuthentication.SignOut();
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
        }
        public static bool IsSessionActive()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public static void RedirectToLoginPage(Uri uri)
        {
            HttpContext.Current.Response.Redirect(PhoenixConfiguration.Instance.LoginPageUrl + (uri!=null ? $"?returnUrl={uri.PathAndQuery}" : ""));

        }

    }
}