﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace Phoenix.Common.Helpers
{
    public static class DownloadHelper
    {
        public static void DownloadZipFile(List<string> fileList, string saveFilePath, string staticFileName = "")
        {
            string zipFileName = string.IsNullOrEmpty(staticFileName) ? string.Format("zipfile-{0:yyyy-MM-dd_hh-mm-ss-tt}.zip", DateTime.Now) : staticFileName;
            using (ZipArchive archive = ZipFile.Open(saveFilePath, ZipArchiveMode.Create))
            {
                foreach (string file in fileList)
                {
                    archive.CreateEntryFromFile(file, Path.GetFileName(file));
                }
            }
        }
    }
}
