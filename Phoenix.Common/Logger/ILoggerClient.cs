﻿using System;

namespace Phoenix.Common.Logger
{
    public interface ILoggerClient
    {
        void LogException(Exception ex);

        void LogException(string message);

        void LogWarning(string message);
    }
}
