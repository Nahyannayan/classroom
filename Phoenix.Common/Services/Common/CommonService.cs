﻿
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.Common.Services
{
    public class CommonService : ICommonService
    {
        #region private variables
        static HttpClient _client = new HttpClient();
        readonly string _baseUrl = PhoenixConfiguration.Instance.PhoenixApiUrl;
        readonly string _path = "api/v1/Common";
        public CommonService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        #endregion
        public EmailSettingsView GetEmailSettings()
        {
            var emailSettingsView = new EmailSettingsView();

            var uri = CommonAPI.Common.GetEmailSettings(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                emailSettingsView = EntityMapper<string, EmailSettingsView>.MapFromJson(jsonDataProviders);
            }
            return emailSettingsView;
        }

        public SystemLanguage GetSchoolCurrentLanguage(int schoolId)
        {
            var systemLanguage = new SystemLanguage();
           
            var uri = CommonAPI.Common.GetSchoolCurrentLanguage(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                systemLanguage = EntityMapper<string, SystemLanguage>.MapFromJson(jsonDataProviders);
            }
            return systemLanguage;
        }

        public bool SetSchoolCurrentLanguage(int languageId, int schoolId)
        {
            var result = false;
            var uri = CommonAPI.Common.SetSchoolCurrentLanguage(_path,languageId,schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToBoolean(jsonDataProviders);
            }
            return result;
        }
        public SystemLanguage GetUserCurrentLanguage(int languageId)
        {
            var systemLanguage = new SystemLanguage();

            var uri = CommonAPI.Common.GetUserCurrentLanguage(_path, languageId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                systemLanguage = EntityMapper<string, SystemLanguage>.MapFromJson(jsonDataProviders);
            }
            return systemLanguage;
        }
        public IEnumerable<SystemLanguage> GetSystemLanguages()
        {
            IEnumerable<SystemLanguage> systemLanguage = new List<SystemLanguage>();

            var uri = CommonAPI.Common.GetSystemLanguages(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                systemLanguage = EntityMapper<string, IEnumerable<SystemLanguage>>.MapFromJson(jsonDataProviders);
            }
            return systemLanguage;
        }
    }
}