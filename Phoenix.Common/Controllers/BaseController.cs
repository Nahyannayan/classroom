﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.Common
{
    public class BaseController : Controller,IDisposable
    {
        public static PagePermission InnerPagePermission;
        private ILoggerClient _loggerClient;
        readonly string LoggingEnable = ConfigurationManager.AppSettings["DataLoggingEnable"];
        private bool disposed = false;
        public static PagePermission CurrentPagePermission
        {
            get
            {
                return (PagePermission)System.Web.HttpContext.Current.Session[CommonHelper.GetPermissionKey()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[CommonHelper.GetPermissionKey()] = value;
            }
        }

        public ActionResult LogOffUser()
        {
            return Redirect("/Account/SignOut");
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            int isDataLoogiing = Convert.ToInt32(LoggingEnable);
            if (isDataLoogiing==1)
            {
                _loggerClient = LoggerClient.Instance;
                if (filterContext.HttpContext.Request.HttpMethod != "POST")
                {
                    try
                    {
                        //var param = new NameValueCollection { Request.Form, Request.QueryString };
                        //var controller = filterContext.RouteData.Values["controller"].ToString();
                        //var action = filterContext.RouteData.Values["action"].ToString();
                        ////   var signature = filterContext.Controller.GetType().GetMethod(action).ToString();

                        ////Let's write it to the stream in this case
                        //_loggerClient.LogWarning(string.Format("Controller:{0}", controller));
                        //_loggerClient.LogWarning(string.Format("Action:{0}", action));
                        ////  _loggerClient.LogWarning(string.Format("Signature:", signature));

                        //foreach (var key in param.AllKeys)
                        //{
                        //    _loggerClient.LogWarning(string.Format("<strong>Key:</strong> {0} = {1}<br />", key, param[key]));
                        //}

                    }
                    catch (Exception ex)
                    {
                        _loggerClient.LogWarning(ex.Message);
                        _loggerClient.LogWarning(ex.StackTrace);
                    }
                }
                else {
                    //var form = filterContext.HttpContext.Request.Form;

                    //// need to convert Form object into dictionary so it can be converted to json properly
                    //var dictionary = form.AllKeys.ToDictionary(k => k, k => form[k]);

                    //// You'll need Newtonsoft.Json nuget package here.
                    //var jsonPostedData = JsonConvert.SerializeObject(dictionary);
                    //_loggerClient.LogWarning(jsonPostedData);
                }
             
                base.OnActionExecuting(filterContext);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                disposed = true;
                if (disposing)
                    Dispose();
            }
            base.Dispose(disposing);
        }
    }
}